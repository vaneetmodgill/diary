//
//  Utility.h
//  Viva
//
//  Created by administrator on 12/3/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>


@interface Utility : NSObject
{

}
+(BOOL)IsiPhone3G;
+(void)SetLatitudeLongitude:(double)latUser :(double)longUser;
+(double)GetLatitude;
+(double)GetLongitude;
+(BOOL)CheckInternetConnection:(char*)host_name;
+(NSString*)GetLatLong:(NSString*)zip;
@end
