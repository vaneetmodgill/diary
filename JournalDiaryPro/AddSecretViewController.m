///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   AddSecretViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "AddSecretViewController.h"
#import "eDiaryAppDelegate.h"
#import "Vault.h"
#import "Settings.h"
#import "LockerViewController.h"

@implementation AddSecretViewController
@synthesize isEditMode;
@synthesize vaultObj;
@synthesize lockerViewController;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        
        
        backgroundImageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        backgroundImageView.frame = CGRectMake(0, -20, 320, 568+40);
        //   self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 568+20);
    }
    else
    {
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 480+20);
    }
    
    

    
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];
	if(isEditMode){
		keyField.text = vaultObj.keyStr;
		valueField.text = vaultObj.valueStr;
	}
	[keyField becomeFirstResponder];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

-(IBAction)savePressed{
	if([keyField.text isEqualToString:@""]){
		[eDiaryAppDelegate showMessage:@"Type field is empty" title:@"Empty Field"];
		return;
	}
	if([valueField.text isEqualToString:@""]){
		[eDiaryAppDelegate showMessage:@"Value field is empty" title:@"Empty Field"];
		return;
	}
	Vault *vault = [[Vault alloc] init];

	if(isEditMode){
		[vault updateSecret:keyField.text value:valueField.text oldKeyName:vaultObj.keyStr];
	}else{
		NSArray *keys = [vault getAllKeys];
		for (NSDictionary *dictionary in keys){
			NSString *key = [dictionary valueForKey:@"key_name"];
			if([keyField.text isEqualToString:key]){
				[eDiaryAppDelegate showMessage:@"The type already exists.Please enter a unique type" title:@"Type Exists"];
				return;
			}
		}
		[vault insert:keyField.text value:valueField.text];
	}
	[vault release];
	
	[keyField resignFirstResponder];
	[valueField resignFirstResponder];
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	LockerViewController *lockerView = [[LockerViewController alloc] initWithNibName:@"LockerViewController" bundle:nil];
	self.lockerViewController = lockerView;
	[self.view addSubview:self.lockerViewController.view];
	[lockerView release];
	[UIView commitAnimations];
}

-(IBAction)backPressed{
	[keyField resignFirstResponder];
	[valueField resignFirstResponder];
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	LockerViewController *lockerView = [[LockerViewController alloc] initWithNibName:@"LockerViewController" bundle:nil];
	self.lockerViewController = lockerView;
	[self.view addSubview:self.lockerViewController.view];
	[lockerView release];
	
	[UIView commitAnimations];
}
- (void)dealloc {
    [super dealloc];
}


@end
