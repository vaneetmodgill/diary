//
//  ReverseGeoCoder.m
//  PMR
//
//  Created by Administrator on 4/15/10.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "ReverseGeoCoder.h"


@implementation ReverseGeoCoder
@synthesize currentElementName;
+(NSString*)GetAddress:(CLLocationCoordinate2D)loc
{
	NSURL *url =[ [NSURL alloc] initWithString:[NSString stringWithFormat:@"http://maps.google.com/maps/geo?q=%f,%f&output=xml&oe=utf8&sensor=true_or_false&key=ABQIAAAAtPRVrSzcgi-1QDHfjA7RtxT-FyPSuXEFB16F17K6V-LEY6TDvBQiyvuBq5Brm0F1pHEmz0VakwsC3A",loc.latitude,loc.longitude]];
	//NSMutableData *nsDataObject = [[NSData alloc] initWithContentsOfURL:url];
	NSMutableData *myMutableData;
	myMutableData=[[NSMutableData data] retain]; 
	myMutableData = [[NSMutableData alloc]initWithContentsOfURL:url];
	NSError *parseError;
	
	//NSString *theXml = [[NSString alloc]initWithBytes:[myMutableData mutableBytes] length:[myMutableData length] encoding:NSUTF8StringEncoding];
	ReverseGeoCoder *geoCoder = [[ReverseGeoCoder alloc]init];
	NSString *returnValue = [geoCoder parseXMLFileFromData:myMutableData parseError:&parseError];
	
	return returnValue; 
}

- (NSString*)parseXMLFileFromData:(NSMutableData *)mutData parseError:(NSError **)error
{
	NSXMLParser *parser = [[NSXMLParser alloc] initWithData:mutData];
    // Set self as the delegate of the parser so that it will receive the parser delegate methods callbacks.
    [parser setDelegate:self];
    // Depending on the XML document you're parsing, you may want to enable these features of NSXMLParser.
    [parser setShouldProcessNamespaces:NO];
    [parser setShouldReportNamespacePrefixes:NO];
    [parser setShouldResolveExternalEntities:NO];
    
    [parser parse];
    
	//NSMutableArray *parentArray1 = self.parentArray;
    NSError *parseError = [parser parserError];
    if (parseError && error) {
        *error = parseError;
    }
    
    [parser release];
	//NSString* retvalue = @"";
	if(Address==nil)
	{
		Address = @"";
	}
	
	return Address;
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
	
}

- (void)parserDidStartDocument:(NSXMLParser *)parser{		
	currentElementName=@"";
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
	currentElementName = [elementName copy];
	if ([elementName isEqualToString:@"coordinates"]) {
		if(Address ==nil)
		{
			Address = [[NSMutableString alloc]init];
		}
	}
	
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
	if ([elementName isEqualToString:@"coordinates"]) {		
	}
	
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	if ([currentElementName  isEqualToString:@"coordinates"]) {
		[Address appendString: string];
		[parser abortParsing];
	} 
	
}


@end
