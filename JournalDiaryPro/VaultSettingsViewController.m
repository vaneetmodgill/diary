///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   VaultSettingsViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "VaultSettingsViewController.h"
#import "Secure.h"
#import "LockerViewController.h"
#import "eDiaryAppDelegate.h"
#import "Settings.h"
#import "SettingsQAViewController.h"
@implementation VaultSettingsViewController
@synthesize settingsQAViewController;
@synthesize lockerViewController;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        
        
        backgroundImageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        backgroundImageView.frame = CGRectMake(0, -20, 320, 568+40);
        //   self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 568+20);
    }
    else
    {
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 480+20);
    }
    
    

    
    
	[super viewDidLoad];
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];
	vaultArray = [[NSArray alloc] initWithObjects:@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",@"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z",@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",nil];
    Secure *secure = [[Secure alloc] init];
	[vaultSwitch setOn:[secure isVaultSecure]];
	NSString *passCode = [secure getVaultPassword];
	if(![passCode isEqualToString:@""]){
		////NSLog(@"%c %c %c %c %c %c",[passCode characterAtIndex:0] ,[passCode characterAtIndex:1],[passCode characterAtIndex:2],[passCode characterAtIndex:3],
		//	[passCode characterAtIndex:4] ,[passCode characterAtIndex:5]);
		[vaultPicker selectRow:[vaultArray indexOfObject:[NSString stringWithFormat:@"%c",[passCode characterAtIndex:0]]] inComponent:0 animated:YES];
		[vaultPicker selectRow:[vaultArray indexOfObject:[NSString stringWithFormat:@"%c",[passCode characterAtIndex:1]]] inComponent:1 animated:YES];
		[vaultPicker selectRow:[vaultArray indexOfObject:[NSString stringWithFormat:@"%c",[passCode characterAtIndex:2]]] inComponent:2 animated:YES];
		[vaultPicker selectRow:[vaultArray indexOfObject:[NSString stringWithFormat:@"%c",[passCode characterAtIndex:3]]] inComponent:3 animated:YES];
		[vaultPicker selectRow:[vaultArray indexOfObject:[NSString stringWithFormat:@"%c",[passCode characterAtIndex:4]]] inComponent:4 animated:YES];
		[vaultPicker selectRow:[vaultArray indexOfObject:[NSString stringWithFormat:@"%c",[passCode characterAtIndex:5]]] inComponent:5 animated:YES];
	}
	[secure release];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

-(IBAction)savePressed{
	Secure *secure = [[Secure alloc] init];
	NSString *passCode = @"";
	passCode = [passCode stringByAppendingString:[vaultArray objectAtIndex:[vaultPicker selectedRowInComponent:0]]];
	passCode = [passCode stringByAppendingString:[vaultArray objectAtIndex:[vaultPicker selectedRowInComponent:1]]];
	passCode = [passCode stringByAppendingString:[vaultArray objectAtIndex:[vaultPicker selectedRowInComponent:2]]];
	passCode = [passCode stringByAppendingString:[vaultArray objectAtIndex:[vaultPicker selectedRowInComponent:3]]];
	passCode = [passCode stringByAppendingString:[vaultArray objectAtIndex:[vaultPicker selectedRowInComponent:4]]];
	passCode = [passCode stringByAppendingString:[vaultArray objectAtIndex:[vaultPicker selectedRowInComponent:5]]];
	if([secure getCount] == 0){
		[secure insert:@"" lockerPass:passCode question:@"" answer:@"" vaultQ:@"" vaultA:@""];
		[secure updateVaultProtection:[vaultSwitch isOn]];
	}else {
		[secure updateVaultPassword:passCode];
		[secure updateVaultProtection:[vaultSwitch isOn]];
	}
	[secure release];
	if([vaultSwitch isOn]){
		[eDiaryAppDelegate showMessage:[NSString stringWithFormat:@"The passcode saved is %@.Please do not forget the passcode",passCode] title:@"Information"];
	}
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
	LockerViewController *lockerView = [[LockerViewController alloc] initWithNibName:@"LockerViewController" bundle:nil];
	self.lockerViewController = lockerView;
	[self.view addSubview:self.lockerViewController.view];
	[lockerView release];
	
	[UIView commitAnimations];
}
-(IBAction)cancelPressed{
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	LockerViewController *lockerView = [[LockerViewController alloc] initWithNibName:@"LockerViewController" bundle:nil];
	self.lockerViewController = lockerView;
	[self.view addSubview:self.lockerViewController.view];
	[lockerView release];
	[UIView commitAnimations];
}
-(IBAction)viewQA{
	SettingsQAViewController *settingsQAView = [[SettingsQAViewController alloc] initWithNibName:@"SettingsQAViewController" bundle:nil];
	settingsQAView.isVault = YES;
	self.settingsQAViewController = settingsQAView;
	[self.view addSubview:self.settingsQAViewController.view];
	[settingsQAView release];
}
- (void)dealloc {
	[vaultArray release];
    [super dealloc];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
	return 6;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
	//	if(component ==kEmoticon){
	return [vaultArray count];
	//	}
	
	/*else if(component == kDescription){
	 return [descriptionArray count];
	 }else return 0;*/
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row
		   forComponent:(NSInteger)component{
	//	if(component == kDescription){
	return [vaultArray objectAtIndex:row];
	//	}else return @"";
	
}

@end
