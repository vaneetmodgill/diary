///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   DiaryEntryViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "DiaryEntryViewController.h"
#import "HomeViewController.h"
#import "CalendarViewController.h"
#import "AddEntryViewController.h"
#import "ShowEntryViewController.h"
#import "eDiaryAppDelegate.h"
#import "DiaryObject.h"
#import "Diary.h"
#import "Settings.h"
#import "DayPicture.h"
@implementation DiaryEntryViewController
@synthesize diaryDate;
@synthesize image;
@synthesize searchText;
@synthesize searchDone;
@synthesize calendarViewController;
@synthesize addEntryViewController;
@synthesize showEntryViewController;
@synthesize homeViewController;
@synthesize imageback;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    
    self.view.autoresizesSubviews = YES ;
    // self.view.autoresizingMask = UIViewAutoresizingFlexibleHeight ;
    
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
      

        
        backgroundImageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        backgroundImageView.frame = CGRectMake(0, -20, 320, 568+40);
     //   self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 568+20);
           }
    else
    {
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 480+20);
    }
    
    
	Diary *diary = [[Diary alloc] init];
	NSMutableArray *mutArray = [[NSMutableArray alloc] init];
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
		
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy-MM-dd"];
	//[formatter setTimeStyle:NSDateFormatterShortStyle];
	

	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit | NSMinuteCalendarUnit ;
	NSDateComponents *compsDate = [gregorian components:unitFlags fromDate:[formatter dateFromString:[NSString stringWithFormat:@"%@",settings.globalDate]]];
	
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	NSDateComponents *compsTime = [gregorian components:unitFlags fromDate:[NSDate date]];
	[compsTime setDay:[compsDate day]];
	[compsTime setMonth:[compsDate month]];
	[compsTime setYear:[compsDate year]];
	self.diaryDate = [gregorian dateFromComponents:compsTime];
	
		if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];
	[gregorian release];
//	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterNoStyle];
	if(self.diaryDate !=nil){
		dateLabel.text = [formatter stringFromDate:self.diaryDate];
	}else {
		dateLabel.text = [formatter stringFromDate:[NSDate date]];
		self.diaryDate = [NSDate date];
	}
	[formatter setDateFormat:@"YYYY-MM-dd"];	
	if(searchDone){
		NSArray *array = [diary getSearchData:searchText];
		for (NSDictionary *dictionary in array){
			DiaryObject *diaryObj = [[DiaryObject alloc] init];
			diaryObj.dateStr = [dictionary valueForKey:@"date"];
			diaryObj.timeStr = [dictionary valueForKey:@"time"];
			diaryObj.content = [dictionary valueForKey:@"entry"];
			diaryObj.emoticon =[dictionary valueForKey:@"emoticon"];
			[mutArray addObject:diaryObj];
			[diaryObj release];
		}
	}else{
		NSArray *array = [diary getData:[formatter stringFromDate:self.diaryDate]];
		//NSLog(@"array %@",array);
		for (NSDictionary *dictionary in array){
			DiaryObject *diaryObj = [[DiaryObject alloc] init];
			diaryObj.dateStr = [dictionary valueForKey:@"date"];
			diaryObj.timeStr = [dictionary valueForKey:@"time"];
			diaryObj.content = [dictionary valueForKey:@"entry"];
			diaryObj.emoticon =[dictionary valueForKey:@"emoticon"];
			[mutArray addObject:diaryObj];
			[diaryObj release];
	}
	}
	entriesArray = [mutArray mutableCopy];
	[mutArray release];
	[formatter release];
	[diaryTable reloadData];
	[diary release];
	
	UIImage *smile = [UIImage imageNamed:@"smile.png"];
	UIImage *angry = [UIImage imageNamed:@"beyond_endurance.png"];
	UIImage *cool = [UIImage imageNamed:@"rockn_roll.png"];
	UIImage *frown = [UIImage imageNamed:@"unhappy.png"];
	UIImage *grin = [UIImage imageNamed:@"big_smile.png"];
	UIImage *innocent = [UIImage imageNamed:@"shame.png"];
	UIImage *kiss = [UIImage imageNamed:@"greeding.png"];
	UIImage *naughty = [UIImage imageNamed:@"the_devil.png"];
	UIImage *sealed = [UIImage imageNamed:@"what.png"];
	UIImage *tear = [UIImage imageNamed:@"cry.png"];	
	UIImage *toungue = [UIImage imageNamed:@"bad_smile.png"];	
	UIImage *undecided = [UIImage imageNamed:@"i_have_no_idea.png"];	
	UIImage *wink = [UIImage imageNamed:@"wicked.png"];
	UIImage *wow = [UIImage imageNamed:@"surprise.png"];	
	UIImage *yay = [UIImage imageNamed:@"pretty_smile.png"];
	/*
	UIGraphicsBeginImageContext(CGSizeMake(60.0,60.0));
	[[UIImage imageNamed:@"smile.png"] drawInRect:CGRectMake(0.0, 0.0, 60.0, 60.0)];
	UIImage *smile = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(60.0,60.0));
	[[UIImage imageNamed:@"beyond_endurance.png"] drawInRect:CGRectMake(0.0, 0.0, 60.0, 60.0)];
	UIImage *angry = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(60.0,60.0));
	[[UIImage imageNamed:@"rockn_roll.png"] drawInRect:CGRectMake(0.0, 0.0, 60.0, 60.0)];
	UIImage *cool = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(60.0,60.0));
	[[UIImage imageNamed:@"unhappy.png"] drawInRect:CGRectMake(0.0, 0.0, 60.0, 60.0)];
	UIImage *frown = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(60.0,60.0));
	[[UIImage imageNamed:@"big_smile.png"]  drawInRect:CGRectMake(0.0, 0.0, 60.0, 60.0)];
	UIImage *grin = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(60.0,60.0));
	[[UIImage imageNamed:@"shame.png"]  drawInRect:CGRectMake(0.0, 0.0, 60.0, 60.0)];
	UIImage *innocent = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(60.0,60.0));
	[[UIImage imageNamed:@"greeding.png"]  drawInRect:CGRectMake(0.0, 0.0, 60.0, 60.0)];
	UIImage *kiss = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(60.0,60.0));
	[[UIImage imageNamed:@"the_devil.png"]  drawInRect:CGRectMake(0.0, 0.0, 60.0, 60.0)];
	UIImage *naughty = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(60.0,60.0));
	[[UIImage imageNamed:@"what.png"]  drawInRect:CGRectMake(0.0, 0.0, 60.0, 60.0)];
	UIImage *sealed = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(60.0,60.0));
	[[UIImage imageNamed:@"cry.png"]  drawInRect:CGRectMake(0.0, 0.0, 60.0, 60.0)];
	UIImage *tear = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(60.0,60.0));
	[[UIImage imageNamed:@"bad_smile.png"] drawInRect:CGRectMake(0.0, 0.0, 60.0, 60.0)];
	UIImage *toungue = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(60.0,60.0));
	[[UIImage imageNamed:@"i_have_no_idea.png"] drawInRect:CGRectMake(0.0, 0.0, 60.0, 60.0)];	
	UIImage *undecided = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(60.0,60.0));
	[[UIImage imageNamed:@"wicked.png"] drawInRect:CGRectMake(0.0, 0.0, 60.0, 60.0)];
	UIImage *wink = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(60.0,60.0));
	[[UIImage imageNamed:@"surprise.png"]  drawInRect:CGRectMake(0.0, 0.0, 60.0, 60.0)];
	UIImage *wow = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(60.0,60.0));
	[[UIImage imageNamed:@"pretty_smile.png"]  drawInRect:CGRectMake(0.0, 0.0, 60.0, 60.0)];
	UIImage *yay = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	*/
	/*
	UIImageView *smileView = [[UIImageView alloc] initWithImage:smile];
	UIImageView *angryView = [[UIImageView alloc] initWithImage:angry];
	UIImageView *coolView = [[UIImageView alloc] initWithImage:cool];
	UIImageView *frownView = [[UIImageView alloc] initWithImage:frown];
	UIImageView *grinView = [[UIImageView alloc] initWithImage:grin];
	UIImageView *innocentView = [[UIImageView alloc] initWithImage:innocent];
	UIImageView *kissView = [[UIImageView alloc] initWithImage:kiss];
	UIImageView *naughtyView = [[UIImageView alloc] initWithImage:naughty];
	UIImageView *sealedView = [[UIImageView alloc] initWithImage:sealed];
	UIImageView *tearView = [[UIImageView alloc] initWithImage:tear];
	UIImageView *toungueView = [[UIImageView alloc] initWithImage:toungue];
	UIImageView *undecidedView = [[UIImageView alloc] initWithImage:undecided];
	UIImageView *winkView = [[UIImageView alloc] initWithImage:wink];
	UIImageView *wowView = [[UIImageView alloc] initWithImage:wow];
	UIImageView *yayView = [[UIImageView alloc] initWithImage:yay];
	 */
	
	NSArray *array = [[NSArray alloc] initWithObjects:smile,angry,cool,frown,grin,innocent,kiss,naughty,sealed,tear,toungue,undecided,wink,wow,yay,nil];
	emoticonArray = [array copy];
	[array release];
	//	emoticonArray = [[NSArray alloc] initWithObjects:smileView,angryView,coolView,frownView,grinView,innocentView,kissView,naughtyView,sealedView,tearView,toungueView,undecidedView,winkView,wowView,yayView,nil];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
	//NSLog(@"RECEIVED MEMORY WARNING in DiaryEntryViewController!!!");
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	NSLog(@"Did unload called");
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

-(IBAction)calendarPressed{
	[entrySearchBar resignFirstResponder];
	CalendarViewController *calendarView = [[CalendarViewController alloc] initWithNibName:@"CalendarViewController" bundle:nil];
	self.calendarViewController = calendarView;
	[eDiaryAppDelegate setCalendarName:@"Diary"];
	[self.view addSubview:self.calendarViewController.view];
	[calendarView release];
}
-(IBAction)addPressed{
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
	AddEntryViewController *addEntryView = [[AddEntryViewController alloc] initWithNibName:@"AddEntryViewController" bundle:nil];
	
	addEntryView.timeSelected = self.diaryDate;
	self.addEntryViewController = addEntryView;
	[self.view addSubview:self.addEntryViewController.view];
	[addEntryView release];
	[UIView commitAnimations];
}
-(IBAction)backPressed{
	[entrySearchBar resignFirstResponder];
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	HomeViewController *homeView = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
	self.homeViewController = homeView;
	[self.view addSubview:self.homeViewController.view];
    
    
    NSLog(@"VIEw dimensions %f  %f \n ",self.view.frame.origin.y,self.view.frame.size.height);
    
	[homeView release];
	[UIView commitAnimations];
	
}

-(IBAction)leftPressed{
	[entrySearchBar resignFirstResponder];
	[entriesArray removeAllObjects];
	Diary *diary = [[Diary alloc] init];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterNoStyle];
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit | NSMinuteCalendarUnit;
	NSDateComponents *comps = [gregorian components:unitFlags fromDate:self.diaryDate];
//	[comps setDay:[comps day]-1];
//	[formatter setTimeStyle:NSDateFormatterShortStyle];
	NSDateComponents *compsTime = [gregorian components:unitFlags fromDate:[NSDate date]];
	[compsTime setDay:[comps day]-1];
	[compsTime setMonth:[comps month]];
	[compsTime setYear:[comps year]];
	self.diaryDate = [gregorian dateFromComponents:compsTime];
	NSLog(@"date %@",self.diaryDate);
//	self.diaryDate = [gregorian dateFromComponents:comps];
	[gregorian release];
	dateLabel.text = [formatter stringFromDate:self.diaryDate];
	
	[formatter setDateFormat:@"YYYY-MM-dd"];	
	NSArray *array = [diary getData:[formatter stringFromDate:self.diaryDate]];
	//NSLog(@"array %@",array);
	for (NSDictionary *dictionary in array){
		DiaryObject *diaryObj = [[DiaryObject alloc] init];
		diaryObj.dateStr = [dictionary valueForKey:@"date"];
		diaryObj.timeStr = [dictionary valueForKey:@"time"];
		diaryObj.content = [dictionary valueForKey:@"entry"];
		diaryObj.emoticon =[dictionary valueForKey:@"emoticon"];
		[entriesArray addObject:diaryObj];
		[diaryObj release];
	}
	[diaryTable reloadData];
	Settings *settings = [[Settings alloc] init];
	
	[settings storeGlobalDate:[formatter stringFromDate:self.diaryDate]];
	[settings loadSettings];
	if(settings.picOfDayEnabled && settings.picEnabled){
		DayPicture *dayPicture = [[DayPicture alloc] init];
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *imgName =[dayPicture getPictureNameForDate:settings.globalDate];
		//NSLog(@"name %@",imgName);
		if(![imgName isEqualToString:@""]){ 
		UIImage *img= [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",documentsDirectory,imgName]];
		[eDiaryAppDelegate setHomeImage:img];
		imageView.image = img;
		}else{
			[eDiaryAppDelegate setHomeImage:nil];
			imageView.image = nil;
		}
		[dayPicture release];
	}
	[settings release];
	[formatter release];
	[diary release];
}
-(IBAction)rightPressed{
	[entrySearchBar resignFirstResponder];
	[entriesArray removeAllObjects];
	Diary *diary = [[Diary alloc] init];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterNoStyle];
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit | NSMinuteCalendarUnit;
//	NSDateComponents *comps = [gregorian components:unitFlags fromDate:self.diaryDate];
	NSDateComponents *comps = [gregorian components:unitFlags fromDate:self.diaryDate];
//	[formatter setTimeStyle:NSDateFormatterShortStyle];
	NSDateComponents *compsTime = [gregorian components:unitFlags fromDate:[NSDate date]];
	[compsTime setDay:[comps day]+1];
	[compsTime setMonth:[comps month]];
	[compsTime setYear:[comps year]];
	self.diaryDate = [gregorian dateFromComponents:compsTime];
	NSLog(@"date %@",self.diaryDate);
	[gregorian release];
	dateLabel.text = [formatter stringFromDate:self.diaryDate];
	
	[formatter setDateFormat:@"YYYY-MM-dd"];	
	NSArray *array = [diary getData:[formatter stringFromDate:self.diaryDate]];
	//NSLog(@"array %@",array);
	for (NSDictionary *dictionary in array){
		DiaryObject *diaryObj = [[DiaryObject alloc] init];
		diaryObj.dateStr = [dictionary valueForKey:@"date"];
		diaryObj.timeStr = [dictionary valueForKey:@"time"];
		diaryObj.content = [dictionary valueForKey:@"entry"];
		diaryObj.emoticon =[dictionary valueForKey:@"emoticon"];
		[entriesArray addObject:diaryObj];
		[diaryObj release];
	}
	[diaryTable reloadData];
	Settings *settings = [[Settings alloc] init];
	
	[settings storeGlobalDate:[formatter stringFromDate:self.diaryDate]];
	[settings loadSettings];
	if(settings.picOfDayEnabled && settings.picEnabled){
		DayPicture *dayPicture = [[DayPicture alloc] init];
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *imgName =[dayPicture getPictureNameForDate:settings.globalDate];
		//NSLog(@"name %@",imgName);
		if(![imgName isEqualToString:@""]){ 
			UIImage *img= [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",documentsDirectory,imgName]];
			[eDiaryAppDelegate setHomeImage:img];
			imageView.image = img;
		}else{
			[eDiaryAppDelegate setHomeImage:nil];
			imageView.image = nil;
		}
		[dayPicture release];
	}
	[settings release];	
	[formatter release];
	[diary release];
}
- (void)dealloc {
	[entriesArray release];
	[emoticonArray release];
    [super dealloc];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
	//NSLog(@"search bar");
	if([searchBar.text isEqualToString:@""]){
	
		[eDiaryAppDelegate showMessage:@"Please enter a search text" title:@"Empty text"];
	}else{
		Diary *diary = [[Diary alloc] init];

		NSArray *array = [diary getSearchData:searchBar.text];
		[diary release];
		if([array count] == 0){
			[eDiaryAppDelegate showMessage:@"Please enter a different search text" title:@"No matches found"];
		}else{
			self.searchText = searchBar.text; 	
			self.searchDone = YES;
			[entriesArray removeAllObjects];
			for (NSDictionary *dictionary in array){
				DiaryObject *diaryObj = [[DiaryObject alloc] init];
				diaryObj.dateStr = [dictionary valueForKey:@"date"];
				diaryObj.timeStr = [dictionary valueForKey:@"time"];
				diaryObj.content = [dictionary valueForKey:@"entry"];
				diaryObj.emoticon =[dictionary valueForKey:@"emoticon"];
				[entriesArray addObject:diaryObj];
				[diaryObj release];
			}
			[diaryTable reloadData];
		}
	}
	[searchBar resignFirstResponder];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [entriesArray count];
}

- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	/*    if (cell == nil) {
	 cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
	 }
	 
	 // Set up the cell...
	 if(indexPath.section == 0) {
	 cell.text = [foodItems objectAtIndex:indexPath.row];
	 } else if(indexPath.section == 1) {
	 cell.text = [activityItems objectAtIndex:indexPath.row];
	 }*/
	
	
    if (cell == nil) {
        cell = [self getCellContentView:CellIdentifier];
    }
	
	UILabel *timeLbl =  (UILabel *)[cell viewWithTag:1];
	UILabel *contentLbl =  (UILabel *)[cell viewWithTag:2];
//	UIView *tempView = (UIView *)[cell viewWithTag:3];
	UIImageView *tempView = (UIImageView*)[cell viewWithTag:3];
	
	
	
		DiaryObject *obj = (DiaryObject *)[entriesArray objectAtIndex:indexPath.row];
	if(self.searchDone == YES){
		timeLbl.text = [NSString stringWithFormat:@"%@ %@",obj.dateStr,obj.timeStr];
	}else {
		timeLbl.text = obj.timeStr;
	}

		contentLbl.text = obj.content;
	
		if(![obj.emoticon isEqualToString:@"0"]){
//		tempView = [emoticonArray objectAtIndex:[(NSNumber*)obj.emoticon integerValue]-1];
			tempView.image = (UIImage*)[emoticonArray objectAtIndex:[(NSNumber*)obj.emoticon integerValue]-1];
		}else{
			tempView.image = nil;
		}
	//	[tempView release];
	
	/*	
	 if(indexPath.row%2) {
	 [lblTemp1 setBackgroundColor:[UIColor colorWithRed:0.219 green:0.586 blue:0.590 alpha:1.0]];
	 [lblTemp2 setBackgroundColor:[UIColor colorWithRed:0.219 green:0.586 blue:0.590 alpha:1.0]];
	 } else {
	 [lblTemp1 setBackgroundColor:[UIColor colorWithRed:0.391 green:0.871 blue:0.852 alpha:1.0]];
	 [lblTemp2 setBackgroundColor:[UIColor colorWithRed:0.391 green:0.871 blue:0.852 alpha:1.0]];
	 }
	 */
    return cell;
}
- (UITableViewCell *) getCellContentView:(NSString *)cellIdentifier {
	
	CGRect CellFrame = CGRectMake(0.0f, 0.0f, 320.0f, 75.0f);
	CGRect imgFrame = CGRectMake(25.0f, 30.0f, 60.0f, 60.0f);
	CGRect lblFrame1 = CGRectMake(60.0f, 0.0f, 280.0f, 25.0f);
	CGRect lblFrame2 = CGRectMake(60.0f, 15.0f, 260.0f, 50.0f);
//	CGRect lblFrame3 = CGRectMake(17.0f, 0.0f, 15.0f, 15.0f);
	UILabel *lblTemp1;
	UILabel *lblTemp2;
//	UILabel *lblTemp3;
	UITableViewCell *cell = [[[UITableViewCell alloc] initWithFrame:CellFrame reuseIdentifier:cellIdentifier] autorelease];
	
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	
	//Initialize Label 1 with tag 1.
	lblTemp1 = [[UILabel alloc] initWithFrame:lblFrame1];
	[lblTemp1 setBackgroundColor:[UIColor clearColor]];
	//	[lblTemp1 sizeToFit];
	//	[lblTemp1 setMinimumFontSize:8.0];
	//[lblTemp1 setFont:[UIFont systemFontOfSize:12]];
	if([settings.fontName isEqualToString:@""] || settings.fontName == nil){
		[lblTemp1 setFont:[UIFont systemFontOfSize:15]];
		[settings storeFontSettings:@"" size:15.0];
	}else{
		//NSLog(@"font name %@",settings.fontName);	
		NSArray *fontNames = [UIFont fontNamesForFamilyName:settings.fontName];
		[lblTemp1 setFont:[UIFont fontWithName:[fontNames objectAtIndex:0] size:settings.fontSize]];
		//	[lblTemp2 setFont:[UIFont systemFontOfSize:15]];
	}
	lblTemp1.tag = 1;
	[cell.contentView addSubview:lblTemp1];
	[lblTemp1 release];
	
	//Initialize Label 2 with tag 2.
	lblTemp2 = [[UILabel alloc] initWithFrame:lblFrame2];
	[lblTemp2 setBackgroundColor:[UIColor clearColor]];
	lblTemp2.tag = 2;
	lblTemp2.numberOfLines = 2;
	//	[lblTemp2 sizeToFit];
	//	[lblTemp2 setMinimumFontSize:8.0];
	
	if([settings.fontName isEqualToString:@""] || settings.fontName == nil){
		[lblTemp2 setFont:[UIFont systemFontOfSize:20]];
		[settings storeFontSettings:@"" size:20.0];
	}else{
		//NSLog(@"font name %@",settings.fontName);	
	NSArray *fontNames = [UIFont fontNamesForFamilyName:settings.fontName];
	[lblTemp2 setFont:[UIFont fontWithName:[fontNames objectAtIndex:0] size:settings.fontSize]];
	//	[lblTemp2 setFont:[UIFont systemFontOfSize:15]];
	}
	[settings release];
	[cell.contentView addSubview:lblTemp2];
	[lblTemp2 release];
	/*
	lblTemp3 = [[UILabel alloc] initWithFrame:lblFrame3];
	[lblTemp3 setBackgroundColor:[UIColor clearColor]];
	lblTemp3.tag = 3;
	
	//	[lblTemp2 sizeToFit];
	//	[lblTemp2 setMinimumFontSize:8.0];
	
	[cell.contentView addSubview:lblTemp3];
	[lblTemp3 release];
	*/
	/*
	UIView *tempView = [[UIView alloc] initWithFrame:lblFrame ] ;
	tempView.tag = 3;
	[cell.contentView addSubview:tempView];
	[tempView release];
	 */
	UIImageView *tempView = [[UIImageView alloc] initWithFrame:imgFrame];
	tempView.tag = 3;
	[tempView setCenter:CGPointMake(27.0f,37.5f)];
	[cell.contentView addSubview:tempView];
	[tempView release];
	
	return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 75.0f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[entrySearchBar resignFirstResponder];
	ShowEntryViewController *showEntryView = [[ShowEntryViewController alloc] initWithNibName:@"ShowEntryViewController" bundle:nil];
	DiaryObject *obj = (DiaryObject*)[entriesArray objectAtIndex:indexPath.row];
	showEntryView.entryObj = obj;
	showEntryView.searchDone = self.searchDone;
	showEntryView.searchStr = self.searchText;
	self.showEntryViewController = showEntryView;
	[self.view addSubview:showEntryViewController.view];
	[showEntryView release];
}

@end
