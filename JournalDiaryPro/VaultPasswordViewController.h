///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   VaultPasswordViewController.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>
#import "SKPSMTPMessage.h"
@class HomeViewController;
@class LockerViewController;
@interface VaultPasswordViewController : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource,UIActionSheetDelegate,SKPSMTPMessageDelegate> {
	IBOutlet UIPickerView *vaultPicker;
	IBOutlet UIImageView *imageView;
	IBOutlet UIView *forgotView;
	IBOutlet UITextField *forgotText;
	IBOutlet UILabel *titleLbl;
	IBOutlet UILabel *messageLbl;
	NSArray *vaultArray;
	NSString *answer;
	HomeViewController *homeViewController;
	LockerViewController *lockerViewController;
    
    IBOutlet UIImageView *backgroundImageView;
}
@property(nonatomic,retain) HomeViewController *homeViewController;
@property(nonatomic,retain)LockerViewController *lockerViewController;
-(IBAction)openVault;
-(IBAction)backPressed;
-(IBAction)forgotPressed;
-(IBAction)openPressed;
-(IBAction)cancelPressed;
@end
