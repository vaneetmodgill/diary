///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   SettingsExportViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "SettingsExportViewController.h"
#import "SettingsViewController.h"
#import "CalendarViewController.h"
#import "Diary.h"
#import "Settings.h"
#import "eDiaryAppDelegate.h"
#import "SKPSMTPMessage.h"
#import "Events.h"
#import "Vault.h"
@implementation SettingsExportViewController
@synthesize startDate;
@synthesize endDate;
@synthesize settingsViewController;
@synthesize calendarViewController;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];
	[exportTable setBackgroundColor:[UIColor clearColor]];
	if(self.startDate == nil){
		self.startDate = [NSDate date];
	}
	if(self.endDate == nil){
		self.endDate = [NSDate date];
	}
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterNoStyle];
	fromLbl.text = [formatter stringFromDate:self.startDate];
	toLbl.text = [formatter stringFromDate:self.endDate];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
-(IBAction)backPressed{
	SettingsViewController *settingsView = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController"bundle:nil];
	self.settingsViewController = settingsView;
	[self.view addSubview:self.settingsViewController.view];
	[settingsView release];
}
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

-(IBAction)exportPressed{
	int count = 0;
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	//NSLog(@"start date %@",self.startDate);
	//NSLog(@"end date %@",self.endDate);
	
	if((diaryCell.accessoryType != UITableViewCellAccessoryCheckmark) && (eventCell.accessoryType != UITableViewCellAccessoryCheckmark)&&(vaultCell.accessoryType != UITableViewCellAccessoryCheckmark)){
		[eDiaryAppDelegate showMessage:@"No item selected to export" title:@"No Selection"];
		[settings release];
		return;
	}
	if(!settings.emailEnabled){
		[eDiaryAppDelegate showMessage:@"Please turn on Send Email in Settings" title:@"Email not enabled"];
		[settings release];
		return;
	}
	if([settings.emailAddress isEqualToString:@""] || settings.emailAddress == nil){
		[eDiaryAppDelegate showMessage:@"Please configure email in Settings" title:@"Email settings not configured"];
		[settings release];
		return;
	}
	
	if([self.startDate timeIntervalSince1970] > [self.endDate timeIntervalSince1970]){
//		NSDate *swapDate = self.startDate;
//		self.startDate = [self.startDate earlierDate:self.endDate];
//		self.endDate = [self.startDate laterDate:self.endDate];
	//	//NSLog(@"swapping");
		[eDiaryAppDelegate showMessage:@"Please enter a start date  earlier than end date" title:@"Error!"];
		return;
	}
	NSDateFormatter *form = [[NSDateFormatter alloc] init];
	[form setDateFormat:@"YYYY-MM-dd"];
	NSMutableString *buffer = [NSMutableString stringWithString:@""];
	if(settings.userName != nil || ![settings.userName isEqualToString:@""]){
	[buffer appendFormat:@"Hi %@,\n\n",settings.userName];
	}
	[buffer appendString:@"Journal Diary Export Mail\n\n"];
	if(diaryCell.accessoryType == UITableViewCellAccessoryCheckmark){
	Diary *diary = [[Diary alloc] init];
	NSArray *array = [diary getExportData:[form stringFromDate:self.startDate] and:[form stringFromDate:self.endDate]];
	/*	
	if([array count] == 0){
		count++;
	}*/
		[buffer appendString:@"Diary Entries"];
		[buffer appendString:@"\n---------------------------------------------------------------------\n"];
	for(NSDictionary *dictionary in array){
		[buffer appendString:@""];
		[buffer appendFormat:@"%@",[dictionary valueForKey:@"date"]];
		[buffer appendString:@"  "];
		[buffer appendFormat:@"%@",[dictionary valueForKey:@"time"]];
		[buffer appendString:@"\n"];
		[buffer appendFormat:@"%@",[dictionary valueForKey:@"entry"]];
		[buffer appendString:@"\n\n"];
		count++;
	}
		[buffer appendString:@"---------------------------------------------------------------------\n\n"];
	}
	if(eventCell.accessoryType == UITableViewCellAccessoryCheckmark){
		Events *events = [[Events alloc] init];
		NSArray *array = [events getExportData:[form stringFromDate:self.startDate] and:[form stringFromDate:self.endDate]];
		[buffer appendString:@"Events"];
		[buffer appendString:@"\n---------------------------------------------------------------------\n"];
		for(NSDictionary *dictionary in array){
			[buffer appendString:@""];
			[buffer appendFormat:@"%@",[dictionary valueForKey:@"date"]];
			[buffer appendString:@"  "];
			[buffer appendFormat:@"%@",[dictionary valueForKey:@"time"]];
			[buffer appendString:@"\n"];
			[buffer appendFormat:@"%@",[dictionary valueForKey:@"location"]];
			[buffer appendString:@"\n"];
			[buffer appendFormat:@"%@",[dictionary valueForKey:@"title"]];
			[buffer appendString:@"\n"];
			[buffer appendFormat:@"%@",[dictionary valueForKey:@"description"]];
			[buffer appendString:@"\n"];
			count++;
		}
		[buffer appendString:@"---------------------------------------------------------------------\n\n"];
	}
	
	if(vaultCell.accessoryType == UITableViewCellAccessoryCheckmark){
		[buffer appendString:@"Vault"];
		[buffer appendString:@"\n---------------------------------------------------------------------\n"];
		Vault *vault = [[Vault alloc] init];
		NSArray *array = [vault getData];
		[vault release];
		for (NSDictionary *dictionary in array){
			[buffer appendString:@""];
			[buffer appendFormat:@"%@",[dictionary valueForKey:@"key_name"]];
			[buffer appendString:@" : "];
			[buffer appendFormat:@"%@",[dictionary valueForKey:@"key_value"]];
			[buffer appendString:@"\n"];
			count++;
		}
		[buffer appendString:@"---------------------------------------------------------------------\n\n"];
	}
	[buffer appendString:@"The Journal Diary Team"];
	//NSLog(@"export %@",buffer);
	if(count == 0){
		[eDiaryAppDelegate showMessage:@"Please select a date range which has entries" title:@"No Entries to export"];
		[settings release];
		return;
	}
	[settings loadSettings];
	SKPSMTPMessage *testMsg = [[SKPSMTPMessage alloc] init];
	testMsg.fromEmail = settings.emailAddress;
	testMsg.toEmail = settings.emailAddress;
	testMsg.relayHost = settings.emailServerAddress;
	testMsg.requiresAuth = YES;
	testMsg.login = settings.emailAddress;
	testMsg.pass = settings.emailPassword;
	testMsg.subject = [NSString stringWithFormat:@"Journal Diary Export from %@ to %@",[form stringFromDate:self.startDate],[form stringFromDate:self.endDate]];
	// testMsg.bccEmail = @"lemuelsinghr@gmail.com";
	testMsg.wantsSecure = YES; // smtp.gmail.com doesn't work without TLS!
	
	// Only do this for self-signed certs!
	// testMsg.validateSSLChain = NO;
	testMsg.delegate = self;
	
	
	NSDictionary *plainPart = [NSDictionary dictionaryWithObjectsAndKeys:@"text/plain",kSKPSMTPPartContentTypeKey,
							   buffer,kSKPSMTPPartMessageKey,@"8bit",kSKPSMTPPartContentTransferEncodingKey,nil];
	
	
	//NSString *vcfPath = [[NSBundle mainBundle] pathForResource:@"test" ofType:@"vcf"];
	//NSData *vcfData = [NSData dataWithContentsOfFile:vcfPath];
	/*
	 NSDictionary *vcfPart = [NSDictionary dictionaryWithObjectsAndKeys:@"text/directory;\r\n\tx-unix-mode=0644;\r\n\tname=\"test.vcf\"",kSKPSMTPPartContentTypeKey,
	 @"attachment;\r\n\tfilename=\"test.vcf\"",kSKPSMTPPartContentDispositionKey,[vcfData encodeBase64ForData],kSKPSMTPPartMessageKey,@"base64",kSKPSMTPPartContentTransferEncodingKey,nil];
	 */
	//testMsg.parts = [NSArray arrayWithObjects:plainPart,vcfPart,nil];
	testMsg.parts = [NSArray arrayWithObjects:plainPart,nil];
	[testMsg send];
	[settings release];
	
}
-(IBAction)startDatePressed{
	CalendarViewController *calendarView = [[CalendarViewController alloc] initWithNibName:@"CalendarViewController" bundle:nil];
	calendarView.preView = @"ExportFrom";
	calendarView.anotherDate = self.endDate;
	[eDiaryAppDelegate setCalendarName:@"Diary"];
	self.calendarViewController = calendarView;
	[self.view addSubview:self.calendarViewController.view];
	[calendarView release];
}
-(IBAction)endDatePressed{
	CalendarViewController *calendarView = [[CalendarViewController alloc] initWithNibName:@"CalendarViewController" bundle:nil];
	calendarView.preView = @"ExportTo";
	calendarView.anotherDate = self.startDate;
	[eDiaryAppDelegate setCalendarName:@"Diary"];
	self.calendarViewController = calendarView;
	[self.view addSubview:self.calendarViewController.view];
	[calendarView release];
}
- (void)dealloc {
    [super dealloc];
}
// Number of groups
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
	return 1;
}

// Section Titles
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	return @"Select the items to export";
}

// Number of rows per section
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
	
	return 3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	NSInteger row = [indexPath row];
//	NSInteger section = [indexPath section];
	UITableViewCell *cell;
	//	_BoardsAppDelegate *appDelegate = (_BoardsAppDelegate *) [UIApplication sharedApplication].delegate;
	
		
			if(row ==0){
				cell = [tableView dequeueReusableCellWithIdentifier:@"DiaryCell"];
				if (!cell) {
					cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"DiaryCell"] autorelease];
					cell.accessoryType = UITableViewCellAccessoryCheckmark;
					UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 300.0f, 20.0f)];
					[label setText:@"Diary entries"];
					label.backgroundColor = [UIColor clearColor];
					[cell addSubview:label];
					[label release];
				}
				diaryCell = cell;
				return cell;				
			}else if(row == 1){
				cell = [tableView dequeueReusableCellWithIdentifier:@"EventsCell"];
				if (!cell) {
					cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"EventsCell"] autorelease];
					cell.accessoryType = UITableViewCellAccessoryCheckmark;
					UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 300.0f, 20.0f)];
					[label setText:@"Events"];
					label.backgroundColor = [UIColor clearColor];
					[cell addSubview:label];
					[label release];
				}	
				eventCell = cell;
				return cell;
			}else{
				cell = [tableView dequeueReusableCellWithIdentifier:@"VaultCell"];
				if (!cell) {
					cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"VaultCell"] autorelease];
					cell.accessoryType = UITableViewCellAccessoryCheckmark;
					UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 300.0f, 20.0f)];
					[label setText:@"Vault"];
					label.backgroundColor = [UIColor clearColor];
					[cell addSubview:label];
					[label release];
				}	
				vaultCell = cell;
				return cell;
			}
	return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)newIndexPath
{
	[tableView deselectRowAtIndexPath:newIndexPath animated:NO];
	int row = newIndexPath.row;
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:newIndexPath];
	if(cell.accessoryType == UITableViewCellAccessoryCheckmark){
		cell.accessoryType = UITableViewCellAccessoryNone;
	}else{
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	}
	if(row == 0){
		diaryCell = cell;
	}else if(row == 1){
		eventCell = cell;
	}else{
		vaultCell = cell;
	}
}
- (void)messageSent:(SKPSMTPMessage *)message
{
    //[message release];
    
    //NSLog(@"delegate - message sent");
	[eDiaryAppDelegate showMessage:@"Diary Entries sent to email address" title:@"Email sent"];
}

- (void)messageFailed:(SKPSMTPMessage *)message error:(NSError *)error
{
    //[message release];
    
    ////NSLog(@"delegate - error(%d): %@", [error code], [error localizedDescription]);
	[eDiaryAppDelegate showMessage:[NSString stringWithFormat:@"Diary entries could not be sent to email address.Error %@",[error localizedDescription]] title:@"Error"];
	
	
}


@end
