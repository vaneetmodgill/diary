///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   VaultPasswordViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "VaultPasswordViewController.h"
#import "LockerViewController.h"
#import "HomeViewController.h"
#import "eDiaryAppDelegate.h"
#import "Secure.h"
#import "Vault.h"
#import "Settings.h"
#import "SKPSMTPMessage.h"
@implementation VaultPasswordViewController
@synthesize homeViewController;
@synthesize lockerViewController;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        
        
        backgroundImageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        backgroundImageView.frame = CGRectMake(0, -20, 320, 568+40);
        //   self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 568+20);
    }
    else
    {
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 480+20);
    }
    

    
	forgotView.hidden = YES;
	vaultArray = [[NSArray alloc] initWithObjects:@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",@"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z",@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",nil];
    
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];	[super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

-(IBAction)openVault{
	Secure *secure = [[Secure alloc] init];
	NSString *passCode = @"";
	passCode = [passCode stringByAppendingString:[vaultArray objectAtIndex:[vaultPicker selectedRowInComponent:0]]];
	passCode = [passCode stringByAppendingString:[vaultArray objectAtIndex:[vaultPicker selectedRowInComponent:1]]];
	passCode = [passCode stringByAppendingString:[vaultArray objectAtIndex:[vaultPicker selectedRowInComponent:2]]];
	passCode = [passCode stringByAppendingString:[vaultArray objectAtIndex:[vaultPicker selectedRowInComponent:3]]];
	passCode = [passCode stringByAppendingString:[vaultArray objectAtIndex:[vaultPicker selectedRowInComponent:4]]];
	passCode = [passCode stringByAppendingString:[vaultArray objectAtIndex:[vaultPicker selectedRowInComponent:5]]];
	if([passCode isEqualToString:[secure getVaultPassword]]){
		[secure release];
		LockerViewController *locker = [[LockerViewController alloc] initWithNibName:@"LockerViewController" bundle:nil];
		[self.view addSubview:locker.view];
	}else{
		[secure release];
		[eDiaryAppDelegate showMessage:@"Sorry wrong passcode.Please try again" title:@"Wrong Passcode"];
	}
}
-(IBAction)backPressed{
	HomeViewController *homeView = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
	self.homeViewController = homeView;
	[self.view addSubview:self.homeViewController.view];
	[homeView release];
}
-(IBAction)forgotPressed{
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if(settings.emailEnabled && (![settings.emailAddress isEqualToString:@""])){
		UIActionSheet *mailAction = [[UIActionSheet alloc] initWithTitle:@"Do you want a password reminder to be sent to your email address?" delegate:self cancelButtonTitle:@"No" destructiveButtonTitle:@"Yes" otherButtonTitles:nil];
		[mailAction showInView:self.view];
		[mailAction release];
		[settings release];
	}else{ 
		Vault *vault = [[Vault alloc] init];
//		NSArray *array = [vault getData];
		Secure *secure = [[Secure alloc] init];
		if([[secure getVaultQuestion] isEqualToString:@""]){
//			LockerViewController *lockerViewController = [[LockerViewController alloc] initWithNibName:@"LockerViewController" bundle:nil];
//			[self.view addSubview:lockerViewController.view];
			[eDiaryAppDelegate showMessage:@"Vault Question and answer not configured" title:@"Sorry"];
		}else{
			
//			NSDictionary *dictionary = [array objectAtIndex:0];
//			answer = [dictionary valueForKey:@"key_value"];
			
			answer = [secure getVaultAnswer];
			forgotView.hidden = NO;
			forgotText.clearButtonMode = UITextFieldViewModeWhileEditing;
			forgotText.keyboardType = UIKeyboardTypeAlphabet;
			forgotText.keyboardAppearance = UIKeyboardAppearanceAlert;
			forgotText.autocapitalizationType = UITextAutocapitalizationTypeNone;
			titleLbl.text = @"Forgot Password";
			messageLbl.text = [secure getVaultQuestion];
			[forgotText becomeFirstResponder];
			
		}
		[vault release];
		[secure release];
	}	
}
- (void)dealloc {
    [super dealloc];
}

-(IBAction)openPressed{
	forgotView.hidden = YES;
	[forgotText resignFirstResponder];
	if([forgotText.text isEqualToString:answer]){
		[forgotText resignFirstResponder];
		LockerViewController *lockerView = [[LockerViewController alloc] initWithNibName:@"LockerViewController" bundle:nil];
		self.lockerViewController = lockerView;
		[self.view addSubview:self.lockerViewController.view];
		[lockerView release];
	}else {
		[eDiaryAppDelegate showMessage:@"Please try again" title:@"Wrong answer"];
	}
	
}
-(IBAction)cancelPressed{
	forgotView.hidden = YES;
	[forgotText resignFirstResponder];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
	return 6;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
	//	if(component ==kEmoticon){
	return [vaultArray count];
	//	}
	
	/*else if(component == kDescription){
	 return [descriptionArray count];
	 }else return 0;*/
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row
		   forComponent:(NSInteger)component{
	//	if(component == kDescription){
	return [vaultArray objectAtIndex:row];
	//	}else return @"";	
}


- (void)messageSent:(SKPSMTPMessage *)message
{
    //[message release];
    
    //NSLog(@"delegate - message sent");
	[eDiaryAppDelegate showMessage:@"Password sent to email address" title:@"Password sent"];
}

- (void)messageFailed:(SKPSMTPMessage *)message error:(NSError *)error
{
    //[message release];
    
    //NSLog(@"delegate - error(%d): %@", [error code], [error localizedDescription]);
	[eDiaryAppDelegate showMessage:@"Password could not be sent to email address" title:@"Error"];
	
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
	//NSLog(@"index %d",buttonIndex);
	if(buttonIndex == 0){
		////NSLog(@"delete");
		Settings *settings = [[Settings alloc] init];
		[settings loadSettings];
		SKPSMTPMessage *testMsg = [[SKPSMTPMessage alloc] init];
		testMsg.fromEmail = settings.emailAddress;
		testMsg.toEmail = settings.emailAddress;
		testMsg.relayHost = settings.emailServerAddress;
		testMsg.requiresAuth = YES;
		testMsg.login = settings.emailAddress;
		testMsg.pass = settings.emailPassword;
		testMsg.subject = @"Journal Diary vault password reminder";
		// testMsg.bccEmail = @"lemuelsinghr@gmail.com";
		testMsg.wantsSecure = YES; // smtp.gmail.com doesn't work without TLS!
		
		// Only do this for self-signed certs!
		// testMsg.validateSSLChain = NO;
		testMsg.delegate = self;
		[settings release];
		Secure *secure = [[Secure alloc] init];
		NSString *message = [NSString stringWithFormat:@"This is a password reminder from Journal Diary iphone application.Please keep the passcode in a safe place.The passcode is used to open the Vault \n PASSCODE: %@",[secure getVaultPassword]];
		NSDictionary *plainPart = [NSDictionary dictionaryWithObjectsAndKeys:@"text/plain",kSKPSMTPPartContentTypeKey,
								   message,kSKPSMTPPartMessageKey,@"8bit",kSKPSMTPPartContentTransferEncodingKey,nil];
		
		[secure release];
		//NSString *vcfPath = [[NSBundle mainBundle] pathForResource:@"test" ofType:@"vcf"];
		//NSData *vcfData = [NSData dataWithContentsOfFile:vcfPath];
		/*
		 NSDictionary *vcfPart = [NSDictionary dictionaryWithObjectsAndKeys:@"text/directory;\r\n\tx-unix-mode=0644;\r\n\tname=\"test.vcf\"",kSKPSMTPPartContentTypeKey,
		 @"attachment;\r\n\tfilename=\"test.vcf\"",kSKPSMTPPartContentDispositionKey,[vcfData encodeBase64ForData],kSKPSMTPPartMessageKey,@"base64",kSKPSMTPPartContentTransferEncodingKey,nil];
		 */
		//testMsg.parts = [NSArray arrayWithObjects:plainPart,vcfPart,nil];
		testMsg.parts = [NSArray arrayWithObjects:plainPart,nil];
		[testMsg send];
		
	}else{
		Vault *vault = [[Vault alloc] init];
//		NSArray *array = [vault getData];
		Secure *secure = [[Secure alloc] init];
		if([[secure getVaultQuestion] isEqualToString:@""]){
//			LockerViewController *lockerViewController = [[LockerViewController alloc] initWithNibName:@"LockerViewController" bundle:nil];
//			[self.view addSubview:lockerViewController.view];
			
			[eDiaryAppDelegate showMessage:@"Vault Question and answer not configured" title:@"Sorry"];
		}else{
			answer = [secure getVaultAnswer];
			answer = [secure getVaultAnswer];
			forgotView.hidden = NO;
			forgotText.clearButtonMode = UITextFieldViewModeWhileEditing;
			forgotText.keyboardType = UIKeyboardTypeAlphabet;
			forgotText.keyboardAppearance = UIKeyboardAppearanceAlert;
			forgotText.autocapitalizationType = UITextAutocapitalizationTypeNone;
			titleLbl.text = @"Forgot Password";
			messageLbl.text = [secure getVaultQuestion];
			[forgotText becomeFirstResponder];
			}
		[vault release];
		   [secure release];
	}
	[actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
}	
@end
