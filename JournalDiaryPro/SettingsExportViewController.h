///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   SettingsExportViewController.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>
#import "SKPSMTPMessage.h"
@class SKPSMTPMessage;
@class SettingsViewController;
@class CalendarViewController;
@interface SettingsExportViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,SKPSMTPMessageDelegate>{
	IBOutlet UILabel *fromLbl;
	IBOutlet UILabel *toLbl;
	IBOutlet UITableView *exportTable;
	IBOutlet UIImageView *imageView;
	NSDate *startDate;
	NSDate *endDate;
	UITableViewCell *diaryCell;
	UITableViewCell *eventCell;
	UITableViewCell *vaultCell;
	SettingsViewController *settingsViewController;
	CalendarViewController *calendarViewController;
	
}
@property(nonatomic,retain)NSDate *startDate;
@property(nonatomic,retain)NSDate *endDate;
@property(nonatomic,retain)SettingsViewController *settingsViewController;
@property(nonatomic,retain)CalendarViewController *calendarViewController;
-(IBAction)backPressed;
-(IBAction)exportPressed;
-(IBAction)startDatePressed;
-(IBAction)endDatePressed;
@end
