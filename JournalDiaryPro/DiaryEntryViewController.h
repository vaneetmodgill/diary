///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   DiaryEntryViewController.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class CalendarViewController;
@class AddEntryViewController;
@class HomeViewController;
@class ShowEntryViewController;
@interface DiaryEntryViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate> {
	IBOutlet UILabel *dateLabel;
	IBOutlet UITableView *diaryTable;
	IBOutlet UISearchBar *entrySearchBar;
	IBOutlet UIImageView *imageView;
	NSDate *diaryDate;
	NSMutableArray *entriesArray;
	NSArray *emoticonArray;
	UIImage *image;
	NSString *searchText;
	BOOL searchDone;
	CalendarViewController *calendarViewController;
	AddEntryViewController *addEntryViewController;
	HomeViewController *homeViewController;
	ShowEntryViewController *showEntryViewController;
	UIImage *imageback;
    
    IBOutlet UIImageView *backgroundImageView ;

}
@property(nonatomic,retain)UIImage *imageback;
@property(nonatomic,retain)NSDate *diaryDate;
@property(nonatomic,retain)UIImage *image;
@property(nonatomic,retain)NSString *searchText;
@property(nonatomic,retain)CalendarViewController *calendarViewController;
@property(nonatomic,retain)AddEntryViewController *addEntryViewController;
@property(nonatomic,retain)HomeViewController *homeViewController;
@property(nonatomic,retain)ShowEntryViewController *showEntryViewController;
@property(assign)BOOL searchDone;
- (UITableViewCell *) getCellContentView:(NSString *)cellIdentifier;
-(IBAction)calendarPressed;
-(IBAction)addPressed;
-(IBAction)backPressed;
-(IBAction)leftPressed;
-(IBAction)rightPressed;
@end
