///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   SaveMemoViewController.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class VoiceMemoViewController;
@class ChooseDateViewController;
@class AddMemoViewController;
@interface SaveMemoViewController : UIViewController {
	IBOutlet UITextField *titleField;
	IBOutlet UILabel *dateLabel;
	IBOutlet UIImageView *imageView;
	NSDate *memoDate;
	NSString *fileName;
	NSString *oldFileName;
	NSString *tempFilePath;
	NSString *titleStr;
	BOOL editMode;
	VoiceMemoViewController *voiceMemoViewController;
	ChooseDateViewController *dateViewController;
	AddMemoViewController *addMemoViewController;
    
    IBOutlet UIImageView *backgroundImageView;
}
@property(nonatomic,retain)NSString *fileName; 
@property(nonatomic,retain)NSString *oldFileName;
@property(nonatomic,retain)NSDate *memoDate;
@property(nonatomic,retain)NSString *tempFilePath;
@property(nonatomic,retain)NSString *titleStr;
@property(assign)BOOL editMode;
@property(nonatomic,retain)VoiceMemoViewController *voiceMemoViewController;
@property(nonatomic,retain)ChooseDateViewController *dateViewController;
@property(nonatomic,retain)AddMemoViewController *addMemoViewController;
-(IBAction)savePressed;
-(IBAction)calendarPressed;
-(IBAction)backPressed;
@end
