//
//  Shader.fsh
//  CubeExample
//
//  Created by Brad Larson on 4/20/2010.
//

varying lowp vec4 colorVarying;

void main()
{
    gl_FragColor = colorVarying;
}
