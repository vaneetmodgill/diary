//
//  Utility.m
//  Viva
//
//  Created by administrator on 12/3/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "Utility.h"
#include <sys/sysctl.h>
#import "ReverseGeoCoder.h"


@implementation Utility
double latitude;
double longitude;

+(void)SetLatitudeLongitude:(double)latUser :(double)longUser
{
	latitude=latUser;
	longitude=longUser;
	
}
+(double)GetLatitude
{
	return latitude;
}
+(double)GetLongitude
{
	return longitude;
}


+(BOOL)IsiPhone3G
{
	BOOL returnType=NO;
	size_t size;
	sysctlbyname("hw.machine", nil,&size, nil, 0);
	char *machine = malloc(size);
	sysctlbyname("hw.machine", machine,&size, nil, 0);
	NSString *platform = [NSString stringWithCString:machine encoding: NSUTF8StringEncoding];
	free(machine);
	
	if([platform  compare:@"iPhone1,2"] == NSOrderedSame)
	{
		returnType = YES;
	}
	if([platform  compare:@"iPhone2,1"] == NSOrderedSame)
	{
		returnType = YES;
	}
	if([platform  compare:@"iPod4,1"] == NSOrderedSame)
	{
		returnType = YES;
	}
	if([platform  compare:@"iPod1,4"] == NSOrderedSame)
	{
		returnType = YES;
	}
	if([platform  compare:@"iPhone1,3"] == NSOrderedSame)
	{
		returnType = YES;
	}
	if([platform  compare:@"iPod3,1"] == NSOrderedSame)
	{
		returnType = YES;
	}
	
	//NSLog(platform);
	return returnType;
}

+(BOOL)CheckInternetConnection:(char*)host_name
{
	BOOL _isDataSourceAvailable = NO;
    Boolean success;    
    //Creates a reachability reference to the specified 
    //network host or node name.
    SCNetworkReachabilityRef reachability = 
	SCNetworkReachabilityCreateWithName(NULL, host_name);
	
    //Determines if the specified network target is reachable 
    //using the current network configuration.
    SCNetworkReachabilityFlags flags;
	
    success = SCNetworkReachabilityGetFlags(reachability, &flags);
    _isDataSourceAvailable = success &&
	(flags & kSCNetworkFlagsReachable) &&
	!(flags & kSCNetworkFlagsConnectionRequired);
    CFRelease(reachability);
	
    return _isDataSourceAvailable;
}


+(NSString*)GetLatLong:(NSString*)zip
{
	//NSURL *url =[ [NSURL alloc] initWithString:[NSString stringWithFormat:@"http://maps.google.com/maps/geo?q=%@&output=xml&oe=utf8&sensor=true_or_false&key=ABQIAAAAtPRVrSzcgi-1QDHfjA7RtxT-FyPSuXEFB16F17K6V-LEY6TDvBQiyvuBq5Brm0F1pHEmz0VakwsC3A",zip]];
	NSURL *url =[ [NSURL alloc] initWithString:[NSString stringWithFormat:@"http://maps.google.com/maps/geo?output=xml&oe=utf8&sensor=true_or_false&q=%@&key=ABQIAAAAtPRVrSzcgi-1QDHfjA7RtxT-FyPSuXEFB16F17K6V-LEY6TDvBQiyvuBq5Brm0F1pHEmz0VakwsC3A",zip]];

	
	NSMutableData *myMutableData;
	myMutableData=[[NSMutableData data] retain]; 
	myMutableData = [[NSMutableData alloc]initWithContentsOfURL:url];
	NSError *parseError;
	
	//NSString *theXml = [[NSString alloc]initWithBytes:[myMutableData mutableBytes] length:[myMutableData length] encoding:NSUTF8StringEncoding];
	ReverseGeoCoder *geoCoder = [[ReverseGeoCoder alloc]init];
	NSString *returnValue = [geoCoder parseXMLFileFromData:myMutableData parseError:&parseError];
	
	return returnValue; 
	
}
@end
