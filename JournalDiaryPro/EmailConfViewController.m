///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   EmailConfViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "EmailConfViewController.h"
#import "SettingsEmailViewController.h"
#import "SettingsViewController.h"
#import "Settings.h"
#import "eDiaryAppDelegate.h"
#import "Validator.h"
@implementation EmailConfViewController
@synthesize settingsViewController;
@synthesize settingsEmailViewController;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if(![settings.emailType isEqualToString:@"Others"]){
		serverLbl.hidden = YES;
		serverField.hidden = YES;

	}
	[addressField becomeFirstResponder];
	addressField.text = settings.emailAddress;
	serverField.text = settings.emailServerAddress;
	passwordField.text = settings.emailPassword;

	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}
-(IBAction)SavePressed{
	if([addressField.text isEqualToString:@""]){
		[eDiaryAppDelegate showMessage:@"Email address is empty" title:@"Empty Field"];
	}else if([passwordField.text isEqualToString:@""]){
		[eDiaryAppDelegate showMessage:@"Password field is empty" title:@"Empty Field"];
	}/*else if([serverField.text isEqualToString:@""]){
		[eDiaryAppDelegate showMessage:@"SMTP Server field is empty" title:@"Empty Field"];
	}*/else if(![Validator isValidEmailId:addressField.text]){
		[eDiaryAppDelegate showMessage:@"Please enter a valid email address" title:@"Invalid Email Address"];
	} else{
	[addressField resignFirstResponder];
	[passwordField resignFirstResponder];
	[serverField resignFirstResponder];
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
		if([settings.emailType isEqualToString:@"Gmail"]){
			[settings storeEmailSettings:addressField.text SMTPServer:@"smtp.gmail.com" password:passwordField.text];
		}else if([settings.emailType isEqualToString:@"Yahoo"]){
			[settings storeEmailSettings:addressField.text SMTPServer:@"smtp.mail.yahoo.com" password:passwordField.text];
		}else if([settings.emailType isEqualToString:@"Hotmail"]){
			[settings storeEmailSettings:addressField.text SMTPServer:@"smtp.live.com" password:passwordField.text];
		}else{
			if([serverField.text isEqualToString:@""]){
				[eDiaryAppDelegate showMessage:@"SMTP Server field is empty" title:@"Empty Field"];
				return;
			}
			[settings storeEmailSettings:addressField.text SMTPServer:serverField.text password:passwordField.text];
		}
	[settings release];
		SettingsViewController *settingsView = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController"bundle:nil];
		self.settingsViewController = settingsView;
		[self.view addSubview:self.settingsViewController.view];
		[settingsView release];
	}
}
-(IBAction)backPressed{
	[addressField resignFirstResponder];
	[passwordField resignFirstResponder];
	[serverField resignFirstResponder];
	SettingsEmailViewController *settingsEmailView = [[SettingsEmailViewController alloc] initWithNibName:@"SettingsEmailViewController" bundle:nil];
	self.settingsEmailViewController = settingsEmailView;
	[self.view addSubview:self.settingsEmailViewController.view];
	[settingsEmailView release];
}
- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
