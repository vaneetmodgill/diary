///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   ShowSecretViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "ShowSecretViewController.h"
#import "Vault.h"
#import "LockerViewController.h"
#import "AddSecretViewController.h"
#import "Settings.h"
#import "eDiaryAppDelegate.h"
@implementation ShowSecretViewController
@synthesize vaultObj;
@synthesize addSecretViewController;
@synthesize lockerViewController;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}

	keyView.text = vaultObj.keyStr;
	valueView.text = vaultObj.valueStr;
	

	[settings loadSettings];
	if([settings.fontName isEqualToString:@""] || settings.fontName == nil){
		[keyView setFont:[UIFont systemFontOfSize:15]];
		[valueView setFont:[UIFont systemFontOfSize:15]];
		[settings storeFontSettings:@"" size:15.0];
	}else{
		//NSLog(@"font name %@",settings.fontName);	
		NSArray *fontNames = [UIFont fontNamesForFamilyName:settings.fontName];
		[keyView setFont:[UIFont fontWithName:[fontNames objectAtIndex:0] size:settings.fontSize]];
		[valueView setFont:[UIFont fontWithName:[fontNames objectAtIndex:0] size:settings.fontSize]];
		//	[lblTemp2 setFont:[UIFont systemFontOfSize:15]];
	}
	[settings release];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

-(IBAction)editPressed{
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
	AddSecretViewController *addSecretView = [[AddSecretViewController alloc] initWithNibName:@"AddSecretViewController" bundle:nil];
	addSecretView.isEditMode = YES;
	addSecretView.vaultObj = self.vaultObj;
	self.addSecretViewController = addSecretView;
	[self.view addSubview:self.addSecretViewController.view];
	[addSecretView release];
	[UIView commitAnimations];
}
-(IBAction)deletePressed{
	UIActionSheet *deleteAction = [[UIActionSheet alloc] initWithTitle:@"Are you sure you want to delete?" delegate:self cancelButtonTitle:@"No" destructiveButtonTitle:@"Yes" otherButtonTitles:nil];
	[deleteAction showInView:self.view];
	[deleteAction release];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
	//NSLog(@"index %d",buttonIndex);
	if(buttonIndex == 0){
		////NSLog(@"delete");
		Vault *vault = [[Vault alloc] init];
		[vault deleteSecret:vaultObj.keyStr];
		[vault release];
		LockerViewController *lockerView = [[LockerViewController alloc] initWithNibName:@"LockerViewController" bundle:nil];
		self.lockerViewController = lockerView;
		[self.view addSubview:self.lockerViewController.view];
		[lockerView release];
		
	}
	[actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
}
-(IBAction)backPressed{
	LockerViewController *lockerView = [[LockerViewController alloc] initWithNibName:@"LockerViewController" bundle:nil];
	self.lockerViewController = lockerView;
	[self.view addSubview:self.lockerViewController.view];
	[lockerView release];
	
}
- (void)dealloc {
    [super dealloc];
}


@end
