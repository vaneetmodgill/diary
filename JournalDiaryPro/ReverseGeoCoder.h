//
//  ReverseGeoCoder.h
//  PMR
//
//  Created by Administrator on 4/15/10.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


@interface ReverseGeoCoder : NSObject {
	NSString *currentElementName;
	
	NSMutableString *Address;
}
@property(nonatomic,retain)NSString *currentElementName;

+(NSString*)GetAddress:(CLLocationCoordinate2D)loc;

- (void)parserDidStartDocument:(NSXMLParser *)parser;
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict;
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName;
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string;
- (NSString*)parseXMLFileFromData:(NSMutableData *)mutData parseError:(NSError **)error;

@end
