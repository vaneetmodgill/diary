///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   ShowEntryViewController.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>
#import "ChoosePictureViewController.h"
#import "DiaryObject.h"
//#import "facebook_class.h"
#import "ConnectFacebookViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManagerDelegate.h>
#import "TwitpicEngine.h"
#import <MapKit/MKReverseGeocoder.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKAnnotation.h>
#import"MessageUI/MessageUI.h"

@class DiaryObject;
@class AddEntryViewController;
@class DiaryEntryViewController;
@class ChoosePictureViewController;
@class ConnectFacebookViewController;
@class Email;
@interface ShowEntryViewController : UIViewController<UITextFieldDelegate, UIAlertViewDelegate,TwitpicEngineDelegate,MFMailComposeViewControllerDelegate, MKMapViewDelegate, UITextViewDelegate, CLLocationManagerDelegate,MKReverseGeocoderDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate> {
	IBOutlet UILabel *timeLbl;
	IBOutlet UITextView *contentView;
	IBOutlet UIButton *emoticonButton;
	IBOutlet UIImageView *imageView;
	IBOutlet UIImageView *imageViewBackground;
	DiaryObject *entryObj;
	NSString *searchStr;
	BOOL searchDone;
	AddEntryViewController *editEntryViewController;
	DiaryEntryViewController *diaryEntryViewController;
	UIImagePickerController *imagePicker;
	UIActionSheet *actionsheet_photo;
	ChoosePictureViewController *choosePictureViewController;
	BOOL imageforeverydiary;
	NSMutableArray *image_array;
	MKReverseGeocoder *geoCoder;
	MKPlacemark *mPlacemark;
	
	CLLocationManager	*locationMgr;
	NSTimer *Locationtimer;
	double Latitude,Longitude;
	NSString *completeAddress;
	IBOutlet UITextView *textview2;
	NSMutableArray *currentlocation_array;
	BOOL isclupdate;
	BOOL isupdateclicked;
	MKMapView *mapView;
	NSData *image_data;
	NSString *tempPath;
	NSString *image_name_save;
	NSMutableArray *TwitterDetails;
	UIActivityIndicatorView *ActivityIndicator;
	UIAlertView *alertMain;
	UITextField *textField;
	UITextField *textField2;
	BOOL photouploaded;
	BOOL alertemail;
	BOOL isPost;
	NSMutableURLRequest *theRequest;
	NSString *requestBody;
	UIActionSheet *loadingactionsheet;
    
    
    IBOutlet UIImageView *backgrndImaView,*backgroundImageView;
}
@property(nonatomic,retain)	NSString *image_name_save;
@property(nonatomic,retain)	NSString *tempPath;
@property(nonatomic,retain)	NSData *image_data;
@property(nonatomic,retain)	IBOutlet UITextView *textview2;
@property BOOL imageforeverydiary;
@property(nonatomic,retain)NSString *completeAddress;
@property(nonatomic,retain)ChoosePictureViewController *choosePictureViewController;
@property(nonatomic,retain)IBOutlet UIImageView *imageViewBackground;
@property(nonatomic,retain)UIImagePickerController *imagePicker;
@property(nonatomic,retain)DiaryObject *entryObj;
@property(nonatomic,retain)NSString *searchStr;
@property(assign)BOOL searchDone;
@property(nonatomic,retain)AddEntryViewController *editEntryViewController;
@property(nonatomic,retain)DiaryEntryViewController *diaryEntryViewController;
@property double Latitude;
@property double Longitude;
- (void)setupTimer;
-(IBAction)editPressed;
-(IBAction)deletePressed;
-(IBAction)backPressed;

-(IBAction)facebookPressed;
-(IBAction)twitterPressed;
-(IBAction)emailPressed;
-(IBAction)photoPressed;
-(void)UploadAllImages;
-(void)status_updatecallback;
@end
