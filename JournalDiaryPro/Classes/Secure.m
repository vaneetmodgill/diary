///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   Secure.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "Secure.h"
#import "Sqlite.h"
#import "DBSqlite.h"

@implementation Secure
-(void)insert:(NSString*)pass lockerPass:(NSString *)lockerPasswd question:(NSString *)ques answer:(NSString *)ans vaultQ:(NSString*)vaultQues vaultA:(NSString *)vaultAns{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return;
	sqlite = dbSqlite.sqlite;	
	
	[sqlite executeNonQuery:@"INSERT INTO secure(app_password,locker_password,secret_question,secret_answer,app_secure,vault_secure,vault_question,vault_answer) VALUES (?, ?, ?, ?,'0','0',?,?);",pass,lockerPasswd,ques,ans,vaultQues,vaultAns];
	
	[dbSqlite release];	
}
-(void)updateAppProtection:(BOOL)enabled{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return;
	sqlite = dbSqlite.sqlite;
	if(enabled){
		[sqlite executeNonQuery:@"update secure set app_secure = 1;"];
	}else {
		[sqlite executeNonQuery:@"update secure set app_secure = 0;"];
	}
	
	[dbSqlite release];
}
-(void)updateVaultProtection:(BOOL)enabled{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return;
	sqlite = dbSqlite.sqlite;
	if(enabled){
		[sqlite executeNonQuery:@"update secure set vault_secure = 1;"];
	}else {
		[sqlite executeNonQuery:@"update secure set vault_secure = 0;"];
	}
	
	[dbSqlite release];
}
-(void)updatePassword:(NSString *)password{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return;
	sqlite = dbSqlite.sqlite;
	[sqlite executeNonQuery:@"update secure set app_password = ?;",password];
	[dbSqlite release];
}
-(void)updateVaultPassword:(NSString *)password{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return;
	sqlite = dbSqlite.sqlite;
	[sqlite executeNonQuery:@"update secure set locker_password = ?;",password];
	[dbSqlite release];
}
-(void)updateQuestion:(NSString *)qstn answer:(NSString *)ansr{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return;
	sqlite = dbSqlite.sqlite;
	[sqlite executeNonQuery:@"update secure set secret_question = ?,secret_answer = ?;",qstn,ansr];
	[dbSqlite release];
}
-(int)getCount{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	NSArray *results;
	if (![dbSqlite getDBConnection])
		return 0;
	sqlite = dbSqlite.sqlite;	
	results = [sqlite executeQuery:@"Select * from secure;"];
	[dbSqlite release];
	return [results count];
}
-(NSString*)getPassword{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	NSArray *results;
	NSString *value = @"";
	if (![dbSqlite getDBConnection])
		return value;
	sqlite = dbSqlite.sqlite;	
	results = [sqlite executeQuery:@"Select app_password from secure;"];
	[dbSqlite release];
	if([results count]==0){
		return value;
	}
	for (NSDictionary *dictionary in results) {
		value = [dictionary valueForKey:@"app_password"];
	}
	return value;
}
-(NSString*)getVaultPassword{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	NSArray *results;
	NSString *value = @"";
	if (![dbSqlite getDBConnection])
		return value;
	sqlite = dbSqlite.sqlite;	
	results = [sqlite executeQuery:@"Select locker_password from secure;"];
	[dbSqlite release];
	if([results count]==0){
		return value;
	}
	for (NSDictionary *dictionary in results) {
		value = [dictionary valueForKey:@"locker_password"];
	}
	return value;
}
-(NSString *)getQuestion{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	NSArray *results;
	NSString *value = @"";
	if (![dbSqlite getDBConnection])
		return value;
	sqlite = dbSqlite.sqlite;	
	results = [sqlite executeQuery:@"Select secret_question from secure;"];
	[dbSqlite release];
	if([results count]==0){
		return value;
	}
	for (NSDictionary *dictionary in results) {
		value = [dictionary valueForKey:@"secret_question"];
	}
	return value;
	
}
-(NSString *)getAnswer{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	NSArray *results;
	NSString *value = @"";
	if (![dbSqlite getDBConnection])
		return value;
	sqlite = dbSqlite.sqlite;	
	results = [sqlite executeQuery:@"Select secret_answer from secure;"];
	[dbSqlite release];
	if([results count]==0){
		return value;
	}
	for (NSDictionary *dictionary in results) {
		value = [dictionary valueForKey:@"secret_answer"];
	}
	return value;
	
}

-(BOOL)isAppSecure{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init] ;
	Sqlite *sqlite;
	NSArray *results = [[[NSArray alloc] init] autorelease];
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;	
	results = [sqlite executeQuery:@"Select app_secure from secure;"];
	[dbSqlite release];
	if([results count]==0){
		return NO;
	}
	for (NSDictionary *dictionary in results) {
		if([(NSNumber*)[dictionary valueForKey:@"app_secure"] intValue] == 1){
			return YES;
		}else{
			return NO;
		}
	}
	return NO;
}
-(BOOL)isVaultSecure{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	NSArray *results;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;	
	results = [sqlite executeQuery:@"Select vault_secure from secure;"];
	[dbSqlite release];
	if([results count]==0){
		return NO;
	}
	for (NSDictionary *dictionary in results) {
		if([(NSNumber *)[dictionary valueForKey:@"vault_secure"] intValue]  == 1){
			return YES;
		}else{
			return NO;
		}
	}
	return NO;
}

-(void)updateVaultQuestion:(NSString *)qstn answer:(NSString *)ansr{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return;
	sqlite = dbSqlite.sqlite;
	[sqlite executeNonQuery:@"update secure set vault_question = ?,vault_answer = ?;",qstn,ansr];
	[dbSqlite release];
}

-(NSString *)getVaultQuestion{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	NSArray *results;
	NSString *value = @"";
	if (![dbSqlite getDBConnection])
		return value;
	sqlite = dbSqlite.sqlite;	
	results = [sqlite executeQuery:@"Select vault_question from secure;"];
	[dbSqlite release];
	if([results count]==0){
		return value;
	}
	for (NSDictionary *dictionary in results) {
		value = [dictionary valueForKey:@"vault_question"];
	}
	return value;
	
}
-(NSString *)getVaultAnswer{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	NSArray *results;
	NSString *value = @"";
	if (![dbSqlite getDBConnection])
		return value;
	sqlite = dbSqlite.sqlite;	
	results = [sqlite executeQuery:@"Select vault_answer from secure;"];
	[dbSqlite release];
	if([results count]==0){
		return value;
	}
	for (NSDictionary *dictionary in results) {
		value = [dictionary valueForKey:@"vault_answer"];
	}
	return value;
	
}
@end
