//
//  VideoMemoViewController.h
//  Journal Diary
//
//  Created by Ashish Verma on 14/09/10.
//  Copyright 2010 Sulaba Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
@class HomeViewController;
@class CalendarViewController;
@class SaveMemoViewController;
@class AddVideoMemo;
@class Settings;
@class ShowVideoController;
@interface VideoMemoViewController :  UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate,MPMediaPickerControllerDelegate>
{
	IBOutlet UIImageView *imageView;
	IBOutlet UILabel *dateLabel;
	HomeViewController *homeViewController;
	CalendarViewController *calendarViewController;
	SaveMemoViewController *saveMemoViewController;
	AddVideoMemo *addVideoMemo;
	IBOutlet UIWebView *webView1;
	MPMoviePlayerController *theMovie;
	NSString *filename;
	UITextField *textfield;
		NSDate *videoMemoDate;
	NSMutableArray *memosArray;
	IBOutlet UITableView *memoTable;
	ShowVideoController *showVideoController;
	BOOL iseditmode;
	NSString *date2;
	NSString *time2;
	BOOL istableviewclicked;
    
    IBOutlet UIImageView *backgroundImageView;
}
@property 	BOOL istableviewclicked;
@property(nonatomic,retain)	NSString *date2;
@property(nonatomic,retain) NSString *time2;
@property BOOL iseditmode;
@property(nonatomic,retain)ShowVideoController *showVideoController;@property(nonatomic,retain)	IBOutlet UIImageView *imageView;
@property(nonatomic,retain)	IBOutlet UITableView *memoTable;

@property(nonatomic,retain)IBOutlet UILabel *dateLabel;
@property(nonatomic,retain)NSDate *videoMemoDate;
@property(nonatomic,retain)AddVideoMemo *addVideoMemo;
@property(nonatomic,retain)CalendarViewController *calendarViewController;
@property(nonatomic,retain)HomeViewController *homeViewController;
@property(nonatomic,retain)SaveMemoViewController *saveMemoViewController;

-(IBAction)calendarPressed;
-(IBAction)addPressed;
-(IBAction)backPressed;
-(IBAction)leftPressed;
-(IBAction)rightPressed;

- (UITableViewCell *) getCellContentView:(NSString *)cellIdentifier;

@end
