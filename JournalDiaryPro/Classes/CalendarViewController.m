///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   CalendarViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "CalendarViewController.h"
#import "DiaryEntryViewController.h"
#import "TKCalendarView.h"
#import "VoiceMemoViewController.h"
#import "EventsViewController.h"
#import "SettingsExportViewController.h"
#import "Settings.h"
#import "eDiaryAppDelegate.h"
#import "DayPicture.h"
@implementation CalendarViewController
@synthesize calendarView;
@synthesize selectedDate;
@synthesize anotherDate;
@synthesize preView;
@synthesize voiceMemoViewController;
@synthesize eventsViewController;
@synthesize diaryEntryViewController;
@synthesize fromViewCtlr;
@synthesize toViewCtlr;
@synthesize videoMemoViewController;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        backgrndImaView.frame =CGRectMake(0, -20, 320, 568+40);
        imageView.frame = CGRectMake(0, -20, 320, 568+20);
        
        
        
    }
    
    else
    {
        imageView.frame =CGRectMake(12, 0, 320, 480);
        
        
    }

    
	TKCalendarView *tkcalendar = [[TKCalendarView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, 320, 200) 
												delegate:self];
	self.calendarView = tkcalendar;
	[self.view addSubview:self.calendarView];
	[tkcalendar release];
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
	NSDateComponents *comps = [gregorian components:unitFlags fromDate:[NSDate date]];
	[comps setHour:0];
	[comps setMinute:0];
	[comps setSecond:0];
	self.selectedDate = [gregorian dateFromComponents:comps];
	[gregorian release];
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (NSArray*) calendarView:(TKCalendarView*)calendar itemsForDaysInMonth:(NSDate*)monthDate{
	
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *comps = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSWeekdayCalendarUnit) fromDate:monthDate];
	
	[comps setMonth:comps.month+1];
	[comps setDay:0];
	
	NSDate *lastDayInMonth = [gregorian dateFromComponents:comps];
	int days = [[gregorian components:NSDayCalendarUnit fromDate:lastDayInMonth] day];
	[gregorian release];
	NSMutableArray *ar = [[NSMutableArray alloc] initWithCapacity:days];
	
	for(int i = 0; i < days; i++){
		if(i %2==0){
			[ar addObject:[NSNumber numberWithBool:YES]];
		}else{
			[ar addObject:[NSNumber numberWithBool:NO]];
		}
	}
	
	return [[ar copy] autorelease];
}
- (void) calendarView:(TKCalendarView*)calendar dateWasSelected:(NSInteger)integer ofMonth:(NSDate*)monthDate{
	
	
	
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
	NSDateComponents *comps = [gregorian components:unitFlags fromDate:monthDate];
	[comps setDay:integer];
	self.selectedDate = [gregorian dateFromComponents:comps];
	[gregorian release];
	//NSLog(@"Selected: %d %@",integer,self.selectedDate);
}



- (void) calendarView:(TKCalendarView*)calendar willShowMonth:(NSDate*)monthDate{
	
}

-(IBAction)donePressed{
	////NSLog(@"selected %@",self.selectedDate);
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	Settings *settings = [[Settings alloc] init];
	[formatter setDateFormat:@"yyyy-MM-dd"];
	 [settings storeGlobalDate:[formatter stringFromDate:self.selectedDate]];
	[settings loadSettings];
	if(settings.picOfDayEnabled && settings.picEnabled){
		DayPicture *dayPicture = [[DayPicture alloc] init];
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *imgName =[dayPicture getPictureNameForDate:settings.globalDate];
		//NSLog(@"name %@",imgName);
		if(![imgName isEqualToString:@""]){ 
			UIImage *img= [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",documentsDirectory,imgName]];
			[eDiaryAppDelegate setHomeImage:img];
			imageView.image = img;
		}else{
			[eDiaryAppDelegate setHomeImage:nil];
			imageView.image = nil;
		}
		[dayPicture release];
	}
	[settings release];
	[formatter release];
	if([preView isEqualToString:@"VideoMemo"])
	{
		VideoMemoViewController *videoMemoView = [[VideoMemoViewController alloc] initWithNibName:@"VideoMemoViewController" bundle:nil];
		//videoMemoView.memoDate = self.selectedDate;
		self.videoMemoViewController = videoMemoView;
		[self.view addSubview:self.videoMemoViewController.view];
		[videoMemoView release];
	}
	else if([preView isEqualToString:@"VoiceMemo"]){
		VoiceMemoViewController *voiceMemoView = [[VoiceMemoViewController alloc] initWithNibName:@"VoiceMemoViewController" bundle:nil];
		voiceMemoView.memoDate = self.selectedDate;
		self.voiceMemoViewController = voiceMemoView;
		[self.view addSubview:self.voiceMemoViewController.view];
		[voiceMemoView release];
	}else if([preView isEqualToString:@"Events"]){
		EventsViewController *eventsView = [[EventsViewController alloc] initWithNibName:@"EventsViewController" bundle:nil];
		eventsView.eventDate = self.selectedDate;
		self.eventsViewController = eventsView;
		[self.view addSubview:self.eventsViewController.view];
		[eventsView release];
	}else if([preView isEqualToString:@"ExportFrom"]){
		SettingsExportViewController *fromView = [[SettingsExportViewController alloc] initWithNibName:@"SettingsExportViewController" bundle:nil];
		fromView.startDate = self.selectedDate;
		fromView.endDate = self.anotherDate;
		self.fromViewCtlr = fromView;
		[self.view addSubview:self.fromViewCtlr.view];
		[fromView release];
	}else if([preView isEqualToString:@"ExportTo"]){
		SettingsExportViewController *toView = [[SettingsExportViewController alloc] initWithNibName:@"SettingsExportViewController" bundle:nil];
		toView.startDate = self.anotherDate;
		toView.endDate = self.selectedDate;
		self.toViewCtlr = toView;
		[self.view addSubview:self.toViewCtlr.view];
		[toView release];
	}else{
		DiaryEntryViewController *diaryEntryView = [[DiaryEntryViewController alloc] initWithNibName:@"DiaryEntryViewController" bundle:nil];
		diaryEntryView.diaryDate = self.selectedDate;
		self.diaryEntryViewController = diaryEntryView;
		[self.view addSubview:self.diaryEntryViewController.view];
		[diaryEntryView release];
	}
}
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[calendarView release];
    [super dealloc];
}


@end
