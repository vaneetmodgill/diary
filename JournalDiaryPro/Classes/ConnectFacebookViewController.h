//
//  ConnectFacebookViewController.h
//  AtomicChalk
//
//  Created by administrator on 25/08/09.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TwitpicEngine.h"
#import "FBConnect.h"
#import "Constants.h"
#import "FBLoginDialog.h"
#import "FBLoginButton.h"
#import "PermissionStatus.h"
#import "FBPermissionDialog.h"

@interface ConnectFacebookViewController : UIViewController<PermissionStatusDelegate,FBDialogDelegate, FBSessionDelegate, FBRequestDelegate> {
	
	
	FBSession *facebookSession;
	IBOutlet FBLoginButton	*facebookLoginButton;
	FBLoginButtonStyle _style;
	FBSession* _session;
	FBLoginDialog* dialog;
	UILabel *_label;
	UIButton *timeStampButton,*uploadButton,*logoutButton;
	UIImageView *LogoutImageImg;
	UIImageView *ConnectoFacebookImg;
	FBLoginButton *LoginButton;
	UIButton *HomeClickedButton;
	NSString *image_name;
	NSString *cont;
	
	UIImage *imageupload;
	UIButton *uploadButton1;
	
	PermissionStatus *permissionStatusForUser;
	
	
}
@property(nonatomic,retain)UIImage *imageupload;
@property(nonatomic,retain)	NSString *image_name;
@property(nonatomic,retain)NSString *cont;
@property(nonatomic,retain)UILabel *_label;

@end
