///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   VoiceMemoViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "VoiceMemoViewController.h"
#import "HomeViewController.h"
#import "AddMemoViewController.h"
#import "VoiceMemo.h"
#import "DiaryObject.h"
#import "ShowMemoViewController.h"
#import "CalendarViewController.h"
#import "Settings.h"
#import "eDiaryAppDelegate.h"
#import "DayPicture.h"
@implementation VoiceMemoViewController
@synthesize memoDate;
@synthesize image;
@synthesize homeViewController;
@synthesize addMemoViewController;
@synthesize showMemoViewController;
@synthesize calendarViewController;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        
        
        backgroundImageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        backgroundImageView.frame = CGRectMake(0, -20, 320, 568+40);
        //   self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 568+20);
    }
    else
    {
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 480+20);
    }
    
    

    
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy-MM-dd"];
	//[formatter setTimeStyle:NSDateFormatterShortStyle];
	
	
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit | NSMinuteCalendarUnit ;
	NSDateComponents *compsDate = [gregorian components:unitFlags fromDate:[formatter dateFromString:[NSString stringWithFormat:@"%@",settings.globalDate]]];
	
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	NSDateComponents *compsTime = [gregorian components:unitFlags fromDate:[NSDate date]];
	[compsTime setDay:[compsDate day]];
	[compsTime setMonth:[compsDate month]];
	[compsTime setYear:[compsDate year]];
	self.memoDate = [gregorian dateFromComponents:compsTime];
	[gregorian release];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
	}
	[settings release];
	VoiceMemo *voiceMemo = [[VoiceMemo alloc] init];
	NSMutableArray *mutArray = [[NSMutableArray alloc] init];
// 	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterNoStyle];
	if(self.memoDate !=nil){
		dateLabel.text = [formatter stringFromDate:self.memoDate];
	}else {
		dateLabel.text = [formatter stringFromDate:[NSDate date]];
		self.memoDate = [NSDate date];
	}
	[formatter setDateFormat:@"YYYY-MM-dd"];	
	NSArray *array = [voiceMemo getData:[formatter stringFromDate:self.memoDate]];
	//NSLog(@"array %@",array);
	for (NSDictionary *dictionary in array){
		DiaryObject *diaryObj = [[DiaryObject alloc] init];
		diaryObj.dateStr = [dictionary valueForKey:@"date"];
		diaryObj.timeStr = [dictionary valueForKey:@"time"];
		diaryObj.content = [dictionary valueForKey:@"title"];
		diaryObj.emoticon =[dictionary valueForKey:@"file_name"];
		[mutArray addObject:diaryObj];
		[diaryObj release];
	}
	memosArray = [mutArray mutableCopy];
	[mutArray release];
	[voiceMemo release];
	[memoTable reloadData];
	[formatter release];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}
-(IBAction)leftPressed{
	[memosArray removeAllObjects];
	VoiceMemo *memo = [[VoiceMemo alloc] init];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterNoStyle];
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit |NSMinuteCalendarUnit;
	NSDateComponents *comps = [gregorian components:unitFlags fromDate:self.memoDate];
	NSDateComponents *compsTime = [gregorian components:unitFlags fromDate:[NSDate date]];
	[compsTime setDay:[comps day]-1];
	[compsTime setMonth:[comps month]];
	[compsTime setYear:[comps year]];
	self.memoDate = [gregorian dateFromComponents:compsTime];
	[gregorian release];
	dateLabel.text = [formatter stringFromDate:self.memoDate];
	
	[formatter setDateFormat:@"YYYY-MM-dd"];	
	NSArray *array = [memo getData:[formatter stringFromDate:self.memoDate]];
	[memo release];
	//NSLog(@"array %@",array);
	for (NSDictionary *dictionary in array){
		DiaryObject *diaryObj = [[DiaryObject alloc] init];
		diaryObj.dateStr = [dictionary valueForKey:@"date"];
		diaryObj.timeStr = [dictionary valueForKey:@"time"];
		diaryObj.content = [dictionary valueForKey:@"title"];
		diaryObj.emoticon =[dictionary valueForKey:@"file_name"];
		[memosArray addObject:diaryObj];
		[diaryObj release];
	}
	[memoTable reloadData];
	
	Settings *settings = [[Settings alloc] init];
	
	[settings storeGlobalDate:[formatter stringFromDate:self.memoDate]];
	[settings loadSettings];
	if(settings.picOfDayEnabled && settings.picEnabled){
		DayPicture *dayPicture = [[DayPicture alloc] init];
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *imgName =[dayPicture getPictureNameForDate:settings.globalDate];
		//NSLog(@"name %@",imgName);
		if(![imgName isEqualToString:@""]){ 
			UIImage *img= [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",documentsDirectory,imgName]];
			[eDiaryAppDelegate setHomeImage:img];
			imageView.image = img;
		}else{
			[eDiaryAppDelegate setHomeImage:nil];
			imageView.image = nil;
		}
		[dayPicture release];
	}
	[settings release];
	[formatter release];
}
-(IBAction)rightPressed{
	[memosArray removeAllObjects];
	VoiceMemo *memo = [[VoiceMemo alloc] init];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterNoStyle];
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit | NSMinuteCalendarUnit;
	NSDateComponents *comps = [gregorian components:unitFlags fromDate:self.memoDate];
	NSDateComponents *compsTime = [gregorian components:unitFlags fromDate:[NSDate date]];
	[compsTime setDay:[comps day]+1];
	[compsTime setMonth:[comps month]];
	[compsTime setYear:[comps year]];
	self.memoDate = [gregorian dateFromComponents:compsTime];
	[gregorian release];
	dateLabel.text = [formatter stringFromDate:self.memoDate];
	
	[formatter setDateFormat:@"YYYY-MM-dd"];	
	NSArray *array = [memo getData:[formatter stringFromDate:self.memoDate]];
	[memo release];
	//NSLog(@"array %@",array);
	for (NSDictionary *dictionary in array){
		DiaryObject *diaryObj = [[DiaryObject alloc] init];
		diaryObj.dateStr = [dictionary valueForKey:@"date"];
		diaryObj.timeStr = [dictionary valueForKey:@"time"];
		diaryObj.content = [dictionary valueForKey:@"title"];
		diaryObj.emoticon =[dictionary valueForKey:@"file_name"];
		[memosArray addObject:diaryObj];
		[diaryObj release];
	}
	[memoTable reloadData];
	Settings *settings = [[Settings alloc] init];
	
	[settings storeGlobalDate:[formatter stringFromDate:self.memoDate]];
	[settings loadSettings];
	if(settings.picOfDayEnabled && settings.picEnabled){
		DayPicture *dayPicture = [[DayPicture alloc] init];
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *imgName =[dayPicture getPictureNameForDate:settings.globalDate];
		//NSLog(@"name %@",imgName);
		if(![imgName isEqualToString:@""]){ 
			UIImage *img= [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",documentsDirectory,imgName]];
			[eDiaryAppDelegate setHomeImage:img];
			imageView.image = img;
		}else{
			[eDiaryAppDelegate setHomeImage:nil];
			imageView.image = nil;
		}
		[dayPicture release];
	}
	[settings release];
	[formatter release];
}

-(IBAction)calendarPressed{
	CalendarViewController *calendarView = [[CalendarViewController alloc] initWithNibName:@"CalendarViewController" bundle:nil];
	calendarView.preView = @"VoiceMemo";
	[eDiaryAppDelegate setCalendarName:@"VoiceMemo"];
	self.calendarViewController = calendarView;
	[self.view addSubview:self.calendarViewController.view];
	[calendarView release];
}
-(IBAction)addPressed{
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
	AddMemoViewController *addMemoView = [[AddMemoViewController alloc] initWithNibName:@"AddMemoViewController" bundle:nil];
	addMemoView.memoDate = self.memoDate;
	self.addMemoViewController = addMemoView;
	[self.view addSubview:self.addMemoViewController.view];
	[addMemoView release];
	[UIView commitAnimations];
}
-(IBAction)backPressed{
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	HomeViewController *homeView = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
	self.homeViewController = homeView;
	[self.view addSubview:self.homeViewController.view];
	[homeView release];
	[UIView commitAnimations];
}
- (void)dealloc {
	[memosArray release];
    [super dealloc];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}
// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [memosArray count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	/*    if (cell == nil) {
	 cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
	 }
	 
	 // Set up the cell...
	 if(indexPath.section == 0) {
	 cell.text = [foodItems objectAtIndex:indexPath.row];
	 } else if(indexPath.section == 1) {
	 cell.text = [activityItems objectAtIndex:indexPath.row];
	 }*/
	
	
    if (cell == nil) {
        cell = [self getCellContentView:CellIdentifier];
    }
	
	UILabel *timeLbl =  (UILabel *)[cell viewWithTag:1];
	UILabel *contentLbl =  (UILabel *)[cell viewWithTag:2];
	
	
	DiaryObject *obj = (DiaryObject *)[memosArray objectAtIndex:indexPath.row];
	timeLbl.text = obj.timeStr;
	contentLbl.text = obj.content;
	
	/*	
	 if(indexPath.row%2) {
	 [lblTemp1 setBackgroundColor:[UIColor colorWithRed:0.219 green:0.586 blue:0.590 alpha:1.0]];
	 [lblTemp2 setBackgroundColor:[UIColor colorWithRed:0.219 green:0.586 blue:0.590 alpha:1.0]];
	 } else {
	 [lblTemp1 setBackgroundColor:[UIColor colorWithRed:0.391 green:0.871 blue:0.852 alpha:1.0]];
	 [lblTemp2 setBackgroundColor:[UIColor colorWithRed:0.391 green:0.871 blue:0.852 alpha:1.0]];
	 }
	 */
    return cell;
}
- (UITableViewCell *) getCellContentView:(NSString *)cellIdentifier {
	
	CGRect CellFrame = CGRectMake(0.0f, 0.0f, 320.0f, 50.0f);
	CGRect lblFrame1 = CGRectMake(15.0f, 0.0f, 305.0f, 25.0f);
	CGRect lblFrame2 = CGRectMake(15.0f, 25.0f, 300.0f, 25.0f);
	
	UILabel *lblTemp1;
	UILabel *lblTemp2;
	
	UITableViewCell *cell = [[[UITableViewCell alloc] initWithFrame:CellFrame reuseIdentifier:cellIdentifier] autorelease];
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	
	//Initialize Label 1 with tag 1.
	lblTemp1 = [[UILabel alloc] initWithFrame:lblFrame1];
	[lblTemp1 setBackgroundColor:[UIColor clearColor]];
	//	[lblTemp1 sizeToFit];
	//	[lblTemp1 setMinimumFontSize:8.0];
	if([settings.fontName isEqualToString:@""] || settings.fontName == nil){
		[lblTemp1 setFont:[UIFont systemFontOfSize:15]];
		[settings storeFontSettings:@"" size:15.0];
	}else{
		//NSLog(@"font name %@",settings.fontName);	
		NSArray *fontNames = [UIFont fontNamesForFamilyName:settings.fontName];
		[lblTemp1 setFont:[UIFont fontWithName:[fontNames objectAtIndex:0] size:settings.fontSize]];
		//	[lblTemp2 setFont:[UIFont systemFontOfSize:15]];
	}
	lblTemp1.tag = 1;
	[cell.contentView addSubview:lblTemp1];
	[lblTemp1 release];
	
	//Initialize Label 2 with tag 2.
	lblTemp2 = [[UILabel alloc] initWithFrame:lblFrame2];
	[lblTemp2 setBackgroundColor:[UIColor clearColor]];
	lblTemp2.tag = 2;
	lblTemp2.numberOfLines = 1;
	//	[lblTemp2 sizeToFit];
	//	[lblTemp2 setMinimumFontSize:8.0];
	if([settings.fontName isEqualToString:@""] || settings.fontName == nil){
		[lblTemp2 setFont:[UIFont systemFontOfSize:20]];
		[settings storeFontSettings:@"" size:20.0];
	}else{
		////NSLog(@"font name2 %@",settings.fontName);	
		NSArray *fontNames = [UIFont fontNamesForFamilyName:settings.fontName];
		[lblTemp2 setFont:[UIFont fontWithName:[fontNames objectAtIndex:0] size:settings.fontSize]];
		//	[lblTemp2 setFont:[UIFont systemFontOfSize:15]];
	}	[cell.contentView addSubview:lblTemp2];
	[lblTemp2 release];
	[settings release];
	return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 50.0f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	ShowMemoViewController *showMemoView = [[ShowMemoViewController alloc] initWithNibName:@"ShowMemoViewController" bundle:nil];
	DiaryObject *obj = (DiaryObject*)[memosArray objectAtIndex:indexPath.row];
	showMemoView.diaryObj = obj;
	self.showMemoViewController = showMemoView;
	[self.view addSubview:self.showMemoViewController.view];
	[showMemoView release];
}

@end
