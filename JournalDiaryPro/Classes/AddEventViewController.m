///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   AddEventViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "AddEventViewController.h"
#import "Events.h"
#import "ChooseDateViewController.h"
#import "eDiaryAppDelegate.h"
#import "EventsViewController.h"
#import "Settings.h"
@implementation AddEventViewController
@synthesize timeSelected;
@synthesize contentStr;
@synthesize locationStr;
@synthesize titleStr;
@synthesize oldObj;
@synthesize isEditMode;
@synthesize chooseDateViewController;
@synthesize eventsViewController,ar_timeselection,datestringevent;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/
-(void)addLocalNotificationSetFireDate:(NSDate *)nDate setAlertAction:(NSString *)nAction setRepeatInterval:(NSCalendarUnit)nRepeat setAlertBody:(NSString *)nBody //a17
{
	UILocalNotification* alarm = [[[UILocalNotification alloc] init] autorelease];
	if (alarm)
	{
		alarm.fireDate = nDate;  //self.timeSelected; //[formatter stringFromDate:self.timeSelected];
		//NSLog(@"\ndate=%@\n",alarm.fireDate);
		
		alarm.alertAction = nAction; //events.title;
		//NSLog(@"\naction=%@\n",alarm.alertAction);
		alarm.timeZone = [NSTimeZone defaultTimeZone];
		//NSLog(@"\nzone=%@\n",alarm.timeZone);
		alarm.soundName = UILocalNotificationDefaultSoundName;
		alarm.repeatInterval = nRepeat; //0; //NSWeekCalendarUnit;
		alarm.alertBody = nBody; //[NSString stringWithString: events.descriptionStr];
		//NSLog(@"\nbody=%@\n",alarm.alertBody);
		
		[[UIApplication sharedApplication] scheduleLocalNotification:alarm];
	}
}

-(void)CANCEL_LocalNotificationSetFireDate:(NSDate *)nDate setAlertAction:(NSString *)nAction setRepeatInterval:(NSCalendarUnit)nRepeat setAlertBody:(NSString *)nBody //a17
{
	UILocalNotification* alarm = [[[UILocalNotification alloc] init] autorelease];
	if (alarm)
	{
		alarm.fireDate = nDate;  //self.timeSelected; //[formatter stringFromDate:self.timeSelected];
		//NSLog(@"\ndate=%@\n",alarm.fireDate);
		
		alarm.alertAction = nAction; //events.title;
		//NSLog(@"\naction=%@\n",alarm.alertAction);
		alarm.timeZone = [NSTimeZone defaultTimeZone];
		//NSLog(@"\nzone=%@\n",alarm.timeZone);
		alarm.soundName = UILocalNotificationDefaultSoundName;
		alarm.repeatInterval = nRepeat; //0; //NSWeekCalendarUnit;
		alarm.alertBody = nBody; //[NSString stringWithString: events.descriptionStr];
		//NSLog(@"\nbody=%@\n",alarm.alertBody);
		
		[[UIApplication sharedApplication] cancelLocalNotification:alarm];
	}
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        
        
        backgroundImageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        backgroundImageView.frame = CGRectMake(0, -20, 320, 568+40);
        
        imageView.frame = CGRectMake(0, -20, 320, 568+40);
        // self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 568+20);
    }
    else
    {
        imageView.frame =CGRectMake(0, 0, 320, 480);
        
    }

    
    
	if(contentStr != nil){
		contentText.text = contentStr;
	}
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	////NSLog(@"time %@",self.timeSelected);
	if(self.timeSelected != nil){
		timeLabel.text = [formatter stringFromDate:self.timeSelected];
	}else{
		timeLabel.text = [formatter stringFromDate:[NSDate date]];
		self.timeSelected = [NSDate date];
	}
	[formatter release];
	
	if(self.locationStr == nil){
		self.locationStr = @"";
		self.titleStr = @"";
		self.contentStr = @"";
	}
	locationField.text = self.locationStr;
	titleField.text = self.titleStr;
	contentText.text = self.contentStr;
	
	/*
	if(self.isEditMode){
		locationField.text = oldObj.locationStr;
		titleField.text = oldObj.title;
		contentText.text = oldObj.descriptionStr;
	}
	 */
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];
	[locationField becomeFirstResponder];
	Events *ev=[[Events alloc] init];
	/*ev.dateStr=oldObj.dateStr;
		ev.timeStr=oldObj.timeStr;*/
	ar_timeselection=[ev getDatadate:oldObj.dateStr time:oldObj.timeStr];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

-(IBAction)chooseTimePressed{
	
	[locationField resignFirstResponder];
	
	[titleField resignFirstResponder];
	
	[contentText resignFirstResponder];
	
	ChooseDateViewController *chooseDateView = [[ChooseDateViewController alloc] initWithNibName:@"ChooseDateViewController" bundle:nil];
	
	chooseDateView.preView = @"Events";
	
	chooseDateView.content = contentText.text;
	
	chooseDateView.initialDate = timeSelected;
	
	chooseDateView.fileName = titleField.text;
	
	chooseDateView.tempFilePath = locationField.text;
	
	chooseDateView.isEditMode = self.isEditMode;
	
	chooseDateView.events = self.oldObj;
	
	self.chooseDateViewController = chooseDateView;
	
	[self.view addSubview:self.chooseDateViewController.view];
	
	[chooseDateView release];
}
-(IBAction)donePressed
{
	if([locationField.text isEqualToString:@""])
	{
		[eDiaryAppDelegate showMessage:@"Please enter the location of event" title:@"No Location"];
		return;
	}
	if([titleField.text isEqualToString:@""])
	{
		[eDiaryAppDelegate showMessage:@"Please enter a title for the event" title:@"No Title"];
		return;
	}
	
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	
	Events *events = [[Events alloc] init];
	
	[formatter setDateFormat:@"YYYY-MM-dd"];
	
	events.dateStr = [formatter stringFromDate:self.timeSelected];
	
	[formatter setDateStyle:NSDateFormatterNoStyle];
	
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	
	events.timeStr = [formatter stringFromDate:self.timeSelected];
	
	events.descriptionStr = contentText.text;
	
	events.locationStr = locationField.text;
	
	events.title = titleField.text;
	
	[formatter setDateFormat:@"yyyy-MM-dd 'at' HH:mm"];
	
	NSString *stt=[formatter stringFromDate:self.timeSelected];
	
	NSLog(@"Time Selecteds%@e",self.timeSelected);
	
	NSDate *d=[formatter dateFromString:stt];
	
		NSLog(@"Datesd%@ed",d);
	if([events checkDuplicate])
	{
		[eDiaryAppDelegate showMessage:@"The event already exists" title:@"Duplicate Event"];
		
		return;
	}
	
	BOOL result;
	
	if(self.isEditMode)
	{
		NSString *oldTitle = self.oldObj.title;
		
		events.datestringevent=stt;
		
		result = [events updateEventdate:stt time:self.oldObj.dateStr date:self.oldObj.timeStr];
		
		result = [events updateEvent:self.oldObj.dateStr time:self.oldObj.timeStr title: oldTitle];
		
		NSDate *dd=[formatter dateFromString:[[ar_timeselection objectAtIndex:0] datestringevent]];

		[self CANCEL_LocalNotificationSetFireDate:dd setAlertAction:events.title setRepeatInterval:0 setAlertBody:events.descriptionStr];
		
		[self addLocalNotificationSetFireDate:d setAlertAction:events.title setRepeatInterval:0 setAlertBody:events.descriptionStr]; //a17

	}
	else
	{
		result = [events insert];

		events.datestringevent=stt;
		
		result=	[events insertdate];
		
		[self addLocalNotificationSetFireDate:d setAlertAction:events.title setRepeatInterval:0 setAlertBody:events.descriptionStr]; //a17
	}
	
	
	[events release];
	
	[formatter release];
	
	[locationField resignFirstResponder];
	
	[titleField resignFirstResponder];
	
	[contentText resignFirstResponder];

	[UIView beginAnimations:nil context:nil];
	
	//change to set the time
	[UIView setAnimationDuration:1];
	
	[UIView setAnimationBeginsFromCurrentState:YES];
	
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	
	EventsViewController *eventsView = [[EventsViewController alloc] initWithNibName:@"EventsViewController" bundle:nil];
	
	eventsView.eventDate = self.timeSelected;
	
	self.eventsViewController = eventsView;
	
	[self.view addSubview:self.eventsViewController.view];
	
	[eventsView release];
	
	[UIView commitAnimations];
}

-(IBAction)cancelPressed{
	[locationField resignFirstResponder];
	[titleField resignFirstResponder];
	[contentText resignFirstResponder];
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	EventsViewController *eventsView = [[EventsViewController alloc] initWithNibName:@"EventsViewController" bundle:nil];
	self.eventsViewController = eventsView;
	[self.view addSubview:self.eventsViewController.view];
	[eventsView release];
	[UIView commitAnimations];
}

-(IBAction)clearPressed{
	locationField.text = @"";
	titleField.text = @"";
	contentText.text = @"";
}
- (void)dealloc {
	[oldObj release];
    [super dealloc];
}


@end
