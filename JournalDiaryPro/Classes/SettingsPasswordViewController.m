///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   SettingsPasswordViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "SettingsPasswordViewController.h"
#import "SettingsViewController.h"
#import "Settings.h"
#import "Secure.h"
#import "eDiaryAppDelegate.h"
#import "SKPSMTPMessage.h"
@implementation SettingsPasswordViewController
@synthesize settingsViewController;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	Secure *secure = [[Secure alloc] init];
	
	if([[secure getPassword] isEqualToString:@""]){
	oldField.hidden = YES;
		[newField becomeFirstResponder];	
	}else{
		[oldField becomeFirstResponder];
	}
	[secure release];
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
-(IBAction)donePressed{
	Secure *secure = [[Secure alloc] init];
	
	if((oldField.hidden == NO)&&([oldField.text isEqualToString:@""])){
		[eDiaryAppDelegate showMessage:@"Field is empty" title:@"Empty Field"];
		[secure release];
		return;
	}
	//NSLog(@"pass %@",[secure getPassword]);
	if((oldField.hidden == NO)&&(![oldField.text isEqualToString:[secure getPassword]])){
		[eDiaryAppDelegate showMessage:@"Please enter old password again" title:@"Old password wrong"];
		[secure release];
		return;
	}
	if([newField.text isEqualToString:@""] || [retypeField.text isEqualToString:@""]){
		[eDiaryAppDelegate showMessage:@"Field is empty" title:@"Empty Field"];
		[secure release];
		return;
	}
	if(![newField.text isEqualToString:retypeField.text]){
		[eDiaryAppDelegate showMessage:@"Please retype the same password in both fields" title:@"Password Mismatch"];
		return;
	}
	if([newField.text length]>12 || [newField.text length]<3){
		[eDiaryAppDelegate showMessage:@"Please type a password within 3 to 12 characters" title:@"Password out of range"];
		return;
	}
	if([secure getCount]==0){
		[secure insert:newField.text lockerPass:@"" question:@"" answer:@"" vaultQ:@"" vaultA:@""];
	}else{
		[secure updatePassword:newField.text];
	} 
	[secure release];
//	Settings *settings = [[Settings alloc] init];
//	[settings storeUserName:nameField.text];
//	[settings release];
	[oldField resignFirstResponder];
	[newField resignFirstResponder];
	[retypeField resignFirstResponder];
	SettingsViewController *settingsView = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController"bundle:nil];
	self.settingsViewController = settingsView;
	[self.view addSubview:self.settingsViewController.view];
	[settingsView release];
}
-(IBAction)resetPressed{
	SKPSMTPMessage *testMsg = [[SKPSMTPMessage alloc] init];
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
    testMsg.fromEmail = settings.emailAddress;
    testMsg.toEmail = settings.emailAddress;
    testMsg.relayHost = settings.emailServerAddress;
    testMsg.requiresAuth = YES;
    testMsg.login = settings.emailAddress;
    testMsg.pass = settings.emailPassword;
    testMsg.subject = @"Journal Diary password reminder";
   // testMsg.bccEmail = @"lemuelsinghr@gmail.com";
    testMsg.wantsSecure = YES; // smtp.gmail.com doesn't work without TLS!
	[settings release];
    // Only do this for self-signed certs!
    // testMsg.validateSSLChain = NO;
    testMsg.delegate = self;
    
    NSDictionary *plainPart = [NSDictionary dictionaryWithObjectsAndKeys:@"text/plain",kSKPSMTPPartContentTypeKey,
                               @"This is a test message - yahoo",kSKPSMTPPartMessageKey,@"8bit",kSKPSMTPPartContentTransferEncodingKey,nil];
    
    //NSString *vcfPath = [[NSBundle mainBundle] pathForResource:@"test" ofType:@"vcf"];
    //NSData *vcfData = [NSData dataWithContentsOfFile:vcfPath];
    /*
    NSDictionary *vcfPart = [NSDictionary dictionaryWithObjectsAndKeys:@"text/directory;\r\n\tx-unix-mode=0644;\r\n\tname=\"test.vcf\"",kSKPSMTPPartContentTypeKey,
                             @"attachment;\r\n\tfilename=\"test.vcf\"",kSKPSMTPPartContentDispositionKey,[vcfData encodeBase64ForData],kSKPSMTPPartMessageKey,@"base64",kSKPSMTPPartContentTransferEncodingKey,nil];
    */
    //testMsg.parts = [NSArray arrayWithObjects:plainPart,vcfPart,nil];
    testMsg.parts = [NSArray arrayWithObjects:plainPart,nil];
    [testMsg send];
	
}

-(IBAction)backPressed{
	[oldField resignFirstResponder];
	[newField resignFirstResponder];
	[retypeField resignFirstResponder];	
	SettingsViewController *settingsView = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController"bundle:nil];
	self.settingsViewController = settingsView;
	[self.view addSubview:self.settingsViewController.view];
	[settingsView release];
}
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

- (void)messageSent:(SKPSMTPMessage *)message
{
    //[message release];
    
    //NSLog(@"delegate - message sent");
}

- (void)messageFailed:(SKPSMTPMessage *)message error:(NSError *)error
{
    //[message release];
    
    ////NSLog(@"delegate - error(%d): %@", [error code], [error localizedDescription]);
}

@end
