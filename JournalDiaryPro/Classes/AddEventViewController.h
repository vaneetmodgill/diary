///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   AddEventViewController.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class Events;
@class ChooseDateViewController;
@class EventsViewController;
@interface AddEventViewController : UIViewController {
	IBOutlet UILabel *timeLabel;
	IBOutlet UITextField *locationField;
	IBOutlet UITextField *titleField;
	IBOutlet UITextView *contentText;
	IBOutlet UIImageView *imageView;
	NSDate *timeSelected;
	NSString *contentStr;
	NSString *titleStr;
	NSString *locationStr;
	BOOL isEditMode;
	Events *oldObj;
	ChooseDateViewController *chooseDateViewController;
	EventsViewController *eventsViewController;
	NSMutableArray *ar_timeselection;
	NSString *datestringevent;
    
    IBOutlet UIImageView *backgroundImageView;

}
@property(nonatomic,retain)	NSString *datestringevent;
@property(nonatomic,retain)	NSMutableArray *ar_timeselection;
@property(nonatomic,retain)NSDate *timeSelected;
@property(nonatomic,retain)NSString *contentStr;
@property(nonatomic,retain)NSString *titleStr;
@property(nonatomic,retain)NSString *locationStr;
@property(nonatomic,retain)Events *oldObj;
@property(nonatomic,retain)ChooseDateViewController *chooseDateViewController;
@property(nonatomic,retain)EventsViewController *eventsViewController;
@property(assign)BOOL isEditMode;
-(IBAction)chooseTimePressed;
-(IBAction)donePressed;
-(IBAction)cancelPressed;
-(IBAction)clearPressed;
-(void)addLocalNotificationSetFireDate:(NSDate *)nDate setAlertAction:(NSString *)nAction setRepeatInterval:(NSCalendarUnit)nRepeat setAlertBody:(NSString *)nBody;

@end
