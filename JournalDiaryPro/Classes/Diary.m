///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   Diary.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "Diary.h"
#import "DBSqlite.h"
#import "Sqlite.h"

@implementation Diary
@synthesize dateStr;
@synthesize timeStr;
@synthesize entry;
@synthesize emoticon,twitter_un,twitter_password;
@synthesize background_image,imagename,current_location,filename;
static sqlite3_stmt *insert_statement = nil;
static sqlite3_stmt *search_cases_statement=nil;
-(BOOL)insert{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;
	
	//database=sqlite._db;
	//NSLog(@"%@ %@ %@ %@",self.dateStr,self.timeStr,self.entry,self.emoticon);
	self.imagename=@"g";
	BOOL result = [sqlite executeNonQuery:@"INSERT INTO diary(date,time,entry,emoticon) VALUES (?, ?, ?, ?);",self.dateStr,self.timeStr,self.entry,self.emoticon];
	
	[dbSqlite release];	
	return result;
}
-(void)passdatabasereference:(sqlite3*)db
{
	//database=	db;
}
-(BOOL)setTwitterDetails:(NSString*)par_un :(NSString*)par_password{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;
	self.twitter_un=par_un;
		self.twitter_password=par_password;
	//database=sqlite._db;
	//NSLog(@"%@ %@ %@ %@",self.dateStr,self.timeStr,self.entry,self.emoticon);
	//self.imagename=@"g";
	
	BOOL result = [sqlite executeNonQuery:@"INSERT into twitter_details(username,password) VALUES(?,?);",self.twitter_un,self.twitter_password];
	
	[dbSqlite release];	
	return result;
}
-(BOOL)updateTwitterDetails{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;

	//database=sqlite._db;
	//NSLog(@"%@ %@ %@ %@",self.dateStr,self.timeStr,self.entry,self.emoticon);
	//self.imagename=@"g";
	
	BOOL result = [sqlite executeNonQuery:@"UPDATE twitter_details SET username = ? , password = ?;",self.twitter_un,self.twitter_password];
	
	[dbSqlite release];	
	return result;
}


-(NSMutableArray*)getTwitterDetails{
	NSMutableArray *tempArray;
	tempArray =[[NSMutableArray alloc] init];
	/*DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	 Sqlite *sqlite;
	 if (![dbSqlite getDBConnection])
	 return NO;
	 sqlite = dbSqlite.sqlite;*/
	sqlite3 *database;
	
	char *sql_query = "Select * from twitter_details";
	search_cases_statement=nil;
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"eDiary.sqlite"];
	
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
		if(search_cases_statement ==nil)
		{
			
			if (sqlite3_prepare_v2(database, sql_query, -1, &search_cases_statement, NULL) == SQLITE_OK)
			{
				//sqlite3_bind_text(search_cases_statement,1, [par_date UTF8String],-1,SQLITE_TRANSIENT);
			//	sqlite3_bind_text(search_cases_statement,2, [par_time UTF8String],-1,SQLITE_TRANSIENT);
				//sqlite3_bind_text(search_cases_statement,3, [par_notes UTF8String],-1,SQLITE_TRANSIENT);
				
				while (sqlite3_step(search_cases_statement) == SQLITE_ROW)
				{
					
					
					
					Diary *obj_savescore_local = [[Diary alloc] init];
					obj_savescore_local.twitter_un=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,0)];
					obj_savescore_local.twitter_password=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,1)];


										[tempArray addObject:obj_savescore_local];
					//	obj_savescore_local.dateStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,14)];
					//	obj_savescore_local.timeStr = [NSString stringWithUTF8String:(char*)sqlite3_column_text(search_cases_statement, 15)];
					
				}
			}
		}
	}
	sqlite3_close(database);
	sqlite3_reset(search_cases_statement);
	sqlite3_finalize(search_cases_statement);
	return tempArray;
	
}

-(void)insertimagetodiary:(NSString*)par_date time:(NSString*)par_time entry:(NSString*)notes picture:(UIImage*)par_picture
{//http://www.facebook.com/ahuja.om?ref=pymk#
	//DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	//if (![dbSqlite getDBConnection])
	//	return NO;
	//sqlite3 *database=sqlite._db;
	//sqlite = dbSqlite.sqlite;
	//sqlite3 *database=sqlite._db;
	sqlite3 *database;
	
	insert_statement=nil;
	self.dateStr =[[NSString alloc] initWithFormat:@"%@",par_date];
	self.timeStr =[[NSString alloc]initWithFormat:@"%@",par_time];
	self.entry =[[NSString alloc]initWithFormat:@"%@",notes];
	self.background_image=par_picture;
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *path = [documentsDirectory stringByAppendingPathComponent:@"eDiary.sqlite"];
	
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
		{
	//self.notes =[[NSString alloc] initWithFormat:@"%@",n1];
	if (insert_statement == nil) {
		// Note the '?' at the end of the query. This is a parameter which can be replaced by a bound variable.
		// This is a great way to optimize because frequently used queries can be compiled once, then with each
		// use new variable values can be bound to placeholders.
		//UIImage *img_local=[UIImage imageNamed:@"clock.png"];
		NSData *imgData = UIImagePNGRepresentation(par_picture);
		
		//		,todo_item , Notes_rem ,nqty1 , nqty2 ,pqty1 ,pqty2 , strt_date ,end_date ,rep , alpref , team_person , store_name
		const char *sql_query = "INSERT INTO diary_image (date ,time ,picture ) values (?,?,?)";
		//"INSERT INTO tb_SubCategory(subcategory_name,picture,notes,majorcategory_name,days,hours,minutes) values (?,?,?,?,?,?,?)";
		
		if (sqlite3_prepare_v2(database,sql_query, -1, &insert_statement, NULL) == SQLITE_OK)
		{
			
			sqlite3_bind_text(insert_statement, 1, [self.dateStr UTF8String],-1,SQLITE_TRANSIENT);

			sqlite3_bind_text(insert_statement, 2, [self.timeStr UTF8String],-1,SQLITE_TRANSIENT);

			if(imgData != nil)
			{
				sqlite3_bind_blob(insert_statement, 3, [imgData bytes], [imgData length], NULL);
				//[imgData release];
			}
			else
				sqlite3_bind_blob(insert_statement, 3, nil, -1, NULL);
			
			sqlite3_bind_text(insert_statement, 4, [self.entry UTF8String], -1, SQLITE_TRANSIENT);
			
			
		int x=	sqlite3_step(insert_statement);
			NSLog(@"%d",x);
		}
		
	}
		}
			sqlite3_close(database);
			sqlite3_reset(insert_statement);
			sqlite3_finalize(insert_statement);
			
}

-(void)insertimagetodiaryefficiently:(NSString*)par_date time:(NSString*)par_time img_name:(NSString*)notes file_name:(NSString*)par_file
{//http://www.facebook.com/ahuja.om?ref=pymk#
	//DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	//if (![dbSqlite getDBConnection])
	//	return NO;
	//sqlite3 *database=sqlite._db;
	//sqlite = dbSqlite.sqlite;
	//sqlite3 *database=sqlite._db;
	sqlite3 *database;
	
	insert_statement=nil;
	self.dateStr =[[NSString alloc] initWithFormat:@"%@",par_date];
	self.timeStr =[[NSString alloc]initWithFormat:@"%@",par_time];
	self.imagename =[[NSString alloc]initWithFormat:@"%@",notes];
	self.filename=[[NSString alloc]initWithFormat:@"%@",par_file];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"eDiary.sqlite"];
	
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
		//self.notes =[[NSString alloc] initWithFormat:@"%@",n1];
		if (insert_statement == nil) {
			// Note the '?' at the end of the query. This is a parameter which can be replaced by a bound variable.
			// This is a great way to optimize because frequently used queries can be compiled once, then with each
			// use new variable values can be bound to placeholders.
			//UIImage *img_local=[UIImage imageNamed:@"clock.png"];
			//NSData *imgData = UIImagePNGRepresentation(par_picture);
			
			//		,todo_item , Notes_rem ,nqty1 , nqty2 ,pqty1 ,pqty2 , strt_date ,end_date ,rep , alpref , team_person , store_name
			const char *sql_query = "INSERT INTO imagewithname (date ,time ,imagename,imagepath ) values (?,?,?,?)";
			//"INSERT INTO tb_SubCategory(subcategory_name,picture,notes,majorcategory_name,days,hours,minutes) values (?,?,?,?,?,?,?)";
			
			if (sqlite3_prepare_v2(database,sql_query, -1, &insert_statement, NULL) == SQLITE_OK)
			{
				
				sqlite3_bind_text(insert_statement, 1, [self.dateStr UTF8String],-1,SQLITE_TRANSIENT);
				
				sqlite3_bind_text(insert_statement, 2, [self.timeStr UTF8String],-1,SQLITE_TRANSIENT);
				sqlite3_bind_text(insert_statement, 3, [self.imagename UTF8String],-1,SQLITE_TRANSIENT);

								sqlite3_bind_text(insert_statement, 4, [self.filename UTF8String], -1, SQLITE_TRANSIENT);
				
				
				int x=	sqlite3_step(insert_statement);
				NSLog(@"%d",x);
			}
			
		}
	}
	sqlite3_close(database);
	sqlite3_reset(insert_statement);
	sqlite3_finalize(insert_statement);
	
}
-(void)updateimagetodiaryefficiently:(NSString*)par_date time:(NSString*)par_time image_name:(NSString*)par_image filename:(NSString*)par_file
{//http://www.facebook.com/ahuja.om?ref=pymk#
	//DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	//if (![dbSqlite getDBConnection])
	//	return NO;
	//sqlite3 *database=sqlite._db;
	//sqlite = dbSqlite.sqlite;
	//sqlite3 *database=sqlite._db;
	sqlite3 *database;
	
	insert_statement=nil;
	self.dateStr =[[NSString alloc] initWithFormat:@"%@",par_date];
	self.timeStr =[[NSString alloc]initWithFormat:@"%@",par_time];
	self.imagename =[[NSString alloc]initWithFormat:@"%@",par_image];
	self.filename=[[NSString alloc]initWithFormat:@"%@",par_file];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"eDiary.sqlite"];
	
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
		//self.notes =[[NSString alloc] initWithFormat:@"%@",n1];
		if (insert_statement == nil) {
			// Note the '?' at the end of the query. This is a parameter which can be replaced by a bound variable.
			// This is a great way to optimize because frequently used queries can be compiled once, then with each
			// use new variable values can be bound to placeholders.
			//UIImage *img_local=[UIImage imageNamed:@"clock.png"];
			//NSData *imgData = UIImagePNGRepresentation(par_picture);
			
			//		,todo_item , Notes_rem ,nqty1 , nqty2 ,pqty1 ,pqty2 , strt_date ,end_date ,rep , alpref , team_person , store_name
			const char *sql_query = "UPDATE imagewithname SET imagepath = ? WHERE date = ? and time = ?";
			//"INSERT INTO tb_SubCategory(subcategory_name,picture,notes,majorcategory_name,days,hours,minutes) values (?,?,?,?,?,?,?)";
			
			if (sqlite3_prepare_v2(database,sql_query, -1, &insert_statement, NULL) == SQLITE_OK)
			{
				
				sqlite3_bind_text(insert_statement, 2, [self.dateStr UTF8String],-1,SQLITE_TRANSIENT);
				sqlite3_bind_text(insert_statement, 3, [self.timeStr UTF8String],-1,SQLITE_TRANSIENT);
				
				sqlite3_bind_text(insert_statement, 1, [self.filename UTF8String],-1,SQLITE_TRANSIENT);

				
				
				int x=	sqlite3_step(insert_statement);
				NSLog(@"%d",x);
			}
			
		}
	}
	sqlite3_close(database);
	sqlite3_reset(insert_statement);
	sqlite3_finalize(insert_statement);
	
}

-(NSMutableArray*)selectImagewhereEfficiently:(NSString*)par_date time:(NSString*)par_time notes:(NSString*)par_notes
{
	NSMutableArray *tempArray;
	tempArray =[[NSMutableArray alloc] init];
	/*DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	 Sqlite *sqlite;
	 if (![dbSqlite getDBConnection])
	 return NO;
	 sqlite = dbSqlite.sqlite;*/
	sqlite3 *database;
	
	char *sql_query = "Select * from imagewithname where date = ? and time= ? ";
	search_cases_statement=nil;
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"eDiary.sqlite"];
	
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
		if(search_cases_statement ==nil)
		{
			
			if (sqlite3_prepare_v2(database, sql_query, -1, &search_cases_statement, NULL) == SQLITE_OK)
			{
				sqlite3_bind_text(search_cases_statement,1, [par_date UTF8String],-1,SQLITE_TRANSIENT);
				sqlite3_bind_text(search_cases_statement,2, [par_time UTF8String],-1,SQLITE_TRANSIENT);
				//sqlite3_bind_text(search_cases_statement,3, [par_notes UTF8String],-1,SQLITE_TRANSIENT);
				
				while (sqlite3_step(search_cases_statement) == SQLITE_ROW)
				{
					
					
					
					Diary *obj_savescore_local = [[Diary alloc] init];
					obj_savescore_local.filename=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,3)];

					obj_savescore_local.imagename=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,2)];
					
					[tempArray addObject:obj_savescore_local];
					//	obj_savescore_local.dateStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,14)];
					//	obj_savescore_local.timeStr = [NSString stringWithUTF8String:(char*)sqlite3_column_text(search_cases_statement, 15)];
					
				}
			}
		}
	}
	sqlite3_close(database);
	sqlite3_reset(search_cases_statement);
	sqlite3_finalize(search_cases_statement);
	return tempArray;
	
}
-(void)updatecurrentlocationtodiary:(NSString*)par_date time:(NSString*)par_time entry:(NSString*)notes cl:(NSString*)par_cl
{//http://www.facebook.com/ahuja.om?ref=pymk#
	//DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	//if (![dbSqlite getDBConnection])
	//	return NO;
	//sqlite3 *database=sqlite._db;
	//sqlite = dbSqlite.sqlite;
	//sqlite3 *database=sqlite._db;
	sqlite3 *database;
	
	insert_statement=nil;
	self.dateStr =[[NSString alloc] initWithFormat:@"%@",par_date];
	self.timeStr =[[NSString alloc]initWithFormat:@"%@",par_time];
	self.entry =[[NSString alloc]initWithFormat:@"%@",notes];
	self.current_location=[[NSString alloc]initWithFormat:@"%@",par_cl];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"eDiary.sqlite"];
	
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
		//self.notes =[[NSString alloc] initWithFormat:@"%@",n1];
		if (insert_statement == nil) {
			// Note the '?' at the end of the query. This is a parameter which can be replaced by a bound variable.
			// This is a great way to optimize because frequently used queries can be compiled once, then with each
			// use new variable values can be bound to placeholders.
			//UIImage *img_local=[UIImage imageNamed:@"clock.png"];
		//	NSData *imgData = UIImagePNGRepresentation(par_picture);
			
			//		,todo_item , Notes_rem ,nqty1 , nqty2 ,pqty1 ,pqty2 , strt_date ,end_date ,rep , alpref , team_person , store_name
			const char *sql_query = "UPDATE diary_currentlocation SET current_loc = ? WHERE date = ? and time = ?";
			//"INSERT INTO tb_SubCategory(subcategory_name,picture,notes,majorcategory_name,days,hours,minutes) values (?,?,?,?,?,?,?)";
			
			if (sqlite3_prepare_v2(database,sql_query, -1, &insert_statement, NULL) == SQLITE_OK)
			{
				
				sqlite3_bind_text(insert_statement, 2, [self.dateStr UTF8String],-1,SQLITE_TRANSIENT);
				sqlite3_bind_text(insert_statement, 3, [self.timeStr UTF8String],-1,SQLITE_TRANSIENT);
				
				
				
				sqlite3_bind_text(insert_statement, 1, [self.current_location UTF8String], -1, SQLITE_TRANSIENT);
				
				
				
				
				int x=	sqlite3_step(insert_statement);
				NSLog(@"%d",x);
			}
			
		}
	}
	sqlite3_close(database);
	sqlite3_reset(insert_statement);
	sqlite3_finalize(insert_statement);
	
}
-(BOOL)deleteCurrentLocation:(NSString *)oldDate time:(NSString *)oldTime entry:(NSString *)notes{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;
	BOOL result = [sqlite executeNonQuery:@"DELETE from diary_currentlocation where date =? and time = ? ;",oldDate,oldTime];
	[dbSqlite release];
	return result;
}
-(void)updateimagetodiary:(NSString*)par_date time:(NSString*)par_time entry:(NSString*)notes picture:(UIImage*)par_picture
{//http://www.facebook.com/ahuja.om?ref=pymk#
	//DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	//if (![dbSqlite getDBConnection])
	//	return NO;
	//sqlite3 *database=sqlite._db;
	//sqlite = dbSqlite.sqlite;
	//sqlite3 *database=sqlite._db;
	sqlite3 *database;
	
	insert_statement=nil;
	self.dateStr =[[NSString alloc] initWithFormat:@"%@",par_date];
	self.timeStr =[[NSString alloc]initWithFormat:@"%@",par_time];
	self.entry =[[NSString alloc]initWithFormat:@"%@",notes];
	self.background_image=par_picture;
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"eDiary.sqlite"];
	
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
		//self.notes =[[NSString alloc] initWithFormat:@"%@",n1];
		if (insert_statement == nil) {
			// Note the '?' at the end of the query. This is a parameter which can be replaced by a bound variable.
			// This is a great way to optimize because frequently used queries can be compiled once, then with each
			// use new variable values can be bound to placeholders.
			//UIImage *img_local=[UIImage imageNamed:@"clock.png"];
			NSData *imgData = UIImagePNGRepresentation(par_picture);
			
			//		,todo_item , Notes_rem ,nqty1 , nqty2 ,pqty1 ,pqty2 , strt_date ,end_date ,rep , alpref , team_person , store_name
			const char *sql_query = "UPDATE diary_image SET picture = ? WHERE date = ? and time = ?";
			//"INSERT INTO tb_SubCategory(subcategory_name,picture,notes,majorcategory_name,days,hours,minutes) values (?,?,?,?,?,?,?)";
			
			if (sqlite3_prepare_v2(database,sql_query, -1, &insert_statement, NULL) == SQLITE_OK)
			{
				
				sqlite3_bind_text(insert_statement, 2, [self.dateStr UTF8String],-1,SQLITE_TRANSIENT);
				sqlite3_bind_text(insert_statement, 3, [self.timeStr UTF8String],-1,SQLITE_TRANSIENT);

		
				if(imgData != nil)
				{
					sqlite3_bind_blob(insert_statement, 1, [imgData bytes], [imgData length], NULL);
					//[imgData release];
				}
				else
					sqlite3_bind_blob(insert_statement, 1, nil, -1, NULL);
				
				
				
				
				int x=	sqlite3_step(insert_statement);
				NSLog(@"%d",x);
			}
			
		}
	}
	sqlite3_close(database);
	sqlite3_reset(insert_statement);
	sqlite3_finalize(insert_statement);
	
}
-(void)insertcurrentlocationtodiary:(NSString*)par_date time:(NSString*)par_time entry:(NSString*)notes cl:(NSString*)par_cl
{
	Sqlite *sqlite;
	//if (![dbSqlite getDBConnection])
	//	return NO;
	//sqlite3 *database=sqlite._db;
	//sqlite = dbSqlite.sqlite;
	//sqlite3 *database=sqlite._db;
	sqlite3 *database;
	
	insert_statement=nil;
	self.dateStr =[[NSString alloc] initWithFormat:@"%@",par_date];
	self.timeStr =[[NSString alloc]initWithFormat:@"%@",par_time];
	self.entry =[[NSString alloc]initWithFormat:@"%@",notes];
	self.current_location=[[NSString alloc]initWithFormat:@"%@",par_cl];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"eDiary.sqlite"];
	
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
		//self.notes =[[NSString alloc] initWithFormat:@"%@",n1];
		if (insert_statement == nil) {
			// Note the '?' at the end of the query. This is a parameter which can be replaced by a bound variable.
			// This is a great way to optimize because frequently used queries can be compiled once, then with each
			// use new variable values can be bound to placeholders.
			//UIImage *img_local=[UIImage imageNamed:@"clock.png"];
		//	NSData *imgData = UIImagePNGRepresentation(par_picture);
			
			//		,todo_item , Notes_rem ,nqty1 , nqty2 ,pqty1 ,pqty2 , strt_date ,end_date ,rep , alpref , team_person , store_name
			const char *sql_query = "INSERT INTO diary_currentlocation (date ,time ,notes , current_loc ) values (?,?,?,?)";
			//"INSERT INTO tb_SubCategory(subcategory_name,picture,notes,majorcategory_name,days,hours,minutes) values (?,?,?,?,?,?,?)";
			
			if (sqlite3_prepare_v2(database,sql_query, -1, &insert_statement, NULL) == SQLITE_OK)
			{
				
				sqlite3_bind_text(insert_statement, 1, [self.dateStr UTF8String],-1,SQLITE_TRANSIENT);
				
				sqlite3_bind_text(insert_statement, 2, [self.timeStr UTF8String],-1,SQLITE_TRANSIENT);
				
				sqlite3_bind_text(insert_statement, 3, [self.entry UTF8String],-1,SQLITE_TRANSIENT);

				sqlite3_bind_text(insert_statement, 4, [self.current_location UTF8String], -1, SQLITE_TRANSIENT);
				
				
				int x=	sqlite3_step(insert_statement);
				NSLog(@"%d",x);
			}
			
		}
	}
	sqlite3_close(database);
	sqlite3_reset(insert_statement);
	sqlite3_finalize(insert_statement);
	
}
-(NSMutableArray*)selectcurrenlocation:(NSString*)par_date time:(NSString*)par_time notes:(NSString*)par_notes
{
	NSMutableArray *tempArray;
	tempArray =[[NSMutableArray alloc] init];
	/*DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	 Sqlite *sqlite;
	 if (![dbSqlite getDBConnection])
	 return NO;
	 sqlite = dbSqlite.sqlite;*/
	sqlite3 *database;
	
	char *sql_query = "Select * from diary_currentlocation where date = ? and time= ? ";
	search_cases_statement=nil;
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"eDiary.sqlite"];
	
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
		if(search_cases_statement ==nil)
		{
			
			if (sqlite3_prepare_v2(database, sql_query, -1, &search_cases_statement, NULL) == SQLITE_OK)
			{
				sqlite3_bind_text(search_cases_statement,1, [par_date UTF8String],-1,SQLITE_TRANSIENT);
				sqlite3_bind_text(search_cases_statement,2, [par_time UTF8String],-1,SQLITE_TRANSIENT);
				//sqlite3_bind_text(search_cases_statement,3, [par_notes UTF8String],-1,SQLITE_TRANSIENT);
				
				while (sqlite3_step(search_cases_statement) == SQLITE_ROW)
				{
					
					
					
					Diary *obj_savescore_local = [[Diary alloc] init];
					//[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,14)];

					obj_savescore_local.current_location=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,3)];
					
					
					[tempArray addObject:obj_savescore_local];
					//	obj_savescore_local.dateStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,14)];
					//	obj_savescore_local.timeStr = [NSString stringWithUTF8String:(char*)sqlite3_column_text(search_cases_statement, 15)];
					
				}
			}
		}
	}
	sqlite3_close(database);
	sqlite3_reset(search_cases_statement);
	sqlite3_finalize(search_cases_statement);
	return tempArray;
	
}
-(NSMutableArray*)selectImagewhere:(NSString*)par_date time:(NSString*)par_time notes:(NSString*)par_notes
{
	NSMutableArray *tempArray;
	tempArray =[[NSMutableArray alloc] init];
	/*DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;*/
	sqlite3 *database;

	char *sql_query = "Select * from diary_image where date = ? and time= ? ";
	search_cases_statement=nil;
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"eDiary.sqlite"];
	
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
	if(search_cases_statement ==nil)
	{

		if (sqlite3_prepare_v2(database, sql_query, -1, &search_cases_statement, NULL) == SQLITE_OK)
		{
			sqlite3_bind_text(search_cases_statement,1, [par_date UTF8String],-1,SQLITE_TRANSIENT);
			sqlite3_bind_text(search_cases_statement,2, [par_time UTF8String],-1,SQLITE_TRANSIENT);
			//sqlite3_bind_text(search_cases_statement,3, [par_notes UTF8String],-1,SQLITE_TRANSIENT);

			while (sqlite3_step(search_cases_statement) == SQLITE_ROW)
			{
				
				
				
				Diary *obj_savescore_local = [[Diary alloc] init];
				
				
				NSData *data = [[NSData alloc] initWithBytes:sqlite3_column_blob(search_cases_statement,2) length:sqlite3_column_bytes(search_cases_statement,2)];
				
				if(data == nil)
					NSLog(@"No image found.");
				else
				{
					
					obj_savescore_local.background_image = [UIImage imageWithData:data];
					[data release];
				}
				
				[tempArray addObject:obj_savescore_local];
			//	obj_savescore_local.dateStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,14)];
			//	obj_savescore_local.timeStr = [NSString stringWithUTF8String:(char*)sqlite3_column_text(search_cases_statement, 15)];
			
		}
		}
	}
	}
	sqlite3_close(database);
	sqlite3_reset(search_cases_statement);
	sqlite3_finalize(search_cases_statement);
	return tempArray;
	
}
-(BOOL)updateImage:(NSString *)oldDate time:(NSString *)oldTime entry:(NSString *)notes image:(NSString*)par_image{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;
	imagename=par_image;
	BOOL result = [sqlite executeNonQuery:@"UPDATE diary set background_name where date =? and time = ? and entry = ? ;",par_image,oldDate,oldTime,notes];

	[dbSqlite release];
	return result;
}
-(BOOL)updateImageNew:(NSString *)oldDate time:(NSString *)oldTime entry:(NSString *)notes{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;
		NSData *imgData = UIImagePNGRepresentation(self.background_image);
	////NSLog(@"time %@ date %@ entry %@",oldTime,oldDate,notes);
	BOOL result = [sqlite executeNonQuery:@"UPDATE diary set backgroundimage = ? where date =? and time = ? and entry = ? ;",imgData,oldDate,oldTime,notes];
	[dbSqlite release];
	return result;
}
-(BOOL)updateEntry:(NSString *)oldDate time:(NSString *)oldTime entry:(NSString *)notes{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;
	////NSLog(@"time %@ date %@ entry %@",oldTime,oldDate,notes);
	BOOL result = [sqlite executeNonQuery:@"UPDATE diary set date = ?,time = ?,entry = ?,emoticon = ? where date =? and time = ? and entry = ? ;",self.dateStr,self.timeStr,self.entry,self.emoticon,oldDate,oldTime,notes];
	[dbSqlite release];
	return result;
}
-(BOOL)deleteEntry:(NSString *)oldDate time:(NSString *)oldTime entry:(NSString *)notes{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;
	BOOL result = [sqlite executeNonQuery:@"DELETE from diary where date =? and time = ? and entry = ? ;",oldDate,oldTime,notes];
	[dbSqlite release];
	return result;
}
-(NSArray*)getData:(NSString *)entryDate{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select * from diary where date = ?;",entryDate];	
	return results;
}
-(NSArray*)getSearchData:(NSString*)searchText{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	NSString *query = [NSString stringWithFormat:@"Select * from diary where entry LIKE '%%%@%%' order by date,time;",searchText];
	NSArray *results = [sqlite executeQuery:query];	
	return results;
}

-(NSArray*)getExportData:(NSString *)from and:(NSString *)to{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select * from diary where date between ? and ? order by date asc;",from,to];	
	return results;	
}

-(BOOL)isEntryAddedForDate:(NSString *)dateOfEntry{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;	
	NSArray *results = [sqlite executeQuery:@"Select * from diary where date = ?;",dateOfEntry];
	[dbSqlite release];
	if([results count] == 0){
		return NO;
	}else{
		return YES;
	}
}
-(NSArray*)getDatesInMonth:(NSString *)fromDate	to:(NSString *)toDate{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select date from diary where date between ? and ?;",fromDate,toDate];	
	return results;	
}


//:(NSString*)oldDate timeStamp:(NSString*)oldTime content:(NSString*)oldContent
-(BOOL)checkDuplicate{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;	
	NSArray *results = [sqlite executeQuery:@"Select * from diary where date = ? and time = ? and entry = ? and emoticon = ?;",self.dateStr,self.timeStr,self.entry,self.emoticon];
	[dbSqlite release];
	if([results count] == 0){
		return NO;
	}else{
		return YES;
	}
}
@end
