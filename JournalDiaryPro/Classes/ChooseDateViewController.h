///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   ChooseDateViewController.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class AddEntryViewController;
@class AddEventViewController;
@class SaveMemoViewController;
@class Events;
@interface ChooseDateViewController : UIViewController {
	IBOutlet UIDatePicker *timePicker;
	IBOutlet UIImageView *imageView;
	NSDate *initialDate;
	NSString *content; 
	NSString *preView;
	BOOL isEditMode;
	NSString *fileName;
	NSString *tempFilePath;
	NSString *emote;
	NSString *oldFileName;
	Events *events;
	SaveMemoViewController *saveViewController;
	AddEventViewController *addEventViewController;
	AddEntryViewController *addEntryViewController;
    
    IBOutlet UIImageView *backgrndImaView ;

}
@property(nonatomic,retain)NSString *content;
@property(nonatomic,retain)NSDate *initialDate;
@property(nonatomic,retain)NSString *preView;
@property(nonatomic,retain)NSString *fileName;
@property(nonatomic,retain)NSString *oldFileName;
@property(nonatomic,retain)NSString *tempFilePath;
@property(nonatomic,retain)NSString *emote;
@property(assign)BOOL isEditMode;
@property(nonatomic,retain)Events *events;
@property(nonatomic,retain)SaveMemoViewController *saveViewController;
@property(nonatomic,retain)AddEventViewController *addEventViewController;
@property(nonatomic,retain)AddEntryViewController *addEntryViewController;
-(IBAction)donePressed;
@end
