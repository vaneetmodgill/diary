///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   ShowMemoViewController.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>
#import"MessageUI/MessageUI.h"
#import <AVFoundation/AVFoundation.h>
@class DiaryObject;
@class VoiceMemoUtils;
@class VoiceMemoViewController;
@class AddMemoViewController;

@interface ShowMemoViewController : UIViewController<MFMailComposeViewControllerDelegate,AVAudioPlayerDelegate,UIActionSheetDelegate> {
	IBOutlet UITextView *titleView;
	IBOutlet UILabel *dateLabel;
	IBOutlet UILabel *timeLabel;
	IBOutlet UISlider *memoSlider;
	IBOutlet UIButton *playButton;
	IBOutlet UIButton *stopButton;
	IBOutlet UIImageView *imageView;
	DiaryObject *diaryObj;
	AVAudioPlayer *player;
	NSTimer	*timer;
	VoiceMemoUtils *utils;
	VoiceMemoViewController *voiceMemoViewController;
	AddMemoViewController *addMemoViewController;
	NSData *data_voice;
	NSString *filepath;
    
    MFMailComposeViewController *globalMailComposer;
    
    IBOutlet UIImageView *backgrndImaView,*backgroundImageView;

}
@property(nonatomic,retain)DiaryObject *diaryObj;
@property(nonatomic,retain)VoiceMemoViewController *voiceMemoViewController;
@property(nonatomic,retain)AddMemoViewController *addMemoViewController;
-(IBAction)backPressed;
-(IBAction)playPressed;
-(IBAction)stopPressed;
-(IBAction)updatePressed;
-(IBAction)deletePressed;
-(IBAction)sliderMoved;
-(IBAction)emailPressed;
@end
