///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   eDiaryAppDelegate.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManagerDelegate.h>
#import "Events.h"

@class eDiaryViewController;
@class PasswordViewController;
@class HomeViewController;
@class SettingsViewController;
@class HelpViewController;
@class ActivityIndicator;
@class Events;
@interface eDiaryAppDelegate : NSObject <UIApplicationDelegate , CLLocationManagerDelegate> {
    UIWindow *window;
    eDiaryViewController *viewController;
	PasswordViewController *passwordViewController;
	HomeViewController *homeViewController;
	SettingsViewController *settingsViewController;
	HelpViewController *helpViewController;
	ActivityIndicator *indicator;
	
	CLLocationManager	*locationMgr;
	NSTimer *Locationtimer;
	double Latitude,Longitude;
	NSString *string_global;
}
@property(nonatomic,retain)	NSString *string_global;

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet eDiaryViewController *viewController;
@property (nonatomic, retain) IBOutlet PasswordViewController *passwordViewController;
@property (nonatomic, retain) IBOutlet HomeViewController *homeViewController;
@property (nonatomic,retain) SettingsViewController *settingsViewController;
@property double Latitude;
@property double Longitude;
- (void)setupTimer;

+(void) showMessage:(NSString *)message title:(NSString *)title;
- (void)createEditableCopyOfDatabaseIfNeeded;
+(void)setHomeImage:(UIImage *)img;
+(UIImage *)getHomeImage;
+(void)setCalendarName:(NSString *)nameStr;
+(NSString *)getCalendarName;
@end

