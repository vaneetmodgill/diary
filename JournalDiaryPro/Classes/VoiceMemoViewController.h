///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   VoiceMemoViewController.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>

@class CalendarViewController;
@class AddMemoViewController;
@class HomeViewController;
@class ShowMemoViewController;
@interface VoiceMemoViewController : UIViewController<UITableViewDelegate,UITableViewDataSource> {
	IBOutlet UILabel *dateLabel;
	IBOutlet UITableView *memoTable;
	IBOutlet UIImageView *imageView;
	NSDate *memoDate;
	NSMutableArray *memosArray;
	UIImage *image;
	CalendarViewController *calendarViewController;
	AddMemoViewController *addMemoViewController;
	HomeViewController *homeViewController;
	ShowMemoViewController *showMemoViewController;
    
    IBOutlet UIImageView *backgroundImageView;
}
@property(nonatomic,retain)NSDate *memoDate;
@property(nonatomic,retain)UIImage *image;
@property(nonatomic,retain)CalendarViewController *calendarViewController;
@property(nonatomic,retain)AddMemoViewController *addMemoViewController;
@property(nonatomic,retain)HomeViewController *homeViewController;
@property(nonatomic,retain)ShowMemoViewController *showMemoViewController;
- (UITableViewCell *) getCellContentView:(NSString *)cellIdentifier;
-(IBAction)calendarPressed;
-(IBAction)addPressed;
-(IBAction)backPressed;
-(IBAction)leftPressed;
-(IBAction)rightPressed;
@end
