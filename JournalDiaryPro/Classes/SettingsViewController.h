///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   SettingsViewController.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>
@class HomeViewController;
@class SettingsPasswordViewController;
@class SettingsQAViewController;
@class SettingsFontViewController;
@class SettingsPictureViewController;
@class SettingsEmailViewController;
@class SettingsExportViewController;
@interface SettingsViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>{
	IBOutlet UISwitch *passwordSwitch;
    IBOutlet UISwitch *faceIDSwitch;
	IBOutlet UIImageView *imageView;
	IBOutlet UIView *nameView;
	IBOutlet UITextField *nameText;
	IBOutlet UILabel *titleLbl;
	IBOutlet UILabel *messageLbl;
	UILabel *nameLbl;
	UISwitch *emailSwitch;
	UISwitch *pictureSwitch;
	UISwitch *picOfDaySwitch;
	
	
	UISwitch *animateEmoticonSwitch; 
	UISwitch *gpsEnabledSwitch; 
	
//=== ashish 22 start
	UISlider *daysGapSlider; //a22
	UITableViewCell *daysGapSliderCell; //a22
	int daysGapInt;//a22
	UILabel *daysGapLabel; //a22
//=== ashish 22 end
	
	HomeViewController *homeViewController;
	IBOutlet UITableView *settingsTable;
	UITableViewCell *passwordSwitchCell;
    UITableViewCell *faceIDSwitchCell;
	UITableViewCell *emailSwitchCell;
	UITableViewCell *pictureSwitchCell;
	UITableViewCell *picOfDaySwitchCell;
	
	UITableViewCell *gpsEnabledSwitchCell; //a16
	UITableViewCell *animateEmoticonSwitchCell; //a
	
	HomeViewController *homeViewCtlr;
	SettingsPasswordViewController *settingsPasswordViewController;
	SettingsQAViewController *QAViewController;
	SettingsFontViewController *fontsViewController;
	SettingsPictureViewController *pictureViewController;
	SettingsEmailViewController *settingsEmailViewController;
	SettingsExportViewController *exportViewController;
    
    
    IBOutlet UIImageView *backgroundImageView;
}
@property(nonatomic,retain)HomeViewController *homeViewCtlr;
@property(nonatomic,retain)SettingsPasswordViewController *settingsPasswordViewController;
@property(nonatomic,retain)SettingsQAViewController *QAViewController;
@property(nonatomic,retain)SettingsFontViewController *fontsViewController;
@property(nonatomic,retain)SettingsPictureViewController *pictureViewController;
@property(nonatomic,retain)SettingsEmailViewController *settingsEmailViewController;
@property(nonatomic,retain)SettingsExportViewController *exportViewController;
-(IBAction)savePressed;
-(IBAction)backPressed;
-(IBAction)openPressed;
-(IBAction)cancelPressed;

-(void)handleSlider; //a22

@end
