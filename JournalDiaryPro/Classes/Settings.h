///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   Settings.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	18/11/09
// Change reason :  params added
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#define kPasswordEnabled @"PasswordEnabled"
#define kFontName @"FontName"
#define kFontSize @"fontSize"
#define kEmailEnabled @"EmailEnabled"
#define	kEmailType @"EmailType"
#define kEmailAddress @"EmailAddress"
#define kEmailSMTPServer @"EmailSMTPServer"
#define kEmailPassword @"EmailPassword"
#define kPicOfDayEnabled @"PictureOfTheDayEnabled"

#define kAnimateEmoticonEnabled @"AnimateEmoticonEnabled"
#define kGpsEnabled @"GpsEnabled"
#define kDaysGap @"DaysGap" //a22
#define kFaceIDEnabled @"FaceIDEnabled"
#define	kBackgroundPictureEnabled @"BackgroundPictureEnabled" 
#define kCurrentPicture @"CurrentPicture"
#define kPicDate @"PictureDate"
#define kUserName @"UserName"
#define kPicOfDay @"PicOfTheDay"
#define kGlobalDate @"GlobalDate"
@interface Settings : NSObject 
{
	int daysGap; //a22

	BOOL passwordEnabled;
	NSString *passwordStr; 
	NSString *fontName;
	float fontSize;
	BOOL emailEnabled;
	NSString *emailType;
	NSString *emailServerAddress;
	NSString *emailAddress;
	NSString *emailPassword;
	NSString *currentPicture;
	NSString *pictureDate;
	NSString *picOfTheDay;
	NSString *userName;
	NSString *globalDate;
	BOOL picOfDayEnabled;
	BOOL picEnabled;
	BOOL animateEmoticonEnabled;
	BOOL gpsEnabled;
    BOOL faceIDEnabled;
	
}
@property (assign) BOOL passwordEnabled;

@property (assign) int daysGap; //a22


@property(nonatomic,retain) NSString *passwordStr;
@property(nonatomic,retain) NSString *fontName;
@property(assign) float fontSize;
@property(assign)BOOL emailEnabled;
@property(nonatomic,retain)NSString *emailType;
@property(nonatomic,retain)NSString *emailServerAddress;
@property(nonatomic,retain)NSString *emailAddress;
@property(nonatomic,retain)NSString *emailPassword;
@property(nonatomic,retain)NSString *currentPicture;
@property(nonatomic,retain)NSString *pictureDate;
@property(nonatomic,retain)NSString *globalDate;
@property(nonatomic,retain)NSString *userName;
@property(nonatomic,retain)NSString *picOfTheDay;
@property(assign)BOOL picOfDayEnabled;
@property(assign)BOOL picEnabled;


@property BOOL animateEmoticonEnabled;
@property BOOL gpsEnabled;
@property BOOL faceIDEnabled;


-(void)loadSettings;
-(void)storeSettings:(BOOL)passOn;
-(void)storeFontSettings:(NSString *)fName size:(float)fSize;
-(void)storeEmailEnabled:(BOOL)enabled;
-(void)storeEmailType:(NSString *)type;
-(void)storeEmailSettings:(NSString *)address SMTPServer:(NSString*)server password:(NSString *)pwd;
-(void)storePictureName:(NSString *)pic;
-(void)storePictureEnabled:(BOOL)enabled;
-(void)storePictureOfTheDayEnabled:(BOOL)enabled;
-(void)storePictureDate:(NSString *)picDate picture:(NSString *)pic;
-(void)storeFaceIDEnabled:(BOOL)enabled;

-(void)storeAnimateEmoticonEnabled:(BOOL)enabled; 
-(void)storeGpsEnabled:(BOOL)enabled;

-(void)storeDaysGap:(int)value; //a22


-(void)storeUserName:(NSString *)name;
-(void)storeGlobalDate:(NSString *)dateStr;
-(void)initEmail;
-(void) wantToUseGps;
@end
