///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   ChooseEmoticonViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "ChooseEmoticonViewCtlr.h"
#import "AddEntryViewController.h"
#import "Settings.h"
#import	"eDiaryAppDelegate.h"
@implementation ChooseEmoticonViewCtlr
@synthesize contentStr;
@synthesize diaryObj;
@synthesize isEditMode;
@synthesize dateAdded;
@synthesize entryViewController;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        backgrndImaView.frame =CGRectMake(0, -20, 320, 568+40);
     imageView.frame = CGRectMake(0, -20, 320, 568+20);
        
        
    
    }
    
    else
    {
        imageView.frame =CGRectMake(12, 0, 320, 480);
        
        
    }
    

//	UIGraphicsBeginImageContext(CGSizeMake(40.0,40.0));

    
    
//	Sulaba[myThumbNail drawInRect:CGRectMake(0.0, 0.0, 80.0, 80.0)];
	UIGraphicsBeginImageContext(CGSizeMake(40.0,40.0));
	 [[UIImage imageNamed:@"smile.png"] drawInRect:CGRectMake(0.0, 0.0, 40.0, 40.0)];
	UIImage *smile = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(40.0,40.0));
	[[UIImage imageNamed:@"beyond_endurance.png"] drawInRect:CGRectMake(0.0, 0.0, 40.0, 40.0)];
	UIImage *angry = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(40.0,40.0));
	[[UIImage imageNamed:@"rockn_roll.png"] drawInRect:CGRectMake(0.0, 0.0, 40.0, 40.0)];
	UIImage *cool = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(40.0,40.0));
	[[UIImage imageNamed:@"unhappy.png"]  drawInRect:CGRectMake(0.0, 0.0, 40.0, 40.0)];
	UIImage *frown = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(40.0,40.0));
	[[UIImage imageNamed:@"big_smile.png"]  drawInRect:CGRectMake(0.0, 0.0, 40.0, 40.0)];
	UIImage *grin = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(40.0,40.0));
	[[UIImage imageNamed:@"shame.png"]  drawInRect:CGRectMake(0.0, 0.0, 40.0, 40.0)];
	UIImage *innocent = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(40.0,40.0));
	[[UIImage imageNamed:@"greeding.png"]  drawInRect:CGRectMake(0.0, 0.0, 40.0, 40.0)];
	UIImage *kiss = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(40.0,40.0));
	[[UIImage imageNamed:@"the_devil.png"]  drawInRect:CGRectMake(0.0, 0.0, 40.0, 40.0)];
	UIImage *naughty = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(40.0,40.0));
	[[UIImage imageNamed:@"what.png"]  drawInRect:CGRectMake(0.0, 0.0, 40.0, 40.0)];
	UIImage *sealed = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(40.0,40.0));
	[[UIImage imageNamed:@"cry.png"]  drawInRect:CGRectMake(0.0, 0.0, 40.0, 40.0)];
	UIImage *tear = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(40.0,40.0));
	[[UIImage imageNamed:@"bad_smile.png"]  drawInRect:CGRectMake(0.0, 0.0, 40.0, 40.0)];
	UIImage *toungue = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(40.0,40.0));
	[[UIImage imageNamed:@"i_have_no_idea.png"]  drawInRect:CGRectMake(0.0, 0.0, 40.0, 40.0)];	
	UIImage *undecided = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(40.0,40.0));
	[[UIImage imageNamed:@"wicked.png"]  drawInRect:CGRectMake(0.0, 0.0, 40.0, 40.0)];
	UIImage *wink = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(40.0,40.0));
	[[UIImage imageNamed:@"surprise.png"]  drawInRect:CGRectMake(0.0, 0.0, 40.0, 40.0)];
	UIImage *wow = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	UIGraphicsBeginImageContext(CGSizeMake(40.0,40.0));
	[[UIImage imageNamed:@"pretty_smile.png"]  drawInRect:CGRectMake(0.0, 0.0, 40.0, 40.0)];
	UIImage *yay = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
/*	
	UIImage *smile = [UIImage imageNamed:@"smile.png"];
	UIImage *angry = [UIImage imageNamed:@"beyond_endurance.png"];
	UIImage *cool = [UIImage imageNamed:@"rockn_roll.png"];
	UIImage *frown = [UIImage imageNamed:@"unhappy.png"];
	UIImage *grin = [UIImage imageNamed:@"big_smile.png"];
	UIImage *innocent = [UIImage imageNamed:@"shame.png"];
	UIImage *kiss = [UIImage imageNamed:@"greeding.png"];
	UIImage *naughty = [UIImage imageNamed:@"the_devil.png"];
	UIImage *sealed = [UIImage imageNamed:@"what.png"];
	UIImage *tear = [UIImage imageNamed:@"cry.png"];	
	UIImage *toungue = [UIImage imageNamed:@"bad_smile.png"];	
	UIImage *undecided = [UIImage imageNamed:@"i_have_no_idea.png"];	
	UIImage *wink = [UIImage imageNamed:@"wicked.png"];
	UIImage *wow = [UIImage imageNamed:@"surprise.png"];	
	UIImage *yay = [UIImage imageNamed:@"smile.png"];
*/	
	UIView *emptyView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 40.0f, 40.0f)]; 
	UIImageView *smileView = [[UIImageView alloc] initWithImage:smile];
	UIImageView *angryView = [[UIImageView alloc] initWithImage:angry];
	UIImageView *coolView = [[UIImageView alloc] initWithImage:cool];
	UIImageView *frownView = [[UIImageView alloc] initWithImage:frown];
	UIImageView *grinView = [[UIImageView alloc] initWithImage:grin];
	UIImageView *innocentView = [[UIImageView alloc] initWithImage:innocent];
	UIImageView *kissView = [[UIImageView alloc] initWithImage:kiss];
	UIImageView *naughtyView = [[UIImageView alloc] initWithImage:naughty];
	UIImageView *sealedView = [[UIImageView alloc] initWithImage:sealed];
	UIImageView *tearView = [[UIImageView alloc] initWithImage:tear];
	UIImageView *toungueView = [[UIImageView alloc] initWithImage:toungue];
	UIImageView *undecidedView = [[UIImageView alloc] initWithImage:undecided];
	UIImageView *winkView = [[UIImageView alloc] initWithImage:wink];
	UIImageView *wowView = [[UIImageView alloc] initWithImage:wow];
	UIImageView *yayView = [[UIImageView alloc] initWithImage:yay];
	
	 /*
	
	// lets essentially make a copy of the selected image.
	//	UIImage *myThumbNail    = [[UIImage alloc] initWithData:pngImage];
	// begin an image context that will essentially "hold" our new image
	UIGraphicsBeginImageContext(CGSizeMake(80.0,80.0));
	// now redraw our image in a smaller rectangle.
	[myThumbNail drawInRect:CGRectMake(0.0, 0.0, 80.0, 80.0)];
	//[myThumbNail release];
	// make a "copy" of the image from the current context
	UIImage *newImage    = UIGraphicsGetImageFromCurrentImageContext();
	
	// create a new view to place on our screen (OPTIONAL-- testing)
	UIImageView *thumbView    = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 80.0, 80.0)];
	thumbView.image    = newImage;
	[tempView addSubview:thumbView];
*/	
	
	NSArray *emoArray = [[NSArray alloc] initWithObjects:emptyView,smileView,angryView,coolView,frownView,grinView,innocentView,kissView,naughtyView,sealedView,tearView,toungueView,undecidedView,winkView,wowView,yayView,nil];
	NSArray *disArray = [[NSArray alloc] initWithObjects:@"No Emoticon",@"happy",@"angry",@"cool",@"sad",@"grin",@"innocent",@"romantic",@"naughty",@"mum",@"crying",@"kidding",@"undecided",@"wicked",@"surprised",@"blushed",nil];
	emoticonArray = [emoArray copy];
	[emoArray release];
	descriptionArray = [disArray copy];
	[disArray release];
	[emoticonPicker selectRow:0 inComponent:0 animated:YES];
	[emptyView release];
	[smileView release];
	[angryView release];
	[coolView release];
	[frownView release];
	[grinView release];
	[innocentView release];
	[kissView release];
	[naughtyView release];
	[sealedView release];
	[tearView release];
	[toungueView release];
	[undecidedView release];
	[winkView release];
	[wowView release];
	[yayView release];
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

-(IBAction)donePressed{
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	AddEntryViewController *entryView = [[AddEntryViewController alloc] initWithNibName:@"AddEntryViewController" bundle:nil];
	entryView.emoticonNo = [NSString stringWithFormat:@"%d",[emoticonPicker selectedRowInComponent:0]];
	entryView.contentStr = self.contentStr;
	entryView.isEditMode = self.isEditMode;
	entryView.oldDate = self.dateAdded;
	entryView.timeSelected = self.dateAdded;
	self.entryViewController = entryView;
	[self.view addSubview:self.entryViewController.view];
	[entryView release];
	[UIView commitAnimations];
}

- (void)dealloc {
	[emoticonArray release];
	[descriptionArray release];
    [super dealloc];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
	return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
//	if(component ==kEmoticon){
		return [emoticonArray count];
//	}
	
	/*else if(component == kDescription){
		return [descriptionArray count];
	}else return 0;*/
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row
		   forComponent:(NSInteger)component{
//	if(component == kDescription){
		return [descriptionArray objectAtIndex:row];
//	}else return @"";
	
}



- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
	
	UIView *tempView = [[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 40.0f, 40.0f) ] autorelease];
	UILabel *tmpLbl = [[UILabel alloc] initWithFrame:CGRectMake(-100.0f, 10.0f, 100.0f, 20.0f)];
	tmpLbl.text = [descriptionArray objectAtIndex:row];
	tmpLbl.textAlignment = UITextAlignmentCenter;
	tmpLbl.backgroundColor = [UIColor clearColor];
	[tempView addSubview:tmpLbl];
	
//	UIView *emoticonView = (UIView*)[emoticonArray objectAtIndex:row];
//	emoticonView.center = CGPointMake(20.0,30.0);
    
   // UIImageView *temp =[emoticonArray objectAtIndex:row];
    //temp.backgroundColor = [UIColor redColor];
	//[tempView addSubview:temp];
    
    
    
    
    // self.myImages is an array of UIImageView objects
    UIImageView * myView = [emoticonArray objectAtIndex:row];
    
    // first convert to a UIImage
    UIGraphicsBeginImageContextWithOptions(myView.bounds.size, NO, 0);
    
    [myView.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    // then convert back to a UIImageView and return it
    UIImageView *emoImage= [[UIImageView alloc] initWithImage:image];
    
	[tempView addSubview:emoImage];
    
	//	tempView.center = CGPointMake(20.0,30.0);
//	[emoticonView release];
	[tmpLbl release];
	return tempView;
}

@end
