///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   eDiaryAppDelegate.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#import "eDiaryAppDelegate.h"
#import "PasswordViewController.h"
#import "HomeViewController.h"
#import "SettingsViewController.h"
#import "HelpViewController.h"
#import "Settings.h"
#import "Secure.h"
#import "ActivityIndicator.h"
#import "Picture.h"
#import "Utility.h"
#import "Constants.h"

 @implementation eDiaryAppDelegate

@synthesize window;
@synthesize viewController;
@synthesize passwordViewController;
@synthesize homeViewController;
@synthesize settingsViewController;
@synthesize Latitude,Longitude,string_global;

static UIImage *homeImg;
static NSString *calendar; 
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
   
    // window.frame = [UIScreen mainScreen].bounds;
     self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
	[self createEditableCopyOfDatabaseIfNeeded];
    // Override point for customization after app launch    
    //[window addSubview:viewController.view];
	Settings *settings = [[Settings alloc] init];
//	[settings loadSettings];
	
	[settings storeGpsEnabled:NO];
	[settings loadSettings];
	
	
	if([settings.fontName isEqualToString:@""] || settings.fontName == nil){
	[settings storeFontSettings:@"Trebuchet MS" size:21.0];
	}
	if(settings.userName == nil){
		[settings storeUserName:@""];
	}
	if(settings.emailAddress == nil){
		[settings initEmail];
	}
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy-MM-dd"];
	[settings storeGlobalDate:[formatter stringFromDate:[NSDate date]]];

	//[formatter release];
	
    Secure *secure = [[Secure alloc] init];
	
	
	//[secure release];

	
    BOOL g=YES ; //As build now needs to work for all phone not only 3GS	[Utility IsiPhone3G];
	
	
	//NSString *devicetype=[[UIDevice currentDevice] model];
	
	if(g)
	{
		locationMgr = [[CLLocationManager alloc]init];
		[locationMgr setDelegate:self];
		[locationMgr setDesiredAccuracy:kCLLocationAccuracyBest];
        if(IS_OS_8_OR_LATER) {
            [locationMgr requestWhenInUseAuthorization];
        }

		[locationMgr startUpdatingLocation];
		[self setupTimer];
		
	}	
	else if([CLLocationManager locationServicesEnabled])//(!locationMgr.locationServicesEnabled)
	{
		UIAlertView * Alert = [[[UIAlertView alloc] initWithTitle:@"" message:@"Your GPS service seems disable. You will not be able to locate your self." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
		[Alert show];
		
	}
	
	
	
	Events *pending_obj = [[Events alloc]init];
	NSString *currentDate;
	NSString *toDate;
	//Settings *settings = [[Settings alloc] init];
	//[settings loadSettings];
	int daysGap = settings.daysGap;
	[formatter setDateFormat:@"YYYY-MM-dd"];
	//NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	//[formatter setDateFormat:@"YYYY-MM-dd"];
	currentDate = [formatter stringFromDate:[NSDate date]];
	NSLog(@"\ncurrent date= %@\n",currentDate);
	NSTimeInterval secondsPerDay = 24 * 60 * 60 * daysGap;
	toDate =  [formatter stringFromDate:[[NSDate date]addTimeInterval:secondsPerDay]];
	NSLog(@"\nto date= %@\n",toDate);
	string_global=[[NSString alloc] init];
	NSMutableArray *ar=[pending_obj getEventsInDays:currentDate to:toDate];

	
	if([ar count]>1)
	{
		for(int i=0; i<[ar count];i++)
		{
			
			NSString *st=[[NSString  alloc] initWithString:[NSString stringWithFormat:@"\n%@   %@\n",[[ar objectAtIndex:i] descriptionStr],[[ar objectAtIndex:i] datestringevent]]];
			string_global=[string_global stringByAppendingString:st];
			
			//NSString *st=[[pending_obj 
		}
	UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Entries" message:string_global delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
	[alert show];
	[alert release];
	}
	//[ar release];
	[pending_obj release];
		[settings release];
//	[currentDate release];
    NSArray *windows = [[UIApplication sharedApplication] windows];
    for(UIWindow *window in windows) {
        if(window.rootViewController == nil){
            UIViewController* vc = [[UIViewController alloc]initWithNibName:nil bundle:nil];
            window.rootViewController = vc;
        }
    }
    if([secure isAppSecure]){
       // window.rootViewController = passwordViewController;
   //     [window addSubview:passwordViewController.view];

          [self.window setRootViewController:passwordViewController];
         [self.window addSubview:passwordViewController.view];

    }else {
       // window.rootViewController = homeViewController;
        [self.window setRootViewController:homeViewController];

     [self.window addSubview:homeViewController.view];

    }
    
    [self.window makeKeyAndVisible];
     return YES;
}



-(void)applicationDidEnterBackground:(UIApplication *)application
{
	Events *pending_obj = [[Events alloc]init];
	NSString *currentDate;
	NSString *toDate;
	
	
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	int daysGap = settings.daysGap;
	
	
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"YYYY-MM-dd"];
	currentDate = [formatter stringFromDate:[NSDate date]];
	NSLog(@"\ncurrent date= %@\n",currentDate);
	NSTimeInterval secondsPerDay = 24 * 60 * 60 * daysGap;
	toDate =  [formatter stringFromDate:[[NSDate date]addTimeInterval:secondsPerDay]];
	NSLog(@"\nto date= %@\n",toDate);
	
	NSMutableArray *ar=[pending_obj getEventsInDays:currentDate to:toDate];
	
	[[UIApplication sharedApplication]setApplicationIconBadgeNumber:[ar count]];
	//[ar release];
	[pending_obj release];
	[formatter release];
		[settings release];
	exit(0);
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation 
		   fromLocation:(CLLocation *)oldLocation
{
	//[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	CLLocationCoordinate2D newLoc = [newLocation coordinate];
	if (newLocation.horizontalAccuracy < 0) return;
	if(newLoc.latitude!=0 && newLoc.longitude!=0)
	{
		self.Latitude=newLoc.latitude;
		self.Longitude=newLoc.longitude;	
	}
	
	//[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;	
	
	
}
- (void)setupTimer
{
	Locationtimer = [NSTimer scheduledTimerWithTimeInterval:(8) target:self selector:@selector(stopLocatingAutomatic:) userInfo:nil repeats:YES];
}


- (void)stopLocatingAutomatic:(NSTimer *)aTimer
{
	[locationMgr stopUpdatingLocation];	
	[Utility SetLatitudeLongitude:self.Latitude :self.Longitude];
	[Locationtimer invalidate];
	Locationtimer=nil;
	[locationMgr startUpdatingLocation];
}


+(void) showMessage:(NSString *)message title:(NSString *)title {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
	[alert show];
	[alert release];
}

- (void)createEditableCopyOfDatabaseIfNeeded {
	
	//	//NSLog(@"Creating editable copy of database");
	// First, test for existence.
	BOOL success;
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"eDiary.sqlite"];
	//	//NSLog(@"In Creating : %@",writableDBPath);
	success = [fileManager fileExistsAtPath:writableDBPath];
	if (success) return;
//	////NSLog(@"DB does not exist");
	// The writable database does not exist, so copy the default to the appropriate location.
	NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"eDiary.sqlite"];
	success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
	if (!success) {
		NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
	}
}
+(void)setHomeImage:(UIImage *)img{
	homeImg = nil;
	homeImg = img;
}
+(UIImage *)getHomeImage{
	return homeImg;
}
+(void)setCalendarName:(NSString *)nameStr{
	calendar = nameStr;
}
+(NSString *)getCalendarName{
	return calendar;
}
- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}



@end
