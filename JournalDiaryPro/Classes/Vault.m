///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   Vault.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "Vault.h"
#import "DBSqlite.h"
#import "Sqlite.h"

@implementation Vault
@synthesize keyStr;
@synthesize valueStr;
-(void)insert:(NSString *)keyName value:(NSString *)keyValue{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return;
	sqlite = dbSqlite.sqlite;	
	[sqlite executeNonQuery:@"INSERT INTO safety_locker(key_name,key_value) VALUES (?,?);",keyName,keyValue];
	
	[dbSqlite release];
	
}
-(void)updateSecret:(NSString *)keyName value:(NSString *)keyValue oldKeyName:(NSString*)oldKey{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return;
	sqlite = dbSqlite.sqlite;	
	[sqlite executeNonQuery:@"UPDATE safety_locker set key_name = ?,key_value = ? where key_name = ?;",keyName,keyValue,oldKey];
	
	[dbSqlite release];
}
-(void)deleteSecret:(NSString *)keyName{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return;
	sqlite = dbSqlite.sqlite;
	[sqlite executeNonQuery:@"DELETE from safety_locker where key_name = ?;",keyName];
	[dbSqlite release];
}
-(NSArray*)getAllKeys{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select key_name from safety_locker;"];	
	return results;		
}
-(NSArray*)getData{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select * from safety_locker;"];	
	return results;		
}
@end
