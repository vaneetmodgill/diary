///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   HomeViewController.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MobileCoreServices/UTCoreTypes.h>
@class EAGLView;
@class SettingsViewController;
@class HelpViewController;
@class ActivityIndicator;
@class EventsViewController;
@class DiaryEntryViewController;
@class VoiceMemoViewController;
@class VaultPasswordViewController;
@class LockerViewController;
@class VideoMemoViewController;
@interface HomeViewController : UIViewController<UINavigationControllerDelegate, UIAlertViewDelegate, UIImagePickerControllerDelegate> {
	SettingsViewController *settingsViewController;
	HelpViewController *helpViewController;
	IBOutlet UIImageView *imageView;
	ActivityIndicator *indicator;
	
	UIAlertView *alerts;
	
	EventsViewController *eventsViewController;
	DiaryEntryViewController *diaryEntryViewController;
	VoiceMemoViewController *voiceMemoViewController;
	VaultPasswordViewController *vault;
	LockerViewController *locker;
	VideoMemoViewController *videoMemoViewController;
	BOOL isautorotate;
	EAGLView *glView;

    IBOutlet UIImageView *backgroundImageView ;
}
@property (nonatomic, retain) IBOutlet EAGLView *glView;

@property BOOL isautorotate;
@property(nonatomic,retain) SettingsViewController *settingsViewController;
@property(nonatomic,retain) HelpViewController *helpViewController;
@property(nonatomic,retain)DiaryEntryViewController *diaryEntryViewController;
@property(nonatomic,retain)EventsViewController *eventsViewController;
@property(nonatomic,retain)VoiceMemoViewController *voiceMemoViewController;
@property(nonatomic,retain)VaultPasswordViewController *vault;
@property(nonatomic,retain)LockerViewController *locker;
@property(nonatomic,retain)	VideoMemoViewController *videoMemoViewController;


-(IBAction)diaryPressed;
-(IBAction)memoPressed;
-(IBAction)eventPressed;
-(IBAction)lockerPressed;
-(IBAction)settingsPressed;
-(IBAction)helpPressed;
-(IBAction)timeLinePressed;
@end
