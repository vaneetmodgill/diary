//
//  AddVideoMemo.m
//  Journal Diary
//
//  Created by seasia on 15/09/10.
//  Copyright 2010 Sulaba Inc. All rights reserved.
//
#import "eDiaryAppDelegate.h"
#import "AddVideoMemo.h"
#import "VideoMemoViewController.h"
#import "VoiceMemo.h"
#import "ShowVideoController.h"
@implementation AddVideoMemo
@synthesize videoDate,filename,tempPath,videoMemoViewController,videoData;
@synthesize textfield,iseditmode,old_date,old_time,istableviewclicked,showVideoController;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
		self.filename=self.textfield.text;
	[self.textfield removeFromSuperview];
	[textfield resignFirstResponder];
	BOOL success;
	NSString *st=[NSString stringWithFormat:@"%@.MOV",filename];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *documentPath = [documentsDirectory stringByAppendingPathComponent:st];	
	NSLog(@"document path= %@\n",documentPath);
	
	self.tempPath=documentPath;
	success = [self.videoData writeToFile:documentPath atomically:YES];
	if(success)
	{
		NSLog(@"oye!! video successfully saved on iphone");
	}
	//BOOL success;
	
	VoiceMemo *voiceMemo = [[VoiceMemo alloc] init];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"YYYY-MM-dd"];
	voiceMemo.dateStr = [formatter stringFromDate:self.videoDate];
	[formatter setDateStyle:NSDateFormatterNoStyle];
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	voiceMemo.timeStr = [formatter stringFromDate:self.videoDate];
	//NSLog(@"time %@",voiceMemo.timeStr);
	voiceMemo.video_title = self.textfield.text;
	voiceMemo.fileName = self.tempPath;
	if(!self.iseditmode)
	{
		[voiceMemo insertVideo:voiceMemo.dateStr time:voiceMemo.timeStr title:voiceMemo.video_title filename:voiceMemo.fileName];
	}
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	VideoMemoViewController *voiceMemoView = [[VideoMemoViewController alloc] initWithNibName:@"VideoMemoViewController" bundle:nil];
	self.videoMemoViewController = voiceMemoView;
	[self.view addSubview:self.videoMemoViewController.view];
	[voiceMemo release];
	[voiceMemoView release];
	[UIView commitAnimations];
	
	return YES;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        imageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        
        
        self.view.bounds=CGRectMake(0, 0, 320, 568);
        imageView.frame=CGRectMake(0, 0, 320, 568);
        
        
        backgroundImageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        backgroundImageView.frame = CGRectMake(0, -20, 320, 568+40);

        
        NSLog(@"Dimensions in potrait %f  \n %f",self.view.frame.size.width ,self.view.frame.size.height);
        
        NSLog(@"\n\n\n Bounds in potrait %f  \n %f",self.view.bounds.size.width ,self.view.bounds.size.height);
        
        
    }
    else
    {
        
        imageView.image=[UIImage imageNamed:@"bg6.png"];
        
        self.view.bounds=CGRectMake(0, 0, 320, 460);
        imageView.frame=CGRectMake(0, 0, 320, 460);
    }
    

    
    NSLog(@"Add Video Memo Size  %f",self.view.frame.size.height);
    
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	dateLabel.text = [formatter stringFromDate:self.videoDate];
	[formatter release];
	
	
	textfield = [[UITextField alloc] initWithFrame:CGRectMake(20, 120, 190, 50)];
	textfield.borderStyle = UITextBorderStyleRoundedRect;
	textfield.textColor = [UIColor blackColor];
	textfield.placeholder = @"Enter Title";
	textfield.returnKeyType = UIReturnKeyDone;
	textfield.font = [UIFont boldSystemFontOfSize:17.0];
	[textfield becomeFirstResponder];
	//tf_country_code.returnKeyType = UIReturnKeyDefault;
	textfield.keyboardType = UIKeyboardTypeDefault;
	textfield.delegate = self;
	textfield.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	
    NSLog(@"Add Video Memo Size  End  %f",self.view.frame.size.height);
    
	
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}



-(IBAction)recordPressed
{
    
    NSLog(@"Checking if any issue with size    %f   ",self.view.frame.size.height);
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];

    
    if (screenBounds.size.height == 568)
    {
        
        self.view.frame = CGRectMake(self.view.frame.origin.x, self
                                     .view.frame.origin.y, self.view.frame.size.width, 568);
    }
    
    self.view.bounds =CGRectMake(self.view.frame.origin.x, self
                                 .view.frame.origin.y, self.view.frame.size.width, 568);
    
    
    self.view.frame = CGRectMake(self.view.frame.origin.x, self
                                 .view.frame.origin.y, self.view.frame.size.width, 568);
    
    NSLog(@"\n\n\nChecking if any issue with size    %f   ",self.view.frame.size.height);

    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Warning!!" message:@"Sorry..! Video Recording is not supported for this device" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        return;
    }
   
    
    
	/*NSString *devicetype=[[UIDevice currentDevice] model];
	NSLog([NSString stringWithFormat:@"dt%@",devicetype]);
	NSString *devices=[[UIDevice currentDevice] name];
	NSLog([NSString stringWithFormat:@"name%@",devices]);
	
	NSString *devicess=[[UIDevice currentDevice] systemName];
	NSLog([NSString stringWithFormat:@"sname%@",devicess]);
	
	NSString *devicesvs=[[UIDevice currentDevice] systemVersion];
	NSLog([NSString stringWithFormat:@"sversion%@",devicesvs]);
	*/

	//if([devicetype isEqualToString:@"iPhone"] )
	//{
		UIImagePickerController *ipc = [[UIImagePickerController alloc] init];
    
//    ipc.view.frame = CGRectMake(self.view.frame.origin.x, self
//                                .view.frame.origin.y, self.view.frame.size.width, 568);
		
		ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
		
		NSArray *sourceTypes = [UIImagePickerController availableMediaTypesForSourceType:ipc.sourceType];
		if (![sourceTypes containsObject:(NSString *)kUTTypeMovie ])
		{
			UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Warning!!" message:@"Sorry..! Video Recording is not supported for this device" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
			[alert show];
			[alert release];
			
		}
		else {
			ipc.mediaTypes =[UIImagePickerController availableMediaTypesForSourceType:ipc.sourceType];     
			ipc.cameraCaptureMode = UIImagePickerControllerCameraCaptureModeVideo;
			ipc.delegate = self;
            
            UIViewController *abc = [[UIViewController alloc] init];
            [self.view addSubview:abc.view];
            
            [abc presentViewController:ipc animated:YES completion:^{
                
            }];
		}

    
	
	/*}
	else 
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Warning!!" message:@"Feature not available on your device" 
													 delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}*/
	
}
-(IBAction)backPressed{
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	VideoMemoViewController *voiceMemoView = [[VideoMemoViewController alloc] initWithNibName:@"VideoMemoViewController" bundle:nil];
	self.videoMemoViewController = voiceMemoView;
	[self.view addSubview:self.videoMemoViewController.view];
	[voiceMemoView release];
	[UIView commitAnimations];
}
-(IBAction)playPausePressed
{
	ShowVideoController *showMemoView = [[ShowVideoController alloc] initWithNibName:@"ShowVideoController" bundle:nil];
	showMemoView.filepath=self.filename;
	//showMemoView.istableviewclicked=YES;
	showMemoView.olddate=self.old_date;
	showMemoView.oldtime=self.old_time;
	//DiaryObject *obj = (DiaryObject*)[memosArray objectAtIndex:indexPath.row];
	//showMemoView.diaryObj = obj;
	self.showVideoController = showMemoView;
	[self.view addSubview:self.showVideoController.view];
	[showMemoView release];
}
-(IBAction)savePressed
{
	BOOL success;
	
	VoiceMemo *voiceMemo = [[VoiceMemo alloc] init];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"YYYY-MM-dd"];
	voiceMemo.dateStr = [formatter stringFromDate:self.videoDate];
	[formatter setDateStyle:NSDateFormatterNoStyle];
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	voiceMemo.timeStr = [formatter stringFromDate:self.videoDate];
	//NSLog(@"time %@",voiceMemo.timeStr);
	voiceMemo.video_title = self.textfield.text;
	voiceMemo.fileName = self.tempPath;
	if(!self.iseditmode)
	{
		if([voiceMemo.video_title isEqualToString:@""] || voiceMemo.video_title !=nil)
	[voiceMemo insertVideo:voiceMemo.dateStr time:voiceMemo.timeStr title:voiceMemo.video_title filename:voiceMemo.fileName];
	}
	else {
		NSString *st=[NSString stringWithFormat:@"%@.MOV",filename];
		
		NSFileManager *fileManager = [NSFileManager defaultManager];
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *documentPath = [documentsDirectory stringByAppendingPathComponent:st];	
		NSLog(@"document path= %@\n",documentPath);
		
		self.tempPath=documentPath;
		success = [self.videoData writeToFile:documentPath atomically:YES];
		[voiceMemo updatevideo:self.old_date time:self.old_time title:voiceMemo.video_title filename:self.tempPath];
		
		 //insertVideo:voiceMemo.dateStr time:voiceMemo.timeStr title:voiceMemo.video_title filename:voiceMemo.fileName];

		
	}

	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	VideoMemoViewController *voiceMemoView = [[VideoMemoViewController alloc] initWithNibName:@"VideoMemoViewController" bundle:nil];
	self.videoMemoViewController = voiceMemoView;
	[self.view addSubview:self.videoMemoViewController.view];
	[voiceMemoView release];
	[voiceMemo release];
	[UIView commitAnimations];
	
	
	
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setBool:NO forKey:@"autorotateenabled"];
	return (toInterfaceOrientation == UIInterfaceOrientationPortrait);	
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{	
	NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
	
	if ([mediaType isEqualToString:@"public.image"])
	{
		[picker dismissModalViewControllerAnimated:YES];
		
	}
	
	
	else if ([mediaType isEqualToString:@"public.movie"])
	{
		NSURL *aURL = [info objectForKey:UIImagePickerControllerMediaURL];
		NSLog(@"found a video :- %@",aURL);
		[picker dismissModalViewControllerAnimated:YES];
		
		self.videoData = [NSData dataWithContentsOfURL:aURL];
		
		if(self.iseditmode==NO)
		{
		[self.view addSubview:textfield];
		}
		
	
		
		}
	
	
	
	
}
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[tempPath release];
	[videoData release];
	[textfield release];
    [super dealloc];
}


@end
