///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:	ActivityIndicator
/// Overview	:	Implementation for ActivityIndicator class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :	Initial version
//
// (c) COPYRIGHT 2009-2010 ,  ALL RIGHTS RESERVED.
// 
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND 
// CONSTITUTES TRADE SECRETS OF ,  (", LLC").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE 
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY ,  IF YOU ARE NOT 
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO , LLC 
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE 
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY 
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "ActivityIndicator.h"


@implementation ActivityIndicator
@synthesize window;
static ActivityIndicator *activityIndicator;

- (id) init
{
	self = [super init];
	if (self != nil) {
		[[NSBundle mainBundle] loadNibNamed:@"ActivityIndicator" owner:self options:nil];
	}
	
	[window resignKeyWindow];
	window.hidden = YES;
	
	return self;
}

+ (ActivityIndicator *)sharedActivityIndicator {
	
	if (!activityIndicator)
	{	
		activityIndicator = [[ActivityIndicator alloc] init];
		activityIndicator.window.windowLevel = UIWindowLevelAlert;
	}
	
	return activityIndicator;
}

- (void)show:(NSString *)msg {
	//NSLog(@"show called");
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	label.text = msg;
	[window makeKeyAndVisible];
	window.hidden = NO;
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	[pool release];
}

- (void)hide {
	////NSLog(@"hide called");
    dispatch_async(dispatch_get_main_queue(), ^{
	[window resignKeyWindow];
	window.hidden = YES;
	[label setTextColor:[UIColor whiteColor]];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        });
}

- (void)setText:(NSString *)msg {
	label.text = msg;
//	[label setTextColor:[UIColor colorWithRed:0.122 green:0.518 blue:1.0 alpha:1.0]];
}

@end
