///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   DBSqlite.m
/// Overview	:   implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 : Karthick Ramalingam
// Change date	 : 12-06-09
// Change reason : initial version	
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
// 
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND 
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE 
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT 
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba 
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE 
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY 
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "DBSqlite.h"
#import <Foundation/Foundation.h>


@implementation DBSqlite

@synthesize  sqlite;


- (id)init {
    if (self = [super init]) {
       	sqlite = [[Sqlite alloc] init];
    }
    return self;
}

- (void)dealloc {
    [sqlite release];
    [super dealloc];
}

-(BOOL)getDBConnection{ 	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"eDiary.sqlite"];
	////NSLog(@"In DBSqlite :%@",path);
	// Open the database. The database was prepared outside the application.
	if ([sqlite open:path]) {

	//  //NSLog(@"Database Successfully Opened :)");
		return YES;

	} else {
	  //////NSLog(@"Error in opening database :(");
	  return NO;
	}	
}
@end