///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   Secure.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>


@interface Secure : NSObject {
	NSString *appPassword;
	NSString *lockerPassword;
	NSString *secretQuestion;
	NSString *secretAnswer;
	NSString *vaultQuestion;
	NSString *vaultAnswer;
}
-(void)insert:(NSString*)pass lockerPass:(NSString *)lockerPasswd question:(NSString *)ques answer:(NSString *)ans vaultQ:(NSString*)vaultQues vaultA:(NSString *)vaultAns;
-(void)updatePassword:(NSString *)password;
-(void)updateVaultPassword:(NSString *)password;
-(void)updateQuestion:(NSString *)qstn answer:(NSString *)ansr;
-(NSString*)getPassword;
-(NSString*)getVaultPassword;
-(NSString *)getQuestion;
-(NSString *)getAnswer;
-(void)updateAppProtection:(BOOL)enabled;
-(void)updateVaultProtection:(BOOL)enabled;
-(BOOL)isAppSecure;
-(BOOL)isVaultSecure;
-(void)updateVaultQuestion:(NSString *)qstn answer:(NSString *)ansr;
-(NSString *)getVaultQuestion;
-(NSString *)getVaultAnswer;
-(int)getCount;
@end
