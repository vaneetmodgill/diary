//
//  AddVideoMemo.h
//  Journal Diary
//
//  Created by seasia on 15/09/10.
//  Copyright 2010 Sulaba Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/UTCoreTypes.h>
@class VideoMemoViewController;
@class VoiceMemo;
@class ShowVideoController;
@interface AddVideoMemo : UIViewController <UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>{
	IBOutlet UIButton *recordPauseButton;
	IBOutlet UIButton *playPauseButton;
	IBOutlet UIButton *clearBufferButton;
	IBOutlet UIButton *stopButton;
	IBOutlet UIButton *saveButton;
	IBOutlet UIButton *backButton;
	IBOutlet UILabel *timerLabel;
	IBOutlet UILabel *dateLabel;
	IBOutlet UILabel *statusLabel;
	IBOutlet UIImageView *imageView;
	UIImagePickerController *imagepicker;
	NSDate *videoDate;
	UITextField *textfield;
	NSString *filename;
	NSString *tempPath;
	NSData *videoData;
	VideoMemoViewController *videoMemoViewController;
	BOOL iseditmode;
	NSString *old_date;
		NSString *old_time;
	BOOL istableviewclicked;
	ShowVideoController *showVideoController;
    
    IBOutlet UIImageView *backgroundImageView;

    
    
}
@property(nonatomic,retain)	ShowVideoController *showVideoController;
@property 	BOOL istableviewclicked;
@property(nonatomic,retain)NSString *old_date;
@property(nonatomic,retain)NSString *old_time;
@property BOOL iseditmode;
@property(nonatomic,retain)UITextField *textfield;
@property(nonatomic,retain)NSData *videoData;
@property(nonatomic,retain)VideoMemoViewController *videoMemoViewController;
@property(nonatomic,retain)NSString *filename;
@property(nonatomic,retain)NSString *tempPath;
@property(nonatomic,retain)NSDate *videoDate;
@property(nonatomic,retain)IBOutlet UIButton *recordPauseButton;
@property(nonatomic,retain)IBOutlet UIButton *playPauseButton;
@property(nonatomic,retain)IBOutlet UIButton *clearBufferButton;
@property(nonatomic,retain)IBOutlet UIButton *stopButton;
@property(nonatomic,retain)IBOutlet UIButton *saveButton;
@property(nonatomic,retain)IBOutlet UIButton *backButton;
@property(nonatomic,retain)IBOutlet UILabel *timerLabel;
@property(nonatomic,retain)IBOutlet UILabel *dateLabel;
@property(nonatomic,retain)IBOutlet UILabel *statusLabel;
@property(nonatomic,retain)IBOutlet UIImageView *imageView;
-(IBAction)recordPausePressed;
-(IBAction)playPausePressed;
-(IBAction)stopPressed;
-(IBAction)savePressed;
-(IBAction)backPressed;
-(IBAction)clearBufferPressed;
-(IBAction)recordPressed;
@end
