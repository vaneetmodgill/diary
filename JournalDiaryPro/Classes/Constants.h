/*

File: Constants.h
Abstract: Common constants across source files (screen coordinate consts, etc.)

Version: 1.7

// these are the various screen placement constants used across all the UIViewControllers
*/

// padding for margins
#define kLeftMargin				20.0
#define kTopMargin				20.0
#define kRightMargin			20.0
#define kBottomMargin			20.0
#define kTweenMargin			10.0

// control dimensions
#define kStdButtonWidth			106.0
#define kStdButtonHeight		40.0
#define kSegmentedControlHeight 40.0
#define kPageControlHeight		20.0
#define kPageControlWidth		160.0
#define kSliderHeight			7.0
#define kSwitchButtonWidth		94.0
#define kSwitchButtonHeight		27.0
#define kTextFieldHeight		30.0
#define kSearchBarHeight		40.0
#define kLabelHeight			20.0
#define kProgressIndicatorSize	40.0
#define kToolbarHeight			40.0
#define kUIProgressBarWidth		160.0
#define kUIProgressBarHeight	24.0
#define kUITableViewRowHeight	55.0

// specific font metrics used in our text fields and text views
#define kFontName				@"Arial"
#define kTextFieldFontSize		18.0
#define kTextViewFontSize		18.0

// UITableView row heights
#define kUIRowHeight			50.0
#define kUIRowLabelHeight		22.0

// table view cell content offsets
#define kCellLeftOffset			8.0
#define kCellTopOffset			12.0

//component fill up offsets
#define kMood					@"MOOD"
#define kHair					@"HAIR"
#define kUpperInterior			@"UPPER INTERIOR"
#define kUpperExterior			@"UPPER EXTERIOR"
#define kLowerInterior			@"LOWER INTERIOR"
#define kLowerExterior			@"LOWER EXTERIOR"
#define kShoes					@"SHOES"
#define kGlasses				@"GLASSES"
#define kHandbag				@"HANDBAGS"
#define kAccessories			@"ACCESSORIES"
#define kPet					@"PETS"
#define kHat					@"HATS"
#define kBackground				@"BACKGROUNDS"

#define kBoy					@"BOY"
#define kGirl					@"GIRL"

//Project wide
#define kFIleName				@"data.plist"
#define kAvatarCreationDate		@"AvatarCreationDate"
#define kAvatarId				@"AvatarId"

///////////////////////////////////////////////////////////////////////////////////////////////////
// your Facebook application's API key here:
#define kApiKey					@"968402084027c8b76a189e3e698e504a"

// Enter either your API secret or a callback URL (as described in documentation):
#define kApiSecret				@"54a57abafa6196356ffc27c6330ce4ac"



///////////////////////////////////////////////////////////////////////////////////////////////////