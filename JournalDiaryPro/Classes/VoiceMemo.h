///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   VoiceMemo.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>


@interface VoiceMemo : NSObject {
	NSString *dateStr;
	NSString *timeStr;
	NSString *title;
	NSString *fileName;
	NSString *video_title;
}
@property(nonatomic,retain)NSString *video_title;
@property(nonatomic,retain)NSString *dateStr;
@property(nonatomic,retain)NSString *timeStr;
@property(nonatomic,retain)NSString *title;
@property(nonatomic,retain)NSString *fileName;
-(BOOL)insert;
-(NSArray*)getData:(NSString *)memoDate;
-(BOOL)updateMemo:(NSString *)fName;
-(BOOL)deleteMemo:(NSString *)memoName;
-(NSArray*)getDatesInMonth:(NSString *)fromDate	to:(NSString *)toDate;
-(void)insertVideo:(NSString*)par_date time:(NSString*)par_time title:(NSString*)par_title filename:(NSString*)par_filename;
//-(BOOL)checkDuplicate;
-(NSMutableArray*)selectAllVideos;
-(BOOL)deleteVideo:(NSString*)par_file;
-(void)updatevideo:(NSString*)par_date time:(NSString*)par_time title:(NSString*)par_title filename:(NSString*)par_filename;
-(NSArray*)getDatesInMonthVideo:(NSString *)fromDate	to:(NSString *)toDate;
@end
