///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   DayPicture.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "DayPicture.h"
#import "Sqlite.h"
#import "DBSqlite.h"

@implementation DayPicture
@synthesize dateStr;
@synthesize picName;
-(void)insert{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return;
	sqlite = dbSqlite.sqlite;	
	[sqlite executeNonQuery:@"INSERT INTO day_picture(date,picture_name) VALUES (?,?);",self.dateStr,self.picName];
	
	[dbSqlite release];
}
-(NSString*)getPictureNameForDate:(NSString*)picDate{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	NSString *str = @"";
	if (![dbSqlite getDBConnection])
		return str;
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select picture_name from day_picture where date = ?;",picDate];
	if([results count] == 0) return str;
	NSDictionary *dic = [[NSDictionary alloc] initWithDictionary:[results objectAtIndex:0]];
	str = [dic valueForKey:@"picture_name"];
	//[results release];
	[dic release];
	return str;
}

-(void)updatePictureName:(NSString *)nameOfPic forDate:(NSString *)picDate{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return;
	sqlite = dbSqlite.sqlite;	
	
	 [sqlite executeNonQuery:@"UPDATE day_picture set picture_name = ? where date = ?;",nameOfPic,picDate];
	
}
-(void)deletePictureNameforDate:(NSString *)picDate{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return;
	sqlite = dbSqlite.sqlite;	
	
	[sqlite executeNonQuery:@"DELETE from day_picture where date = ?;",picDate];
	
}
@end
