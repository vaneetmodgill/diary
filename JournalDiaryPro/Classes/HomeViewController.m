///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   HomeViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "HomeViewController.h"
#import "EAGLView.h"
#import "SettingsViewController.h"
#import "HelpViewController.h"
#import "eDiaryAppDelegate.h"
#import "DiaryEntryViewController.h"
#import "VoiceMemoViewController.h"
#import "LockerViewController.h"
#import "EventsViewController.h"
#import "Settings.h"
#import "Picture.h"
#import "Secure.h"
#import "VaultPasswordViewController.h"
#import "ActivityIndicator.h"
#import "DayPicture.h"
#import "VideoMemoViewController.h"
@implementation HomeViewController
@synthesize settingsViewController;
@synthesize helpViewController;
@synthesize diaryEntryViewController;
@synthesize eventsViewController;
@synthesize voiceMemoViewController;
@synthesize vault;
@synthesize locker;
@synthesize videoMemoViewController,isautorotate;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return NO;
	//NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//	
//	self.isautorotate= [userDefaults boolForKey:@"autorotateenabled"];
//	
//
//	return (toInterfaceOrientation == UIInterfaceOrientationPortrait);	
	
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {


}



- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
   if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
       
        self.edgesForExtendedLayout=UIRectEdgeNone;

        self.extendedLayoutIncludesOpaqueBars = YES;

    }
    else
    {
        [self moveAllSubviewsDown ];
    }
    
   // self.view.autoresizesSubviews = YES ;
   // self.view.autoresizingMask = UIViewAutoresizingFlexibleHeight ;
    
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
               
        backgroundImageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        backgroundImageView.frame = CGRectMake(0, -20, 320, 568+20);
        imageView.frame =CGRectMake(12, -20, 320, 568+20);
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 568+20);
    }
    else
    {
        backgroundImageView.frame = CGRectMake(0, 0, 320, 480);
        imageView.frame =CGRectMake(12, 0, 320, 480);
        
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 480+20);

    }
    
    
	self.isautorotate =NO;
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setBool:NO forKey:@"autorotateenabled"];

	indicator = [ActivityIndicator sharedActivityIndicator];
//	if([eDiaryAppDelegate getHomeImage]==nil){
		[indicator show:@"Please Wait"];
		[self performSelectorInBackground:@selector(loadImage) withObject:nil];
//	}else{
//		imageView.image = [eDiaryAppDelegate getHomeImage];
//	}
//	[self loadImage];
}

- (void) moveAllSubviewsDown{
    float barHeight = 45.0;
    for (UIView *view in self.view.subviews) {
        
        if ([view isKindOfClass:[UIScrollView class]]) {
            view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y + barHeight, view.frame.size.width, view.frame.size.height - barHeight);
        } else {
            view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y + barHeight, view.frame.size.width, view.frame.size.height);
        }
    }
}
-(void)loadImage{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	//NSLog(@"here");
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	//paths = nil;
	if(settings.picOfDayEnabled && settings.picEnabled){
		DayPicture *dayPicture = [[DayPicture alloc] init];
		
		NSString *imgName =[dayPicture getPictureNameForDate:settings.globalDate];
		//NSLog(@"name %@",imgName);
		if(![imgName isEqualToString:@""]){ 
			UIImage *img= [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",documentsDirectory,imgName]];
			[eDiaryAppDelegate setHomeImage:img];
			imageView.image = img;
		}else{
			[eDiaryAppDelegate setHomeImage:nil];
			imageView.image = nil;
		}		
		[dayPicture release];
/*		
		Picture *picture = [[Picture alloc] init];
		NSArray *array = [picture getData];
		NSMutableArray *picNameArray = [[NSMutableArray alloc] init];
		for(NSDictionary *dictionary in array){
			//NSLog(@"name %@",[dictionary valueForKey:@"name"]);
			[picNameArray addObject:(NSString *)[dictionary valueForKey:@"name"]];
		}
		//	//NSLog(@"%d",random()%3);
		//NSLog(@"count %d",[picNameArray count]);
		if([picNameArray count] > 0){
			NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
			[formatter setDateFormat:@"YYYY-MM-dd"];
			NSString *currentDate = [formatter stringFromDate:[NSDate date]];
	//		[formatter release];
			if([settings.pictureDate isEqualToString:@""]){
				NSString *nameOfPic = [picNameArray objectAtIndex:random()%[picNameArray count]];
				UIImage *img= [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@.png",documentsDirectory,nameOfPic]];
				***********
			//	[eDiaryAppDelegate setHomeImage:img];
				UIGraphicsBeginImageContext(CGSizeMake(307.0,460.0));
				// now redraw our image in a smaller rectangle.
				[img drawInRect:CGRectMake(0.0, 0.0, 307.0, 460.0)];
				//[myThumbNail release];
				// make a "copy" of the image from the current context
				UIImage *newImage    = UIGraphicsGetImageFromCurrentImageContext();
				UIGraphicsEndImageContext();
				// create a new view to place on our screen (OPTIONAL-- testing)
				
				imageView.image = newImage;
				 ***********
				imageView.image = img;
				[eDiaryAppDelegate setHomeImage:img];
				[settings storePictureDate:currentDate picture:nameOfPic];
	//			img = nil;
				
			}else if(![settings.pictureDate isEqualToString:currentDate]) {
				//NSLog(@"picture change");
				NSString *nameOfPic = [picNameArray objectAtIndex:random()%[picNameArray count]];
				while ([nameOfPic isEqualToString:settings.picOfTheDay]) {
					nameOfPic = [picNameArray objectAtIndex:random()%[picNameArray count]];
				}
				*************
				UIGraphicsBeginImageContext(CGSizeMake(307.0,460.0));
				// now redraw our image in a smaller rectangle.
				[img drawInRect:CGRectMake(0.0, 0.0, 307.0, 460.0)];
				//[myThumbNail release];
				// make a "copy" of the image from the current context
				UIImage *newImage    = UIGraphicsGetImageFromCurrentImageContext();
				UIGraphicsEndImageContext();
				// create a new view to place on our screen (OPTIONAL-- testing)
				
				imageView.image = newImage;****************
				UIImage *img = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@.png",documentsDirectory,nameOfPic]];
				[eDiaryAppDelegate setHomeImage:img];
				imageView.image = img;
				[settings storePictureDate:currentDate picture:nameOfPic];
	//			img = nil;
			}else{
				if([eDiaryAppDelegate getHomeImage] == nil){
					//NSLog(@"pic nil");
					
					UIImage *img = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@.png",documentsDirectory,settings.picOfTheDay]];
					//NSLog(@"picture %@",settings.picOfTheDay);
					//NSLog(@"pic date %@",settings.pictureDate);
					[eDiaryAppDelegate setHomeImage:img];
	//				img = nil;
				}
				**************
				UIGraphicsBeginImageContext(CGSizeMake(307.0,460.0));
				// now redraw our image in a smaller rectangle.
				[img drawInRect:CGRectMake(0.0, 0.0, 307.0, 460.0)];
				//[myThumbNail release];
				// make a "copy" of the image from the current context
				UIImage *newImage    = UIGraphicsGetImageFromCurrentImageContext();
				UIGraphicsEndImageContext();
				// create a new view to place on our screen (OPTIONAL-- testing)
			
				imageView.image = newImage;
				***************
				imageView.image = [eDiaryAppDelegate getHomeImage];
				
			}
		}
	*/
	}else if(settings.picEnabled && !([settings.currentPicture isEqualToString:@""]||settings.currentPicture ==nil)){
		if([eDiaryAppDelegate getHomeImage] == nil){
		//NSLog(@"1");
		UIImage *img = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",documentsDirectory,settings.currentPicture]];
		[eDiaryAppDelegate setHomeImage:img];
		//NSLog(@"2");
		//		img = nil;
		}
		//NSLog(@"3");
		/*
		UIGraphicsBeginImageContext(CGSizeMake(307.0,460.0));
		// now redraw our image in a smaller rectangle.
		[img drawInRect:CGRectMake(0.0, 0.0, 307.0, 460.0)];
		//[myThumbNail release];
		// make a "copy" of the image from the current context
		UIImage *newImage    = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		// create a new view to place on our screen (OPTIONAL-- testing)
		
		imageView.image = newImage;
		*/
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//NSLog(@"4");
		//[eDiaryAppDelegate setHomeImage:img];
	}else{
		UIImage *img =[UIImage imageNamed:@"ico-bg.png"];
		imageView.image = img;
		//NSLog(@"default");
//		img = nil;
	}
		[settings release];
	[indicator hide];
	[pool release];
}
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
	//NSLog(@"RECEIVED MEMORY WARNING in HomeViewController!!!");
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}
/*
- (void) performCurl
{
	// Curl the image up or down
	CATransition *animation = [CATransition animation];
	[animation setDelegate:self];
	[animation setDuration:1.0f];
	[animation setTimingFunction:UIViewAnimationCurveEaseInOut];
	[animation setType:(notCurled ? @"mapCurl" : @"mapUnCurl")];
	[animation setRemovedOnCompletion:NO];
	[animation setFillMode: @"extended"];
	[animation setRemovedOnCompletion: NO];
	notCurled = !notCurled;
	[[topView layer] addAnimation:animation forKey:@"pageFlipAnimation"];
	topView setUserInteractionEnabled: NO;
	bottomView setUserInteractionEnabled: YES;
}*/
- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
	self.diaryEntryViewController = nil;
	//NSLog(@"view did unload called");
}
- (void)viewWillDisappear:(BOOL)animated{
	//NSLog(@"disappear called");
}
-(IBAction)diaryPressed{
	
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
    
    NSLog(@"self view dimensione   %f",self.view.frame.size.height);
    
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
	DiaryEntryViewController *diary = [[DiaryEntryViewController alloc] initWithNibName:@"DiaryEntryViewController" bundle:nil];
//	diaryEntryViewController.image = imageView.image;
	self.diaryEntryViewController = diary;
	[self.view addSubview:self.diaryEntryViewController.view];
	[diary release];
	// do your view swapping here
	//[diaryEntryViewController.view setUserInteractionEnabled: YES];
	[UIView commitAnimations];	
}
-(IBAction)eventPressed{
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
	
	EventsViewController *events = [[EventsViewController alloc] initWithNibName:@"EventsViewController" bundle:nil];
//	eventsViewController.image = imageView.image;
	self.eventsViewController = events;
	[self.view addSubview:self.eventsViewController.view];
	[events release];
	[UIView commitAnimations];	
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if([alertView isEqual:alerts])
		
	{
		if(buttonIndex==2) //0 is by default for cancel button
		{

						
			
					[UIView beginAnimations:nil context:nil];
					[UIView setAnimationDuration:1];
					[UIView setAnimationBeginsFromCurrentState:YES];
					[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
					
					VideoMemoViewController *video = [[VideoMemoViewController alloc] initWithNibName:@"VideoMemoViewController" bundle:nil];
					self.videoMemoViewController = video;
					[self.view addSubview:self.videoMemoViewController.view];
					[video release];
					
					[UIView commitAnimations];

				

		
		}
		else if(buttonIndex==1)
		{
			[UIView beginAnimations:nil context:nil];
			//change to set the time
			[UIView setAnimationDuration:1];
			[UIView setAnimationBeginsFromCurrentState:YES];
			[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
			
			VoiceMemoViewController *voice = [[VoiceMemoViewController alloc] initWithNibName:@"VoiceMemoViewController" bundle:nil];
			//	voiceMemoViewController.image = imageView.image;
			self.voiceMemoViewController = voice;
			[self.view addSubview:self.voiceMemoViewController.view];
			[voice release];
			[UIView commitAnimations];
		}
	}
}
-(IBAction)memoPressed{
	
alerts=[[UIAlertView alloc] initWithTitle:@"What Do you want to save" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Voice",@"Video",nil];
	[alerts show];
	//[alert release];
	
		
}
-(IBAction)lockerPressed{
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
	
	Secure *secure = [[Secure alloc] init];
	if([secure isVaultSecure]){
		[secure release];
		VaultPasswordViewController *vaultView = [[VaultPasswordViewController alloc] initWithNibName:@"VaultPasswordViewController" bundle:nil];
		self.vault = vaultView;
		[self.view addSubview:self.vault.view];
		[vaultView release];
	}else{	
		[secure release];
		LockerViewController *lock = [[LockerViewController alloc] initWithNibName:@"LockerViewController" bundle:nil];
		self.locker = lock;
		[self.view addSubview:self.locker.view];
		[lock release];
	}
	
	[UIView commitAnimations];	
}
-(IBAction)settingsPressed {
    
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
	
	if(self.settingsViewController == nil){
		//NSLog(@"settings created");	
	SettingsViewController *settingsView = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
	self.settingsViewController = settingsView;	
	[settingsView release];	
	}
    
    NSLog(@"%@",NSStringFromCGRect(self.view.frame));
    NSLog(@"%@",NSStringFromCGRect(self.settingsViewController.view.frame));

	[self.view addSubview:self.settingsViewController.view];
	
	[UIView commitAnimations];	
}
-(IBAction)helpPressed {
    
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
	
	//[self.view removeFromSuperview];
	if(!self.helpViewController){
	 HelpViewController *help = [[HelpViewController alloc] initWithNibName:@"HelpViewController" bundle:nil];
		self.helpViewController = help;	
		[help release];
	}
	[self.view addSubview:self.helpViewController.view];
	[UIView commitAnimations];	
}
-(IBAction)timeLinePressed
{
	glView=[[EAGLView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
	[self.view addSubview:glView];
	[glView release];
	
	
}
- (void)dealloc {
	////NSLog(@"dealloc called");
	[eventsViewController release];
	[diaryEntryViewController release];
	[voiceMemoViewController release];
	[vault release];
	[locker release];
	[self.settingsViewController release];
	[self.helpViewController release];
    [super dealloc];
}


@end
