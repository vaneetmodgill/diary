///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   SettingsViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "SettingsViewController.h"
#import "SettingsPasswordViewController.h"
#import "SettingsQAViewController.h"
#import "SettingsFontViewController.h"
#import "SettingsPictureViewController.h"
#import "SettingsExportViewController.h"
#import "SettingsEmailViewController.h"
#import "HomeViewController.h"
#import "HelpViewController.h"
#import "eDiaryAppDelegate.h"
#import "Settings.h"
#import "Secure.h"
#import "DayPicture.h"

@implementation SettingsViewController
@synthesize homeViewCtlr;
@synthesize settingsPasswordViewController;
@synthesize QAViewController;
@synthesize fontsViewController;
@synthesize pictureViewController;
@synthesize settingsEmailViewController;
@synthesize exportViewController;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    NSLog(@"Setting screeen Height %f ", screenBounds.size.height);
   
    if (screenBounds.size.height == 667)
    {
        
        
        
        backgroundImageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        backgroundImageView.frame = CGRectMake(0, -20, 320, 667+40);
        //   self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 568+20);
        
        settingsTable.frame = CGRectMake(settingsTable.frame.origin.x,settingsTable.frame.origin.y , settingsTable.frame.size.width, 637);
    }
    
   else if (screenBounds.size.height == 568)
    {
        
        
        
        backgroundImageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        backgroundImageView.frame = CGRectMake(0, -20, 320, 568+40);
        //   self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 568+20);
        
        settingsTable.frame = CGRectMake(settingsTable.frame.origin.x,settingsTable.frame.origin.y , settingsTable.frame.size.width, 536);
    }
    else
    {
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 480+20);
        
        settingsTable.frame = CGRectMake(settingsTable.frame.origin.x,settingsTable.frame.origin.y , settingsTable.frame.size.width, 436);

    }

    
    
	//NSLog(@"view did load called");
	//[passwordText becomeFirstResponder];
	nameView.hidden = YES;
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	[emailSwitch setOn:settings.emailEnabled animated:NO];
    [faceIDSwitch setOn:settings.faceIDEnabled animated:NO];
	[pictureSwitch setOn:settings.picEnabled animated:NO];
	[picOfDaySwitch setOn:settings.picOfDayEnabled animated:NO];
	
	[animateEmoticonSwitch setOn:settings.animateEmoticonEnabled animated:NO]; //a15

	[gpsEnabledSwitch setOn:settings.gpsEnabled animated:NO]; //16

//=== ashish 22 start

	daysGapInt = settings.daysGap;
	
	daysGapSlider=[[UISlider alloc] initWithFrame: CGRectMake(200.0f, 10.0f, 100.0f, 20.0f)];
	[daysGapSlider setMaximumValue:30.0f];
	[daysGapSlider setMinimumValue:1.0f];
	[daysGapSlider addTarget:self action:@selector(handleSlider) forControlEvents:UIControlEventValueChanged];
	[daysGapSlider setValue:daysGapInt];

	daysGapLabel = [[UILabel alloc]initWithFrame: CGRectMake(170.0f, 10.0f, 25.0f, 20.0f)];

	daysGapLabel.text=[NSString stringWithFormat:@"%d",settings.daysGap];
//=== ashish 22 end
	
	Secure *secure = [[Secure alloc] init];
	[passwordSwitch setOn:[secure isAppSecure] animated:NO];
	[secure release];	
	[settingsTable setBackgroundColor:[UIColor clearColor]];
	[settingsTable setDelegate:self];
	[settingsTable reloadData];
	@try {
		[settingsTable reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationRight];
		[settingsTable reloadSections:[NSIndexSet indexSetWithIndex:3] withRowAnimation:UITableViewRowAnimationRight];
	}
	@catch (NSException * e) {
		//NSLog(@"Exception occured %@", e);
	}

	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
	//NSLog(@"RECEIVED MEMORY WARNING in SettingsViewController!!!");
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

-(void)viewWillAppear:(BOOL)animated{

}

-(IBAction)savePressed{
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if(emailSwitchCell != nil){
	UISwitch *tempSwitch2 = (UISwitch *)[emailSwitchCell viewWithTag:2];
	[settings storeEmailEnabled:[tempSwitch2 isOn]];
		if([tempSwitch2 isOn] == YES && ([settings.emailAddress isEqualToString:@""]|| settings.emailAddress == nil)){
			[eDiaryAppDelegate showMessage:@"Email is not configured" title:@"Warning"];
			return;
		}	
	}else {
		//NSLog(@"email nil");
	}

	if(pictureSwitchCell != nil){
	UISwitch *tempSwitch3 =(UISwitch *)[pictureSwitchCell viewWithTag:2];
	[settings storePictureEnabled:[tempSwitch3 isOn]];
	}else {
		//NSLog(@"home pic nil");
	}

//---
	if(animateEmoticonSwitchCell != nil) //a15
	{
		UISwitch *tempSwitch5 =(UISwitch *)[animateEmoticonSwitchCell viewWithTag:2];
		[settings storeAnimateEmoticonEnabled:[tempSwitch5 isOn]];
	}
	else
	{
		//NSLog(@"animate emoticon nil");
	}
	

	if(gpsEnabledSwitchCell != nil) //a16
	{
		UISwitch *tempSwitch6 =(UISwitch *)[gpsEnabledSwitchCell viewWithTag:2];
		[settings storeGpsEnabled:[tempSwitch6 isOn]];
	}
	else
	{
		//NSLog(@"animate emoticon nil");
	}
	
	
	if(picOfDaySwitchCell != nil){
	UISwitch *tempSwitch4 =(UISwitch *)[picOfDaySwitchCell viewWithTag:2];
	[settings storePictureOfTheDayEnabled:[tempSwitch4 isOn]];
	}else {
		//NSLog(@"pic of the day nil");
	}
	[settings release];
	Secure *secure = [[Secure alloc] init];
	if(passwordSwitchCell != nil){
	UISwitch *tempSwitch = (UISwitch *)[passwordSwitchCell viewWithTag:2];
	
	if([secure getCount]==0){
		[secure insert:@"" lockerPass:@"" question:@"" answer:@"" vaultQ:@"" vaultA:@""];
		[secure updateAppProtection:[tempSwitch isOn]];
//		[eDiaryAppDelegate showMessage:@"Application Password not set" title:@"Warning"];
	}else {
		
		[secure updateAppProtection:[tempSwitch isOn]];
		
	}
		if([[secure getPassword] isEqualToString:@""] && ([tempSwitch isOn] == YES)){
			[eDiaryAppDelegate showMessage:@"Application Password is not set. Please press the Change password item in Application Settings and enter your password" title:@"Warning"];
			return;
		}	
	}else 
	{
		//NSLog(@"password nil");
	}
	
	
	[secure release];
	 
	//if(!self.homeViewController){
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	
	HomeViewController *homeView = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:[NSBundle mainBundle]];
	self.homeViewCtlr = homeView;
	[self.view addSubview:self.homeViewCtlr.view];
	[homeView release];
	[UIView commitAnimations];
}
-(IBAction)backPressed{
	//[passwordText resignFirstResponder];
	//if(!self.homeViewController){
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	
	HomeViewController *homeView = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:[NSBundle mainBundle]];
	self.homeViewCtlr = homeView;
	[self.view addSubview:self.homeViewCtlr.view];
	[homeView release];	
	[UIView commitAnimations];
}

-(IBAction)openPressed{
	nameView.hidden = YES;
	[nameText resignFirstResponder];
	if([nameText.text isEqualToString:@""]){
		[eDiaryAppDelegate showMessage:@"Please enter your name" title:@"Empty text"];
	}else{
		Settings *settings = [[Settings alloc] init];
		[settings storeUserName:nameText.text];
		[settings release];
		nameLbl.text = nameText.text;
		[settingsTable reloadData];
		
	} 
}
-(IBAction)cancelPressed{
	nameView.hidden = YES;
	[nameText resignFirstResponder];
}
- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

// Number of groups
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
	return 5;
}

// Section Titles
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	switch (section) 
	{
		case 0:
			return @"Application Settings";
		case 1:
			return @"Fonts";
		case 2:
			return @"Background Picture";	
		case 3:
			return @"Email Settings";
		case 4:
			return @"Export";
		default:
			return @"";
	}
}



// Number of rows per section
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
	switch (section) 
	{
		case 0:
			//return 8; // a22
            return 9;
		case 1:
			return 1;
		case 2:
			return 4;   
		case 3:
			return 2;
		case 4:
			return 1;	
		default:
			return 0;
	}
}

/*
// Heights per row
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{	
	switch ([indexPath section]) 
	{
		case 0:
			return 80.0f;
		case 3:
			return 200.0f;
		case 4:
			return 120.0f;
		default:
			return 44.0f;
	}
}
*/
-(void)changeSwitch:(UISwitch*)sender{
    Settings *settings = [[Settings alloc] init];
    UISwitch *tempSwitch = (UISwitch *)[passwordSwitchCell viewWithTag:2];

    if(emailSwitch == sender){
     //   UISwitch *tempSwitch2 = (UISwitch *)[emailSwitchCell viewWithTag:2];
        [settings storeEmailEnabled:[sender isOn]];
    }else {
        //NSLog(@"email nil");
    }
    
    if(pictureSwitch == sender){
     //   UISwitch *tempSwitch3 =(UISwitch *)[pictureSwitchCell viewWithTag:2];
        [settings storePictureEnabled:[sender isOn]];
    }else {
        //NSLog(@"home pic nil");
    }
    if(faceIDSwitch == sender){
      //  UISwitch *tempSwitch3 =(UISwitch *)[faceIDSwitchCell viewWithTag:3];
        [settings storeFaceIDEnabled:[sender isOn]];
        [tempSwitch setOn:YES animated:NO];
    }else {
        //NSLog(@"home pic nil");
    }
    //---
    if(animateEmoticonSwitch == sender)  //a15
    {
     //   UISwitch *tempSwitch5 =(UISwitch *)[picOfDaySwitchCell viewWithTag:2];
        [settings storePictureOfTheDayEnabled:[sender isOn]];
    }
    else
    {
        NSLog(@"animate emoticon msg-II  nil");
    }
    
    if(gpsEnabledSwitch == sender)  //a16
    {
       // UISwitch *tempSwitch6 =(UISwitch *)[gpsEnabledSwitchCell viewWithTag:2];
        [settings storeGpsEnabled:[sender isOn]];
    }
    else
    {
        NSLog(@"gps enablded switch msg-II  nil");
    }
    
    
    if(picOfDaySwitch == sender){
       // UISwitch *tempSwitch4 =(UISwitch *)[picOfDaySwitchCell viewWithTag:2];
        [settings storePictureOfTheDayEnabled:[sender isOn]];
    }else {
        ////NSLog(@"pic of the day nil");
    }
    [settings release];
    Secure *secure = [[Secure alloc] init];
    
    if([secure getCount]==0){
        [secure insert:@"" lockerPass:@"" question:@"" answer:@"" vaultQ:@"" vaultA:@""];
        [secure updateAppProtection:[tempSwitch isOn]];
    }else {
        [secure updateAppProtection:[tempSwitch isOn]];
    }
    
    [secure release];
    [settingsTable reloadData];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	NSInteger row = [indexPath row];
	NSInteger section = [indexPath section];
	UITableViewCell *cell;
	//	_BoardsAppDelegate *appDelegate = (_BoardsAppDelegate *) [UIApplication sharedApplication].delegate;
	switch (section) 
	{
		case 0:
			if(row ==0){
				cell = [tableView dequeueReusableCellWithIdentifier:@"NameCell"];
				if (!cell) {
					cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"NameCell"] autorelease];
					cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
					UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 300.0f, 20.0f)];
					[label setText:@"Your Name"];
					label.backgroundColor = [UIColor clearColor];
					[cell addSubview:label];
					[label release];
					nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(100.0f, 10.0f, 180.0f, 20.0f)];
					Settings *settings = [[Settings alloc] init];
					[settings loadSettings];
					[nameLbl setText:settings.userName];
					nameLbl.textAlignment = UITextAlignmentRight;
					[settings release];
					nameLbl.backgroundColor = [UIColor clearColor];
					[cell addSubview:nameLbl];
					[nameLbl release];
					//NSLog(@"called");
				}
                
                [cell setBackgroundColor:[UIColor clearColor]];

                
				return cell;				
			}else if (row == 1) {
				cell = [tableView dequeueReusableCellWithIdentifier:@"PasswordProtectionCell"];
				if (!cell) {
					cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"PasswordProtectionCell"] autorelease];
					
					UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 100.0f, 20.0f)];
					[label setText:@"Password"];
					label.backgroundColor = [UIColor clearColor];
					label.tag = 1;
					[cell addSubview:label];
					[label release];
					
					passwordSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(200.0f, 10.0f, 100.0f, 20.0f)];
					passwordSwitch.tag = 2;
					[passwordSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
					[cell addSubview:passwordSwitch];
					Secure *secure = [[Secure alloc] init];
					[passwordSwitch setOn:[secure isAppSecure] animated:NO];
					[secure release];
				}
				passwordSwitchCell = cell;
                
                [cell setBackgroundColor:[UIColor clearColor]];

				return cell;
			} else if(row == 2){
				cell = [tableView dequeueReusableCellWithIdentifier:@"FaceIDProtectionCell"];
                if (!cell) {
                    cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"FaceIDProtectionCell"] autorelease];
                    NSString *faceIdText = @"TouchID";
                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 200.0f, 20.0f)];
                    BOOL iPhoneX = NO;
                    if (@available(iOS 11.0, *)) {
                        UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
                        if (mainWindow.safeAreaInsets.top > 0.0) {
                            iPhoneX = YES;
                        }
                    }
                    if (iPhoneX == YES) {
                       faceIdText = @"FaceID";
                    }
                    [label setText:faceIdText];
                    label.backgroundColor = [UIColor clearColor];
                    label.tag = 1;
                    [cell addSubview:label];
                    [label release];
                    
                    faceIDSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(200.0f, 10.0f, 100.0f, 20.0f)];
                    faceIDSwitch.tag = 3;
                    [faceIDSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
                    [cell addSubview:faceIDSwitch];
                    Settings *settings = [[Settings alloc] init];
                    [settings loadSettings];
                    [faceIDSwitch setOn:settings.faceIDEnabled animated:NO];
                    [settings release];

                }
                
                
                faceIDSwitchCell = cell;
                [cell setBackgroundColor:[UIColor clearColor]];
                
				return cell;
            }else if(row == 3){
                cell = [tableView dequeueReusableCellWithIdentifier:@"ChangePasswordCell"];
                if (!cell) {
                    cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"ChangePasswordCell"] autorelease];
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 200.0f, 20.0f)];
                    [label setText:@"Change Password"];
                    label.backgroundColor = [UIColor clearColor];
                    [cell addSubview:label];
                    [label release];
                    /*
                     firstName = [[UITextField alloc] initWithFrame:CGRectMake(20.0f, 40.0f, 280.0f, 30.0f)];
                     firstName.placeholder = @"Enter Your First Name Here";
                     firstName.delegate = self;
                     firstName.borderStyle = UITextBorderStyleBezel;
                     firstName.keyboardAppearance = UIKeyboardAppearanceAlert;
                     firstName.clearButtonMode = UITextFieldViewModeWhileEditing;
                     */
                    //    [cell addSubview:lastName];
                }
                [cell setBackgroundColor:[UIColor clearColor]];
                
                return cell;
            }else if(row == 4){
				cell = [tableView dequeueReusableCellWithIdentifier:@"SecretQuestionCell"];
				if (!cell) {
					cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"SecretQuestionCell"] autorelease];
					cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
					UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 300.0f, 20.0f)];
					[label setText:@"View Secret Question and Answer"];
					label.backgroundColor = [UIColor clearColor];
					[cell addSubview:label];
					[label release];
				}
                
                [cell setBackgroundColor:[UIColor clearColor]];

                
				return cell;
			}else if(row == 5){
				cell = [tableView dequeueReusableCellWithIdentifier:@"FreeSpaceCell"];
				if (!cell) {
					cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"FreeSpaceCell"] autorelease];
					UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 170.0f, 20.0f)];
					[label setText:@"Free Space available"];
					label.backgroundColor = [UIColor clearColor];
					[cell addSubview:label];
					[label release];
					NSFileManager *fileManager = [NSFileManager defaultManager];
					NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
					NSString *path = [paths objectAtIndex:0];
					NSDictionary *fsAttributes =
					[fileManager fileSystemAttributesAtPath:path];
						
					UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(160.0f, 10.0f, 145.0f, 20.0f)];
					label1.backgroundColor = [UIColor clearColor];
					label1.textAlignment = UITextAlignmentRight;
					float free = [[fsAttributes objectForKey:NSFileSystemFreeSize] floatValue];
					float value = free/(1024*1024*1024);     //1073741824;
					if(value > 0){
						label1.text = [NSString stringWithFormat:@"%.1f GB",value];
					}else {
						value = free/(1024*1024);       //1048576;
						if(value > 0){
							label1.text = [NSString stringWithFormat:@"%.1f MB",value];
						}else {
							value = free/1024;
							label1.text = [NSString stringWithFormat:@"%.1f KB",value];
						}
					}

					
					[cell addSubview:label1];
					[label1 release];
					
				}
				
                [cell setBackgroundColor:[UIColor clearColor]];

                
				return cell;
			}else if(row == 6){
				cell = [tableView dequeueReusableCellWithIdentifier:@"VersionCell"];
				if (!cell) {
					cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"VersionCell"] autorelease];
					UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 160.0f, 20.0f)];
					[label setText:@"JD Pro Version"];
					label.backgroundColor = [UIColor clearColor];
					[cell addSubview:label];
					[label release];
					
					UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(160.0f, 10.0f, 145.0f, 20.0f)];
					label1.backgroundColor = [UIColor clearColor];
					label1.text = @"1.2.3";
					label1.textAlignment = UITextAlignmentRight;
					[cell addSubview:label1];
					[label1 release];
			}
                [cell setBackgroundColor:[UIColor clearColor]];

                
				return cell;
			}

			else if(row == 7)
			{
				cell = [tableView dequeueReusableCellWithIdentifier:@"GpsCell"];
				if (!cell) 
				{
					cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"GpsCell"] autorelease];
					
					UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 200.0f, 20.0f)];
					[label setText:@"GPS"];
					label.backgroundColor = [UIColor clearColor];
					label.tag = 1;
					[cell addSubview:label];
					[label release];
					
					gpsEnabledSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(200.0f, 10.0f, 100.0f, 20.0f)];
					gpsEnabledSwitch.tag = 2;
                    [gpsEnabledSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
					[cell addSubview:gpsEnabledSwitch];
					Settings *settings = [[Settings alloc] init];
					[settings loadSettings];
					[gpsEnabledSwitch setOn:settings.gpsEnabled animated:NO];
					[settings release];
				}
				gpsEnabledSwitchCell = cell;
				
                [cell setBackgroundColor:[UIColor clearColor]];

                return cell;
                
			}
			
			
			
//==== Ashish 22 start
			else if(row == 8)
			{
				cell = [tableView dequeueReusableCellWithIdentifier:@"DaysGapCell"];
				if (!cell) 
				{
					cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"DaysGapCell"] autorelease];
					
					UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 10.0f, 180.0f, 20.0f)];
					[label setText:@" Event Alert in days"];
					label.backgroundColor = [UIColor clearColor];
					label.tag = 1;
					[cell addSubview:label];
					[label release];
					/*
					daysGapSlider=[[UISlider alloc] initWithFrame: CGRectMake(200.0f, 10.0f, 100.0f, 20.0f)];
					[daysGapSlider setMaximumValue:30.0f];
					[daysGapSlider setMinimumValue:1.0f];
					[daysGapSlider addTarget:self action:@selector(handleSlider) forEvents:7];
					*/
					 daysGapSlider.tag = 2;
					[cell addSubview:daysGapSlider];
					
									
					//daysGapLabel = [[UILabel alloc]initWithFrame: CGRectMake(170.0f, 10.0f, 25.0f, 20.0f)];
					daysGapLabel.tag = 3;
					[cell addSubview:daysGapLabel];
					
					
					Settings *settings = [[Settings alloc] init];
					[settings loadSettings];
					[settings release];
				}
				daysGapSliderCell = cell;
                
                [cell setBackgroundColor:[UIColor clearColor]];

                
				return cell;
			}
//==== Ashish 22 end
			
			break;
		case 1:
			cell = [tableView dequeueReusableCellWithIdentifier:@"FontsCell"];
			if (!cell) {
				cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"FontsCell"] autorelease];
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
				UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 300.0f, 20.0f)];
				[label setText:@"Change font settings"];
				label.backgroundColor = [UIColor clearColor];
				[cell addSubview:label];
				[label release];
			}
            [cell setBackgroundColor:[UIColor clearColor]];

            
			return cell;
			break;
			case 2:
			if(row ==0)
			{
				cell = [tableView dequeueReusableCellWithIdentifier:@"PictureCell"];
				if (!cell) 
				{
					cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"PictureCell"] autorelease];
					
					UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 200.0f, 20.0f)];
					[label setText:@"Home Picture Selection"];
					label.backgroundColor = [UIColor clearColor];
					label.tag = 1;
					[cell addSubview:label];
					[label release];
					
					pictureSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(200.0f, 10.0f, 100.0f, 20.0f)];
					pictureSwitch.tag = 2;
                    [pictureSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
					[cell addSubview:pictureSwitch];
					Settings *settings = [[Settings alloc] init];
					[settings loadSettings];
					[pictureSwitch setOn:settings.picEnabled animated:NO];
					[settings release];
				}
				pictureSwitchCell = cell;
                
                [cell setBackgroundColor:[UIColor clearColor]];

                
				return cell;
			}
			else if(row == 1)
			{
				cell = [tableView dequeueReusableCellWithIdentifier:@"PictureBGCell"];
				if (!cell) 
				{
					cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"PictureBGCell"] autorelease];
					
					UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 200.0f, 20.0f)];
					[label setText:@"Picture of the day"];
					label.backgroundColor = [UIColor clearColor];
					label.tag = 1;
					[cell addSubview:label];
					[label release];
					
					picOfDaySwitch = [[UISwitch alloc] initWithFrame:CGRectMake(200.0f, 10.0f, 100.0f, 20.0f)];
					picOfDaySwitch.tag = 2;
                    [picOfDaySwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
					[cell addSubview:picOfDaySwitch];
					Settings *settings = [[Settings alloc] init];
					[settings loadSettings];
					[picOfDaySwitch setOn:settings.picOfDayEnabled animated:NO];
					[settings release];
				}
				picOfDaySwitchCell = cell;
                
                [cell setBackgroundColor:[UIColor clearColor]];

                
				return cell;
			}
			else if (row == 2) 
			{
				cell = [tableView dequeueReusableCellWithIdentifier:@"ChangePictureCell"];
				if (!cell) 
				{
					cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"ChangePictureCell"] autorelease];
					cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
					UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 300.0f, 20.0f)];
					[label setText:@"Change Background Picture"];
					label.backgroundColor = [UIColor clearColor];
					[cell addSubview:label];
					[label release];
				}
				
                [cell setBackgroundColor:[UIColor clearColor]];

                
				return cell;
			}
			//---			
			else if(row == 3)
			{
				cell = [tableView dequeueReusableCellWithIdentifier:@"EmCell"];
				if (!cell) 
				{
					cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"EmCell"] autorelease];
					
					UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 200.0f, 20.0f)];
					[label setText:@"Animate Emoticon"];
					label.backgroundColor = [UIColor clearColor];
					label.tag = 1;
					[cell addSubview:label];
					[label release];
					
					animateEmoticonSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(200.0f, 10.0f, 100.0f, 20.0f)];
					animateEmoticonSwitch.tag = 2;
                    [animateEmoticonSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
					[cell addSubview:animateEmoticonSwitch];
					Settings *settings = [[Settings alloc] init];
					[settings loadSettings];
					[animateEmoticonSwitch setOn:settings.animateEmoticonEnabled animated:NO];
					[settings release];
				}
				animateEmoticonSwitchCell = cell;
                
                [cell setBackgroundColor:[UIColor clearColor]];

                
				return cell;
			}
			break;
			case 3:
			if (row == 0) {
				cell = [tableView dequeueReusableCellWithIdentifier:@"EmailProtectionCell"];
				if (!cell) {
					cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"EmailProtectionCell"] autorelease];
					
					UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 100.0f, 20.0f)];
					[label setText:@"Send Email"];
					label.backgroundColor = [UIColor clearColor];
					label.tag = 1;
					[cell addSubview:label];
					[label release];
					
					emailSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(200.0f, 10.0f, 100.0f, 20.0f)];
					emailSwitch.tag = 2;
                    [emailSwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
					[cell addSubview:emailSwitch];
					Settings *settings = [[Settings alloc] init];
					[settings loadSettings];
					[emailSwitch setOn:settings.emailEnabled animated:NO];
					[settings release];
				}
				emailSwitchCell = cell;
                [cell setBackgroundColor:[UIColor clearColor]];

                
				return cell;
			} else if(row == 1){
			cell = [tableView dequeueReusableCellWithIdentifier:@"ChangeEmailCell"];
			if (!cell) {
				cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"ChangeEmailCell"] autorelease];
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
				UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 300.0f, 20.0f)];
				[label setText:@"Change email settings"];
				label.backgroundColor = [UIColor clearColor];
				[cell addSubview:label];
				[label release];
			}
			}
            [cell setBackgroundColor:[UIColor clearColor]];

			return cell;
			break;
			case 4:
			cell = [tableView dequeueReusableCellWithIdentifier:@"ExportCell"];
			if (!cell) {
				cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"ExportCell"] autorelease];
				cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
				UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 10.0f, 300.0f, 20.0f)];
				[label setText:@"Export entries"];
				label.backgroundColor = [UIColor clearColor];
				[cell addSubview:label];
				[label release];
			}
            [cell setBackgroundColor:[UIColor clearColor]];

            
			return cell;
			break;
	}
	// Return a generic cell if all else fails
	cell = [tableView dequeueReusableCellWithIdentifier:@"any-cell"];
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"any-cell"] autorelease];
	}
	
	return cell;		
				
}

//==== Ashish 22 start
-(void)handleSlider
{

	daysGapInt =[daysGapSlider value];
	NSLog(@"\ndg=%i\n",daysGapInt);
	
	[daysGapLabel setText:[NSString stringWithFormat:@"%.0f", [daysGapSlider value]]];
	
	Settings *settings = [[Settings alloc]init];
	[settings storeDaysGap:daysGapInt];
	[settings release];

}
//==== Ashish 22 end

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)newIndexPath
{
	Settings *settings = [[Settings alloc] init];
	if(emailSwitchCell != nil){
		UISwitch *tempSwitch2 = (UISwitch *)[emailSwitchCell viewWithTag:2];
		[settings storeEmailEnabled:[tempSwitch2 isOn]];
	}else {
		//NSLog(@"email nil");
	}
	
	if(pictureSwitchCell != nil){
		UISwitch *tempSwitch3 =(UISwitch *)[pictureSwitchCell viewWithTag:2];
		[settings storePictureEnabled:[tempSwitch3 isOn]];
	}else {
		//NSLog(@"home pic nil");
	}
    if(faceIDSwitchCell != nil){
        UISwitch *tempSwitch3 =(UISwitch *)[faceIDSwitchCell viewWithTag:2];
        [settings storeFaceIDEnabled:[tempSwitch3 isOn]];
    }else {
        //NSLog(@"home pic nil");
    }
//---	
	if(animateEmoticonSwitch != nil)  //a15
	{
		UISwitch *tempSwitch5 =(UISwitch *)[picOfDaySwitchCell viewWithTag:2];
		[settings storePictureOfTheDayEnabled:[tempSwitch5 isOn]];
	}
	else
	{
		NSLog(@"animate emoticon msg-II  nil");
	}
	
	if(gpsEnabledSwitch != nil)  //a16
	{
		UISwitch *tempSwitch6 =(UISwitch *)[gpsEnabledSwitchCell viewWithTag:2];
		[settings storeGpsEnabled:[tempSwitch6 isOn]];
	}
	else
	{
		NSLog(@"gps enablded switch msg-II  nil");
	}
	

	if(picOfDaySwitchCell != nil){
		UISwitch *tempSwitch4 =(UISwitch *)[picOfDaySwitchCell viewWithTag:2];
		[settings storePictureOfTheDayEnabled:[tempSwitch4 isOn]];
	}else {
		////NSLog(@"pic of the day nil");
	}
	[settings release];
	Secure *secure = [[Secure alloc] init];
	UISwitch *tempSwitch = (UISwitch *)[passwordSwitchCell viewWithTag:2];
	
	if([secure getCount]==0){
		[secure insert:@"" lockerPass:@"" question:@"" answer:@"" vaultQ:@"" vaultA:@""];
		[secure updateAppProtection:[tempSwitch isOn]];
	}else {
		[secure updateAppProtection:[tempSwitch isOn]];
	}
	
	[secure release];
	int section = [newIndexPath section];
	int row = [newIndexPath row];
//	UITableViewCell *cell = [tableView cellForRowAtIndexPath:newIndexPath];
	switch (section) 
	{
		case 0:
		{
			if(row == 0){
				Settings *settings = [[Settings alloc] init];
				[settings loadSettings];
				nameView.hidden = NO;
				nameText.clearButtonMode = UITextFieldViewModeWhileEditing;
				nameText.keyboardType = UIKeyboardTypeAlphabet;
				nameText.keyboardAppearance = UIKeyboardAppearanceAlert;
				nameText.autocapitalizationType = UITextAutocapitalizationTypeNone;
				titleLbl.text = @"Name";
				messageLbl.text = @"Enter your name here";
				[nameText becomeFirstResponder];
				nameText.text = settings.userName; 
								
				[settings loadSettings];
				[settingsTable reloadData];
				[settings release];
			}else if(row == 3){
				SettingsPasswordViewController *settingsPasswordView = [[SettingsPasswordViewController alloc] initWithNibName:@"SettingsPasswordViewController" bundle:nil];
				self.settingsPasswordViewController = settingsPasswordView;
				[self.view addSubview:self.settingsPasswordViewController.view];
				[settingsPasswordView release];
			}else if(row == 4){
				SettingsQAViewController *QAView = [[SettingsQAViewController alloc] initWithNibName:@"SettingsQAViewController" bundle:nil];
				self.QAViewController = QAView;
				[self.view addSubview:self.QAViewController.view];
				[QAView release];
			}
		}
		break;
		case 1:{
			SettingsFontViewController *fontsView = [[SettingsFontViewController alloc] initWithNibName:@"SettingsFontViewController" bundle:nil];
			self.fontsViewController = fontsView;
			[self.view addSubview:self.fontsViewController.view];
			[fontsView release];
		}
		break;
		case 2:{
			if(row == 2){
			SettingsPictureViewController *pictureView = [[SettingsPictureViewController alloc] initWithNibName:@"SettingsPictureViewController" bundle:nil];
				Settings *settings = [[Settings alloc] init];
				[settings loadSettings];
				if(settings.picOfDayEnabled && settings.picEnabled){
					DayPicture *dayPicture = [[DayPicture alloc] init];
					NSString *imgName =[dayPicture getPictureNameForDate:settings.globalDate];
					pictureView.fileName = imgName;
					[dayPicture release];
				}else{
				pictureView.fileName = settings.currentPicture;
				}
				[settings release];
				self.pictureViewController = pictureView;
				[self.view addSubview:self.pictureViewController.view];
				[pictureView release];
			}	
		}
		break;
		case 3:{
			if(row == 1){
			SettingsEmailViewController *settingsEmailView = [[SettingsEmailViewController alloc] initWithNibName:@"SettingsEmailViewController" bundle:nil];
				self.settingsEmailViewController = settingsEmailView;
				[self.view addSubview:self.settingsEmailViewController.view];
				[settingsEmailView release];
			}
		}break;
	
		case 4:{
			SettingsExportViewController *exportView = [[SettingsExportViewController alloc] initWithNibName:@"SettingsExportViewController" bundle:nil];
			self.exportViewController = exportView;
			[self.view addSubview:self.exportViewController.view];
			[exportView release];
		}
		break;
	}
	[tableView deselectRowAtIndexPath:newIndexPath animated:NO];
}	

- (void)dealloc {
	[picOfDaySwitch release];
	[passwordSwitch release];
	[pictureSwitch release];
	[emailSwitch release];
    [super dealloc];
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [cell setBackgroundColor:[UIColor clearColor]];

    
	int section = [indexPath section];
//	int row = [indexPath row];
	switch (section) 
	{
		case 2:
			
			break;

	}
    
 
    
	
}


@end
