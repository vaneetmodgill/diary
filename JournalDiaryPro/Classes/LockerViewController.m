///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   LockerViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "LockerViewController.h"
#import "Vault.h"
#import "VaultSettingsViewController.h"
#import "AddSecretViewController.h"
#import "HomeViewController.h"
#import "ShowSecretViewController.h"
#import "eDiaryAppDelegate.h"
#import "Settings.h"
@implementation LockerViewController
@synthesize addSecretViewController;
@synthesize settingsViewController;
@synthesize homeViewController;
@synthesize showSecretViewController;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        
        
        backgroundImageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        backgroundImageView.frame = CGRectMake(0, -20, 320, 568+40);
        //   self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 568+20);
    }
    else
    {
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 480+20);
    }
    

    
    
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];
	NSMutableArray *mutArray = [[NSMutableArray alloc] init];
	Vault *vault = [[Vault alloc] init];
	NSArray *array = [vault getData];
    resultArray = array;
	[vault release];
	for (NSDictionary *dictionary in array){
		Vault *vault = [[Vault alloc] init];
		vault.keyStr = [dictionary valueForKey:@"key_name"];
		vault.valueStr = [dictionary valueForKey:@"key_value"];
		[mutArray addObject:vault];
		[vault release];
	}
	 lockerArray=[mutArray mutableCopy];
    filterLocakerArray=[mutArray mutableCopy];
	[mutArray release];
	[lockerTable reloadData];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

-(IBAction)addPressed{
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
	AddSecretViewController *addSecretView = [[AddSecretViewController alloc] initWithNibName:@"AddSecretViewController" bundle:nil];
	self.addSecretViewController = addSecretView;
	[self.view addSubview:self.addSecretViewController.view];
	[addSecretView release];
	[UIView commitAnimations];
}
-(IBAction)settingsPressed{
	VaultSettingsViewController *settings = [[VaultSettingsViewController alloc] initWithNibName:@"VaultSettingsViewController" bundle:nil];
	self.settingsViewController = settings;
	[self.view addSubview:self.settingsViewController.view];
	[settings release];
}
-(IBAction)backPressed{
    [searchBar resignFirstResponder];
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	HomeViewController *homeView = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
	self.homeViewController = homeView;
	[self.view addSubview:self.homeViewController.view];
	[homeView release];
	[UIView commitAnimations];
}

-(IBAction)deletePressed{
	if([lockerTable indexPathForSelectedRow] ==nil){
		[eDiaryAppDelegate showMessage:@"Please select a row to delete" title:@"No Selection"];
	}else {
		UIActionSheet *deleteAction = [[UIActionSheet alloc] initWithTitle:@"Are you sure you want to delete?" delegate:self cancelButtonTitle:@"No" destructiveButtonTitle:@"Yes" otherButtonTitles:nil];
		[deleteAction showInView:self.view];
		[deleteAction release];
	}
}



- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
	//NSLog(@"index %d",buttonIndex);
    [searchBar resignFirstResponder];
	if(buttonIndex == 0){
		//NSLog(@"delete");
		Vault *vault = [[Vault alloc] init];
		[vault deleteSecret:keyString];
		[lockerArray removeAllObjects];
        [filterLocakerArray removeAllObjects];
		NSArray *array = [vault getData];
        resultArray = array;
		[vault release];
		for (NSDictionary *dictionary in array){
			Vault *vault = [[Vault alloc] init];
			vault.keyStr = [dictionary valueForKey:@"key_name"];
			vault.valueStr = [dictionary valueForKey:@"key_value"];
			[lockerArray addObject:vault];
            [filterLocakerArray addObject:vault];
			[vault release];
		}
		[lockerTable reloadData];	
//		[eDiaryAppDelegate showMessage:@"Vault item deleted" title:@"Deleted"];
	}
	[actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
}
- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 60.0f;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [filterLocakerArray count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	/*
    static NSString *SimpleTableIdentifier = @"SimpleTableIdentifier";
	UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:SimpleTableIdentifier];
	if(cell == nil){
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SimpleTableIdentifier]autorelease];
	}
	NSUInteger row = [indexPath row];
	cell.textLabel.text = [lockerArray objectAtIndex:row];
	return cell;
	*/
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	/*    if (cell == nil) {
	 cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
	 }
	 
	 // Set up the cell...
	 if(indexPath.section == 0) {
	 cell.text = [foodItems objectAtIndex:indexPath.row];
	 } else if(indexPath.section == 1) {
	 cell.text = [activityItems objectAtIndex:indexPath.row];
	 }*/
	
	
    if (cell == nil) {
        cell = [self getCellContentView:CellIdentifier];
    }
	cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
//	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	UILabel *keyLbl =  (UILabel *)[cell viewWithTag:1];
	UILabel *valueLbl =  (UILabel *)[cell viewWithTag:2];
	
	
	Vault *obj = (Vault *)[filterLocakerArray objectAtIndex:indexPath.row];
	keyLbl.text = obj.keyStr;
	valueLbl.text = obj.valueStr;
	
	/*	
	 if(indexPath.row%2) {
	 [lblTemp1 setBackgroundColor:[UIColor colorWithRed:0.219 green:0.586 blue:0.590 alpha:1.0]];
	 [lblTemp2 setBackgroundColor:[UIColor colorWithRed:0.219 green:0.586 blue:0.590 alpha:1.0]];
	 } else {
	 [lblTemp1 setBackgroundColor:[UIColor colorWithRed:0.391 green:0.871 blue:0.852 alpha:1.0]];
	 [lblTemp2 setBackgroundColor:[UIColor colorWithRed:0.391 green:0.871 blue:0.852 alpha:1.0]];
	 }
	 */
    return cell;
}
- (UITableViewCell *) getCellContentView:(NSString *)cellIdentifier {
	
	CGRect CellFrame = CGRectMake(0.0f, 0.0f, 320.0f, 60.0f);
	CGRect lblFrame1 = CGRectMake(15.0f, 0.0f, 260.0f, 30.0f);
	CGRect lblFrame2 = CGRectMake(15.0f, 31.0f, 260.0f, 30.0f);
	
	UILabel *lblTemp1;
	UILabel *lblTemp2;
	
	UITableViewCell *cell = [[[UITableViewCell alloc] initWithFrame:CellFrame reuseIdentifier:cellIdentifier] autorelease];
	
	
	//Initialize Label 1 with tag 1.
	lblTemp1 = [[UILabel alloc] initWithFrame:lblFrame1];
	[lblTemp1 setBackgroundColor:[UIColor clearColor]];
	//	[lblTemp1 sizeToFit];
	//	[lblTemp1 setMinimumFontSize:8.0];
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if([settings.fontName isEqualToString:@""] || settings.fontName == nil){
		[lblTemp1 setFont:[UIFont systemFontOfSize:20]];
		[settings storeFontSettings:@"" size:20.0];
	}else{
		//NSLog(@"font name %@",settings.fontName);	
		NSArray *fontNames = [UIFont fontNamesForFamilyName:settings.fontName];
		[lblTemp1 setFont:[UIFont fontWithName:[fontNames objectAtIndex:0] size:settings.fontSize]];
		//	[lblTemp2 setFont:[UIFont systemFontOfSize:15]];
	}
	
	lblTemp1.tag = 1;
	[cell.contentView addSubview:lblTemp1];
	[lblTemp1 release];
	
	//Initialize Label 2 with tag 2.
	lblTemp2 = [[UILabel alloc] initWithFrame:lblFrame2];
	[lblTemp2 setBackgroundColor:[UIColor clearColor]];
	lblTemp2.tag = 2;
	//	[lblTemp2 sizeToFit];
	//	[lblTemp2 setMinimumFontSize:8.0];
	
	[settings loadSettings];
	if([settings.fontName isEqualToString:@""] || settings.fontName == nil){
		[lblTemp2 setFont:[UIFont systemFontOfSize:20]];
		[settings storeFontSettings:@"" size:20.0];
	}else{
		////NSLog(@"font name %@",settings.fontName);	
		NSArray *fontNames = [UIFont fontNamesForFamilyName:settings.fontName];
		[lblTemp2 setFont:[UIFont fontWithName:[fontNames objectAtIndex:0] size:settings.fontSize]];
		//	[lblTemp2 setFont:[UIFont systemFontOfSize:15]];
	}
	[settings release];
	[cell.contentView addSubview:lblTemp2];
	[lblTemp2 release];
	
	return cell;
}

- (void)dealloc {
    [super dealloc];
}
- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}


- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
	ShowSecretViewController *showSecretView = [[ShowSecretViewController alloc] initWithNibName:@"ShowSecretViewController" bundle:nil];
	Vault *obj = (Vault*)[filterLocakerArray objectAtIndex:indexPath.row];
	showSecretView.vaultObj = obj;
	self.showSecretViewController = showSecretView;
	[self.view addSubview:self.showSecretViewController.view];
	[showSecretView release];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	Vault *obj = [filterLocakerArray objectAtIndex:indexPath.row];
	keyString = obj.keyStr;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    filterLocakerArray = [lockerArray mutableCopy];
    [lockerTable reloadData];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [self filterContentForSearchText:searchBar.text];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText isEqualToString:@""]) {
        filterLocakerArray = [lockerArray mutableCopy];
        [lockerTable reloadData];
        return;
    }
    
    [filterLocakerArray removeAllObjects];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"key_name contains[c] %@", searchText];
    NSArray *tempArray = [[resultArray filteredArrayUsingPredicate:resultPredicate]mutableCopy];
    NSPredicate *resultPredicate1 = [NSPredicate predicateWithFormat:@"key_value contains[c] %@", searchText];
    NSArray *tempArray1 = [[resultArray filteredArrayUsingPredicate:resultPredicate1]mutableCopy];
    for (NSDictionary *dictionary in tempArray){
        Vault *vault = [[Vault alloc] init];
        vault.keyStr = [dictionary valueForKey:@"key_name"];
        vault.valueStr = [dictionary valueForKey:@"key_value"];
        [filterLocakerArray addObject:vault];
        [vault release];
    }
    for (NSDictionary *dictionary in tempArray1){
        NSPredicate *resultPredicate3 = [NSPredicate predicateWithFormat:@"key_name == %@", [dictionary valueForKey:@"key_name"]];
        NSArray *tempArray3 = [[tempArray filteredArrayUsingPredicate:resultPredicate3]mutableCopy ];
        if (tempArray3.count > 0 ){
            continue;
        }
        Vault *vault = [[Vault alloc] init];
        vault.keyStr = [dictionary valueForKey:@"key_name"];
        vault.valueStr = [dictionary valueForKey:@"key_value"];
        [filterLocakerArray addObject:vault];
        [vault release];
    }
    [lockerTable reloadData];
}

- (void)filterContentForSearchText:(NSString*)searchText
{
    [searchBar resignFirstResponder];
    [filterLocakerArray removeAllObjects];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"key_name contains[c] %@", searchText];
    NSArray *tempArray = [[resultArray filteredArrayUsingPredicate:resultPredicate] mutableCopy];
    NSPredicate *resultPredicate1 = [NSPredicate predicateWithFormat:@"key_value contains[c] %@", searchText];
    NSArray *tempArray1 = [[resultArray filteredArrayUsingPredicate:resultPredicate1] mutableCopy];
    for (NSDictionary *dictionary in tempArray){
        Vault *vault = [[Vault alloc] init];
        vault.keyStr = [dictionary valueForKey:@"key_name"];
        vault.valueStr = [dictionary valueForKey:@"key_value"];
        [filterLocakerArray addObject:vault];
        [vault release];
    }
    for (NSDictionary *dictionary in tempArray1){
        NSPredicate *resultPredicate3 = [NSPredicate predicateWithFormat:@"key_name == %@", [dictionary valueForKey:@"key_name"]];
        NSArray *tempArray3 = [[tempArray filteredArrayUsingPredicate:resultPredicate3] mutableCopy];
        if (tempArray3.count > 0 ){
            continue;
        }
        Vault *vault = [[Vault alloc] init];
        vault.keyStr = [dictionary valueForKey:@"key_name"];
        vault.valueStr = [dictionary valueForKey:@"key_value"];
        [filterLocakerArray addObject:vault];
        [vault release];
    }
    [lockerTable reloadData];
}

@end
