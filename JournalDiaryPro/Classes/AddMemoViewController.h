///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   AddMemoViewController.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
@class VoiceMemoUtils;
@class SaveMemoViewController;
@class VoiceMemoViewController;
@interface AddMemoViewController : UIViewController <AVAudioPlayerDelegate>{
	IBOutlet UIButton *recordPauseButton;
	IBOutlet UIButton *playPauseButton;
	IBOutlet UIButton *clearBufferButton;
	IBOutlet UIButton *stopButton;
	IBOutlet UIButton *saveButton;
	IBOutlet UIButton *backButton;
	IBOutlet UILabel *timerLabel;
	IBOutlet UILabel *dateLabel;
	IBOutlet UILabel *statusLabel;
	IBOutlet UIImageView *imageView;
	AVAudioPlayer *player;
	VoiceMemoUtils *utils;
	BOOL recording;
	BOOL playing;
	BOOL editMode;
	BOOL retainRecording;
	BOOL blink;
	NSTimer *playTimer;
	NSTimer *recordTimer;
	NSString *tempFilePath;
	NSString *fileName;
	NSDate *memoDate;
	NSString *titleStr;
	SaveMemoViewController *saveMemoViewController;
	VoiceMemoViewController *voiceMemoViewController;
    
    IBOutlet UIImageView *backgroundImageView;
}
@property(nonatomic,retain)NSString *tempFilePath;
@property(nonatomic,retain)NSString *fileName;
@property(assign)BOOL editMode;
@property(assign)BOOL retainRecording;
@property(nonatomic,retain)NSDate *memoDate;
@property(nonatomic,retain)NSString *titleStr;
@property(nonatomic,retain)SaveMemoViewController *saveMemoViewController;
@property(nonatomic,retain)VoiceMemoViewController *voiceMemoViewController;
-(IBAction)recordPausePressed;
-(IBAction)playPausePressed;
-(IBAction)stopPressed;
-(IBAction)savePressed;
-(IBAction)backPressed;
-(IBAction)clearBufferPressed;
@end
