///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   SettingsPasswordViewController.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>
#import "SKPSMTPMessage.h"
@class SKPSMTPMessage;
@class SettingsViewController;
@interface SettingsPasswordViewController : UIViewController<SKPSMTPMessageDelegate>{
	IBOutlet UITextField *oldField;
	IBOutlet UITextField *newField;
	IBOutlet UITextField *retypeField;
	IBOutlet UITextField *nameField;
	IBOutlet UIImageView *imageView;
	SettingsViewController *settingsViewController;
}
@property(nonatomic,retain)SettingsViewController *settingsViewController;
-(IBAction)donePressed;
-(IBAction)backPressed;
-(IBAction)resetPressed;
@end
