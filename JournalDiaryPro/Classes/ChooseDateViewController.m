///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   ChooseDateViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "ChooseDateViewController.h"
#import "AddEntryViewController.h"
#import "SaveMemoViewController.h"
#import "AddEventViewController.h"
#import "Settings.h"
#import "eDiaryAppDelegate.h"
@implementation ChooseDateViewController
@synthesize content;
@synthesize initialDate;
@synthesize fileName;
@synthesize tempFilePath;
@synthesize preView;
@synthesize isEditMode;
@synthesize emote;
@synthesize events;
@synthesize oldFileName;
@synthesize saveViewController;
@synthesize addEntryViewController;
@synthesize addEventViewController;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        backgrndImaView.frame =CGRectMake(0, -20, 320, 568+40);
        imageView.frame = CGRectMake(0, -20, 320, 568+20);
        
        
        
    }
    
    else
    {
        imageView.frame =CGRectMake(12, 0, 320, 480);
        
        
    }
    
    
	[timePicker setDate:initialDate];
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}
-(IBAction)donePressed{
	if([preView isEqualToString:@"SaveMemo"]){
		
		SaveMemoViewController *saveView = [[SaveMemoViewController alloc] initWithNibName:@"SaveMemoViewController" bundle:nil];
		saveView.fileName = self.fileName;
		saveView.tempFilePath = self.tempFilePath;
		saveView.memoDate = [timePicker date];
		saveView.editMode = self.isEditMode;
		saveView.titleStr = self.content;
		saveView.oldFileName = self.oldFileName;
		self.saveViewController = saveView;
		[self.view addSubview:self.saveViewController.view];
		[saveView release];
	}else if([preView isEqualToString:@"Events"]){
		AddEventViewController *addEventView = [[AddEventViewController alloc] initWithNibName:@"AddEventViewController" bundle:nil];
		addEventView.contentStr = content; 
		addEventView.titleStr = self.fileName;
		addEventView.locationStr = self.tempFilePath;
		addEventView.isEditMode = self.isEditMode;
		addEventView.timeSelected = [timePicker date];
		addEventView.oldObj = self.events;
		self.addEventViewController = addEventView;
		[self.view addSubview:self.addEventViewController.view];
		[addEventView release];
	}else{
		AddEntryViewController *addEntryView = [[AddEntryViewController alloc] initWithNibName:@"AddEntryViewController" bundle:nil];
		addEntryView.contentStr = content;
		addEntryView.isEditMode = self.isEditMode;
		addEntryView.timeSelected = [timePicker date];
		addEntryView.emoticonNo = self.emote;
		addEntryView.oldDate = self.initialDate;
		self.addEntryViewController = addEntryView;
		[self.view addSubview:self.addEntryViewController.view];
		[addEntryView release];
	}
}

- (void)dealloc {
	
    [super dealloc];
}


@end
