///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   SettingsQAViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "SettingsQAViewController.h"
#import "SettingsViewController.h"
#import "eDiaryAppDelegate.h"
#import "Secure.h"
#import "Settings.h"
#import "VaultSettingsViewController.h"

@implementation SettingsQAViewController
@synthesize isVault;
@synthesize vaultSettingsViewController;
@synthesize settingsViewController;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[questionView becomeFirstResponder];
	Secure *secure = [[Secure alloc] init];
	if(isVault){
		questionView.text = [secure	getVaultQuestion];
		answerView.text = [secure getVaultAnswer];
	}else{
		questionView.text = [secure	getQuestion];
		answerView.text = [secure getAnswer];
	}
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	////NSLog(@"pic %d",settings.picEnabled);
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}
-(IBAction)donePressed{
	if([questionView.text isEqualToString:@""]){
		[eDiaryAppDelegate showMessage:@"Please enter a question to update" title:@"Empty Field"];
		return;
	}
	if([answerView.text isEqualToString:@""]){
		[eDiaryAppDelegate showMessage:@"Please enter a answer to update" title:@"Empty Field"];
		return;
	}
	Secure *secure = [[Secure alloc] init];
	if(isVault){
		if([secure getCount] == 0){
			[secure insert:@"" lockerPass:@"" question:@"" answer:@"" vaultQ:questionView.text vaultA:answerView.text];
		}else{ 
			[secure	updateVaultQuestion:questionView.text answer:answerView.text];
		}
	}else{
		if([secure getCount] == 0){
			[secure insert:@"" lockerPass:@"" question:questionView.text answer:answerView.text vaultQ:@"" vaultA:@""];
		}else{ 
			[secure	updateQuestion:questionView.text answer:answerView.text];
		}
	}
	[secure release];
	[questionView resignFirstResponder];
	[answerView resignFirstResponder];
	if(isVault){
		VaultSettingsViewController *vaultSettingsView = [[VaultSettingsViewController alloc] initWithNibName:@"VaultSettingsViewController" bundle:nil];
		self.vaultSettingsViewController = vaultSettingsView;
		[self.view addSubview:self.vaultSettingsViewController.view];
		[vaultSettingsView release];
	}else {
		SettingsViewController *settingsView = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController"bundle:nil];
		self.settingsViewController = settingsView;
		[self.view addSubview:self.settingsViewController.view];
		[settingsView release];
	}
}
-(IBAction)backPressed{
	[questionView resignFirstResponder];
	[answerView resignFirstResponder];
	if(isVault){
		VaultSettingsViewController *vaultSettingsView = [[VaultSettingsViewController alloc] initWithNibName:@"VaultSettingsViewController" bundle:nil];
		self.vaultSettingsViewController = vaultSettingsView;
		[self.view addSubview:self.vaultSettingsViewController.view];
		[vaultSettingsView release];
	}else {
		SettingsViewController *settingsView = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController"bundle:nil];
		self.settingsViewController = settingsView;
		[self.view addSubview:self.settingsViewController.view];
		[settingsView release];
	}
}
-(IBAction)clearPressed{
	questionView.text = @"";
	answerView.text = @"";
	
}
- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
