///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   Settings.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	18/11/09
// Change reason :  methods added
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "Settings.h"



@implementation Settings
@synthesize passwordEnabled;
@synthesize passwordStr;
@synthesize fontName;
@synthesize fontSize;
@synthesize emailEnabled;
@synthesize emailType;
@synthesize emailServerAddress;
@synthesize emailAddress;
@synthesize emailPassword;
@synthesize currentPicture;
@synthesize picOfDayEnabled;


@synthesize animateEmoticonEnabled;
@synthesize gpsEnabled; 
@synthesize faceIDEnabled;
@synthesize daysGap; 

@synthesize picEnabled;
@synthesize pictureDate;
@synthesize userName;
@synthesize picOfTheDay;
@synthesize globalDate;
-(void)loadSettings{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	
	self.passwordEnabled = [userDefaults boolForKey:kPasswordEnabled];
	self.fontName = [userDefaults objectForKey:kFontName];
	self.fontSize = [userDefaults floatForKey:kFontSize];
	self.emailEnabled = [userDefaults boolForKey:kEmailEnabled];
	self.emailType = [userDefaults objectForKey:kEmailType];
	self.emailServerAddress = [userDefaults objectForKey:kEmailSMTPServer];
	self.emailAddress = [userDefaults stringForKey:kEmailAddress];
	self.emailPassword = [userDefaults objectForKey:kEmailPassword];
	self.currentPicture = [userDefaults objectForKey:kCurrentPicture];
	self.picOfDayEnabled = [userDefaults boolForKey:kPicOfDayEnabled];
	

	self.animateEmoticonEnabled = [userDefaults boolForKey:kAnimateEmoticonEnabled];
	self.gpsEnabled = [userDefaults boolForKey:kGpsEnabled]; 
    self.faceIDEnabled = [userDefaults boolForKey:kFaceIDEnabled];
	self.daysGap = [userDefaults integerForKey:kDaysGap]; 
	
	self.picEnabled = [userDefaults boolForKey:kBackgroundPictureEnabled];
	self.pictureDate = [userDefaults objectForKey:kPicDate];
	self.userName = [userDefaults objectForKey:kUserName];
	self.picOfTheDay = [userDefaults objectForKey:kPicOfDay];
	self.globalDate = [userDefaults objectForKey:kGlobalDate];
}
-(void)storeUserName:(NSString *)name{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setObject:name forKey:kUserName];
}
-(void)storeSettings:(BOOL)passOn{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setBool:passOn forKey:kPasswordEnabled];
}
-(void)storeFontSettings:(NSString *)fName size:(float)fSize{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setObject:fName forKey:kFontName];
	[userDefaults setFloat:fSize forKey:kFontSize];
}
-(void)storeEmailEnabled:(BOOL)enabled{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setBool:enabled forKey:kEmailEnabled];
}
-(void)storeEmailType:(NSString *)type{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setObject:type forKey:kEmailType];
}
-(void)initEmail{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setObject:@"" forKey:kEmailAddress];
	[userDefaults setObject:@"" forKey:kEmailPassword];
	[userDefaults setObject:@"" forKey:kEmailSMTPServer];
}
-(void)storeEmailSettings:(NSString *)address SMTPServer:(NSString*)server password:(NSString *)pwd{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setObject:address forKey:kEmailAddress];
	[userDefaults setObject:server forKey:kEmailSMTPServer];
	[userDefaults setObject:pwd forKey:kEmailPassword];
}
-(void)storePictureName:(NSString *)pic{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setObject:pic forKey:kCurrentPicture];
}
-(void)storePictureDate:(NSString *)picDate picture:(NSString *)pic{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setObject:picDate forKey:kPicDate];
	[userDefaults setObject:pic forKey:kPicOfDay];
}
-(void)storePictureEnabled:(BOOL)enabled{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setBool:enabled forKey:kBackgroundPictureEnabled];
}
-(void)storeFaceIDEnabled:(BOOL)enabled{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:enabled forKey:kFaceIDEnabled];
}
-(void)storePictureOfTheDayEnabled:(BOOL)enabled{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setBool:enabled forKey:kPicOfDayEnabled];
}
-(void)storeGlobalDate:(NSString *)dateStr{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setObject:dateStr forKey:kGlobalDate];
}

-(void)storeAnimateEmoticonEnabled:(BOOL)enabled
{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setBool:enabled forKey:kAnimateEmoticonEnabled];
}

-(void)storeGpsEnabled:(BOOL)enabled
{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setBool:enabled forKey:kGpsEnabled];
}

-(void)storeDaysGap:(int)value
{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	//[userDefaults setBool:enabled forKey:kGpsEnabled];
	[userDefaults setInteger:value forKey:kDaysGap];
}


@end
