//
//  ShowVideoController.h
//  Journal Diary
//
//  Created by seasia on 15/09/10.
//  Copyright 2010 Sulaba Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import"MessageUI/MessageUI.h"
@class AddVideoMemo;
@class VoiceMemo;
@class VideoMemoViewController;
@class HomeViewController;
@interface ShowVideoController : UIViewController <MFMailComposeViewControllerDelegate, MPMediaPickerControllerDelegate>{
	MPMoviePlayerController *theMovie;
	NSString *filepath;
	BOOL iseditmode;
	AddVideoMemo *addVideoController;
	NSString *olddate;
	NSString *	oldtime;
	VideoMemoViewController *videoMemoViewController;
	HomeViewController *home;
	BOOL istableviewclicked;
	NSData *data_video;
	NSString *video_title;
	IBOutlet UILabel *videolabel;
}
@property(nonatomic,retain)	IBOutlet UILabel *videolabel;
@property(nonatomic,retain)	NSString *video_title;
@property(nonatomic,retain)HomeViewController *home;
@property 	BOOL istableviewclicked;
@property(nonatomic,retain)VideoMemoViewController *videoMemoViewController;
@property(nonatomic,retain)	NSString *olddate;
@property(nonatomic,retain)	NSString *oldtime;
@property(nonatomic,retain)AddVideoMemo *addVideoController;

@property BOOL iseditmode;
@property(nonatomic,retain)NSString *filepath;
-(IBAction)backPressed;
-(IBAction)updatePressed;
-(IBAction)playPressed;
-(IBAction)stopPressed;
-(IBAction)deletePressed;
-(IBAction)emailPressed;
@end
