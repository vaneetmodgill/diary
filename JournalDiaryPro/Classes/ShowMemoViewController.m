///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   ShowMemoViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "ShowMemoViewController.h"
#import "DiaryObject.h"
#import "DiaryEntryViewController.h"
#import "VoiceMemo.h"
#import "VoiceMemoUtils.h"
#import "VoiceMemoViewController.h"
#import "AddMemoViewController.h"
#import "Settings.h"
#import "eDiaryAppDelegate.h"
@implementation ShowMemoViewController
@synthesize diaryObj;
@synthesize voiceMemoViewController;
@synthesize addMemoViewController;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        backgrndImaView.frame =CGRectMake(0, -20, 320, 568+40);
        imageView.frame = CGRectMake(0, -20, 320, 568+20);
        
        
        
    }
    
    else
    {
        imageView.frame =CGRectMake(12, 0, 320, 480);
        
        
    }

    
	titleView.text = diaryObj.content;
	dateLabel.text =[NSString stringWithFormat:@"%@ %@",diaryObj.dateStr,diaryObj.timeStr];
	utils = [[VoiceMemoUtils alloc] init];
	[utils initSoundFile];
	NSString *path = [utils getDocumentsPath];
	
	path = [path stringByAppendingPathComponent:diaryObj.emoticon];
	//NSLog(@"path %@",path);
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	BOOL success = [fileManager fileExistsAtPath:path];
	if(success){
		//NSLog(@"sucess");	
		player = [utils initPlayer:path];
		player.delegate = self;
		memoSlider.maximumValue = player.duration;
		memoSlider.value = 0.0;
	}
	filepath=[NSString stringWithFormat:@"%@",path];
	stopButton.enabled = NO;
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];
    
    [self cycleTheGlobalMailComposer];

    [super viewDidLoad];
}
-(void)cycleTheGlobalMailComposer
{
    // we are cycling the damned GlobalMailComposer... due to horrible iOS issue
    globalMailComposer = nil;
    globalMailComposer = [[MFMailComposeViewController alloc] init];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{	
	UILabel *message=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
	message.hidden = NO;
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			message.text = @"Result: canceled";
			//[self dismissModalViewControllerAnimated:YES];
			
			break;
		case MFMailComposeResultSaved:
			message.text = @"Result: saved";
			//[self dismissModalViewControllerAnimated:YES];
			break;
		case MFMailComposeResultSent:
			message.text = @"Result: sent";
			UIAlertView *obj_alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Message sent" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			obj_alert.delegate=self;
			[obj_alert show];
			[obj_alert release];
			//[self dismissModalViewControllerAnimated:YES];
			
			
			break;
		case MFMailComposeResultFailed:
			message.text = @"Result: failed";
			UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Error in sending" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			alert.delegate=self;
			[alert show];
			[alert release];
			//[self dismissModalViewControllerAnimated:YES];
			
			break;
		default:
			message.text = @"Result: not sent";
			break;
	}
	//[self dismissModalViewControllerAnimated:YES];
    
  //  [self dismissViewControllerAnimated:YES completion:nil];
    
    [self dismissViewControllerAnimated:YES completion:^
     { [self cycleTheGlobalMailComposer]; }
     ];
	
	//[self.navigationController popViewControllerAnimated:YES];
	
	
}


////////////////// ************ CHECKING INTERNET CONNECTIVITY  **************///////////////////////////
-(BOOL)connectionToInternet
{
	NSString *URLString=[NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://www.google.com"]];
	
	return((URLString!=NULL)?YES:NO);
	// Create zero addy
	
	
}

// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
	
	globalMailComposer.mailComposeDelegate = self;
	
	[globalMailComposer setSubject:@"Diary Entry"];
	//@"All Projects"];
	
	
	//strings containing 
	NSString *emailBody = @"";
	// Fill out the email body text
	
	
	
	emailBody=[NSString stringWithFormat:@" Voice Recorded on %@ at %@ \n",diaryObj.dateStr,diaryObj.timeStr];
	NSLog(@"Email Body  ::::  \n %@",emailBody);
	
	
	//NSData *data1=UIImagePNGRepresentation(imageViewBackground.image);
	[globalMailComposer setMessageBody:emailBody isHTML:NO];
	if([data_voice length]!=0)
	{
		[globalMailComposer addAttachmentData:data_voice mimeType:@"Voice/mp3" fileName:@"Voice"];
	}
	
	//	[self.view addSubview:picker.view];
	//[self presentModalViewController:picker animated:YES];
    
    [self presentViewController:globalMailComposer animated:YES completion:nil];
    
//    [self.view.window.rootViewController presentViewController:globalMailComposer animated:YES completion:nil];
    
    //[self.parentViewController presentViewController:globalMailComposer animated:YES completion:nil];
    
}
-(IBAction)emailPressed
{
	NSString *p=[utils getDocumentsPath];
	NSLog(@"%@",p);
	NSString *pp=[NSString stringWithFormat:@"%@.mp3",p];
	
	p = [p stringByAppendingPathComponent:diaryObj.emoticon];
	data_voice=[NSData dataWithContentsOfURL:[NSURL fileURLWithPath:p]];
    
	[self displayComposerSheet];
}
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}
-(IBAction)backPressed{
	[player stop];
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	VoiceMemoViewController *voiceMemoView = [[VoiceMemoViewController alloc] initWithNibName:@"VoiceMemoViewController" bundle:nil];
	self.voiceMemoViewController = voiceMemoView;
	[self.view addSubview:self.voiceMemoViewController.view];
	[voiceMemoView release];
	[UIView commitAnimations];
}
-(IBAction)sliderMoved{
	player.currentTime = memoSlider.value;
}
-(void)updatePlayTime{
	memoSlider.value = player.currentTime;
	timeLabel.text = [utils getRecordTime:player.currentTime];
}
-(IBAction)playPressed{
	stopButton.enabled = YES;
	player.currentTime = memoSlider.value;
	[player play];
	timer = [NSTimer scheduledTimerWithTimeInterval:0.1  target:self selector:@selector(updatePlayTime) userInfo:nil repeats:YES];
}
-(IBAction)stopPressed{
	[player	 stop];
	stopButton.enabled = NO;
	player.currentTime = 0.0;
	[timer invalidate];
	[self updatePlayTime]; 
}
-(IBAction)deletePressed{
	[player stop];
	UIActionSheet *deleteAction = [[UIActionSheet alloc] initWithTitle:@"Are you sure you want to delete?" delegate:self cancelButtonTitle:@"No" destructiveButtonTitle:@"Yes" otherButtonTitles:nil];
	[deleteAction showInView:self.view];
	[deleteAction release];
}
-(IBAction)updatePressed{
	[player stop];
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
	AddMemoViewController *addMemoView = [[AddMemoViewController alloc] initWithNibName:@"AddMemoViewController" bundle:nil];
	addMemoView.editMode = YES;
	addMemoView.fileName = diaryObj.emoticon;
	addMemoView.titleStr = self.diaryObj.content;
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy-MM-dd"];
	//[formatter setTimeStyle:NSDateFormatterShortStyle];

	
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit | NSMinuteCalendarUnit ;
	NSDateComponents *compsDate = [gregorian components:unitFlags fromDate:[formatter dateFromString:[NSString stringWithFormat:@"%@",self.diaryObj.dateStr]]];
	
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	NSDateComponents *compsTime = [gregorian components:unitFlags fromDate:[formatter dateFromString:[NSString stringWithFormat:@"%@",self.diaryObj.timeStr]]];
	[formatter release];
	[compsTime setDay:[compsDate day]];
	[compsTime setMonth:[compsDate month]];
	[compsTime setYear:[compsDate year]];
	addMemoView.memoDate = [gregorian dateFromComponents:compsTime];
	[gregorian release];
	self.addMemoViewController = addMemoView;
	[self.view addSubview:self.addMemoViewController.view];
	[addMemoView release];
	[UIView commitAnimations];
}
- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)dealloc {
	[utils release];
    [super dealloc];
}

- (void) audioPlayerDidFinishPlaying: (AVAudioPlayer *) avplayer
                        successfully: (BOOL) flag {
    if (flag == YES) {
        //NSLog(@"play sucess");
		[timer invalidate];
		stopButton.enabled = NO;
		avplayer.currentTime = 0.0;
		[self updatePlayTime];
    }else{
		//NSLog(@"play failure");
	}
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
	//NSLog(@"index %d",buttonIndex);
	if(buttonIndex == 0){
		//NSLog(@"delete");
		NSFileManager *fileManager = [NSFileManager defaultManager];
	//	VoiceMemoUtils *utils = [[VoiceMemoUtils alloc] init];
		NSString *path = [utils getDocumentsPath];
		path = [path stringByAppendingPathComponent:self.diaryObj.emoticon];
		//NSLog(@"path %@",path);
//		NSError *error;
		BOOL deleted = [fileManager removeItemAtPath:path error:NULL];
		if(!deleted){
			// //NSLog(@"An error occured while file deletion! -- %@", error);
			////NSLog(@"error occured");
		}
	//	[utils release];
		VoiceMemo *voiceMemo = [[VoiceMemo alloc] init];
		[voiceMemo deleteMemo:self.diaryObj.emoticon];
		[voiceMemo release];
		VoiceMemoViewController *voiceMemoView = [[VoiceMemoViewController alloc] initWithNibName:@"VoiceMemoViewController" bundle:nil];
		self.voiceMemoViewController = voiceMemoView;
		[self.view addSubview:self.voiceMemoViewController.view];
		[voiceMemoView release];
	}
	[actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
}	
@end
