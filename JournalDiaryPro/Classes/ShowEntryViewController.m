///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   ShowEntryViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <Twitter/Twitter.h>

#import "ShowEntryViewController.h"
#import "DiaryEntryViewController.h"
#import "AddEntryViewController.h"
#import "DiaryObject.h"
#import "Diary.h"
#import "Settings.h"
#import "eDiaryAppDelegate.h"
#import "Utility.h"
#import "Email.h"
#import "TwitterRequest.h"
@implementation ShowEntryViewController
@synthesize entryObj;
@synthesize searchStr;
@synthesize searchDone;
@synthesize editEntryViewController;
@synthesize diaryEntryViewController;
@synthesize imagePicker,imageViewBackground,choosePictureViewController,imageforeverydiary;
@synthesize Latitude,Longitude;
@synthesize completeAddress;
@synthesize textview2,image_data,tempPath,image_name_save;

int ptr;
NSMutableArray *imageDetails;
static NSMutableDictionary *photoUrl;
UIImage *image;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

-(IBAction)facebookPressed
{
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *composeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        if (composeViewController) {
            
            NSLog(@"Twiiter Post  %@", entryObj.content);
            
            [composeViewController setInitialText:entryObj.content];
            [composeViewController addImage:self.imageViewBackground.image];
            
            [composeViewController setCompletionHandler:^(SLComposeViewControllerResult result) {
                if (result == SLComposeViewControllerResultDone) {
                    
                    NSLog(@"Posted");
                    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Facebook !!" message:@"Your message has been posted." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    
                    [alert show];
                    
                } else if (result == SLComposeViewControllerResultCancelled) {
                    
                    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Facebook !!" message:@"Your post has been cancelled." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    
                    [alert show];
                    NSLog(@"Post Cancelled");
                } else {
                    
                    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Facebook !!" message:@"Your post has been failed." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    
                    [alert show];
                    
                    NSLog(@"Post Failed");
                }
            }];
            [self presentViewController:composeViewController animated:YES completion:nil];
        }
    }
    else
    {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Facebook !!" message:@"Please configure your Facebook account in settings of your iPhone " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        

    }
    
    
    return ;
    
	ConnectFacebookViewController *obj_fb = [[ConnectFacebookViewController alloc]init];
	//[obj_fb passToPublish:entryObj.content];
	obj_fb.cont=entryObj.content;
	obj_fb.imageupload=imageViewBackground.image;
	[self.view addSubview:obj_fb.view];

}
- (void) enableView:(BOOL)enable hideControls:(BOOL)hide 
{
	
	[[NSNotificationCenter defaultCenter] postNotificationName: @"enableDisableUserInteraction" object: nil];
}

- (void)twitpicEngine:(TwitpicEngine *)engine didUploadImageWithResponse:(NSString *)response{
	
	
	//[ActivityIndicator stopAnimating];
	//[alertMain dismissWithClickedButtonIndex:0 animated:YES];
	[self enableView:YES hideControls:YES];
	
	if([response hasPrefix:@"http://"])
	{
		if(photoUrl==nil)
		{
			photoUrl=[[NSMutableDictionary alloc]init];
		}
		
		[photoUrl setObject:[NSString stringWithFormat:@"http://m.%@/full",[response substringFromIndex:[@"http://" length]]] forKey:@"PhotoURL"];
		//self.photoUrl = [NSString stringWithFormat:@"http://m.%@",[response substringFromIndex:[@"http://" length]]];
		
		if(ptr<imageDetails.count)
		{
			ptr=ptr+1;
			[self UploadAllImages];
		}
		else {
			
			
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Photo Uploaded",@"Photo Uploaded") message:NSLocalizedString(@"Do you want to be redirected to twitter to view the uploaded photo?",@"Do you want to be redirected to twitter to view the uploaded photo?") delegate:self cancelButtonTitle:NSLocalizedString(@"No",@"No") otherButtonTitles:NSLocalizedString(@"Yes",@"Yes"), nil];
			//[alert show];
			[alert release];
		}
	}
	else if([[response uppercaseString] isEqualToString:@"ERROR"])
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",@"Error") message:NSLocalizedString(@"Unable to upload Scribble",@"Unable to upload Scribble") delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss",@"Dismiss") otherButtonTitles: nil];
		[alert show];
		//[alert release];
	}
	else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",@"Error") message:response delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss",@"Dismiss") otherButtonTitles: nil];
		[alert show];
		//[alert release];
	}
}




-(void)AddAlertWindow
{
	alertMain = [[[UIAlertView alloc] initWithTitle:@"Uploading Photo\nPlease Wait..." message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil] autorelease];
	[alertMain show];
	
//	ActivityIndicator=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	//ActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	
	// Adjust the indicator so it is up a few pixels from the bottom of the alert
	//ActivityIndicator.center = CGPointMake(150, 85);
	//[alertMain addSubview:ActivityIndicator];
	//[ActivityIndicator startAnimating];
}

-(void)UploadAllImages
{
	if(ptr<imageDetails.count)
	{
		
		TwitpicEngine *twitpicEngine = [[TwitpicEngine alloc] initWithDelegate:self];
		//[self AddAlertWindow];
		[twitpicEngine uploadImageToTwitpic:self.imageViewBackground.image withMessage:entryObj.content username:[[TwitterDetails objectAtIndex:0] twitter_un] password:[[TwitterDetails objectAtIndex:0] twitter_password]];
		[twitpicEngine release];
		
	}
	
	else 
	{
		photouploaded=YES;
		[self status_updatecallback];
		UIAlertView *baseAlert = [[UIAlertView alloc] initWithTitle:@"Feed Uploaded Successfully." message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
		[baseAlert show];
		
	}
}

-(IBAction)twitterPressed
{
	ptr=0;
	Diary *TManager=[Diary alloc];
    
    
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
            
        SLComposeViewController *composeViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        if (composeViewController) {
            
            NSLog(@"Twiiter Post  %@", entryObj.content);
            
            [composeViewController setInitialText:entryObj.content];
            [composeViewController addImage:self.imageViewBackground.image];

            [composeViewController setCompletionHandler:^(SLComposeViewControllerResult result) {
                if (result == SLComposeViewControllerResultDone) {
                    
                    NSLog(@"Posted");
                    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Twitter !!" message:@"Your message has been tweeted." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    
                    [alert show];
                    
                } else if (result == SLComposeViewControllerResultCancelled) {
                    
                    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Twitter !!" message:@"Your tweet has been cancelled." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    
                    [alert show];
                    NSLog(@"Post Cancelled");
                } else {
                    
                    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Twitter !!" message:@"Your tweet has been failed." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    
                    [alert show];
                    
                    NSLog(@"Post Failed");
                }
            }];
            [self presentViewController:composeViewController animated:YES completion:nil];
        }
    }
        else
        {
            UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Twitter !!" message:@"Please configure your Twitter account in settings of your iPhone " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            
            
        }
    
    return ;

    
    
    
	TwitterDetails=[TManager getTwitterDetails];
	//TwitterDetails=[TManager getTwitterDetails];
	
	//Databasemanager *TManager=[Databasemanager alloc];
	//NSMutableArray *TwitterDetails=[TManager GetTwitterDetails];
	
	alertMain = [[UIAlertView alloc] initWithTitle:@"Twitter Account Details" 
										   message:@"\n\n\n" // IMPORTANT
										  delegate:self 
								 cancelButtonTitle:@"Cancel" 
								 otherButtonTitles:@"Enter", nil];
	
	textField = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 50.0, 260.0, 25.0)]; 
	textField.delegate=self;
	textField.keyboardType=UIKeyboardTypeEmailAddress;
	[textField setBackgroundColor:[UIColor whiteColor]];
	[textField setPlaceholder:@"username"];
	if([TwitterDetails count]>=1)
	{
	textField.text=[[TwitterDetails objectAtIndex:0] twitter_un];
	}
	
	
	[alertMain addSubview:textField];
	
	textField2 = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 85.0, 260.0, 25.0)];
		textField2.delegate=self;
	textField2.keyboardType=UIKeyboardTypeEmailAddress;
	[textField2 setBackgroundColor:[UIColor whiteColor]];
	[textField2 setPlaceholder:@"password"];
	[textField2 setSecureTextEntry:YES];
	if([TwitterDetails count]>=1)
	{

	textField2.text=[[TwitterDetails objectAtIndex:0] twitter_password];
	}
	
	TManager.twitter_un=textField.text;
		TManager.twitter_password=textField2.text;
	
	
	[alertMain addSubview:textField2];
	
	// set place
	[alertMain setTransform:CGAffineTransformMakeTranslation(0.0, 0)];
	[alertMain show];
    [alertMain release];
	
	// set cursor and show keyboard
	//[textField becomeFirstResponder];
	
	
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField1
{
//	[textField resignFirstResponder];
//		[textField re
	
	[textField2 resignFirstResponder];
	
	
	//else
		
		//[self UploadAllImages];
	return YES;	
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	UILabel *message=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
	message.hidden = NO;
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			message.text = @"Result: canceled";
			//[self dismissModalViewControllerAnimated:YES];
			
			break;
		case MFMailComposeResultSaved:
			message.text = @"Result: saved";
			//[self dismissModalViewControllerAnimated:YES];
			break;
		case MFMailComposeResultSent:
			message.text = @"Result: sent";
			UIAlertView *obj_alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Message sent" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			obj_alert.delegate=self;
			[obj_alert show];
			[obj_alert release];
			//[self dismissModalViewControllerAnimated:YES];
			
			
			break;
		case MFMailComposeResultFailed:
			message.text = @"Result: failed";
			UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Error in sending" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			alert.delegate=self;
			[alert show];
			[alert release];
			//[self dismissModalViewControllerAnimated:YES];
			
			break;
		default:
			message.text = @"Result: not sent";
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
	
	//[self.navigationController popViewControllerAnimated:YES];
	
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        
        backgroundImageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        backgroundImageView.frame = CGRectMake(0, -20, 320, 568+40);
        imageView.frame =CGRectMake(12, -20, 320, 568+40);
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 568+20);
    }
    else
    {
        backgroundImageView.frame = CGRectMake(0, 0, 320, 480);
        imageView.frame =CGRectMake(12, 0, 320, 480);
        
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 480+20);
        
    }

	
}



////////////////// ************ CHECKING INTERNET CONNECTIVITY  **************///////////////////////////
-(BOOL)connectionToInternet
{
	NSString *URLString=[NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://www.google.com"]];
	
	return((URLString!=NULL)?YES:NO);
	// Create zero addy
	
	
}

// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	
	picker.mailComposeDelegate = self;
	
	[picker setSubject:@"Diary Entry"];
	//@"All Projects"];
	
	
	//strings containing 
	NSString *emailBody = @"";
	// Fill out the email body text
	
	
	
	emailBody=[NSString stringWithFormat:@"%@  %@\n%@ \n",entryObj.dateStr,entryObj.timeStr,entryObj.content];
	NSLog(@"%@",emailBody);
	
	
	NSData *data1=UIImagePNGRepresentation(imageViewBackground.image);
	[picker setMessageBody:emailBody isHTML:NO];
	if([data1 length]!=0)
	{
	[picker addAttachmentData:data1 mimeType:@"image/png" fileName:@"Image"];
	}
	
	//	[self.view addSubview:picker.view];
//	[self presentModalViewController:picker animated:YES];
    [self presentViewController:picker animated:YES completion:nil];
    
    [picker release];
} 

-(IBAction)emailPressed
{
	alertemail=YES;
	[self displayComposerSheet];	
}
-(IBAction)photoPressed
{

	//UIActionSheet *actionSheet=[[UIActionSheet alloc] initWithTitle: delegate:<#(id <UIActionSheetDelegate>)delegate#> cancelButtonTitle:<#(NSString *)cancelButtonTitle#> destructiveButtonTitle:<#(NSString *)destructiveButtonTitle#> otherButtonTitles:<#(NSString *)otherButtonTitles#>

	actionsheet_photo=[[UIActionSheet alloc] initWithTitle:@"Choose Photo" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Photo Library",@"Camera",nil];
	[actionsheet_photo showInView:self.view];
	//[actionSheet release];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	
	if(!alertemail && !photouploaded)
	{
	if ([alertView title] == @"Twitter Account Details")
	{
		if( buttonIndex == 1)
		{
			Diary *TwitManager=[Diary alloc];
			
			if([TwitterDetails count]<1)
			{
				if(textField.text!=nil && textField2.text!=nil)
				[TwitManager setTwitterDetails:textField.text :textField2.text];

			}
			else{
				TwitManager.twitter_un=textField.text;
								TwitManager.twitter_password=textField2.text;
				[TwitManager updateTwitterDetails];
			}
			
			
		}
		else if( buttonIndex == 0)
		{
			
			
			
		}
		
	}
	
	
		Diary *TManager=[Diary alloc];
		TwitterDetails=[TManager getTwitterDetails];

	if([TwitterDetails count]<1)
	 {
		 if(buttonIndex==1)
		 {
	 UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"Warning!!" message:@"Incorrect Login" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
	 [alt show];
	 [alt release];
		 }
	 
	 
	 }
	 
	 else
	 {
		 if(buttonIndex==1)
		 {
		 loadingactionsheet=[[UIActionSheet alloc] initWithTitle:@"Posting to Twitter" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
		 [loadingactionsheet showInView:self.view];

		// [self statuses_update:entryObj.content delegate:self];
	 [self UploadAllImages];
		 }
	 }
	}
	else {
		if(photouploaded==YES)
		{
			/*TwitterRequest *t=[[TwitterRequest alloc] init];
			t.username=textField.text;
			t.password=textField2.text;*/
						
			//[t statuses_update:entryObj.content delegate:self requestSelector:@selector(status_updatecallback:)];
			
			
		}
		photouploaded=NO;
		alertemail=NO;
	}

	
	
}
-(void)status_updatecallback{
	[loadingactionsheet dismissWithClickedButtonIndex:0 animated:YES];
	[loadingactionsheet release];
	//NSLog(@"%@",[[NSString alloc] initWithData:conten encoding:NSUTF8StringEncoding]);
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{	//[v2 removeFromSuperview];
	// the user clicked one of the OK/Cancel buttons
	if([actionSheet isEqual:actionsheet_photo])
	   {
	if (buttonIndex == 0)
	{
		//isimagepicked=YES;
		imagePicker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
		//[self.view addSubview:imagepicker.view];
		//[v2 removeFromSuperview];
		[self presentModalViewController:imagePicker animated:NO];
	}
	else if(buttonIndex == 1)
	{
		NSString *devicetype=[[UIDevice currentDevice] model];
	//	if([devicetype isEqualToString:@"iPhone"])
	//	{
	//		//isimagepicked=YES;
			imagePicker.sourceType=UIImagePickerControllerSourceTypeCamera;
			[self presentModalViewController:imagePicker animated:NO];
	//	}
	//	else {
	//		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Warning" message:@"This feature is not available on this device" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
	//		[alert show];
	//		[alert release];
	//	}
		
		
	}
	else {
		//isimagepicked=NO;
		
	}
	   }
	   else
	   {
		   if(buttonIndex == 0){
			   ////NSLog(@"delete");
			   Diary *diary = [[Diary alloc] init];
			   [diary deleteEntry:entryObj.dateStr time:entryObj.timeStr entry:entryObj.content];
			   [diary release];
			   DiaryEntryViewController *diaryEntryView = [[DiaryEntryViewController alloc] initWithNibName:@"DiaryEntryViewController" bundle:nil];
			   self.diaryEntryViewController = diaryEntryView;
			   [self.view addSubview:self.diaryEntryViewController.view];
			   [diaryEntryView release];
		   }
	   }
	[actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];

	//[v1 addSubview:v2];
	
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        backgrndImaView.frame =CGRectMake(0, -20, 320, 568+40);
        imageView.frame = CGRectMake(0, -20, 320, 568+20);
        
        
        
    }
    
    else
    {
        imageView.frame =CGRectMake(12, 0, 320, 480);
        
        
    }

    
    
	photouploaded=NO;
	alertemail=NO;
	timeLbl.text = [NSString stringWithFormat:@"%@ %@",entryObj.dateStr,entryObj.timeStr];
	contentView.text = entryObj.content;
	//NSLog(@"time %@",entryObj.timeStr);
	
	UIImage *smile = [UIImage imageNamed:@"smile.png"];		
	UIImage *angry = [UIImage imageNamed:@"beyond_endurance.png"];
	UIImage *cool = [UIImage imageNamed:@"rockn_roll.png"];
	UIImage *frown = [UIImage imageNamed:@"unhappy.png"];
	UIImage *grin = [UIImage imageNamed:@"big_smile.png"];
	UIImage *innocent = [UIImage imageNamed:@"shame.png"];
	UIImage *kiss = [UIImage imageNamed:@"greeding.png"];
	UIImage *naughty = [UIImage imageNamed:@"the_devil.png"];
	UIImage *sealed = [UIImage imageNamed:@"what.png"];
	UIImage *tear = [UIImage imageNamed:@"cry.png"];	
	UIImage *toungue = [UIImage imageNamed:@"bad_smile.png"];	
	UIImage *undecided = [UIImage imageNamed:@"i_have_no_idea.png"];	
	UIImage *wink = [UIImage imageNamed:@"wicked.png"];
	UIImage *wow = [UIImage imageNamed:@"surprise.png"];	
	UIImage *yay = [UIImage imageNamed:@"pretty_smile.png"];
	
	imagePicker=[[UIImagePickerController alloc] init];
	imagePicker.delegate=self;
	
	NSArray *emoticonArray = [[NSArray alloc] initWithObjects:smile,angry,cool,frown,grin,innocent,kiss,naughty,sealed,tear,toungue,undecided,wink,wow,yay,nil];
	if(![entryObj.emoticon isEqualToString:@"0"]){
		UIGraphicsBeginImageContext(CGSizeMake(40.0,40.0));
		[(UIImage*)[emoticonArray objectAtIndex:[(NSNumber*)entryObj.emoticon integerValue]-1] drawInRect:CGRectMake(0.0, 0.0, 40.0, 40.0)];
		UIImage *emoticonImg = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();	
	[emoticonButton setImage:emoticonImg forState:UIControlStateNormal];
	}
	[emoticonArray release];
	
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if([settings.fontName isEqualToString:@""] || settings.fontName == nil){
		[contentView setFont:[UIFont systemFontOfSize:15]];
		[settings storeFontSettings:@"" size:15.0];
	}else{
		//NSLog(@"font name %@",settings.fontName);	
		NSArray *fontNames = [UIFont fontNamesForFamilyName:settings.fontName];
		[contentView setFont:[UIFont fontWithName:[fontNames objectAtIndex:0] size:settings.fontSize]];
	}
	
	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	
	
	[settings release];
	
	Diary *d=[[Diary alloc] init];
	image_array=[d selectImagewhereEfficiently:entryObj.dateStr time:entryObj.timeStr notes:entryObj.content];
	
	/*CGFloat imageWidth = CGImageGetWidth([[[image_array objectAtIndex:0] imageback] CGImage]);
	CGFloat imageHeight = CGImageGetHeight([image CGImage]);
	UIGraphicsBeginImageContext(CGSizeMake(imageWidth, imageHeight));
	CGRect drawRect = CGRectMake( (imageWidth/2 -160), (imageHeight/2 - 240), 320, 480);
	*/
	//TwitterDetails=[d getTwitterDetails];
	//image_array=[d selectImagewhere:entryObj.dateStr time:entryObj.timeStr notes:entryObj.content];
	currentlocation_array=[d selectcurrenlocation:entryObj.dateStr time:entryObj.timeStr notes:entryObj.content]; 
	if([currentlocation_array count] !=0)
	{
		isclupdate=NO;
		textview2.text=[[currentlocation_array objectAtIndex:0] current_location];
	}
	else{
		isclupdate=YES;	
	}
	
	if([image_array count] !=0)
	{
		NSString *image=[[image_array objectAtIndex:0] filename];
		
		UIImage *im=[UIImage imageWithContentsOfFile:image];
		
		
	imageViewBackground.image=im;
	}
	imageDetails = [[NSMutableArray alloc] initWithObjects: @"object1.png", nil];
	
    [super viewDidLoad];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
-(UIImage *)REsize_IMAGE:(UIImage *)to_be_cropped
{
	UIGraphicsBeginImageContext(CGSizeMake(320, 480));
	[to_be_cropped drawInRect:CGRectMake(0, 0, 320, 480)];
	UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return resizedImage;
	
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
//    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
//        
//        [[UIApplication sharedApplication] setStatusBarHidden:YES];
//    }
//
    

    
    
	//NSLog(@"dic %@",info);
	NSString *mediaType = [info valueForKey:UIImagePickerControllerMediaType];
	//NSLog(@"media type %@",mediaType);
	if([mediaType isEqualToString:@"public.image"]){
		
		
		UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
	imageViewBackground.image=image;
		
		NSString *imagename=entryObj.dateStr;
		imagename=[imagename stringByAppendingFormat:@"%@.png",entryObj.timeStr];

		self.image_name_save=imagename;
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *documentPath = [documentsDirectory stringByAppendingPathComponent:imagename];	
		NSLog(@"document path= %@\n",documentPath);
		BOOL success;
		self.tempPath=documentPath;
		
		image=[self REsize_IMAGE:image];
		success = [UIImagePNGRepresentation(image) writeToFile:documentPath atomically:YES];
		
	//NSURL *aURL = [editingInfo objectForKey:UIImagePickerControllerMediaURL];
	NSLog(@"found a photo :- %@",documentPath);

		Diary *diary1=[[Diary alloc] init];
		if(imageViewBackground.image!=nil)
		{
			if([image_array count]==0)
			{
				
				[diary1 insertimagetodiaryefficiently:entryObj.dateStr time:entryObj.timeStr img_name:self.image_name_save file_name:self.tempPath];
				
				//[diary1 insertimagetodiary:entryObj.dateStr time:entryObj.timeStr entry:entryObj.content picture:imageViewBackground.image];
				//[diary1 insertcurrentlocationtodiary:entryObj.dateStr time:entryObj.timeStr entry:entryObj.content cl:completeAddress];
				
			}
			else{
				[diary1 updateimagetodiaryefficiently:entryObj.dateStr time:entryObj.timeStr image_name:self.image_name_save filename:self.tempPath];
				
				//[diary1 updateimagetodiary:entryObj.dateStr time:entryObj.timeStr entry:entryObj.content picture:imageViewBackground.image];
			}
		}
	//self.image_data = [NSData dataWithContentsOfURL:aURL];

	}
	
	[imagePicker dismissModalViewControllerAnimated:YES];

    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        
        backgroundImageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        backgroundImageView.frame = CGRectMake(0, -20, 320, 568+40);
        imageView.frame =CGRectMake(12, 0, 320, 568);
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 568+20);
    }
    else
    {
        backgroundImageView.frame = CGRectMake(0, 0, 320, 480);
        imageView.frame =CGRectMake(12, 0, 320, 480);
        
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 480+20);
        
    }

}
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
	//NSLog(@"RECEIVED MEMORY WARNING!!!!!!!!!!!!!");
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}
-(IBAction)editPressed{
	AddEntryViewController *editEntryView = [[AddEntryViewController alloc] initWithNibName:@"AddEntryViewController" bundle:nil];
	editEntryView.isEditMode = YES;
	editEntryView.contentStr = self.entryObj.content;
	
	
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy-MM-dd"];
	//[formatter setTimeStyle:NSDateFormatterShortStyle];
	//NSLog(@"%@ time",self.entryObj.dateStr);
	
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit | NSMinuteCalendarUnit ;
	NSDateComponents *compsDate = [gregorian components:unitFlags fromDate:[formatter dateFromString:[NSString stringWithFormat:@"%@",self.entryObj.dateStr]]];
	
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	NSDateComponents *compsTime = [gregorian components:unitFlags fromDate:[formatter dateFromString:[NSString stringWithFormat:@"%@",self.entryObj.timeStr]]];
	[compsTime setDay:[compsDate day]];
	[compsTime setMonth:[compsDate month]];
	[compsTime setYear:[compsDate year]];
	editEntryView.oldDate = [gregorian dateFromComponents:compsTime];
	editEntryView.timeSelected = [gregorian dateFromComponents:compsTime];
	editEntryView.emoticonNo = self.entryObj.emoticon;
	[gregorian release];
	[formatter release];
	self.editEntryViewController = editEntryView;
	[self.view addSubview:self.editEntryViewController.view];

		//NSLog(@"time selected is %@",editEntryViewController.timeSelected);
	[editEntryView release];
}

-(IBAction)deletePressed{
	UIActionSheet *deleteAction = [[UIActionSheet alloc] initWithTitle:@"Are you sure you want to delete?" delegate:self cancelButtonTitle:@"No" destructiveButtonTitle:@"Yes" otherButtonTitles:nil];
	[deleteAction showInView:self.view];
	[deleteAction release];
	}

-(IBAction)backPressed{
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	
	
	
	DiaryEntryViewController *diaryEntryView = [[DiaryEntryViewController alloc] initWithNibName:@"DiaryEntryViewController" bundle:nil];
	diaryEntryView.searchText = self.searchStr;
	diaryEntryView.searchDone = self.searchDone;
	self.diaryEntryViewController = diaryEntryView;
	[self.view addSubview:self.diaryEntryViewController.view];
	[diaryEntryView release];
	[UIView commitAnimations];
}
- (void)dealloc {
    [super dealloc];
}


@end
