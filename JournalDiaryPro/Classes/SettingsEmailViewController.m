///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   SettingsEmailViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "SettingsEmailViewController.h"
#import "EmailConfViewController.h"
#import "Settings.h"
#import "SettingsViewController.h"
#import "eDiaryAppDelegate.h"

@implementation SettingsEmailViewController
@synthesize emailConfViewController;
@synthesize settingsViewController;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	emailArray = [[NSArray alloc] initWithObjects:@"Gmail",@"Yahoo",@"Hotmail",@"Others",nil];
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	//[emailPicker selectRow:[emailArray indexOfObject:settings.emailType] inComponent:0 animated:NO];
	
	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}
-(IBAction)donePressed{
	Settings *settings = [[Settings alloc] init];
	NSString *typeStr = [emailArray objectAtIndex:[emailPicker selectedRowInComponent:0]];
	[settings storeEmailType:typeStr];
	[settings release];
	EmailConfViewController *emailConfView = [[EmailConfViewController alloc] initWithNibName:@"EmailConfViewController" bundle:nil];
	self.emailConfViewController = emailConfView;
	[self.view addSubview:self.emailConfViewController.view];
	[emailConfView release];
}
-(IBAction)backPressed{
	SettingsViewController *settingsView = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController"bundle:nil];
	self.settingsViewController = settingsView;
	[self.view addSubview:self.settingsViewController.view];
	[settingsView release];
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
	return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
	return [emailArray count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row
		   forComponent:(NSInteger)component{
	
	return [emailArray objectAtIndex:row];
}

- (void)dealloc {
	[emailArray release];
    [super dealloc];
}


@end
