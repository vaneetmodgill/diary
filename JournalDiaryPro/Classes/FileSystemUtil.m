#import "FileSystemUtil.h"

static FileSystemUtil *_sharedInstance = nil;

@implementation FileSystemUtil

@synthesize documentDirectoryPath;


+(FileSystemUtil*)getSharedInstanceOfFSUtil{

	if(_sharedInstance == nil){
		_sharedInstance = [[FileSystemUtil alloc] init];
		_sharedInstance.documentDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	}
	return _sharedInstance;
}

- (BOOL) isFileExistInDocumentDirectoryOfApplication:(NSString*)fileName{
    return [[NSFileManager defaultManager] fileExistsAtPath:[self getPathOfFileInDocumentDirectory:fileName]];
}

- (void) removeFileInDocDirectory:(NSString*)fileName{
	if([self isFileExistInDocumentDirectoryOfApplication:fileName]){
		NSError *error;
		BOOL success = [[NSFileManager defaultManager] removeItemAtPath:[self getPathOfFileInDocumentDirectory:fileName] error:&error];
		if (!success) {
			//NSLog(@"Failed to remove file with message '%@'.", [error localizedDescription]);
		}
	}
}

-(void)replaceFileInDocDirSourceFileName:(NSString*)srcFileName andTargetFileName:(NSString*)targetFileName{
  
	if([self isFileExistInDocumentDirectoryOfApplication:srcFileName]){
		[self removeFileInDocDirectory:targetFileName];
		NSString *srcfilePath = [self getPathOfFileInDocumentDirectory:srcFileName];
		NSString *trgfilePath = [self getPathOfFileInDocumentDirectory:targetFileName];
		NSError *error;
		[[NSFileManager defaultManager] copyItemAtPath:srcfilePath toPath:trgfilePath error:&error];
	}
}

-(void)makeEditableCopyOfFileToDocumentFromResources:(NSString*)fileName{
	if([self isFileExistInDocumentDirectoryOfApplication:fileName] == NO){
		NSError *error;
		BOOL success = [[NSFileManager defaultManager] copyItemAtPath:[self getPathOfFileInAppResources:fileName] toPath:[self getPathOfFileInDocumentDirectory:fileName] error:&error];
		if (!success) {
			NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
		}
	}
}

-(NSString*)getPathOfFileInDocumentDirectory:(NSString*)fileName{
	return [self.documentDirectoryPath stringByAppendingPathComponent:fileName];
}

- (NSString*) getPathOfFileInAppResources:(NSString*) fileName
{
	return [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:fileName];
}

- (NSMutableDictionary*) readPlistInDocumentDirectory:(NSString*)fileName
{
	NSString *errorDesc = nil;
	NSPropertyListFormat format;
	[self makeEditableCopyOfFileToDocumentFromResources:fileName];
	NSString *plistPath = [self getPathOfFileInDocumentDirectory:fileName];
	NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
	NSDictionary *temp = (NSDictionary *)[NSPropertyListSerialization
										  propertyListFromData:plistXML
										  mutabilityOption:NSPropertyListMutableContainersAndLeaves
										  format:&format errorDescription:&errorDesc];
	if (!temp) {
		 NSAssert1(0,@"plist isn't well formatted.", errorDesc);
		[errorDesc release];
	}
	return (NSMutableDictionary *) temp;
}

-(void) writePlistInDocumentDirectory:(NSString*)fileName withDictionary:(NSMutableDictionary*) plistDict
{
	NSString *errorDesc;
//    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
	[self makeEditableCopyOfFileToDocumentFromResources:fileName];
	NSString *bundlePath = [self getPathOfFileInDocumentDirectory:fileName];   
	NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:plistDict format:NSPropertyListXMLFormat_v1_0 errorDescription:&errorDesc];
    if (plistData) 
	{
        [plistData writeToFile:bundlePath atomically:YES];
    }
    else {
        NSAssert1(0,@"Dictionary isn't well formatted for plist.", errorDesc);
        [errorDesc release];
    }
}

@end
