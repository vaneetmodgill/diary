///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   Events.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "Events.h"
#import "DBSqlite.h"
#import "Sqlite.h"

@implementation Events
@synthesize dateStr;
@synthesize timeStr;
@synthesize locationStr;
@synthesize title;
@synthesize descriptionStr,dateevent,datestringevent,badgenumber;

static sqlite3_stmt *insert_statement = nil;
static sqlite3_stmt *search_cases_statement=nil;


-(BOOL)insert{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;	

	BOOL result =[sqlite executeNonQuery:@"INSERT INTO events(date,time,location,title,description) VALUES (?, ?, ?, ?,?);",self.dateStr,self.timeStr,self.locationStr,self.title,self.descriptionStr];
	
	[dbSqlite release];	
	return result;
}

-(BOOL)insertdate{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;	
	
	BOOL result =[sqlite executeNonQuery:@"INSERT INTO tbl_dateevent(date1,datestr,timestr,eventcontent) VALUES (?,?,?,?);",self.datestringevent,self.dateStr,self.timeStr,self.descriptionStr];
	
	[dbSqlite release];	
	return result;
}
-(NSMutableArray*)getDatadate:(NSString *)eventDate time:(NSString*)eventTime{
	
		NSMutableArray *tempArray;
		tempArray =[[NSMutableArray alloc] init];
		/*DBSqlite *dbSqlite = [[DBSqlite alloc] init];
		 Sqlite *sqlite;
		 if (![dbSqlite getDBConnection])
		 return NO;
		 sqlite = dbSqlite.sqlite;*/
		sqlite3 *database;
		
		char *sql_query = "Select * from tbl_dateevent where datestr = ? and timestr = ? ";
		search_cases_statement=nil;
		
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *path = [documentsDirectory stringByAppendingPathComponent:@"eDiary.sqlite"];
		
		if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
		{
			if(search_cases_statement ==nil)
			{
				
				if (sqlite3_prepare_v2(database, sql_query, -1, &search_cases_statement, NULL) == SQLITE_OK)
				{
					sqlite3_bind_text(search_cases_statement,1, [eventDate UTF8String],-1,SQLITE_TRANSIENT);
					sqlite3_bind_text(search_cases_statement,2, [eventTime UTF8String],-1,SQLITE_TRANSIENT);
					//sqlite3_bind_text(search_cases_statement,3, [par_notes UTF8String],-1,SQLITE_TRANSIENT);
					
					while (sqlite3_step(search_cases_statement) == SQLITE_ROW)
					{
						
						
						
						Events *obj_savescore_local = [[Events alloc] init];
						//NSString *st=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,0)];
						//[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,14)];
					//	obj_savescore_local.dateevent=obj_savescore_local.dateStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,0)];
						obj_savescore_local.datestringevent=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,0)];
						
						
						[tempArray addObject:obj_savescore_local];
						//	obj_savescore_local.dateStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,14)];
						//	obj_savescore_local.timeStr = [NSString stringWithUTF8String:(char*)sqlite3_column_text(search_cases_statement, 15)];
						
					}
				}
			}
		}
		sqlite3_close(database);
		sqlite3_reset(search_cases_statement);
		sqlite3_finalize(search_cases_statement);
		return tempArray;
		
	}
	-(NSMutableArray*)getDatadate{
		{
			NSMutableArray *tempArray;
			tempArray =[[NSMutableArray alloc] init];
			/*DBSqlite *dbSqlite = [[DBSqlite alloc] init];
			 Sqlite *sqlite;
			 if (![dbSqlite getDBConnection])
			 return NO;
			 sqlite = dbSqlite.sqlite;*/
			sqlite3 *database;
			
			char *sql_query = "Select * from tbl_dateevent ";
			search_cases_statement=nil;
			
			NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
			NSString *documentsDirectory = [paths objectAtIndex:0];
			NSString *path = [documentsDirectory stringByAppendingPathComponent:@"eDiary.sqlite"];
			
			if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
			{
				if(search_cases_statement ==nil)
				{
					
					if (sqlite3_prepare_v2(database, sql_query, -1, &search_cases_statement, NULL) == SQLITE_OK)
					{
												while (sqlite3_step(search_cases_statement) == SQLITE_ROW)
						{
							NSDateFormatter *format=[[NSDateFormatter alloc] init];
							
							NSDate *todaydate=[NSDate date];
							[format setDateFormat:@"yyyy-MM-dd 'at' HH:mm"];
							Events *obj_savescore_local = [[Events alloc] init];
							//NSString *st=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,0)];
							//[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,14)];
							//	obj_savescore_local.dateevent=obj_savescore_local.dateStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,0)];
							obj_savescore_local.datestringevent=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,0)];
							
							   NSDate *dc=[format dateFromString:obj_savescore_local.datestringevent];
							   NSComparisonResult result=[todaydate compare:dc];
							if(result!=1)
							{
								self.badgenumber++;
								NSLog(@"future");
								
							}
							
							[tempArray addObject:obj_savescore_local];
							//	obj_savescore_local.dateStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,14)];
							//	obj_savescore_local.timeStr = [NSString stringWithUTF8String:(char*)sqlite3_column_text(search_cases_statement, 15)];
							
						}
					}
				}
			}
			sqlite3_close(database);
			sqlite3_reset(search_cases_statement);
			sqlite3_finalize(search_cases_statement);
			return tempArray;
			
		}
}
-(BOOL)updateEventdate:(NSString *)oldDate time:(NSString *)oldTime date:(NSString *)par_date{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;
	
	BOOL result = [sqlite executeNonQuery:@"UPDATE tbl_dateevent set date1 = ? , eventcontent = ? where datestr =? and timestr = ?;",self.datestringevent,self.descriptionStr,self.dateStr,self.timeStr];
	[dbSqlite release];
	return result;
}
-(BOOL)deleteEventDATE:(NSString *)oldDate time:(NSString *)oldTime {
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;
	BOOL result = [sqlite executeNonQuery:@"DELETE from tbl_dateevent where datestr = ? and timestr = ?;",oldDate,oldTime];
	[dbSqlite release];
	return result;
}
-(BOOL)updateEvent:(NSString *)oldDate time:(NSString *)oldTime title:(NSString *)sub{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;
	
	BOOL result = [sqlite executeNonQuery:@"UPDATE events set date = ?,time = ?,location = ?,title = ?,description = ? where date =? and time = ? and title = ? ;",self.dateStr,self.timeStr,self.locationStr,self.title,self.descriptionStr,oldDate,oldTime,sub];
	[dbSqlite release];
	return result;
}
-(BOOL)deleteEvent:(NSString *)oldDate time:(NSString *)oldTime title:(NSString *)sub{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;
	BOOL result = [sqlite executeNonQuery:@"DELETE from events where date = ? and time = ? and title = ? ;",oldDate,oldTime,sub];
	[dbSqlite release];
	return result;
}
-(NSArray*)getExportData:(NSString *)from and:(NSString *)to{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select * from events where date between ? and ? order by date asc;",from,to];	
	return results;	
}
-(NSArray*)getData:(NSString *)eventDate{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select * from events where date = ?;",eventDate];	
	return results;
}
-(NSArray*)getDatesInMonth:(NSString *)fromDate	to:(NSString *)toDate{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select date from events where date between ? and ?;",fromDate,toDate];	
	return results;	
}
-(BOOL)checkDuplicate{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;	
	NSArray *results = [sqlite executeQuery:@"Select * from events where date = ? and time = ? and title = ? and location = ? and description = ?;",self.dateStr,self.timeStr,self.title,self.locationStr,self.descriptionStr];
	[dbSqlite release];
	if([results count] == 0){
		return NO;
	}else{
		return YES;
	}
}
-(NSMutableArray*)getEventsInDays:(NSString *)fromDate	to:(NSString *)toDate
	{
		NSMutableArray *tempArray;

		tempArray =[[NSMutableArray alloc] init];
		/*DBSqlite *dbSqlite = [[DBSqlite alloc] init];
		 Sqlite *sqlite;
		 if (![dbSqlite getDBConnection])
		 return NO;
		 sqlite = dbSqlite.sqlite;*/
		sqlite3 *database;
		
		char *sql_query = "Select * from tbl_dateevent where datestr between ? and ? ";
		search_cases_statement=nil;
		
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *path = [documentsDirectory stringByAppendingPathComponent:@"eDiary.sqlite"];
		
		if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
		{
			if(search_cases_statement ==nil)
			{
				
				if (sqlite3_prepare_v2(database, sql_query, -1, &search_cases_statement, NULL) == SQLITE_OK)
				{
					
					sqlite3_bind_text(search_cases_statement,1, [fromDate UTF8String],-1,SQLITE_TRANSIENT);
					sqlite3_bind_text(search_cases_statement,2, [toDate UTF8String],-1,SQLITE_TRANSIENT);
					while (sqlite3_step(search_cases_statement) == SQLITE_ROW)
					{
						
						Events *obj_savescore_local = [[Events alloc] init];
						//NSString *st=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,0)];
						//[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,14)];
						//	obj_savescore_local.dateevent=obj_savescore_local.dateStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,0)];
						obj_savescore_local.datestringevent=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,0)];
						
						obj_savescore_local.descriptionStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,3)];
						
						[tempArray addObject:obj_savescore_local];
						//	obj_savescore_local.dateStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,14)];
						//	obj_savescore_local.timeStr = [NSString stringWithUTF8String:(char*)sqlite3_column_text(search_cases_statement, 15)];
						
					}
				}
			}
		}
		sqlite3_close(database);
		sqlite3_reset(search_cases_statement);
		sqlite3_finalize(search_cases_statement);
		return tempArray;
		
	}
/*-(NSArray*)getEventsInDays:(NSString *)fromDate	to:(NSString *)toDate
{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select * from tbl_dateevent where datestr between ? and ?;",fromDate,toDate];	
	return results;	
}
*/

@end
