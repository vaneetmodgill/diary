///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   ChoosePictureViewController.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>
#import "DiaryObject.h"
#import "ShowEntryViewController.h"
#import "DiaryEntryViewController.h"
#import "Diary.h"
@class Diary;
@class DiaryEntryViewController;
@class ShowEntryViewController;
@class DiaryObject;
@class SettingsPictureViewController;
@interface ChoosePictureViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate> {
	IBOutlet UITableView *pictureTable;
	NSMutableArray *pictureArray;
	NSString *selectedFileName;
	SettingsPictureViewController *settingsPictureViewController;
	BOOL imageforeverydiary;
	DiaryObject *entryObj;
	NSString *timestr;
		NSString *datestr;
		NSString *content;
	DiaryEntryViewController *diaryEntryViewController;
	int index;
    
    IBOutlet UIImageView *backgrndImaView;
}
@property(nonatomic,retain)NSString *timestr;
@property(nonatomic,retain)NSString *datestr;
@property(nonatomic,retain)NSString *content;

@property(nonatomic,retain)DiaryObject *entryObj;
@property(nonatomic,retain)	DiaryEntryViewController *diaryEntryViewController;
@property 	BOOL imageforeverydiary;
@property(nonatomic,retain)NSString *selectedFileName;
@property(nonatomic,retain)SettingsPictureViewController *settingsPictureViewController;
-(IBAction)addPicture;
-(IBAction)deletePicture; 
-(IBAction)donePressed;
-(IBAction)backPressed;
- (UITableViewCell *) getCellContentView:(NSString *)cellIdentifier;
@end
