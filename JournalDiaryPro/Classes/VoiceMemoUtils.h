///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   VoiceMemoUtils.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface VoiceMemoUtils : NSObject<AVAudioSessionDelegate,AVAudioRecorderDelegate> {
	NSURL *soundFileURL;
	NSString *soundFilePath;
	AVAudioRecorder *soundRecorder;
	AVAudioPlayer *soundPlayer;
	AVAudioSession *audioSession;
	NSString *fileName;
}
@property(nonatomic,retain)NSString *soundFilePath;
@property(nonatomic,retain)NSString *fileName;
-(NSString *)generateFileName;
-(void)initSoundFile;
-(BOOL)initBuffer;
-(NSString *)getDocumentsPath;
-(AVAudioPlayer*)initPlayer:(NSString *)path;
-(void)startRecording;
-(void)pauseRecording;
-(void)stopRecording;
-(void)saveRecording:(NSString *)tempPath nameOfFile:(NSString *)file;
-(void)startPlaying;
-(void)stopPlaying;
-(void)releaseRecorder;
-(void)deleteRecording;

-(NSString *)zeroDisplay:(int)value;
-(NSString *)getRecorderTime;
-(NSString *)getRecordTime:(double)time;

@end
