    //
//  Email.m
//  pankajtodo
//
//  Created by seasia on 07/06/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Email.h"
#import "Diary.h"

@implementation Email
@synthesize date_str,time_str,content,image_email;

#pragma mark #############################
#pragma mark this is for sending Email
#pragma mark @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

-(id)init{

	if ((self = [super init])) {
        // Custom initialization
    }
    return self;
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	UILabel *message=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
	message.hidden = NO;
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			message.text = @"Result: canceled";
			//[self dismissModalViewControllerAnimated:YES];
			
			break;
		case MFMailComposeResultSaved:
			message.text = @"Result: saved";
			//[self dismissModalViewControllerAnimated:YES];
			break;
		case MFMailComposeResultSent:
			message.text = @"Result: sent";
			UIAlertView *obj_alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Message sent" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			obj_alert.delegate=self;
			[obj_alert show];
			[obj_alert release];
			//[self dismissModalViewControllerAnimated:YES];
			
			
			break;
		case MFMailComposeResultFailed:
			message.text = @"Result: failed";
			UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Error in sending" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			alert.delegate=self;
			[alert show];
			[alert release];
			//[self dismissModalViewControllerAnimated:YES];
			
			break;
		default:
			message.text = @"Result: not sent";
			break;
	}
	[self dismissModalViewControllerAnimated:YES];

	//[self.navigationController popViewControllerAnimated:YES];
	
	
}


////////////////// ************ CHECKING INTERNET CONNECTIVITY  **************///////////////////////////
-(BOOL)connectionToInternet
{
	NSString *URLString=[NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://www.google.com"]];
	
	return((URLString!=NULL)?YES:NO);
	// Create zero addy
	
	
}

// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	
	picker.mailComposeDelegate = self;
	
	[picker setSubject:@"Diary Entry"];
	 //@"All Projects"];
		
	
	//strings containing 
	NSString *emailBody = @"";
	// Fill out the email body text
	
	
	
	emailBody=[NSString stringWithFormat:@"%@  %@\n%@ \n",self.date_str,self.time_str,self.content];
	NSLog(@"%@",emailBody);
	
	
	NSData *data1=UIImagePNGRepresentation(self.image_email);
	[picker setMessageBody:emailBody isHTML:NO];
	[picker addAttachmentData:data1 mimeType:@"image/png" fileName:@"Image"];
	
//	[self.view addSubview:picker.view];
	[self presentModalViewController:picker animated:YES];
    [picker release];
} 


/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/
-(UIButton *)buttonWithTitle:(NSString *)title target:(id)target selector:(SEL)selector frame:(CGRect)frame image:(UIImage *)image imagePressed:(UIImage *)imagePressed darkTextColor:(BOOL)darkTextColor
{	
	UIButton *button = [[UIButton alloc] initWithFrame:frame];
	
	
	button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
	
	[button setTitle:title forState:UIControlStateNormal];	
	if (darkTextColor)
	{
		[button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	}
	else
	{
		[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	}
	
	UIImage *newImage = [image stretchableImageWithLeftCapWidth:12.0 topCapHeight:0.0];
	
	[button setBackgroundImage:newImage forState:UIControlStateNormal];
	
	
	UIImage *newPressedImage = [imagePressed stretchableImageWithLeftCapWidth:12.0 topCapHeight:0.0];
	[button setBackgroundImage:newPressedImage forState:UIControlStateHighlighted];
	
	[button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
	
	// in case the parent view draws with a custom color or gradient, use a transparent color
	button.backgroundColor = [UIColor clearColor];
	
	return button;
}




// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
	
	UIView *v=[[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	self.view=v;
	
	//self.title = @"Email";
	//diary_obj = [[Diary alloc]init];
			
	
	
	[self displayComposerSheet];
	
	
	
 }
		
- (void)dealloc {
    [super dealloc];
}


@end
