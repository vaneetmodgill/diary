///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   PasswordViewController.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>
#import "SKPSMTPMessage.h"
@class ActivityIndicator;
@class HomeViewController;
@class ForgotPasswordViewController;
@interface PasswordViewController : UIViewController <UIAlertViewDelegate,UIActionSheetDelegate,SKPSMTPMessageDelegate>{
	IBOutlet UITextField *passwordText;
	int count;
	ActivityIndicator *indicator;
	HomeViewController *homeViewController;
	ForgotPasswordViewController *forgotPasswordViewController;
}
@property(retain,nonatomic)HomeViewController *homeViewController;
@property(retain,nonatomic)ForgotPasswordViewController *forgotPasswordViewController;
@property (retain, nonatomic) IBOutlet UIButton *faceIdBtn;
@property (retain, nonatomic) IBOutlet UIButton *touchIdBtn;
-(IBAction)enterPressed;
- (IBAction)enterFace:(id)sender;
-(IBAction)forgotPwdPressed;
-(IBAction)enterTouch:(id)sender;
@end
