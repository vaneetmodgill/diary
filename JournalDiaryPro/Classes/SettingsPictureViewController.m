///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   SettingsPictureViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	18/11/09
// Change reason :  Done button crash bug fixed
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "SettingsPictureViewController.h"
#import "SettingsViewController.h"
#import "ChoosePictureViewController.h"
#import "Settings.h"
#import "eDiaryAppDelegate.h"
#import "DayPicture.h"
@implementation SettingsPictureViewController
@synthesize fileName;
@synthesize choosePictureViewController;
@synthesize settingsViewController;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	/*
	if(self.fileName != nil ||![self.fileName isEqualToString:@""]){
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		UIImage *img = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",documentsDirectory,fileName]];
		imageView.image = img;
		img = nil;
	}*/
    
    
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        
        
        backgroundImageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        backgroundImageView.frame = CGRectMake(0, -20, 320, 568+40);
       
    }
    else
    {
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 480+20);
    }
    

    
    
	if(self.fileName == nil){
		self.fileName = @"";
		}else{
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		UIImage *img = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",documentsDirectory,self.fileName]];
		imageView.image = img;
		img = nil;		
	}
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
-(IBAction)backPressed{
	SettingsViewController *settingsView = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController"bundle:nil];
	self.settingsViewController = settingsView;
	[self.view addSubview:self.settingsViewController.view];
	[settingsView release];
}
-(IBAction)donePressed{
	NSLog(@"filename %@",self.fileName);
	if(self.fileName == nil ||[self.fileName isEqualToString:@""]){
		[eDiaryAppDelegate showMessage:@"Please select a picture" title:@"No Picture Selection"];
		return;
	}
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if(settings.picEnabled && settings.picOfDayEnabled){
		DayPicture *dayPicture = [[DayPicture alloc] init];
		if([[dayPicture getPictureNameForDate:settings.globalDate] isEqualToString:@""]){
			dayPicture.picName =self.fileName;
			dayPicture.dateStr = settings.globalDate;
			[dayPicture insert];
		}else{
			[dayPicture updatePictureName:self.fileName forDate:settings.globalDate];
		}
		
		
	}else{
		[settings storePictureName:self.fileName];
	}
//	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//	NSString *documentsDirectory = [paths objectAtIndex:0];
//	UIImage *img = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",documentsDirectory,self.fileName]];
	[eDiaryAppDelegate setHomeImage:nil];
	[settings release];
//	img = nil;
	SettingsViewController *settingsView = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController"bundle:nil];
	self.settingsViewController = settingsView;
	[self.view addSubview:self.settingsViewController.view];
	[settingsView release];
}
-(IBAction)changePressed{
	ChoosePictureViewController *choosePictureView = [[ChoosePictureViewController alloc] initWithNibName:@"ChoosePictureViewController" bundle:nil];
	self.choosePictureViewController = choosePictureView;
	[self.view addSubview:self.choosePictureViewController.view];
	[choosePictureView release];
}
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
	////NSLog(@"RECEIVED MEMORY WARNING in SettingsPictureViewController!!!");
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}




- (void)dealloc {
//	[imageView release];
    [super dealloc];
}


@end
