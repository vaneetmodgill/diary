///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   AddMemoViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	Lemuel Singh
// Change date	 :	6/11/09
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 IMPIGER TECHNOLOGIES PVT LTD. ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF IMPIGER TECHNOLOGIES PVT LTD. ("IMPIGER").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY IMPIGER. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO IMPIGER
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "AddMemoViewController.h"
#import "VoiceMemoUtils.h"
#import "SaveMemoViewController.h"
#import "VoiceMemoViewController.h"
#import "eDiaryAppDelegate.h"
#import "Settings.h"
@implementation AddMemoViewController
@synthesize tempFilePath;
@synthesize fileName;
@synthesize memoDate;
@synthesize editMode;
@synthesize retainRecording;
@synthesize titleStr;
@synthesize voiceMemoViewController;
@synthesize saveMemoViewController;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	dateLabel.text = [formatter stringFromDate:self.memoDate];
	[formatter release];
	utils = [[VoiceMemoUtils alloc] init];
	[utils initSoundFile];
	BOOL ready = [utils initBuffer];
	//NSLog(@"buffer %d",ready);
	if(!ready){
		saveButton.hidden = YES;
		playPauseButton.hidden = YES;
		stopButton.hidden = YES;
		clearBufferButton.hidden = YES;
		[eDiaryAppDelegate showMessage:@"Recording not available" title:@"Sorry"];
	}
	recording = NO;
    playing = NO;
	stopButton.enabled = NO;  
	clearBufferButton.enabled = NO;
	playPauseButton.enabled = NO;
	saveButton.enabled = NO;
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];
    [super viewDidLoad];
	
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
-(IBAction)recordPausePressed{
	
	if(recording){
		[utils pauseRecording];
		recording = NO;
		UIImage *recordImg = [UIImage imageNamed:@"record.png"];
		[recordPauseButton setImage:recordImg forState:UIControlStateNormal];
		[recordPauseButton setImage:recordImg forState:UIControlStateHighlighted];
		stopButton.enabled = NO;
		statusLabel.text = @"Recording Paused";
		[statusLabel setBackgroundColor:[UIColor clearColor]];
		[recordTimer invalidate];
	}else{
		[utils startRecording];
		recording = YES;
		UIImage *pauseImg = [UIImage imageNamed:@"pause.png"];
		[recordPauseButton setImage:pauseImg forState:UIControlStateNormal];
		[recordPauseButton setImage:pauseImg forState:UIControlStateHighlighted];
		clearBufferButton.enabled = NO;
		backButton.enabled = NO;
		saveButton.enabled = NO;
		playPauseButton.enabled = NO;
		stopButton.enabled = YES;
		statusLabel.text = @"Recording...";
		[statusLabel setBackgroundColor:[UIColor redColor]];
//		if(recordTimer == nil){
			recordTimer = [NSTimer scheduledTimerWithTimeInterval:0.5  target:self selector:@selector(updateRecordTime) userInfo:nil repeats:YES];
//		}else {
//			[recordTimer fire];
//		}

	}	 
}

-(void)updateRecordTime{
	if(blink){
		statusLabel.text = @"Recording...";
		blink = NO;
	}else{
		statusLabel.text = @"";
		blink = YES;
	}
	timerLabel.text = [utils getRecorderTime];
}
-(void)updatePlayTime{
	if(blink){
		statusLabel.text = @"Playing...";
		blink = NO;
	}else{
		statusLabel.text = @"";
		blink = YES;
	}
	timerLabel.text = [utils getRecordTime:[player currentTime]];
}
-(IBAction)playPausePressed{
	/*
	if(playing){
		[utils stopPlaying];
		playing = NO;
	}else{*/
	if(!retainRecording){
		player =[utils initPlayer:utils.soundFilePath];
	}else{
		player = [utils initPlayer:self.tempFilePath];
	}
	player.delegate = self;
	//[utils startPlaying];
	[player play];
	playing = YES;
	clearBufferButton.enabled = NO;
	backButton.enabled = NO;
	saveButton.enabled = NO;
	statusLabel.text = @"Playing...";
	[statusLabel setBackgroundColor:[UIColor clearColor]];
//	if(playTimer == nil){
		playTimer = [NSTimer scheduledTimerWithTimeInterval:0.5  target:self selector:@selector(updatePlayTime) userInfo:nil repeats:YES];
//	}else {
//		[playTimer fire];
//	}

//	}  //not this
	 
}
-(IBAction)stopPressed{
	if(playing){
		[playTimer invalidate];
	}
	if(recording){
		[recordTimer invalidate];
	}
	
	if(recording){
		[utils stopRecording];
		recording = NO;
	}
	if(playing){
		[utils stopPlaying];
		playing = NO;
	}
	recordPauseButton.enabled =NO;
	clearBufferButton.enabled = YES;
	backButton.enabled = YES;
	saveButton.enabled = YES;
	playPauseButton.enabled = YES;
	statusLabel.text = @"";
	[statusLabel setBackgroundColor:[UIColor clearColor]];
}
-(IBAction)clearBufferPressed{
	[utils deleteRecording];
	[utils releaseRecorder];
	[utils initBuffer];
//	recordPauseButton.hidden = NO;
//	UIImage *recordImg = [UIImage imageNamed:@"record.png"];
//	[recordPauseButton setImage:recordImg forState:UIControlStateNormal];
//	[recordPauseButton setImage:recordImg forState:UIControlStateHighlighted];
	statusLabel.text = @"Recording...";
	[statusLabel setBackgroundColor:[UIColor redColor]];
	[utils startRecording];
	recordTimer = [NSTimer scheduledTimerWithTimeInterval:0.5  target:self selector:@selector(updateRecordTime) userInfo:nil repeats:YES];
	recording = YES;
	UIImage *pauseImg = [UIImage imageNamed:@"pause.png"];
	[recordPauseButton setImage:pauseImg forState:UIControlStateNormal];
	[recordPauseButton setImage:pauseImg forState:UIControlStateHighlighted];
	recordPauseButton.enabled = YES;
	clearBufferButton.enabled = NO;
	backButton.enabled = NO;
	saveButton.enabled = NO;
	playPauseButton.enabled = NO;
	stopButton.enabled = YES;
	
	
}
-(IBAction)savePressed{
	
	if(!recording && !playing){
		[UIView beginAnimations:nil context:nil];
		//change to set the time
		[UIView setAnimationDuration:1];
		[UIView setAnimationBeginsFromCurrentState:YES];
		[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
		SaveMemoViewController *saveMemoView = [[SaveMemoViewController alloc] initWithNibName:@"SaveMemoViewController" bundle:nil];
		if(!retainRecording){
			
			saveMemoView.tempFilePath = utils.soundFilePath;
			if(self.editMode){
				saveMemoView.oldFileName = self.fileName; 
				saveMemoView.titleStr = self.titleStr;
			}
			saveMemoView.fileName = utils.fileName;
		}else{
			saveMemoView.tempFilePath = self.tempFilePath;
			saveMemoView.fileName = self.fileName;
		}
		 
		saveMemoView.memoDate = self.memoDate;
		saveMemoView.editMode = self.editMode;
		self.saveMemoViewController = saveMemoView;
		[self.view addSubview:self.saveMemoViewController.view];
		[saveMemoView release];
		[UIView commitAnimations];
		
	}
}
-(IBAction)backPressed{
	[utils releaseRecorder];
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	VoiceMemoViewController *voiceMemoView = [[VoiceMemoViewController alloc] initWithNibName:@"VoiceMemoViewController" bundle:nil];
	self.voiceMemoViewController = voiceMemoView;
	[self.view addSubview:self.voiceMemoViewController.view];
	[UIView commitAnimations];
}
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[utils release];
    [super dealloc];
}

- (void) audioPlayerDidFinishPlaying: (AVAudioPlayer *) player
                        successfully: (BOOL) flag {
    if (flag == YES) {
        //NSLog(@"play sucess");
		[playTimer invalidate];
		playing = NO;
		clearBufferButton.enabled = YES;
		backButton.enabled = YES;
		saveButton.enabled = YES;
		playPauseButton.enabled = YES;
		statusLabel.text = @"";
		[statusLabel setBackgroundColor:[UIColor clearColor]];
    }else{
		////NSLog(@"play failure");
	}
}

@end
