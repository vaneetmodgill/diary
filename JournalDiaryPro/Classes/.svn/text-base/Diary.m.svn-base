///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   Diary.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	Lemuel Singh
// Change date	 :	6/11/09
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 IMPIGER TECHNOLOGIES PVT LTD. ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF IMPIGER TECHNOLOGIES PVT LTD. ("IMPIGER").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY IMPIGER. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO IMPIGER
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "Diary.h"
#import "DBSqlite.h"
#import "Sqlite.h"

@implementation Diary
@synthesize dateStr;
@synthesize timeStr;
@synthesize entry;
@synthesize emoticon;

-(BOOL)insert{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;	
	//NSLog(@"%@ %@ %@ %@",self.dateStr,self.timeStr,self.entry,self.emoticon);
	BOOL result = [sqlite executeNonQuery:@"INSERT INTO diary(date,time,entry,emoticon) VALUES (?, ?, ?, ?);",self.dateStr,self.timeStr,self.entry,self.emoticon];
	
	[dbSqlite release];	
	return result;
}
-(BOOL)updateEntry:(NSString *)oldDate time:(NSString *)oldTime entry:(NSString *)notes{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;
	////NSLog(@"time %@ date %@ entry %@",oldTime,oldDate,notes);
	BOOL result = [sqlite executeNonQuery:@"UPDATE diary set date = ?,time = ?,entry = ?,emoticon = ? where date =? and time = ? and entry = ? ;",self.dateStr,self.timeStr,self.entry,self.emoticon,oldDate,oldTime,notes];
	[dbSqlite release];
	return result;
}
-(BOOL)deleteEntry:(NSString *)oldDate time:(NSString *)oldTime entry:(NSString *)notes{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;
	BOOL result = [sqlite executeNonQuery:@"DELETE from diary where date =? and time = ? and entry = ? ;",oldDate,oldTime,notes];
	[dbSqlite release];
	return result;
}
-(NSArray*)getData:(NSString *)entryDate{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select * from diary where date = ?;",entryDate];	
	return results;
}
-(NSArray*)getSearchData:(NSString*)searchText{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	NSString *query = [NSString stringWithFormat:@"Select * from diary where entry LIKE '%%%@%%' order by date,time;",searchText];
	NSArray *results = [sqlite executeQuery:query];	
	return results;
}

-(NSArray*)getExportData:(NSString *)from and:(NSString *)to{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select * from diary where date between ? and ? order by date asc;",from,to];	
	return results;	
}

-(BOOL)isEntryAddedForDate:(NSString *)dateOfEntry{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;	
	NSArray *results = [sqlite executeQuery:@"Select * from diary where date = ?;",dateOfEntry];
	[dbSqlite release];
	if([results count] == 0){
		return NO;
	}else{
		return YES;
	}
}
-(NSArray*)getDatesInMonth:(NSString *)fromDate	to:(NSString *)toDate{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select date from diary where date between ? and ?;",fromDate,toDate];	
	return results;	
}
//:(NSString*)oldDate timeStamp:(NSString*)oldTime content:(NSString*)oldContent
-(BOOL)checkDuplicate{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;	
	NSArray *results = [sqlite executeQuery:@"Select * from diary where date = ? and time = ? and entry = ? and emoticon = ?;",self.dateStr,self.timeStr,self.entry,self.emoticon];
	[dbSqlite release];
	if([results count] == 0){
		return NO;
	}else{
		return YES;
	}
}
@end
