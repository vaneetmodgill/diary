///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   VoiceMemoViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	Lemuel Singh
// Change date	 :	6/11/09
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 IMPIGER TECHNOLOGIES PVT LTD. ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF IMPIGER TECHNOLOGIES PVT LTD. ("IMPIGER").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY IMPIGER. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO IMPIGER
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "VoiceMemoUtils.h"


@implementation VoiceMemoUtils
@synthesize soundFilePath;
@synthesize fileName;

-(NSString *)generateFileName{
	NSDate *tempDate=[NSDate date];
	NSString *tempStr =[NSString stringWithFormat:@"Memo%@.caf",[NSNumber numberWithUnsignedLongLong:[tempDate timeIntervalSince1970]]];
	return tempStr;
}
-(void)initSoundFile{
//	NSString *tempDir = NSTemporaryDirectory ();
	NSString *tempDir = [NSString stringWithFormat:@"%@/",[self getDocumentsPath]];
	self.fileName = [self generateFileName];
	self.soundFilePath = [tempDir stringByAppendingString:@"temp.caf"];
	
    soundFileURL = [[NSURL alloc] initFileURLWithPath:self.soundFilePath];
	
    audioSession = [AVAudioSession sharedInstance];
    audioSession.delegate = self;
    [audioSession setActive: YES error: nil];
}
-(void)startRecording{
	[soundRecorder record];
}
-(BOOL)initBuffer{
	[audioSession setCategory: AVAudioSessionCategoryPlayAndRecord error: nil];
	
	UInt32 audioRouteOverride = 'spkr' ;  // 1  kAudioSessionOverrideAudioRoute_Speaker
	
	AudioSessionSetProperty (	//kAudioSessionProperty_OverrideAudioRoute
							    'ovrd',                         // 2
							 sizeof (audioRouteOverride),                                      // 3
							 &audioRouteOverride                                               // 4
	);
	
	/*
	UInt32 sessionCategory = kAudioSessionCategory_MediaPlayback;
	AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof(sessionCategory), &sessionCategory);    
	UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
	AudioSessionSetProperty (kAudioSessionProperty_OverrideAudioRoute,sizeof (audioRouteOverride),&audioRouteOverride);
	 */
	NSDictionary *recordSettings =
	[[NSDictionary alloc] initWithObjectsAndKeys:
	 [NSNumber numberWithFloat: 44100.0],                 AVSampleRateKey,
	 [NSNumber numberWithInt:'alac'],					  AVFormatIDKey,			//kAudioFormatAppleLossless
	 [NSNumber numberWithInt: 1],                         AVNumberOfChannelsKey,
	 [NSNumber numberWithInt: AVAudioQualityMax],         AVEncoderAudioQualityKey,
	 nil];
	
	soundRecorder = [[AVAudioRecorder alloc] initWithURL: soundFileURL
												settings: recordSettings
												   error: nil];
	[recordSettings release];
	
	
	soundRecorder.delegate = self;
	BOOL prepared = [soundRecorder prepareToRecord];
	return prepared;
}
-(void)deleteRecording{
	[soundRecorder deleteRecording];
}
-(void)pauseRecording{
	[soundRecorder pause];
}
-(void)stopRecording{
	[soundRecorder stop];
	soundRecorder = nil;
	[audioSession setActive: NO error: nil];
}
-(void)saveRecording:(NSString *)tempPath nameOfFile:(NSString *)file{
	BOOL success;
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *documentPath = [documentsDirectory stringByAppendingPathComponent:file];	
	success = [fileManager copyItemAtPath:tempPath toPath:documentPath error:&error];
	if(success){
		//NSLog(@"sucess");
		//self.soundFilePath = documentPath;
	}
	audioSession = nil; 
}
-(NSString *)getDocumentsPath{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	return documentsDirectory;
}

-(AVAudioPlayer*)initPlayer:(NSString *)path{
//	soundFilePath = [[NSBundle mainBundle] pathForResource: @"sound" ofType: @"caf"];
	
	BOOL success;
	NSError *error;
	NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: path];
																					
	soundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: fileURL
														 error: &error];
	[fileURL release];
	
    if (error)
    {
        //NSLog(@"An error occured while initializing the audio recorder! -- %@", error);
        //exit(-1);
    }
	[soundPlayer setVolume:1.0];
	success = [soundPlayer prepareToPlay];
	if(success){
		//NSLog(@"prepare sucess");
	}
	return soundPlayer;
}

-(NSString *)zeroDisplay:(int)value{
	if(value < 10){
		return [NSString stringWithFormat:@"0%d",value];
	}else{
		return [NSString stringWithFormat:@"%d",value];
	}
}
-(NSString *)getRecordTime:(double)time{
	NSString *timeStr;
	int mins =0;
	int seconds = time;
	if(seconds < 60.0){
		timeStr = [NSString stringWithFormat:@"00:%@",[self zeroDisplay:seconds]];
		return timeStr;
	}else{
		mins = seconds/60;
		seconds = seconds % 60;
		
		timeStr = [NSString stringWithFormat:@"%@:%@",[self zeroDisplay:mins],[self zeroDisplay:seconds]];
		return timeStr;
	}
	return @"00:00";
}

-(NSString*)getRecorderTime{
	return [self getRecordTime:[soundRecorder currentTime]];
}
-(void)releaseRecorder{
	[soundRecorder release];
}
-(void)startPlaying{
	// if already playing, then pause
   // if (soundPlayer.playing) {
       
 //       [soundPlayer pause];
		
		// if stopped or paused, start playing
 //   } else {
        
       BOOL sucess = [soundPlayer play];
	if(sucess){
		//NSLog(@"play sucess");
	}
	//NSLog(@"suc %d",sucess);
//}
}
-(void)stopPlaying{
	[soundPlayer stop];
}

- (void) audioPlayerDidFinishPlaying: (AVAudioPlayer *) player
                        successfully: (BOOL) flag {
    if (flag == YES) {
        //NSLog(@"play sucess");
    }else{
		//NSLog(@"play failure");
	}
}

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag{
	
	if (flag == YES) {
        ////NSLog(@"record sucess");
    }
}
 
@end
