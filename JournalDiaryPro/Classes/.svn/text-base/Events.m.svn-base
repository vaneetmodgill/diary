///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   Events.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	Lemuel Singh
// Change date	 :	6/11/09
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 IMPIGER TECHNOLOGIES PVT LTD. ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF IMPIGER TECHNOLOGIES PVT LTD. ("IMPIGER").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY IMPIGER. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO IMPIGER
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "Events.h"
#import "DBSqlite.h"
#import "Sqlite.h"

@implementation Events
@synthesize dateStr;
@synthesize timeStr;
@synthesize locationStr;
@synthesize title;
@synthesize descriptionStr;
-(BOOL)insert{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;	

	BOOL result =[sqlite executeNonQuery:@"INSERT INTO events(date,time,location,title,description) VALUES (?, ?, ?, ?,?);",self.dateStr,self.timeStr,self.locationStr,self.title,self.descriptionStr];
	
	[dbSqlite release];	
	return result;
}
-(BOOL)updateEvent:(NSString *)oldDate time:(NSString *)oldTime title:(NSString *)sub{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;
	
	BOOL result = [sqlite executeNonQuery:@"UPDATE events set date = ?,time = ?,location = ?,title = ?,description = ? where date =? and time = ? and title = ? ;",self.dateStr,self.timeStr,self.locationStr,self.title,self.descriptionStr,oldDate,oldTime,sub];
	[dbSqlite release];
	return result;
}
-(BOOL)deleteEvent:(NSString *)oldDate time:(NSString *)oldTime title:(NSString *)sub{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;
	BOOL result = [sqlite executeNonQuery:@"DELETE from events where date = ? and time = ? and title = ? ;",oldDate,oldTime,sub];
	[dbSqlite release];
	return result;
}
-(NSArray*)getExportData:(NSString *)from and:(NSString *)to{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select * from events where date between ? and ? order by date asc;",from,to];	
	return results;	
}
-(NSArray*)getData:(NSString *)eventDate{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select * from events where date = ?;",eventDate];	
	return results;
}
-(NSArray*)getDatesInMonth:(NSString *)fromDate	to:(NSString *)toDate{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select date from events where date between ? and ?;",fromDate,toDate];	
	return results;	
}
-(BOOL)checkDuplicate{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;	
	NSArray *results = [sqlite executeQuery:@"Select * from events where date = ? and time = ? and title = ? and location = ? and description = ?;",self.dateStr,self.timeStr,self.title,self.locationStr,self.descriptionStr];
	[dbSqlite release];
	if([results count] == 0){
		return NO;
	}else{
		return YES;
	}
}

@end
