///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   ShowEventViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "ShowEventViewController.h"
#import "Events.h"
#import "AddEventViewController.h"
#import "EventsViewController.h"
#import "Settings.h"
#import "eDiaryAppDelegate.h"
@implementation ShowEventViewController
@synthesize event;
@synthesize editEventViewController;
@synthesize eventsViewController;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        
        
        backgroundImageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        backgroundImageView.frame = CGRectMake(0, -20, 320, 568+40);
        
        imageView.frame = CGRectMake(0, -20, 320, 568+40);
        // self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 568+20);
    }
    else
    {
        imageView.frame =CGRectMake(0, 0, 320, 480);
        
    }
    
    
    
    
	timeLbl.text = [NSString stringWithFormat:@"%@ %@",event.dateStr,event.timeStr];
	locationLbl.text = event.locationStr;
	titleLbl.text = event.title;
	contentView.text = event.descriptionStr;
	
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if([settings.fontName isEqualToString:@""] || settings.fontName == nil){
		[locationLbl setFont:[UIFont systemFontOfSize:15]];
		[titleLbl setFont:[UIFont systemFontOfSize:15]];
		[contentView setFont:[UIFont systemFontOfSize:15]];
		[settings storeFontSettings:@"" size:15.0];
	}else{
		//NSLog(@"font name %@",settings.fontName);	
		NSArray *fontNames = [UIFont fontNamesForFamilyName:settings.fontName];
		[contentView setFont:[UIFont fontWithName:[fontNames objectAtIndex:0] size:settings.fontSize]];
		[locationLbl setFont:[UIFont fontWithName:[fontNames objectAtIndex:0] size:settings.fontSize]];
		[titleLbl setFont:[UIFont fontWithName:[fontNames objectAtIndex:0] size:settings.fontSize]];
	}

	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
	//NSLog(@"RECEIVED MEMORY WARNING!!!!!!!!!!!!!");
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

-(IBAction)editPressed{
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
	AddEventViewController *editEventView = [[AddEventViewController alloc] initWithNibName:@"AddEventViewController" bundle:nil];
	editEventView.isEditMode = YES;
	editEventView.oldObj = self.event;
	editEventView.locationStr = self.event.locationStr;
	editEventView.titleStr = self.event.title;
	editEventView.contentStr = self.event.descriptionStr;
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy-MM-dd"];
	//[formatter setTimeStyle:NSDateFormatterShortStyle];
	
	
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit | NSMinuteCalendarUnit ;
	NSDateComponents *compsDate = [gregorian components:unitFlags fromDate:[formatter dateFromString:[NSString stringWithFormat:@"%@",self.event.dateStr]]];
	
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	NSDateComponents *compsTime = [gregorian components:unitFlags fromDate:[formatter dateFromString:[NSString stringWithFormat:@"%@",self.event.timeStr]]];
	[compsTime setDay:[compsDate day]];
	[compsTime setMonth:[compsDate month]];
	[compsTime setYear:[compsDate year]];
	editEventView.timeSelected = [gregorian dateFromComponents:compsTime];
	[gregorian release];
	[formatter release];
	self.editEventViewController = editEventView;
	[self.view addSubview:self.editEventViewController.view];
	[editEventView release];
	[UIView commitAnimations];
}
-(void)CANCEL_LocalNotificationSetFireDate:(NSDate *)nDate setAlertAction:(NSString *)nAction setRepeatInterval:(NSCalendarUnit)nRepeat setAlertBody:(NSString *)nBody //a17
{
	UILocalNotification* alarm = [[[UILocalNotification alloc] init] autorelease];
	if (alarm)
	{
		alarm.fireDate = nDate;  //self.timeSelected; //[formatter stringFromDate:self.timeSelected];
		//NSLog(@"\ndate=%@\n",alarm.fireDate);
		
		alarm.alertAction = nAction; //events.title;
		//NSLog(@"\naction=%@\n",alarm.alertAction);
		alarm.timeZone = [NSTimeZone defaultTimeZone];
		//NSLog(@"\nzone=%@\n",alarm.timeZone);
		alarm.soundName = UILocalNotificationDefaultSoundName;
		alarm.repeatInterval = nRepeat; //0; //NSWeekCalendarUnit;
		alarm.alertBody = nBody; //[NSString stringWithString: events.descriptionStr];
		//NSLog(@"\nbody=%@\n",alarm.alertBody);
		
		[[UIApplication sharedApplication] cancelLocalNotification:alarm];
	}
}

-(IBAction)deletePressed{
	UIActionSheet *deleteAction = [[UIActionSheet alloc] initWithTitle:@"Are you sure you want to delete?" delegate:self cancelButtonTitle:@"No" destructiveButtonTitle:@"Yes" otherButtonTitles:nil];
	[deleteAction showInView:self.view];
	[deleteAction release];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
	//NSLog(@"index %d",buttonIndex);
	if(buttonIndex == 0){
		////NSLog(@"delete");
		Events *events = [[Events alloc] init];
			NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
			[formatter setDateFormat:@"yyyy-MM-dd 'at' HH:mm"];
		NSMutableArray *ar=[events getDatadate:self.event.dateStr time:self.event.timeStr];
		NSString *sa=[[ar objectAtIndex:0] datestringevent];
		NSDate *dd=[formatter dateFromString:sa];
		[self CANCEL_LocalNotificationSetFireDate:dd setAlertAction:self.event.title setRepeatInterval:0 setAlertBody:self.event.descriptionStr];
		
		[events deleteEvent:self.event.dateStr time:self.event.timeStr title:self.event.title];
		[events deleteEventDATE:self.event.dateStr time:self.event.timeStr];
		[events release];
		
		//[self CANCEL_LocalNotificationSetFireDate:self.event.dateStr setAlertAction:<#(NSString *)nAction#> setRepeatInterval:<#(NSCalendarUnit)nRepeat#> setAlertBody:<#(NSString *)nBody#>
#pragma mark delete 
		EventsViewController *eventsView = [[EventsViewController alloc] initWithNibName:@"EventsViewController" bundle:nil];
		self.eventsViewController = eventsView;
		[self.view addSubview:self.eventsViewController.view];
		[eventsView release];

	}
	[actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
	
}	
-(IBAction)backPressed{
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	EventsViewController *eventsView = [[EventsViewController alloc] initWithNibName:@"EventsViewController" bundle:nil];
	self.eventsViewController = eventsView;
	[self.view addSubview:self.eventsViewController.view];
	[eventsView release];
	[UIView commitAnimations];
}

- (void)dealloc {
    [super dealloc];
}


@end
