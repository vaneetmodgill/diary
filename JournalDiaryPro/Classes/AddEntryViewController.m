///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   AddEntryViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "AddEntryViewController.h"
#import "DiaryEntryViewController.h"
#import "ChooseDateViewController.h"
#import "ChooseEmoticonViewCtlr.h"
#import "Diary.h"
#import "DiaryObject.h"
#import "eDiaryAppDelegate.h"
#import "Settings.h"
#import "Utility.h"
@implementation AddEntryViewController
@synthesize timeSelected;
@synthesize contentStr;
@synthesize isEditMode;
@synthesize emoticonNo;
@synthesize oldDate;
@synthesize chooseDateViewController;
@synthesize diaryEntryViewController;
@synthesize emoticonViewCtlr,tickcrossimageview,textview2;
@synthesize completeAddress,gpsStatusLabel,Latitude,Longitude,gpsEnabled,usedprevious,bt_rotate, doneButton, cancelButton, clearButton;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/
-(IBAction)rotatePressed
{
	
	if(!isrotated)
	{
        //for going from Potrait to Landscape
        
		isrotated=YES;
		[[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight];
		//[[UIApplication sharedApplication] setStatusBarHidden:YES];
		[contentText resignFirstResponder];

		
		//imageView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Genarl-Diary.png"]];
		//self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Genarl-Diary.png"]];
		
		//imageView.image=[UIImage imageNamed:@"Genarl-Diary.png"];
        
        
	
		//[self initWithn
		//contentText.frame=CGRectMake(0, 72 , 447, contentText.frame.size.height);
		contentText.frame=CGRectMake(50, 72 , 447, 90);
		bt_rotate.frame=CGRectMake(296,-6,63,37);
		[bt_rotate setImage:[UIImage imageNamed:@"iphone-icon-L.png"] forState:UIControlStateNormal];
		doneButton.frame = CGRectMake(310,-6,70,45);
		cancelButton.frame = CGRectMake(385,-6, 70, 45);
		clearButton.frame = CGRectMake(350, 28, 70, 45);
		//[bt_rotate setTitle:@"RBack" forState:UIControlStateNormal];
		//self.nibName=[NSString stringWithFormat:@"PasswordViewController"];
		imageView.image=[UIImage imageNamed:@"Genarl-Diary.png"];
		//imageView.frame=self.view.frame;
		self.view.transform=CGAffineTransformMakeRotation(M_PI/2);
        
        contentEmoticon.frame = CGRectMake(20+30, 43, 133, 21);
        
        
        emoticonButton.frame=CGRectMake(140,40,29,26);

        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        
        NSLog(@"******** Rotate button pressed   ******* ");
        
        NSLog(@"Rotation Width %f",screenBounds.size.width);

        if (screenBounds.size.width == 667)
        {
            
            
            NSLog(@"******** IPHONE 6 Landscape  ******* ");

            imageView.image = [UIImage imageNamed:@"bg6iPhone5Landscape.png"];
            
            self.view.bounds=CGRectMake(-20, -50, 667, 320);
            imageView.frame=CGRectMake(-25, -55, 667+10, 320+30);
            
            imageView.backgroundColor = [UIColor redColor];
            self.view.backgroundColor = [UIColor yellowColor];
            
            
            timeLabel.frame = CGRectMake(70, 13-50, timeLabel.frame.size.width, timeLabel.frame.size.height);
            
             bt_Clock.frame = CGRectMake(bt_Clock.frame.origin.x, -50, bt_Clock.frame.size.width, bt_Clock.frame.size.height);
            
        gpsStatusLabel.frame = CGRectMake(gpsStatusLabel.frame.origin.x, 0, gpsStatusLabel.frame.size.width, gpsStatusLabel.frame.size.height);

            contentText.frame=CGRectMake(60, 72-40 , 527, 90);
            bt_rotate.frame=CGRectMake(316,-6-40,63,37);
            [bt_rotate setImage:[UIImage imageNamed:@"iphone-icon-L.png"] forState:UIControlStateNormal];
            doneButton.frame = CGRectMake(390,-6-40,70,45);
            cancelButton.frame = CGRectMake(465,-6-40, 70, 45);
            clearButton.frame = CGRectMake(430, 28-40, 70, 45);
            
            contentEmoticon.frame = CGRectMake(80, 43-50, 133, 21);
            
            emoticonButton.frame=CGRectMake(210,40-50,29,26);
            
            
            NSLog(@"******** IPHONE 6 Landscape  ******* ");
            
            
            NSLog(@"Dimensions in Landscape %f  \n %f",self.view.frame.size.width ,self.view.frame.size.height);
            
            NSLog(@"\n\n\n Bounds in Landscape %f  \n %f",self.view.bounds.size.width ,self.view.bounds.size.height);
        }
        
       else if (screenBounds.size.width == 568)
        {
            
            NSLog(@"******** IPHONE 5 Landscape  ******* ");

            imageView.image = [UIImage imageNamed:@"bg6iPhone5Landscape.png"];

            self.view.bounds=CGRectMake(0, -20, 568, 320);
            imageView.frame=CGRectMake(-10, -30, 578+10, 320+30);
            
            imageView.backgroundColor = [UIColor redColor];
            self.view.backgroundColor = [UIColor yellowColor];
            
            
            timeLabel.frame = CGRectMake(40, 13-20, timeLabel.frame.size.width, timeLabel.frame.size.height);

            contentEmoticon.frame = CGRectMake(20+30, 43-20, 133, 21);

        
            
        emoticonButton.frame=CGRectMake(180,40-20,29,26);
            

            bt_Clock.frame = CGRectMake(bt_Clock.frame.origin.x, -20, bt_Clock.frame.size.width, bt_Clock.frame.size.height);

            
            gpsStatusLabel.frame = CGRectMake(gpsStatusLabel.frame.origin.x, 25, gpsStatusLabel.frame.size.width, gpsStatusLabel.frame.size.height);

            
            bt_rotate.frame=CGRectMake(316,-16,63,37);

            
            doneButton.frame = CGRectMake(380,-26,70,45);
            cancelButton.frame = CGRectMake(455,-26, 70, 45);
            clearButton.frame = CGRectMake(420, 8, 70, 45);
            
            
            contentText.frame=CGRectMake(50, 52 , 477, 90);

            
            
            NSLog(@"Dimensions in Landscape %f  \n %f",self.view.frame.size.width ,self.view.frame.size.height);
            
            NSLog(@"\n\n\n Bounds in Landscape %f  \n %f",self.view.bounds.size.width ,self.view.bounds.size.height);
            
        }
        else
        {
            
            NSLog(@"******** IPHONE 4 Landscape  ******* ");

           	self.view.bounds=CGRectMake(0, -20, 460, 320);
            imageView.frame=CGRectMake(-10, 0, 460, 320);
            
        }

        
        
	}
	else {
        
        
        //For Lamndscape to Potrait
          timeLabel.frame = CGRectMake(20, 13, timeLabel.frame.size.width, timeLabel.frame.size.height);
        
        bt_Clock.frame = CGRectMake(261, 0, bt_Clock.frame.size.width, bt_Clock.frame.size.height);
        
          gpsStatusLabel.frame = CGRectMake(251, 52, gpsStatusLabel.frame.size.width, gpsStatusLabel.frame.size.height);

		
		isrotated=NO;
		[[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
		//[[UIApplication sharedApplication] setStatusBarHidden:YES];
		[contentText resignFirstResponder];
		
		
		//imageView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Genarl-Diary.png"]];
		//self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Genarl-Diary.png"]];
		
		//imageView.image=[UIImage imageNamed:@"Genarl-Diary.png"];
        
        imageView.image=[UIImage imageNamed:@"bg6.png"];

	
	//	contentText.frame=CGRectMake(contentText.bounds.origin.x, contentText.bounds.origin.y, 280, contentText.frame.size.height);
		contentText.frame=CGRectMake(18, 72 , 280, contentText.frame.size.height);
        contentEmoticon.frame = CGRectMake(40, 43, 133, 21);

		[bt_rotate setImage:[UIImage imageNamed:@"iphone-icon-P.png"] forState:UIControlStateNormal];
		[bt_rotate setFrame:CGRectMake(203,5,37,63)];
		doneButton.frame = CGRectMake(18, 204, 91,45);
		cancelButton.frame = CGRectMake(120, 204, 91,45);
		clearButton.frame = CGRectMake(219, 204, 91,45);
		//[bt_rotate setTitle:@"Rotate" forState:UIControlStateNormal];
		//self.nibName=[NSString stringWithFormat:@"PasswordViewController"];
		//imageView.frame=self.view.frame;
		
		self.view.transform=CGAffineTransformMakeRotation(0);
		
        emoticonButton.frame=CGRectMake(161,38,29,26);

        
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        
        if (screenBounds.size.height == 568)
        {
            imageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
            
            
            self.view.bounds=CGRectMake(0, 0, 320, 568);
            imageView.frame=CGRectMake(0, 0, 320, 568);
            
            
            NSLog(@"Dimensions in potrait %f  \n %f",self.view.frame.size.width ,self.view.frame.size.height);
            
            NSLog(@"\n\n\n Bounds in potrait %f  \n %f",self.view.bounds.size.width ,self.view.bounds.size.height);
            
            
        }
        else
        {
            
            imageView.image=[UIImage imageNamed:@"bg6.png"];
            
            self.view.bounds=CGRectMake(0, 0, 320, 460);
            imageView.frame=CGRectMake(0, 0, 320, 460);
        }
        

		
	}
		//[contentText becomeFirstResponder];
//	//contentV.transform = CGAffineTransformMakeRotation(-M_PI/2);
	
	//contentText.bounds = CGRectMake(contentText.frame.origin.y, contentText.frame.origin.x, contentText.frame.size.height, contentText.frame.size.width);
    
    
    
    
    NSLog(@"Dimensions in End %f  \n %f",self.view.frame.size.width ,self.view.frame.size.height);
    
    NSLog(@"\n\n\n Bounds in End %f  \n %f",self.view.bounds.size.width ,self.view.bounds.size.height);
    

}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    self.view.autoresizesSubviews = YES ;
    // self.view.autoresizingMask = UIViewAutoresizingFlexibleHeight ;
    
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        imageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        imageView.frame = CGRectMake(0, -20, 320, 568+20);
       // self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 568+20);
    }
    else
    {
        imageView.frame =CGRectMake(0, 0, 320, 480);
        
    }

    
	deletecurrent=NO;
	contentText.delegate=self;
	if(contentStr != nil){
		contentText.text = contentStr;
	}
	[self Network_ckeck];
	textview2.font=[UIFont fontWithName:@"Arial" size:18];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	//NSLog(@"time %@",self.timeSelected);
	if(self.timeSelected != nil){
		timeLabel.text = [formatter stringFromDate:self.timeSelected];
	}else{
		timeLabel.text = [formatter stringFromDate:[NSDate date]];
		self.timeSelected = [NSDate date];
	}
	if(emoticonNo == nil){
		emoticonNo = @"1";
	}
	//NSLog(@"emo %@",emoticonNo);
	[formatter release];
	if(self.isEditMode){
		oldEntry = [[DiaryObject alloc] init];
		NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
		[formatter setDateFormat:@"YYYY-MM-dd"];
		oldEntry.dateStr = [formatter stringFromDate:self.oldDate];
		[formatter setDateStyle:NSDateFormatterNoStyle];
		[formatter setTimeStyle:NSDateFormatterShortStyle];
		oldEntry.timeStr = [formatter stringFromDate:self.oldDate];
		oldEntry.content = contentText.text;
		oldEntry.emoticon = emoticonNo;	
		[formatter release];
	}
	
	UIImage *smile = [UIImage imageNamed:@"smile.png"];		
	UIImage *angry = [UIImage imageNamed:@"beyond_endurance.png"];
	UIImage *cool = [UIImage imageNamed:@"rockn_roll.png"];
	UIImage *frown = [UIImage imageNamed:@"unhappy.png"];
	UIImage *grin = [UIImage imageNamed:@"big_smile.png"];
	UIImage *innocent = [UIImage imageNamed:@"shame.png"];
	UIImage *kiss = [UIImage imageNamed:@"greeding.png"];
	UIImage *naughty = [UIImage imageNamed:@"the_devil.png"];
	UIImage *sealed = [UIImage imageNamed:@"what.png"];
	UIImage *tear = [UIImage imageNamed:@"cry.png"];	
	UIImage *toungue = [UIImage imageNamed:@"bad_smile.png"];	
	UIImage *undecided = [UIImage imageNamed:@"i_have_no_idea.png"];	
	UIImage *wink = [UIImage imageNamed:@"wicked.png"];
	UIImage *wow = [UIImage imageNamed:@"surprise.png"];	
	UIImage *yay = [UIImage imageNamed:@"pretty_smile.png"];
	
	NSArray *emoticonArray = [[NSArray alloc] initWithObjects:smile,angry,cool,frown,grin,innocent,kiss,naughty,sealed,tear,toungue,undecided,wink,wow,yay,nil];
	if(![self.emoticonNo isEqualToString:@"0"]){
		int no = [(NSNumber*)self.emoticonNo integerValue] ;
//		[emoticonButton setImage:(UIImage*)[emoticonArray objectAtIndex:no - 1] forState:UIControlStateNormal];
		UIGraphicsBeginImageContext(CGSizeMake(40.0,40.0));
		[(UIImage*)[emoticonArray objectAtIndex:no - 1] drawInRect:CGRectMake(0.0, 0.0, 40.0, 40.0)];
		emoticonImg = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		[emoticonButton setImage:emoticonImg forState:UIControlStateNormal];
	}
	[emoticonArray release];
	[contentText becomeFirstResponder];
	
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if([settings.fontName isEqualToString:@""] || settings.fontName == nil){
		[contentText setFont:[UIFont systemFontOfSize:15]];
		[settings storeFontSettings:@"" size:15.0];
	}else{
		//NSLog(@"font name %@",settings.fontName);	
		NSArray *fontNames = [UIFont fontNamesForFamilyName:settings.fontName];
		[contentText setFont:[UIFont fontWithName:[fontNames objectAtIndex:0] size:settings.fontSize]];
		//	[lblTemp2 setFont:[UIFont systemFontOfSize:15]];
	}
	
	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	
	//----- chk for emoticon switch enabled or not ================================
	//[emoticonButton setImage:emoticonImg forState:UIControlStateNormal];
	
	[settings loadSettings]; 
	if(settings.animateEmoticonEnabled)
	{
		emoticonTimer = [NSTimer scheduledTimerWithTimeInterval:0.5  target:self selector:@selector(updateEmoticon) userInfo:nil repeats:YES];
	}
	else
	{
		[emoticonButton setImage:emoticonImg forState:UIControlStateNormal];
	}
	
	//NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	//gpsEnabled = [userDefaults boolForKey:@"GPS"];

	//---a16
	[settings loadSettings]; 
	
	if(!isEditMode)
	{
	if(settings.gpsEnabled)
	{
		self.gpsEnabled = YES;}
	else
	{
		self.gpsEnabled = NO;
	}
		[self wantToUseGps];
	}
	else {
		if(settings.gpsEnabled==YES && self.gpsEnabled==YES)
		{
			
			gpsStatusLabel.text = @"[GPS ON]";
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"GPS" message:@"What do you want to do?" delegate:self cancelButtonTitle:@"Retain Old Location" otherButtonTitles:@"Update New Location",@"Erase Previous Location",nil];
		[alert show];
		[alert release];
			
		}
		else {
		/*	UIAlertView *alertt=[[UIAlertView alloc] initWithTitle:@"GPS" message:@"To use GPS switch on from settings" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
			[alertt show];
			[alertt release];*/
			gpsStatusLabel.text = @"[GPS OFF]";
			gpsStatusLabel.alpha=0.5;
		}

	}

	
//	[self performSelector:@selector(wantToUseGps) withObject:self afterDelay:0];
	
	
	
	
	/*
	float fadeAlpha = 0.4, fullAlpha = 1.0;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:0.2];
	//-- if not Selected (shrinked)---
	//if(gpsButton.alpha == fullAlpha)
	if(gpsEnabled)
	{		
		
		gpsButton.alpha = fullAlpha;
		CGAffineTransform t = CGAffineTransformMakeScale(1.0,1.0);	
		gpsButton.transform = t;
		
		//[UIView commitAnimations];
		
		//gpsEnabled = NO;
		
		//[self performSelector:@selector(wantToUseGps) withObject:self afterDelay:0.2];
		
	}
	//-- if selected (grown up)---
	else
	{
		gpsButton.alpha = fadeAlpha;
		CGAffineTransform t = CGAffineTransformMakeScale(0.75,0.75);	
		gpsButton.transform = t;
		
				
		//gpsEnabled = YES;
		
	}
	[UIView commitAnimations];
	*/
	//-------------
	//[gpsButton addTarget:self action:@selector(gpsPressed) forControlEvents:UIControlEventTouchUpInside]; //a
	/*
	 [UIView beginAnimations:nil context:NULL];
	 [UIView setAnimationBeginsFromCurrentState:YES];
	 [UIView setAnimationDuration:0];
	 gpsButton.alpha = 0.4;
	 CGAffineTransform t = CGAffineTransformMakeScale(0.75,0.75);	
	 gpsButton.transform = t;
	 [UIView commitAnimations];
	 */
	
	/*[settings loadSettings];
	if(settings.gpsEnabled)
	{
		gpsEnabled = YES;
	}
	else
	{
		gpsEnabled = NO;
	}*/
	
	//gpsEnabled = NO;
	//[self performSelector:@selector(wantToUseGps) withObject:self afterDelay:0];
	
	
	[settings release];	
    [super viewDidLoad];
}
-(BOOL)connectionToInternet
{
	struct sockaddr_in zeroAddress;
	bzero(&zeroAddress, sizeof(zeroAddress));
	zeroAddress.sin_len = sizeof(zeroAddress);
	zeroAddress.sin_family = AF_INET;
	
	// Recover reachability flags
	SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
	SCNetworkReachabilityFlags flags;
	
	BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
	CFRelease(defaultRouteReachability);
	
	if (!didRetrieveFlags)
	{
		printf("Error. Could not recover network reachability flags\n");
		return 0;
	}
	
	BOOL isReachable = flags & kSCNetworkFlagsReachable;	
	
	return isReachable;
	
}


-(void)Network_ckeck
{
	hostReach = [Reachability reachabilityForInternetConnection];
	
	[hostReach startNotifier];
	[self updateInterfaceWithReachability: hostReach];
	
}

- (void) updateInterfaceWithReachability: (Reachability*) curReach
{
	if(curReach == hostReach ||curReach == hostReach1 )
	{
		NetworkStatus netStatus = [curReach currentReachabilityStatus];
		
		if (!netStatus)
		{
			//NSLog(@"YES");
			//network_reachable=NO;
			UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Connection Not Available." message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
			 [alert show];
			 [alert release];
		}
		else
		{
			gpsEnabled=YES;
			//NSLog(@"NO");
			//network_reachable=YES;	
		}
		
	}
	
}

-(void)wantToUseGps
{	
	
		//BOOL y=[self connectionToInternet];
		
	if(self.gpsEnabled == YES)
	{
		gpsStatusLabel.text = @"[GPS ON]";
		gpsStatusLabel.alpha = 1.0;
		
	
		
		iscurrentlocationenabled=YES;
		CLLocationCoordinate2D location=mapView.userLocation.coordinate;
		MKCoordinateRegion region;
		MKCoordinateSpan span;
		span.latitudeDelta=0.2;
		span.longitudeDelta=0.2;
		
		//BOOL r=[Utility CheckInternetConnection:<#(char *)host_name#>
		
		double lat;
		double longt;
		lat=[Utility GetLatitude];
		longt=[Utility GetLongitude];
		//lat=[Utility GetLatitude];
		//longt=[Utility GetLongitude];
		
		NSString *devicetype=[[UIDevice currentDevice] model];
		
		if([devicetype isEqualToString:@"iPhone"])
		{
			while(lat==0)
			{
				
				//lat=40.814849;
				//longt=-73.622732;
				
				lat=[Utility GetLatitude];
				longt=[Utility GetLongitude];
				
			}
		}
		else {
			if(lat==0)
			{
				
				lat=40.814849;
				longt=-73.622732;
			}
		}
		
		location.latitude=lat;
		location.longitude=longt;
		
		geoCoder=[[MKReverseGeocoder alloc] initWithCoordinate:location];
		geoCoder.delegate=self;
		[geoCoder start];
		NSLog(@"\n GPS-Current_Location set to ON \n");
		

	}
	else if(self.gpsEnabled == NO)
	{
		gpsStatusLabel.text = @"[GPS OFF]";
		gpsStatusLabel.alpha = 0.5;
		iscurrentlocationenabled=NO;
		/*
		UIAlertView *alert2=[[UIAlertView alloc] initWithTitle:@"GPS Disabled" message:@"" delegate:self cancelButtonTitle:@"O K" otherButtonTitles:nil];
		[alert2 show];
		[alert2 release];
		*/
		if(self.isEditMode)
		{
			
			Diary *d=[[Diary alloc] init];
			currentlocation_array=[d selectcurrenlocation:oldEntry.dateStr time:oldEntry.timeStr notes:oldEntry.content]; 
			if([currentlocation_array count] !=0)
			{
				//isclupdate=NO;
				
				self.completeAddress=[[currentlocation_array objectAtIndex:0] current_location];
			}	
		}
		NSLog(@"\n GPS-Current_Location set to OFF \n");
	}
	
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	
if(buttonIndex==0)
{
	gpsEnabled=NO;
}
	else if(buttonIndex==1)
	{
			gpsEnabled=YES;
	}
	else if(buttonIndex==2)
	{
		gpsEnabled=NO;
		deletecurrent=YES;
		
		
	}
	[self wantToUseGps];
}


- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark
{
	
		self.completeAddress=[[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "]; 
		
	//Diary *diary1=[[Diary alloc] init];
		
	
	
	
}

-(UIButton *)buttonWithTitle:(NSString *)title target:(id)target selector:(SEL)selector frame:(CGRect)frame image:(UIImage *)image imagePressed:(UIImage *)imagePressed darkTextColor:(BOOL)darkTextColor
{	
	UIButton *button = [[UIButton alloc] initWithFrame:frame];
	
	
	button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
	
	[button setTitle:title forState:UIControlStateNormal];	
	if (darkTextColor)
	{
		[button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	}
	else
	{
		[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	}
	
	UIImage *newImage = [image stretchableImageWithLeftCapWidth:12.0 topCapHeight:0.0];
	
	[button setBackgroundImage:newImage forState:UIControlStateNormal];
	
	
	UIImage *newPressedImage = [imagePressed stretchableImageWithLeftCapWidth:12.0 topCapHeight:0.0];
	[button setBackgroundImage:newPressedImage forState:UIControlStateHighlighted];
	
	[button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
	
	// in case the parent view draws with a custom color or gradient, use a transparent color
	button.backgroundColor = [UIColor clearColor];
	
	return button;
}
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
-(void)updateEmoticon
{
	if(emoticonButton.currentImage == emoticonImg)
	{
		[emoticonButton setImage:nil forState:UIControlStateNormal];
	}
	else
	{
		[emoticonButton setImage:emoticonImg forState:UIControlStateNormal];
	}
}
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}
-(IBAction)clearPressed{
	contentText.text = @"";
}
-(IBAction)chooseTimePressed{
	
	isrotated=YES;
	[self rotatePressed];
	[[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
	//[[UIApplication sharedApplication] setStatusBarHidden:YES];
	[contentText resignFirstResponder];
	
	
	//imageView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Genarl-Diary.png"]];
	//self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Genarl-Diary.png"]];
	
	//imageView.image=[UIImage imageNamed:@"Genarl-Diary.png"];
    
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        self.view.bounds=CGRectMake(0, 0, 320, 568);
        imageView.frame=CGRectMake(0, 0, 320, 568);
    }
    else
    {
        self.view.bounds=CGRectMake(0, 0, 320, 460);
        imageView.frame=CGRectMake(0, 0, 320, 460);
    }

    
    
	
	//	contentText.frame=CGRectMake(contentText.bounds.origin.x, contentText.bounds.origin.y, 280, contentText.frame.size.height);
	contentText.frame=CGRectMake(18, 72 , 280, contentText.frame.size.height);
	[bt_rotate setImage:[UIImage imageNamed:@"iphone-icon-P.png"] forState:UIControlStateNormal];
	[bt_rotate setFrame:CGRectMake(203,5,37,63)];
	//[self initWithn
	//[bt_rotate setTitle:@"Rotate" forState:UIControlStateNormal];
	//self.nibName=[NSString stringWithFormat:@"PasswordViewController"];
	imageView.image=[UIImage imageNamed:@"bg6.png"];
	//imageView.frame=self.view.frame;
	
	self.view.transform=CGAffineTransformMakeRotation(0);
	
	
	[emoticonTimer invalidate];
	[contentText resignFirstResponder];
	ChooseDateViewController *chooseDateView = [[ChooseDateViewController alloc] initWithNibName:@"ChooseDateViewController" bundle:nil];
	chooseDateView.content = contentText.text;
	chooseDateView.initialDate = timeSelected;
	chooseDateView.isEditMode = self.isEditMode;
	chooseDateView.emote = self.emoticonNo;
	self.chooseDateViewController = chooseDateView;
	[self.view addSubview:self.chooseDateViewController.view];
	[chooseDateView release];
}
-(IBAction)donePressed{

	isrotated=YES;
	[self rotatePressed];
	[contentText resignFirstResponder];
	if([contentText.text isEqualToString:@""])
	{
		[eDiaryAppDelegate showMessage:@"Please enter the content of your diary entry" title:@"No Content"];
		return;
	}
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	Diary *diary = [[Diary alloc] init];
	[formatter setDateFormat:@"YYYY-MM-dd"];
	diary.dateStr = [formatter stringFromDate:self.timeSelected];
	//NSLog(@"datestr %@",diary.dateStr);
	[formatter setDateStyle:NSDateFormatterNoStyle];
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	diary.timeStr = [formatter stringFromDate:self.timeSelected];
	//NSLog(@"timestr %@",diary.timeStr);
	diary.entry = contentText.text;
	//NSLog(@"entry %@",diary.entry);
	diary.emoticon = self.emoticonNo;
	////NSLog(@"emoticon %@",diary.emoticon);
	if([diary checkDuplicate]){
		[eDiaryAppDelegate showMessage:@"The entry already exists" title:@"Duplicate Entry"];
		return;
	}
	[emoticonTimer invalidate];
	BOOL result;
	if(self.isEditMode){
		NSLog(@"old entry %@",oldEntry.content);
		result = [diary updateEntry:oldEntry.dateStr time:oldEntry.timeStr entry:oldEntry.content];
		//[diary updatecurrentlocationtodiary:oldEntry.dateStr time:oldEntry.timeStr entry:oldEntry.content cl:<#(NSString *)par_cl#>
	}else {
		
		result = [diary insert];
		//[diary insertcurrentlocationtodiary:[formatter stringFromDate:self.timeSelected] time:[formatter stringFromDate:self.timeSelected] entry:contentText.text cl:self.completeAddress];

	}

	[[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];

	/*
	if(result) [eDiaryAppDelegate showMessage:@"Entry added" title:@"Sucess"];
	else {
		[eDiaryAppDelegate showMessage:@"Entry could not be added" title:@"Error"];return;
	}*/
	
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	DiaryEntryViewController *diaryEntryView = [[DiaryEntryViewController alloc] initWithNibName:@"DiaryEntryViewController" bundle:nil];
	diaryEntryView.diaryDate = self.timeSelected;
	self.diaryEntryViewController = diaryEntryView;
	
	
	NSMutableArray *ar_check=[diary selectcurrenlocation:diary.dateStr time:diary.timeStr notes:contentText.text];
	
	if(self.gpsEnabled)
	{
	if(isEditMode==NO)
	{
		[diary insertcurrentlocationtodiary:diary.dateStr time:diary.timeStr entry:contentText.text cl:self.completeAddress];
	
	}
	else {
		
		if([ar_check count]>1)
		{
		[diary updatecurrentlocationtodiary:oldEntry.dateStr time:oldEntry.timeStr entry:oldEntry.content cl:self.completeAddress];
		}
		else {
			[diary insertcurrentlocationtodiary:diary.dateStr time:diary.timeStr entry:contentText.text cl:self.completeAddress];

		}
		[ar_check release];

	}
	}
	else if(deletecurrent){
		[diary deleteCurrentLocation:oldEntry.dateStr time:oldEntry.timeStr entry:oldEntry.content];
		
	}

	[diary release];
	[formatter release];
	[self.view addSubview:self.diaryEntryViewController.view];
	[diaryEntryView release];
	[UIView commitAnimations];
}
-(IBAction)cancelPressed{
	
	isrotated=YES;
	[self rotatePressed];
	[[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
	//[[UIApplication sharedApplication] setStatusBarHidden:YES];
	[contentText resignFirstResponder];
	
	
	//imageView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Genarl-Diary.png"]];
	//self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Genarl-Diary.png"]];
	
	//imageView.image=[UIImage imageNamed:@"Genarl-Diary.png"];
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
       // self.view.bounds=CGRectMake(0, 0, 320, 568);
        imageView.frame=CGRectMake(0, 0, 320, 568);
    }
    else
    {
       // self.view.bounds=CGRectMake(0, 0, 320, 460);
        imageView.frame=CGRectMake(0, 0, 320, 460);
    }
    
	
	//	contentText.frame=CGRectMake(contentText.bounds.origin.x, contentText.bounds.origin.y, 280, contentText.frame.size.height);
	contentText.frame=CGRectMake(18, 72 , 280, contentText.frame.size.height);
	[bt_rotate setImage:[UIImage imageNamed:@"iphone-icon-P.png"] forState:UIControlStateNormal];
	[bt_rotate setFrame:CGRectMake(203,5,37,63)];
	//[self initWithn
	//[bt_rotate setTitle:@"Rotate" forState:UIControlStateNormal];
	//self.nibName=[NSString stringWithFormat:@"PasswordViewController"];
	imageView.image=[UIImage imageNamed:@"bg6.png"];
	//imageView.frame=self.view.frame;
	
	self.view.transform=CGAffineTransformMakeRotation(0);
	[emoticonTimer invalidate];
	[contentText resignFirstResponder];
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	DiaryEntryViewController *diaryEntryView = [[DiaryEntryViewController alloc] initWithNibName:@"DiaryEntryViewController" bundle:nil];
	self.diaryEntryViewController = diaryEntryView;
	[self.view addSubview:self.diaryEntryViewController.view];
	[diaryEntryView release];
	[UIView commitAnimations];
}

-(IBAction)chooseEmoticonPressed{
	
	isrotated=YES;
	[self rotatePressed];
	[[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
	//[[UIApplication sharedApplication] setStatusBarHidden:YES];
	[contentText resignFirstResponder];
	
	
	//imageView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Genarl-Diary.png"]];
	//self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"Genarl-Diary.png"]];
	
	//imageView.image=[UIImage imageNamed:@"Genarl-Diary.png"];
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
       // self.view.bounds=CGRectMake(0, 0, 320, 568);
        imageView.frame=CGRectMake(0, 0, 320, 568);
    }
    else
    {
        //self.view.bounds=CGRectMake(0, 0, 320, 460);
        imageView.frame=CGRectMake(0, 0, 320, 460);
    }
    

	//	contentText.frame=CGRectMake(contentText.bounds.origin.x, contentText.bounds.origin.y, 280, contentText.frame.size.height);
	contentText.frame=CGRectMake(18, 72 , 280, contentText.frame.size.height);
	[bt_rotate setImage:[UIImage imageNamed:@"iphone-icon-P.png"] forState:UIControlStateNormal];
	[bt_rotate setFrame:CGRectMake(203,5,37,63)];
	//[self initWithn
	//[bt_rotate setTitle:@"Rotate" forState:UIControlStateNormal];
	//self.nibName=[NSString stringWithFormat:@"PasswordViewController"];
	imageView.image=[UIImage imageNamed:@"bg6.png"];
	//imageView.frame=self.view.frame;
	
	self.view.transform=CGAffineTransformMakeRotation(0);
	
	
	[emoticonTimer invalidate];
	[contentText resignFirstResponder];
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
	ChooseEmoticonViewCtlr *emoticonView = [[ChooseEmoticonViewCtlr alloc] initWithNibName:@"ChooseEmoticonViewCtlr" bundle:nil];
	emoticonView.contentStr = contentText.text;
	emoticonView.isEditMode = self.isEditMode;
	if(self.isEditMode){
	emoticonView.dateAdded = self.oldDate;
	}else{
		emoticonView.dateAdded = self.timeSelected;
	}
	self.emoticonViewCtlr = emoticonView;
	[self.view addSubview:self.emoticonViewCtlr.view];
	[emoticonView release];
	[UIView commitAnimations];
}
//-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
//{
//	//if(toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)
//		
//	return UIInterfaceOrientationLandscapeLeft;
//	
//}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];	
	return YES;
}
- (void)dealloc {
	[gpsStatusLabel release];
	[oldEntry release];
    [super dealloc];
}


@end
