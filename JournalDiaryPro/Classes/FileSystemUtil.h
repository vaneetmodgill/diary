#import <UIKit/UIKit.h>

@interface FileSystemUtil : NSObject {
    NSString *documentDirectoryPath;
}

@property (retain)  NSString *documentDirectoryPath;

+ (FileSystemUtil*) getSharedInstanceOfFSUtil;

- (BOOL) isFileExistInDocumentDirectoryOfApplication:(NSString*)fileName;
- (void) makeEditableCopyOfFileToDocumentFromResources:(NSString*)fileName;
- (NSString*) getPathOfFileInDocumentDirectory:(NSString*)fileName;
- (NSString*) getPathOfFileInAppResources:(NSString*)fileName;
- (void) removeFileInDocDirectory:(NSString*)fileName;
- (void) replaceFileInDocDirSourceFileName:(NSString*)srcFileName andTargetFileName:(NSString*)targetFileName;
- (NSMutableDictionary*) readPlistInDocumentDirectory:(NSString*)fileName;
- (void) writePlistInDocumentDirectory:(NSString*)fileName withDictionary:(NSMutableDictionary*) dictionary;

@end
