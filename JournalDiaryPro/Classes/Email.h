//
//  Email.h
//  pankajtodo
//
//  Created by seasia on 07/06/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//
#import"MessageUI/MessageUI.h"
#import <UIKit/UIKit.h>
#import"Diary.h"


@class Diary; 

@interface Email : UIViewController <UINavigationControllerDelegate, MFMailComposeViewControllerDelegate,UIAlertViewDelegate ,UIActionSheetDelegate ,UIPickerViewDelegate > {

	Diary *diary_obj;
	NSString *date_str;
		NSString *time_str;
		NSString *content;
	UIImage *image_email;
	
	NSMutableArray *ar;
	
}
@property (nonatomic,retain)NSString *date_str;
@property (nonatomic,retain)NSString *time_str;
@property (nonatomic,retain)NSString *content;
@property (nonatomic,retain)UIImage *image_email;

@end
