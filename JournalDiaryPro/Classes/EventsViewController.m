///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   EventsViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "EventsViewController.h"
#import "Events.h"
#import "CalendarViewController.h"
#import "HomeViewController.h"
#import "AddEventViewController.h"
#import "ShowEventViewController.h"
#import "Settings.h"
#import "eDiaryAppDelegate.h"
#import "DayPicture.h"
@implementation EventsViewController
@synthesize eventDate;
@synthesize image;
@synthesize calendarViewController;
@synthesize homeViewController;
@synthesize addEventViewController;
@synthesize showEventViewController,eventdatearray;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        
        
        backgroundImageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        backgroundImageView.frame = CGRectMake(0, -20, 320, 568+40);
    }
    else
    {
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 480+20);
    }
    

    
    
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy-MM-dd"];
	//[formatter setTimeStyle:NSDateFormatterShortStyle];
	
	
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit | NSMinuteCalendarUnit ;
	NSDateComponents *compsDate = [gregorian components:unitFlags fromDate:[formatter dateFromString:[NSString stringWithFormat:@"%@",settings.globalDate]]];
	
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	NSDateComponents *compsTime = [gregorian components:unitFlags fromDate:[NSDate date]];
	[compsTime setDay:[compsDate day]];
	[compsTime setMonth:[compsDate month]];
	[compsTime setYear:[compsDate year]];
	
	self.eventDate = [gregorian dateFromComponents:compsTime];
	[gregorian release];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
	}
	[settings release];
	Events *events = [[Events alloc] init];
	NSMutableArray *mutArray = [[NSMutableArray alloc] init];
//	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterNoStyle];
	if(self.eventDate !=nil){
		dateLabel.text = [formatter stringFromDate:self.eventDate];
	}else {
		dateLabel.text = [formatter stringFromDate:[NSDate date]];
		self.eventDate = [NSDate date];
	}
	[formatter setDateFormat:@"YYYY-MM-dd"];	
	NSArray *array = [events getData:[formatter stringFromDate:self.eventDate]];
	[events release];
	//NSLog(@"array %@",array);
	for (NSDictionary *dictionary in array){
		Events *events = [[Events alloc] init];
		events.dateStr = [dictionary valueForKey:@"date"];
		events.timeStr = [dictionary valueForKey:@"time"];
		events.locationStr = [dictionary valueForKey:@"location"];
		events.title = [dictionary valueForKey:@"title"];
		events.descriptionStr = [dictionary valueForKey:@"description"];
		[mutArray addObject:events];
		[events release];
	}
	eventArray = [mutArray mutableCopy];
	[mutArray release];
	[formatter release]; 
	[eventTable reloadData];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

-(IBAction)calendarPressed{
	CalendarViewController *calendarView = [[CalendarViewController alloc] initWithNibName:@"CalendarViewController" bundle:nil];
	calendarView.preView = @"Events";
	[eDiaryAppDelegate setCalendarName:@"Events"];
	self.calendarViewController = calendarView;
	[self.view addSubview:self.calendarViewController.view];
	[calendarView release];
}
-(IBAction)backPressed{
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	HomeViewController *homeView = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
	self.homeViewController = homeView;
	[self.view addSubview:self.homeViewController.view];
	[homeView release];
	[UIView commitAnimations];
}
-(IBAction)leftPressed{
	[eventArray removeAllObjects];
	Events *events = [[Events alloc] init];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterNoStyle];
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit | NSMinuteCalendarUnit;
	NSDateComponents *comps = [gregorian components:unitFlags fromDate:self.eventDate];
	NSDateComponents *compsTime = [gregorian components:unitFlags fromDate:[NSDate date]];
	[compsTime setDay:[comps day]-1];
	[compsTime setMonth:[comps month]];
	[compsTime setYear:[comps year]];
	self.eventDate = [gregorian dateFromComponents:compsTime];
	[gregorian release];
	dateLabel.text = [formatter stringFromDate:self.eventDate];
	
	[formatter setDateFormat:@"YYYY-MM-dd"];	
	NSArray *array = [events getData:[formatter stringFromDate:self.eventDate]];
	//NSLog(@"array %@",array);
	[events release];
	for (NSDictionary *dictionary in array){
		Events *events = [[Events alloc] init];
		events.dateStr = [dictionary valueForKey:@"date"];
		events.timeStr = [dictionary valueForKey:@"time"];
		events.locationStr = [dictionary valueForKey:@"location"];
		events.title = [dictionary valueForKey:@"title"];
		events.descriptionStr = [dictionary valueForKey:@"description"];
		[eventArray addObject:events];
		[events release];
	}
	
	[eventTable reloadData];
	Settings *settings = [[Settings alloc] init];
	
	[settings storeGlobalDate:[formatter stringFromDate:self.eventDate]];
	[settings loadSettings];
	if(settings.picOfDayEnabled && settings.picEnabled){
		DayPicture *dayPicture = [[DayPicture alloc] init];
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *imgName =[dayPicture getPictureNameForDate:settings.globalDate];
		//NSLog(@"name %@",imgName);
		if(![imgName isEqualToString:@""]){ 
			UIImage *img= [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",documentsDirectory,imgName]];
			[eDiaryAppDelegate setHomeImage:img];
			imageView.image = img;
		}else{
			[eDiaryAppDelegate setHomeImage:nil];
			imageView.image = nil;
		}
		[dayPicture release];
	}
	[settings release];
	[formatter release];
}
-(IBAction)rightPressed{
	[eventArray removeAllObjects];
	Events *events = [[Events alloc] init];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterNoStyle];
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit |NSMinuteCalendarUnit;
	NSDateComponents *comps = [gregorian components:unitFlags fromDate:self.eventDate];
	NSDateComponents *compsTime = [gregorian components:unitFlags fromDate:[NSDate date]];
	[compsTime setDay:[comps day]+1];
	[compsTime setMonth:[comps month]];
	[compsTime setYear:[comps year]];
	self.eventDate = [gregorian dateFromComponents:compsTime];
	[gregorian release];
	dateLabel.text = [formatter stringFromDate:self.eventDate];
	
	[formatter setDateFormat:@"YYYY-MM-dd"];	
	NSArray *array = [events getData:[formatter stringFromDate:self.eventDate]];
	//NSLog(@"array %@",array);
	[events release];
	for (NSDictionary *dictionary in array){
		Events *events = [[Events alloc] init];
		events.dateStr = [dictionary valueForKey:@"date"];
		events.timeStr = [dictionary valueForKey:@"time"];
		events.locationStr = [dictionary valueForKey:@"location"];
		events.title = [dictionary valueForKey:@"title"];
		events.descriptionStr = [dictionary valueForKey:@"description"];
		[eventArray addObject:events];
		[events release];
	}
	[eventTable reloadData];
	
	Settings *settings = [[Settings alloc] init];
	
	[settings storeGlobalDate:[formatter stringFromDate:self.eventDate]];
	[settings loadSettings];
	if(settings.picOfDayEnabled && settings.picEnabled){
		DayPicture *dayPicture = [[DayPicture alloc] init];
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *imgName =[dayPicture getPictureNameForDate:settings.globalDate];
		////NSLog(@"name %@",imgName);
		if(![imgName isEqualToString:@""]){ 
			UIImage *img= [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",documentsDirectory,imgName]];
			[eDiaryAppDelegate setHomeImage:img];
			imageView.image = img;
		}else{
			[eDiaryAppDelegate setHomeImage:nil];
			imageView.image = nil;
		}
		[dayPicture release];
	}
	[settings release];
	[formatter release];
}
-(IBAction)addPressed{
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
	AddEventViewController *addEventView = [[AddEventViewController alloc] initWithNibName:@"AddEventViewController" bundle:nil];
	addEventView.timeSelected = self.eventDate;
	self.addEventViewController = addEventView;
	[self.view addSubview:self.addEventViewController.view];
	[addEventView release];
	[UIView commitAnimations];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [eventArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	/*    if (cell == nil) {
	 cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
	 }
	 
	 // Set up the cell...
	 if(indexPath.section == 0) {
	 cell.text = [foodItems objectAtIndex:indexPath.row];
	 } else if(indexPath.section == 1) {
	 cell.text = [activityItems objectAtIndex:indexPath.row];
	 }*/
	
	
    if (cell == nil) {
        cell = [self getCellContentView:CellIdentifier];
    }
	
	UILabel *timeLbl =  (UILabel *)[cell viewWithTag:1];
	UILabel *locationLbl =  (UILabel *)[cell viewWithTag:2];
	UILabel *titleLbl = (UILabel *)[cell viewWithTag:3]; 
	
	Events *obj = (Events *)[eventArray objectAtIndex:indexPath.row];
	timeLbl.text = obj.timeStr;
	locationLbl.text = obj.locationStr;
	titleLbl.text = obj.title;
	
	/*	
	 if(indexPath.row%2) {
	 [lblTemp1 setBackgroundColor:[UIColor colorWithRed:0.219 green:0.586 blue:0.590 alpha:1.0]];
	 [lblTemp2 setBackgroundColor:[UIColor colorWithRed:0.219 green:0.586 blue:0.590 alpha:1.0]];
	 } else {
	 [lblTemp1 setBackgroundColor:[UIColor colorWithRed:0.391 green:0.871 blue:0.852 alpha:1.0]];
	 [lblTemp2 setBackgroundColor:[UIColor colorWithRed:0.391 green:0.871 blue:0.852 alpha:1.0]];
	 }
	 */
    return cell;
}
- (UITableViewCell *) getCellContentView:(NSString *)cellIdentifier {
	
	CGRect CellFrame = CGRectMake(0.0f, 0.0f, 320.0f, 75.0f);
	CGRect lblFrame1 = CGRectMake(15.0f, 0.0f, 305.0f, 25.0f);
	CGRect lblFrame2 = CGRectMake(15.0f, 25.0f, 300.0f, 25.0f);
	CGRect lblFrame3 = CGRectMake(15.0f, 50.0f, 300.0f, 25.0f);
	
	UILabel *lblTemp1;
	UILabel *lblTemp2;
	UILabel *lblTemp3;
	UITableViewCell *cell = [[[UITableViewCell alloc] initWithFrame:CellFrame reuseIdentifier:cellIdentifier] autorelease];
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];

	
	//Initialize Label 1 with tag 1.
	lblTemp1 = [[UILabel alloc] initWithFrame:lblFrame1];
	[lblTemp1 setBackgroundColor:[UIColor clearColor]];
	//	[lblTemp1 sizeToFit];
	//	[lblTemp1 setMinimumFontSize:8.0];
	if([settings.fontName isEqualToString:@""] || settings.fontName == nil){
		[lblTemp1 setFont:[UIFont systemFontOfSize:15]];
		[settings storeFontSettings:@"" size:15.0];
	}else{
		NSArray *fontNames = [UIFont fontNamesForFamilyName:settings.fontName];
		[lblTemp1 setFont:[UIFont fontWithName:[fontNames objectAtIndex:0] size:settings.fontSize]];
		//	[lblTemp2 setFont:[UIFont systemFontOfSize:15]];
	}
	lblTemp1.tag = 1;
	[cell.contentView addSubview:lblTemp1];
	[lblTemp1 release];
	
	//Initialize Label 2 with tag 2.
	lblTemp2 = [[UILabel alloc] initWithFrame:lblFrame2];
	[lblTemp2 setBackgroundColor:[UIColor clearColor]];
	lblTemp2.tag = 2;
	lblTemp2.numberOfLines = 1;
	//	[lblTemp2 sizeToFit];
	//	[lblTemp2 setMinimumFontSize:8.0];
		if([settings.fontName isEqualToString:@""] || settings.fontName == nil){
		[lblTemp2 setFont:[UIFont systemFontOfSize:20]];
		[settings storeFontSettings:@"" size:20.0];
	}else{
		NSArray *fontNames = [UIFont fontNamesForFamilyName:settings.fontName];
		[lblTemp2 setFont:[UIFont fontWithName:[fontNames objectAtIndex:0] size:settings.fontSize]];
		//	[lblTemp2 setFont:[UIFont systemFontOfSize:15]];
	}
	
	[cell.contentView addSubview:lblTemp2];
	[lblTemp2 release];
	
	
	//Initialize Label 3 with tag 3.
	lblTemp3 = [[UILabel alloc] initWithFrame:lblFrame3];
	[lblTemp3 setBackgroundColor:[UIColor clearColor]];
	lblTemp3.tag = 3;
	lblTemp3.numberOfLines = 1;
	//	[lblTemp2 sizeToFit];
	//	[lblTemp2 setMinimumFontSize:8.0];
	
	[settings loadSettings];
	if([settings.fontName isEqualToString:@""] || settings.fontName == nil){
		[lblTemp3 setFont:[UIFont systemFontOfSize:20]];
		[settings storeFontSettings:@"" size:20.0];
	}else{
		NSArray *fontNames = [UIFont fontNamesForFamilyName:settings.fontName];
		[lblTemp3 setFont:[UIFont fontWithName:[fontNames objectAtIndex:0] size:settings.fontSize]];
		//	[lblTemp2 setFont:[UIFont systemFontOfSize:15]];
	}
	[settings release];
	[cell.contentView addSubview:lblTemp3];
	[lblTemp3 release];
	
	return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 75.0f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	
	ShowEventViewController *showEventView = [[ShowEventViewController alloc] initWithNibName:@"ShowEventViewController" bundle:nil];
	Events *obj = (Events*)[eventArray objectAtIndex:indexPath.row];
	showEventView.event = obj;
	self.showEventViewController = showEventView;
	[self.view addSubview:self.showEventViewController.view];
	[showEventView release];
	 
}

- (void)dealloc {
	[eventArray release];
    [super dealloc];
}


@end
