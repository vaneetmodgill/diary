///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   ChoosePictureViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "ChoosePictureViewController.h"
#import "Picture.h"
#import "SettingsPictureViewController.h"
#import "eDiaryAppDelegate.h"
#import "VoiceMemoUtils.h"
#import "Settings.h"
@implementation ChoosePictureViewController
@synthesize selectedFileName;
@synthesize settingsPictureViewController,imageforeverydiary,diaryEntryViewController,entryObj;
@synthesize timestr,datestr,content;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    NSLog(@"viewDidLoad Frame : %@", NSStringFromCGRect(self.view.frame));
    
   NSArray * wins = [UIApplication sharedApplication].windows ;
    
    for (UIWindow *win in wins) {
        win.frame = [UIScreen mainScreen].bounds;

    }
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        backgrndImaView.frame =CGRectMake(0, -20, 320, 568+40);
        
        
    }
    
    else
    {
        backgrndImaView.frame =CGRectMake(12, 0, 320, 480);
        
    }
    
        
	Picture *picture =[[Picture alloc] init];
	pictureArray = [[NSMutableArray alloc] init];
	NSArray *array = [picture getData];
	[picture release];
	//NSLog(@"array %@",array);
	for(NSDictionary *dic in array){
		[pictureArray addObject:[dic valueForKey:@"name"]];
	}
	[pictureTable reloadData];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
	//NSLog(@"RECEIVED MEMORY WARNING in ChoosePictureViewController!!!");
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}
-(IBAction)addPicture{
	UIActionSheet *menuAction = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Pick from Photo Library",@"Take a new Picture",nil];
	[menuAction showInView:self.view];
	[menuAction release];
}

-(IBAction)deletePicture {
	
	if([pictureTable indexPathForSelectedRow] == nil){
		[eDiaryAppDelegate showMessage:@"Please select a picture to delete" title:@"No Picture Selected"];
		return;
	}
	NSInteger row = [pictureTable indexPathForSelectedRow].row;
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
//	path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",[pictureArray objectAtIndex:row]]];
	NSError *error;
	//NSLog(@"deletable %d",[fileManager isDeletableFileAtPath:[NSString stringWithFormat:@"%@/%@.png", documentsDirectory,[pictureArray objectAtIndex:row]]]);
	BOOL deleted = [fileManager removeItemAtPath:[NSString stringWithFormat:@"%@/%@.png", documentsDirectory,[pictureArray objectAtIndex:row]] error:&error];
	if(!deleted){
		//NSLog(@"An error occured while file deletion! -- %@", error);
	}
	//NSLog(@"deleted %d",deleted);
	Picture *picture = [[Picture alloc] init];
	[picture deletePicture:[pictureArray objectAtIndex:row]];
	[pictureArray removeAllObjects];
	//NSLog(@"2");
	NSArray *array = [picture getData];
	for(NSDictionary *dic in array){
		[pictureArray addObject:[dic valueForKey:@"name"]];
	}
	[pictureTable reloadData];
	[picture release];
}
 
-(IBAction)donePressed{
	if(self.selectedFileName == nil){
		[eDiaryAppDelegate showMessage:@"please select a picture" title:@"No Selection"];
		return;
	}
	//if(self.imageforeverydiary==NO)
	//{
	SettingsPictureViewController *settingsPictureView = [[SettingsPictureViewController alloc] initWithNibName:@"SettingsPictureViewController" bundle:nil];
	settingsPictureView.fileName = self.selectedFileName;
	self.settingsPictureViewController = settingsPictureView;
	[self.view addSubview:self.settingsPictureViewController.view];
	[settingsPictureView release];
	//}
	/*else {
		Diary *diary1=[[Diary alloc] init];
//		self.selectedFileName = [NSString stringWithFormat:@"%@.png",[pictureArray objectAtIndex:indexPath.row]];

		[diary1 updateImage:self.datestr time:self.timestr entry:self.content image:self.selectedFileName];
		[UIView beginAnimations:nil context:nil];
		//change to set the time
		[UIView setAnimationDuration:1];
		[UIView setAnimationBeginsFromCurrentState:YES];
		[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
		DiaryEntryViewController *diary = [[DiaryEntryViewController alloc] initWithNibName:@"DiaryEntryViewController" bundle:nil];
		//	diaryEntryViewController.image = imageView.image;
		self.diaryEntryViewController = diary;
		[self.view addSubview:self.diaryEntryViewController.view];
		[diary release];
		// do your view swapping here
		//[diaryEntryViewController.view setUserInteractionEnabled: YES];
		[UIView commitAnimations];	
	}*/

}
-(IBAction)backPressed{
	
	SettingsPictureViewController *settingsPictureView = [[SettingsPictureViewController alloc] initWithNibName:@"SettingsPictureViewController" bundle:nil];
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	settingsPictureView.fileName = settings.currentPicture;
	[settings release];
	self.settingsPictureViewController = settingsPictureView;
	[self.view addSubview:self.settingsPictureViewController.view];
	[settingsPictureView release];
}

/*
-(void)imagePickerController: (UIImagePickerController *)picker
	   didFinishPickingImage: (UIImage *) image
				 editingInfo:(NSDictionary *)editingInfo {
	imageView.image = image;
	//	//NSLog(@"Image choosed ::> %@",image);
	[picker dismissModalViewControllerAnimated:NO];
}
*/


- (BOOL)prefersStatusBarHidden
{
    return YES;
}
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
 //   [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
	//NSLog(@"dic %@",info);
	NSString *mediaType = [info valueForKey:UIImagePickerControllerMediaType];
	//NSLog(@"media type %@",mediaType);
	if([mediaType isEqualToString:@"public.image"]){
		UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
	//	NSData *pngImage = UIImagePNGRepresentation(image);
		UIGraphicsBeginImageContext(CGSizeMake(308.0,460.0));
		// now redraw our image in a smaller rectangle.
		[image drawInRect:CGRectMake(0.0, 0.0, 308.0, 460.0)];
		//[myThumbNail release];
		// make a "copy" of the image from the current context
		UIImage *newImage    = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		// create a new view to place on our screen (OPTIONAL-- testing)
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		Picture *picture = [[Picture alloc] init];
		Diary *diary=[[Diary alloc] init];
		NSDate *tempDate=[NSDate date];
		NSString *picName = [NSString stringWithFormat:@"Picture%@",[NSNumber numberWithUnsignedLongLong:[tempDate timeIntervalSince1970]]] ;
		if([UIImagePNGRepresentation(newImage) writeToFile:[NSString stringWithFormat:@"%@/%@.png", documentsDirectory,picName] atomically:YES]){
			
			[picture insert:picName];
			
			
		}
		[pictureArray removeAllObjects];
		NSArray *array = [picture getData];
		//NSLog(@"array %@",array);
		for(NSDictionary *dic in array){
			[pictureArray addObject:[dic valueForKey:@"name"]];
		}
		
		[picture release];
//		image = nil;
//		pngImage = nil; 
		[pictureTable reloadData];
	}
	[picker dismissModalViewControllerAnimated:NO];
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
	//NSLog(@"No Image choosed");
	[picker dismissModalViewControllerAnimated:NO];
}


- (void)dealloc {
	[pictureArray release];
	[settingsPictureViewController release];
    [super dealloc];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [pictureArray count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	/*    if (cell == nil) {
	 cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
	 }
	 
	 // Set up the cell...
	 if(indexPath.section == 0) {
	 cell.text = [foodItems objectAtIndex:indexPath.row];
	 } else if(indexPath.section == 1) {
	 cell.text = [activityItems objectAtIndex:indexPath.row];
	 }*/
	
	
    if (cell == nil) {
        cell = [self getCellContentView:CellIdentifier];
    }
	
	UILabel *nameLbl =  (UILabel *)[cell viewWithTag:1];
	
	
	
	NSString *nameStr = [pictureArray objectAtIndex:indexPath.row];
	nameLbl.text = nameStr;
	
	
	CGRect lblFrame = CGRectMake(20.0f, 0.0f, 80.0f, 80.0f);
	UIView *tempView = [[[UIView alloc] initWithFrame:lblFrame ] autorelease];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	UIImage *myThumbNail = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@.png",documentsDirectory,nameStr]]; 
	
	// lets essentially make a copy of the selected image.
//	UIImage *myThumbNail    = [[UIImage alloc] initWithData:pngImage];
	// begin an image context that will essentially "hold" our new image
	UIGraphicsBeginImageContext(CGSizeMake(80.0,80.0));
	// now redraw our image in a smaller rectangle.
	[myThumbNail drawInRect:CGRectMake(0.0, 0.0, 80.0, 80.0)];
	//[myThumbNail release];
	// make a "copy" of the image from the current context
	UIImage *newImage    = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	// create a new view to place on our screen (OPTIONAL-- testing)
	UIImageView *thumbView    = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 80.0, 80.0)];
	thumbView.image    = newImage;
	[tempView addSubview:thumbView];
	
	
	
	/*
	UIImageView *imgView = [[[UIImageView alloc] initWithImage:tmpImg] autorelease];
	[tempView sizeToFit];
	[tempView addSubview:imgView];
	//[tempView setCenter:CGPointMake(20.0f,30.0f)];
	 */
	[cell.contentView addSubview:tempView];
//	[tempView release];
	myThumbNail = nil;
	newImage = nil;	
	/*	
	 if(indexPath.row%2) {
	 [lblTemp1 setBackgroundColor:[UIColor colorWithRed:0.219 green:0.586 blue:0.590 alpha:1.0]];
	 [lblTemp2 setBackgroundColor:[UIColor colorWithRed:0.219 green:0.586 blue:0.590 alpha:1.0]];
	 } else {
	 [lblTemp1 setBackgroundColor:[UIColor colorWithRed:0.391 green:0.871 blue:0.852 alpha:1.0]];
	 [lblTemp2 setBackgroundColor:[UIColor colorWithRed:0.391 green:0.871 blue:0.852 alpha:1.0]];
	 }
	 */
    return cell;
}
- (UITableViewCell *) getCellContentView:(NSString *)cellIdentifier {
	
	CGRect CellFrame = CGRectMake(0.0f, 0.0f, 320.0f, 80.0f);
	CGRect lblFrame1 = CGRectMake(100.0f, 0.0f, 305.0f, 80.0f);
	
	UILabel *lblTemp1;
	
	UITableViewCell *cell = [[[UITableViewCell alloc] initWithFrame:CellFrame reuseIdentifier:cellIdentifier] autorelease];
	
	
	//Initialize Label 1 with tag 1.
	lblTemp1 = [[UILabel alloc] initWithFrame:lblFrame1];
	[lblTemp1 setBackgroundColor:[UIColor clearColor]];
	//	[lblTemp1 sizeToFit];
	//	[lblTemp1 setMinimumFontSize:8.0];
	[lblTemp1 setFont:[UIFont systemFontOfSize:17]];
	lblTemp1.tag = 1;
	[cell.contentView addSubview:lblTemp1];
	[lblTemp1 release];
	
	
	return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 80.0f;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
	//NSLog(@"index %d",buttonIndex);
	
	if(buttonIndex == 1){
	 @try {//take a new picture
		 if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
	 UIImagePickerController *picker = [[UIImagePickerController alloc] init];
	 picker.delegate = self;
	picker.allowsImageEditing = YES;
	 picker.sourceType = UIImagePickerControllerSourceTypeCamera;
	 [self presentModalViewController:picker animated:YES];
	 [picker release];	
		 }else{
			 [eDiaryAppDelegate showMessage:@"Sorry the device does not support Camera" title:@"No Camera available"];
		 }
	 }
	 @catch (NSException * e) {
	 //NSLog(@"Exception occurred. %@", e);
	 }
	}else if(buttonIndex == 0){
	 @try {
         NSLog(@"******** windows******** %@",[UIApplication sharedApplication].windows);
         
	 if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
	 UIImagePickerController *picker = [[UIImagePickerController alloc] init];
	 picker.delegate = self;
	 			picker.allowsImageEditing = YES;
	 picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
	 [self presentModalViewController:picker animated:YES];
	 [picker release];
	 } else {
	 [eDiaryAppDelegate showMessage:@"Device does not support a photo library" title:@"Error accessing photo library"];
	 }
	 }
	 @catch (NSException * e) {
	 ////NSLog(@"Exception occurred. %@", e);
	 }
	 }
	 
	[actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
	
}	

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	index=indexPath.row;
		self.selectedFileName = [NSString stringWithFormat:@"%@.png",[pictureArray objectAtIndex:indexPath.row]];
}	   
	   
@end
