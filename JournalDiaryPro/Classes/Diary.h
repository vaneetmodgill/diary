///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   Diary.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>

#import <sqlite3.h>
@interface Diary : NSObject {
	NSString *dateStr;
	NSString *timeStr;
	NSString *entry;
	NSString *emoticon;
	NSString *imagename;
	UIImage *background_image;
	NSString *current_location;
	NSString *filename;
	NSString *twitter_un;
		NSString *twitter_password;
	//sqlite3 *database;
}
@property(nonatomic,retain)NSString *twitter_un;
@property(nonatomic,retain)NSString *twitter_password;
@property(nonatomic,retain)NSString *filename;
@property(nonatomic,retain) NSString *current_location;
@property(nonatomic,retain)UIImage *background_image;
@property(nonatomic,retain)NSString *dateStr;
@property(nonatomic,retain)NSString *timeStr;
@property(nonatomic,retain)NSString *entry;
@property(nonatomic,retain)NSString *emoticon;
@property(nonatomic,retain)NSString *imagename;
-(BOOL)insert;
-(BOOL)updateEntry:(NSString *)oldDate time:(NSString *)oldTime entry:(NSString *)notes;
-(BOOL)deleteEntry:(NSString *)oldDate time:(NSString *)oldTime entry:(NSString *)notes;
-(NSArray*)getData:(NSString *)entryDate;
-(NSArray*)getSearchData:(NSString*)searchText;
-(NSArray*)getExportData:(NSString *)from and:(NSString *)to;
-(BOOL)isEntryAddedForDate:(NSString *)dateOfEntry;
-(NSArray*)getDatesInMonth:(NSString *)fromDate	to:(NSString *)toDate;
-(BOOL)checkDuplicate;
-(BOOL)updateImage:(NSString *)oldDate time:(NSString *)oldTime entry:(NSString *)notes image:(NSString*)par_image;
-(NSMutableArray*)selectImagewhere:(NSString*)par_date time:(NSString*)par_time;
-(NSMutableArray*)selectImagewhere:(NSString*)par_date time:(NSString*)par_time notes:(NSString*)par_notes;
-(void)updatecurrentlocationtodiary:(NSString*)par_date time:(NSString*)par_time entry:(NSString*)notes cl:(NSString*)par_cl;
-(void)insertcurrentlocationtodiary:(NSString*)par_date time:(NSString*)par_time entry:(NSString*)notes cl:(NSString*)par_cl;
-(NSMutableArray*)selectcurrenlocation:(NSString*)par_date time:(NSString*)par_time notes:(NSString*)par_notes;
-(void)updateimagetodiary:(NSString*)par_date time:(NSString*)par_time entry:(NSString*)notes picture:(UIImage*)par_picture;
	
-(void)insertimagetodiaryefficiently:(NSString*)par_date time:(NSString*)par_time img_name:(NSString*)notes file_name:(NSString*)par_file;
-(void)updateimagetodiaryefficiently:(NSString*)par_date time:(NSString*)par_time image_name:(NSString*)par_image filename:(NSString*)par_file;
-(NSMutableArray*)selectImagewhereEfficiently:(NSString*)par_date time:(NSString*)par_time notes:(NSString*)par_notes;
-(NSMutableArray*)getTwitterDetails;
-(BOOL)setTwitterDetails:(NSString*)par_un :(NSString*)par_password;
-(BOOL)updateTwitterDetails;
@end
