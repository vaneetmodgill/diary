///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:	ActivityIndicator
/// Overview	:	Header file for ActivityIndicator class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :	Initial version
//
// (c) COPYRIGHT 2009-2010 ,  ALL RIGHTS RESERVED.
// 
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND 
// CONSTITUTES TRADE SECRETS OF ,  (", LLC").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE 
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY ,  IF YOU ARE NOT 
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO , LLC 
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE 
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY 
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////
#import <UIKit/UIKit.h>


@interface ActivityIndicator : NSObject {
	IBOutlet UIWindow *window;
	IBOutlet UILabel *label;
}

@property (nonatomic, readonly) UIWindow *window;
+ (ActivityIndicator *)sharedActivityIndicator;

- (void)show:(NSString *)msg;
- (void)hide;
- (void)setText:(NSString *)msg;

@end
