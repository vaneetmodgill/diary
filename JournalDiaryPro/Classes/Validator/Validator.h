///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:	Validator.h
/// Overview	:	Header for Validator class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	28 May 2009
// Change reason :	Initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
// 
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND 
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE 
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT 
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba 
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE 
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY 
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>

// Validator class has static methods for input validations.
@interface Validator : NSObject {

}

//Checks for isEmpty string.
+(BOOL)isEmpty:(NSString *)str;
//Validates the given string is a valid name.
+(BOOL)isValidName:(NSString *)str;
//Validates the given string is a valid email id.
+(BOOL)isValidEmailId:(NSString *)str;
//Validates the given string is a valid user id.
+(BOOL)isValidUserId:(NSString *)str;
//Validates the given string has only numbers.
+(BOOL)isNumbers:(NSString *)str;

@end
