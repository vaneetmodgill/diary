///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:	Validator.m
/// Overview	:	Implementation file for Validator class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	28 May 2009
// Change reason :	Initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
// 
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND 
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE 
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT 
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba 
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE 
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY 
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "Validator.h"
#import "RegexKitLite.h"


@implementation Validator

+(BOOL)isEmpty:(NSString *)str {
	str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
	if((str==nil) || [str isEqualToString:@""]) {
		return YES;
	}
	return NO;
}

+(BOOL)isValidName:(NSString *)str{
	[NSString clearStringCache];
	NSString *nameRegEx = @"[a-zA-Z]+$";
	NSArray *matchArray = [str componentsMatchedByRegex:nameRegEx];
	if ([matchArray count] <= 0) {
		return NO;
	}	
	return YES;	
}

+(BOOL)isValidUserId:(NSString *)str{
	if([str length] >20) {
		return NO;
	}
	[NSString clearStringCache];
	NSString *useridRegEx = @"^([A-Za-z0-9](([_\\.]?[a-zA-Z0-9]+)*)){3,}$";
	NSArray *matchArray = [str componentsMatchedByRegex:useridRegEx];
	if ([matchArray count] <= 0) {
		if (![Validator isValidEmailId:str]) {
			return NO;
		}
	}	
	return YES;	
}

+(BOOL)isValidEmailId:(NSString *)str{
/*	int atloc = [str rangeOfString:@"@"].location;
	int dotloc = [str rangeOfString:@"." options:NSBackwardsSearch].location;
	int atlen = [str rangeOfString:@"@"].length;
	int dotlen = [str rangeOfString:@"." options:NSBackwardsSearch].length;
	
	//NSLog(@"%d %d", atloc, dotloc);
	////NSLog(@"%d %d", atlen, dotlen);
	
	if((atlen != 0) && (dotlen !=0) && ((atloc +1) < dotloc))
		return YES;
	return NO;
 */
//	NSString *emailRegEx = @"[A-Z0-9a-z_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
	[NSString clearStringCache];
	NSString *emailRegEx = @"^[A-Za-z0-9](([_\\.\\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\\.\\-]?[a-zA-Z0-9]+)*)\\.([A-Za-z]{2,})$";
	NSArray *matchArray = [str componentsMatchedByRegex:emailRegEx];
	if ([matchArray count] <= 0) {
		return NO;
	}	
	return YES;	
}

+(BOOL)isNumbers:(NSString *)str{
	[NSString clearStringCache];
	NSString *nameRegEx = @"[0-9]+$";
	NSArray *matchArray = [str componentsMatchedByRegex:nameRegEx];
	if ([matchArray count] <= 0) {
		return NO;
	}	
	return YES;	
}

@end
