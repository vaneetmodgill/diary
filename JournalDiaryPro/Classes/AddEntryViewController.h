///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   AddEntryViewController.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////
#import <SystemConfiguration/SCNetworkReachability.h>
#include <netinet/in.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManagerDelegate.h>
#import <MapKit/MKReverseGeocoder.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKAnnotation.h>
#import "Reachability.h"
@class DiaryObject;
@class ChooseEmoticonViewCtlr;
@class DiaryEntryViewController;
@class ChooseEmoticonViewCtlr;
@class ChooseDateViewController;
@interface AddEntryViewController : UIViewController<UIAlertViewDelegate, CLLocationManagerDelegate,MKReverseGeocoderDelegate,MKMapViewDelegate,UITextFieldDelegate , UITextViewDelegate> {
	IBOutlet UILabel *timeLabel;
	IBOutlet UITextView *contentText;
	IBOutlet UIButton *emoticonButton;
	IBOutlet UIImageView *imageView;
	UIImage *emoticonImg;
	NSDate *timeSelected;
	NSString *contentStr;
	NSString *emoticonNo;
	NSDate *oldDate;
	BOOL isEditMode;
	DiaryObject *oldEntry;
	NSTimer *emoticonTimer;
	ChooseDateViewController *chooseDateViewController;
	DiaryEntryViewController *diaryEntryViewController;
	ChooseEmoticonViewCtlr *emoticonViewCtlr;
	
	IBOutlet UIButton *doneButton;
	IBOutlet UIButton *cancelButton;
	IBOutlet UIButton *clearButton;
	
	MKReverseGeocoder *geoCoder;
	MKPlacemark *mPlacemark;
	
	CLLocationManager	*locationMgr;
	NSTimer *Locationtimer;
	double Latitude,Longitude;
	NSString *completeAddress;
	BOOL isclupdate;
	MKMapView *mapView;
	IBOutlet UIImageView *tickcrossimageview;
	IBOutlet UITextView *textview2;
	NSMutableArray *currentlocation_array;
	BOOL iscurrentlocationenabled;
	
	IBOutlet UIButton *gpsButton; 
	IBOutlet UILabel *gpsStatusLabel; //a16
    
    IBOutlet UILabel *contentEmoticon; //a16

	BOOL gpsEnabled;
	BOOL usedprevious;
	BOOL deletecurrent;
	Reachability *hostReach;
	Reachability *hostReach1;
	
	BOOL isrotated;
	
IBOutlet UIButton *bt_rotate;

    IBOutlet UIButton *bt_Clock;

    
    IBOutlet UIImageView *backgroundImageView;
    
    
    

}
@property(nonatomic,retain)IBOutlet UIButton *bt_rotate;
@property BOOL usedprevious;
@property BOOL gpsEnabled;
@property(nonatomic,retain)IBOutlet UILabel *gpsStatusLabel;
@property(nonatomic,retain)IBOutlet UIImageView *tickcrossimageview;
@property(nonatomic,retain)IBOutlet UITextView *textview2;

@property(nonatomic,retain)NSString *completeAddress;
@property double Latitude;
@property double Longitude;
@property(nonatomic,retain)NSDate *timeSelected;
@property(nonatomic,retain)NSDate *oldDate;
@property(nonatomic,retain)NSString *contentStr;
@property(assign)BOOL isEditMode;
@property(nonatomic,retain)NSString *emoticonNo;
@property(nonatomic,retain)ChooseDateViewController *chooseDateViewController;
@property(nonatomic,retain)DiaryEntryViewController *diaryEntryViewController;
@property(nonatomic,retain)ChooseEmoticonViewCtlr *emoticonViewCtlr;
@property (nonatomic, retain) IBOutlet UIButton *doneButton;
@property (nonatomic, retain) IBOutlet UIButton *cancelButton;
@property (nonatomic, retain) IBOutlet UIButton *clearButton;
-(IBAction)chooseTimePressed;
-(IBAction)chooseEmoticonPressed;
-(IBAction)donePressed;
-(IBAction)cancelPressed;
-(IBAction)clearPressed;
-(void)gpsPressed;
-(void)wantToUseGps;
-(BOOL)connectionToInternet;
-(UIButton *)buttonWithTitle:(NSString *)title target:(id)target selector:(SEL)selector frame:(CGRect)frame image:(UIImage *)image imagePressed:(UIImage *)imagePressed darkTextColor:(BOOL)darkTextColor;
-(IBAction)rotatePressed;

@end
