///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   ForgotPasswordViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "ForgotPasswordViewController.h"
#import "HomeViewController.h"
#import "PasswordViewController.h"
#import "Secure.h"
#import "eDiaryAppDelegate.h"
@implementation ForgotPasswordViewController
@synthesize homeViewController;
@synthesize passwordViewController;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	Secure *secure = [[Secure alloc] init];
	questionView.text = [secure getQuestion];
	[secure release];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

-(IBAction)showPassword{
	
	Secure *secure = [[[Secure alloc] init] autorelease];
//	////NSLog(@"answer start-%@-end",[secure getAnswer]);
	if([answerView.text isEqualToString:@""]){
		[eDiaryAppDelegate showMessage:@"Please enter an answer for the question" title:@"Answer Empty"];
		return;
	}
	if([answerView.text isEqualToString:[secure getAnswer]]){
		[answerView resignFirstResponder];
		[eDiaryAppDelegate showMessage:[NSString stringWithFormat:@"your password is %@",[secure getPassword]] title:@"Password"];
		HomeViewController *homeView = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
		self.homeViewController = homeView;
		[self.view addSubview:self.homeViewController.view];
		[homeView release];
	}else {
		[eDiaryAppDelegate showMessage:@"Your answer is not correct. Please try again" title:@"Wrong Answer"];
	}

}
-(IBAction)backPressed{
	PasswordViewController *passwordView = [[PasswordViewController alloc] initWithNibName:@"PasswordViewController" bundle:nil];
	self.passwordViewController = passwordView;
	[self.view addSubview:self.passwordViewController.view];
	[passwordView release];
}

- (void)dealloc {
    [super dealloc];
}


@end
