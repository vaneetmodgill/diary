//
//  VideoMemoViewController.m
//  Journal Diary
//
//  Created by Ashish Verma on 14/09/10.
//  Copyright 2010 Sulaba Inc. All rights reserved.
//

#import "VideoMemoViewController.h"
#import "HomeViewController.h"
#import "CalendarViewController.h"
#import "eDiaryAppDelegate.h"
#import "SaveMemoViewController.h"
#import "AddVideoMemo.h"
#import "Settings.h"
#import "DiaryObject.h"
#import "VoiceMemo.h"
#import "ShowVideoController.h"
@implementation VideoMemoViewController
@synthesize calendarViewController;
@synthesize homeViewController;
@synthesize saveMemoViewController;
@synthesize addVideoMemo;
@synthesize videoMemoDate,dateLabel,memoTable,imageView;
@synthesize showVideoController,iseditmode,date2,time2,istableviewclicked;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        
        
        backgroundImageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        backgroundImageView.frame = CGRectMake(0, -20, 320, 568+40);
        //   self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 568+20);
    }
    else
    {
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 480+20);
    }
    

	
	Settings *set=[[Settings alloc] init];
	[set loadSettings];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy-MM-dd"];
	//[formatter setTimeStyle:NSDateFormatterShortStyle];
	
	self.iseditmode=NO;
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit | NSMinuteCalendarUnit ;
	NSDateComponents *compsDate = [gregorian components:unitFlags fromDate:[formatter dateFromString:[NSString stringWithFormat:@"%@",set.globalDate]]];
	
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	NSDateComponents *compsTime = [gregorian components:unitFlags fromDate:[NSDate date]];
	[compsTime setDay:[compsDate day]];
	[compsTime setMonth:[compsDate month]];
	[compsTime setYear:[compsDate year]];
	self.videoMemoDate = [gregorian dateFromComponents:compsTime];
	[gregorian release];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterNoStyle];

	dateLabel.text = [formatter stringFromDate:self.videoMemoDate];
	[formatter setDateFormat:@"YYYY-MM-dd"];
	
	
	if(set.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
	}
	[set release];
	VoiceMemo *voiceMemo = [[VoiceMemo alloc] init];
	//NSMutableArray *mutArray = [[NSMutableArray alloc] init];
	// 	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterNoStyle];
	
	
	if(self.videoMemoDate !=nil){
		dateLabel.text = [formatter stringFromDate:self.videoMemoDate];
	}else {
		dateLabel.text = [formatter stringFromDate:[NSDate date]];
		self.videoMemoDate = [NSDate date];
	}
	[formatter setDateFormat:@"YYYY-MM-dd"];	
	NSMutableArray *memosArray1 = [voiceMemo selectvideowhere:[formatter stringFromDate:self.videoMemoDate]];
	//NSLog(@"array %@",array);
	/*for (NSDictionary *dictionary in array){
		DiaryObject *diaryObj = [[DiaryObject alloc] init];
		diaryObj.dateStr = [dictionary valueForKey:@"date"];
		diaryObj.timeStr = [dictionary valueForKey:@"time"];
		diaryObj.content = [dictionary valueForKey:@"title"];
		diaryObj.emoticon =[dictionary valueForKey:@"file_name"];
		[mutArray addObject:diaryObj];
		[diaryObj release];
	}*/
	memosArray = [memosArray1 mutableCopy];
	[memosArray1 release];
	[voiceMemo release];
	[memoTable reloadData];
	[formatter release];
	
	
	
	
	textfield = [[UITextField alloc] initWithFrame:CGRectMake(170, 273, 130, 32)];
	textfield.borderStyle = UITextBorderStyleRoundedRect;
	textfield.textColor = [UIColor blackColor];
	textfield.placeholder = @"Enter Zip Code";
	textfield.returnKeyType = UIReturnKeyDone;
	textfield.font = [UIFont boldSystemFontOfSize:17.0];
	//tf_country_code.returnKeyType = UIReturnKeyDefault;
	textfield.keyboardType = UIKeyboardTypeDefault;
	textfield.delegate = self;
	textfield.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	//[tf_country_code becomeFirstResponder];
	//[self.view addSubview:textfield];
	
    [super viewDidLoad];
	
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
	filename=textfield.text;
	[textfield resignFirstResponder];
	
	
	return YES;
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

-(IBAction)backPressed
{
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	HomeViewController *homeView = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
	self.homeViewController = homeView;
	[self.view addSubview:self.homeViewController.view];
	[homeView release];
	[UIView commitAnimations];
}


-(IBAction)calendarPressed
{
	CalendarViewController *calendarView = [[CalendarViewController alloc] initWithNibName:@"CalendarViewController" bundle:nil];
	calendarView.preView = @"VideoMemo";
	[eDiaryAppDelegate setCalendarName:@"VideoMemo"];
	self.calendarViewController = calendarView;
	[self.view addSubview:self.calendarViewController.view];
	[calendarView release];
}


-(IBAction)addPressed
{
	NSString *devicetype=[[UIDevice currentDevice] model];
	
	NSString *devicetypeversion=[[UIDevice currentDevice] systemVersion];
	NSLog(devicetypeversion);
	//NSLog(devicetype);
	//if([devicetype isEqualToString:@"iPhone"] )
	//{
	//	UIImagePickerController *ipc = [[UIImagePickerController alloc] init];
		
    
   // ipc.sourceType = UIImagePickerControllerSourceTypeCamera;

		
		NSArray *sourceTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
		if (![sourceTypes containsObject:(NSString *)kUTTypeMovie ])
		{
			UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Warning!!" message:@"Sorry..! Video Recording is not supported for this device.!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
			[alert show];
			[alert release];
			
		}
		else {
            

			self.istableviewclicked=NO;
			AddVideoMemo *homeView = [[AddVideoMemo alloc] initWithNibName:@"AddVideoMemo" bundle:nil];
			
			homeView.videoDate=self.videoMemoDate;
			homeView.iseditmode=self.iseditmode;
			homeView.istableviewclicked=self.istableviewclicked;
			
            
            CGRect screenBounds = [[UIScreen mainScreen] bounds];
            
            if (screenBounds.size.height == 568)
            {
                
                
                
                homeView.view.frame = CGRectMake(homeView.view.frame.origin.x, homeView.view.frame.origin.y, homeView.view.frame.size.width, 568);
            }
          
            
            
            self.addVideoMemo = homeView;
            
            
            
            
			
            NSLog(@"Size %f   %f",self.addVideoMemo.view.bounds.size.height,self.addVideoMemo.view.bounds.size.width);
            
            
			[self.view addSubview:self.addVideoMemo.view];
			[homeView release];
			
			
		}
	/*}
	else {
		UIAlertView *a=[[UIAlertView alloc] initWithTitle:@"Warning!!" message:@"Feature is not supported" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[a show];
		[a release];
	}*/
	
		 
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{	
	/*NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
	
	if ([mediaType isEqualToString:@"public.image"])
	{
		[picker dismissModalViewControllerAnimated:YES];

	}
	
	
	else if ([mediaType isEqualToString:@"public.movie"])
	{
		NSURL *aURL = [info objectForKey:UIImagePickerControllerMediaURL];
		NSLog(@"found a video :- %@",aURL);
		[picker dismissModalViewControllerAnimated:YES];
		
		
		//[webView1 loadRequest:[NSURLRequest requestWithURL:aURL]]; 
		//[webView1 reload];
		
		
		
		NSData *webData = [NSData dataWithContentsOfURL:aURL];
		NSString *filename = @"vidrec001.MOV";
		NSLog(@"\nfilename= %@\n",filename);
		BOOL success;
		
		NSFileManager *fileManager = [NSFileManager defaultManager];
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *documentPath = [documentsDirectory stringByAppendingPathComponent:filename];	
		NSLog(@"document path= %@\n",documentPath);

		
		success = [webData writeToFile:documentPath atomically:YES];
		if(success)
		{
			NSLog(@"oye!! video successfully saved on iphone");
		}
		
		NSURL *movieURL = [NSURL fileURLWithPath:documentPath]; 
		theMovie = [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
		theMovie.fullscreen=YES;
		theMovie.view.frame=CGRectMake(0, 0, 320, 300);
		//[theMovie setFrame:CGRectMake(0, 0, 320, 480)];
		//[theMovie prepareToPlay];
		[theMovie play];
		//[self presentModalViewController:theMovie animated:NO];
		[self.view addSubview:theMovie.view];
		
		//NSData *data =[NSData dataWithContentsOfURL:[NSURL URLWithString:documentPath]];// [[NSFileManager defaultManager] contentsAtPath:documentPath];
		
	//	[webView1 loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:documentPath]]];				//loadData:data MIMEType:@"video/mov" textEncodingName: baseURL:[NSURL fileURLWithPath:documentPath]];

		//CFShow([[NSFileManager defaultManager] directoryContentsAtPath:[NSHomeDirectory() stringByAppendingString:@"/Documents"]]);
		//[webView1 loadData:data MIMEType:@"video/mov" textEncodingName:nil baseURL:[NSURL fileURLWithPath:documentPath]];
		
		//[webView1 loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:documentPath isDirectory:NO]]];
							  // [webView1 reload];
	}

	*/
	
	/*
	else if ([mediaType isEqualToString:@"public.movie"])
	{

		//use (button of camera tabbar) pressed (similar to savePressed)
		[UIView beginAnimations:nil context:nil];
				[UIView setAnimationDuration:1];
				[UIView setAnimationBeginsFromCurrentState:YES];
				[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
				SaveMemoViewController *saveMemoView = [[SaveMemoViewController alloc] initWithNibName:@"SaveMemoViewController" bundle:nil];
				
				/*
				if(!retainRecording)
				{
					
					saveMemoView.tempFilePath = utils.soundFilePath;
					if(self.editMode)
					{
						saveMemoView.oldFileName = self.fileName; 
						saveMemoView.titleStr = self.titleStr;
					}
					saveMemoView.fileName = utils.fileName;
				}
				else
				{
					saveMemoView.tempFilePath = self.tempFilePath;
					saveMemoView.fileName = self.fileName;
				}
				
				saveMemoView.memoDate = self.memoDate;
				saveMemoView.editMode = self.editMode;
				self.saveMemoViewController = saveMemoView;
				 //
		
				[self.view addSubview:self.saveMemoViewController.view];
				[saveMemoView release];
		[UIView commitAnimations];
		[picker dismissModalViewControllerAnimated:YES];
	}
*/
}




- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [memosArray count];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setBool:NO forKey:@"autorotateenabled"];
	return (toInterfaceOrientation == UIInterfaceOrientationPortrait);	
}
- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	/*    if (cell == nil) {
	 cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
	 }
	 
	 // Set up the cell...
	 if(indexPath.section == 0) {
	 cell.text = [foodItems objectAtIndex:indexPath.row];
	 } else if(indexPath.section == 1) {
	 cell.text = [activityItems objectAtIndex:indexPath.row];
	 }*/
	
	
    if (cell == nil) {
        cell = [self getCellContentView:CellIdentifier];
    }
	
	NSData *data1=[NSData dataWithContentsOfURL:[NSURL fileURLWithPath:[[memosArray objectAtIndex:indexPath.row] fileName]]]; 
	
	float data_length=[data1 length]/(1024.0f*1024.0f);
	UILabel *timeLbl =  (UILabel *)[cell viewWithTag:1];
	UILabel *contentLbl =  (UILabel *)[cell viewWithTag:2];
	
	
	//DiaryObject *obj = (DiaryObject *)[memosArray objectAtIndex:indexPath.row];
	timeLbl.text = [[memosArray objectAtIndex:indexPath.row] timeStr];
	contentLbl.text = [NSString stringWithFormat:@"%@     %0.2fMB",[[memosArray objectAtIndex:indexPath.row] video_title],data_length];
	//VoiceMemo *vm=[[VoiceMemo alloc] init];
	
	//timeLbl.text=[[memosArray objectAtIndex:indexPath.row] timeStr];
	//contentLbl.text=[[memosArray objectAtIndex:indexPath.row] video_title];
	/*	
	 if(indexPath.row%2) {
	 [lblTemp1 setBackgroundColor:[UIColor colorWithRed:0.219 green:0.586 blue:0.590 alpha:1.0]];
	 [lblTemp2 setBackgroundColor:[UIColor colorWithRed:0.219 green:0.586 blue:0.590 alpha:1.0]];
	 } else {
	 [lblTemp1 setBackgroundColor:[UIColor colorWithRed:0.391 green:0.871 blue:0.852 alpha:1.0]];
	 [lblTemp2 setBackgroundColor:[UIColor colorWithRed:0.391 green:0.871 blue:0.852 alpha:1.0]];
	 }
	 */
    return cell;
}
- (UITableViewCell *) getCellContentView:(NSString *)cellIdentifier {
	
	CGRect CellFrame = CGRectMake(0.0f, 0.0f, 320.0f, 50.0f);
	CGRect lblFrame1 = CGRectMake(15.0f, 0.0f, 305.0f, 25.0f);
	CGRect lblFrame2 = CGRectMake(15.0f, 25.0f, 300.0f, 25.0f);
	
	UILabel *lblTemp1;
	UILabel *lblTemp2;
	
	UITableViewCell *cell = [[[UITableViewCell alloc] initWithFrame:CellFrame reuseIdentifier:cellIdentifier] autorelease];
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	
	//Initialize Label 1 with tag 1.
	lblTemp1 = [[UILabel alloc] initWithFrame:lblFrame1];
	[lblTemp1 setBackgroundColor:[UIColor clearColor]];
	//	[lblTemp1 sizeToFit];
	//	[lblTemp1 setMinimumFontSize:8.0];
	if([settings.fontName isEqualToString:@""] || settings.fontName == nil){
		[lblTemp1 setFont:[UIFont systemFontOfSize:15]];
		[settings storeFontSettings:@"" size:15.0];
	}else{
		//NSLog(@"font name %@",settings.fontName);	
		NSArray *fontNames = [UIFont fontNamesForFamilyName:settings.fontName];
		[lblTemp1 setFont:[UIFont fontWithName:[fontNames objectAtIndex:0] size:settings.fontSize]];
		//	[lblTemp2 setFont:[UIFont systemFontOfSize:15]];
	}
	lblTemp1.tag = 1;
	[cell.contentView addSubview:lblTemp1];
	[lblTemp1 release];
	
	//Initialize Label 2 with tag 2.
	lblTemp2 = [[UILabel alloc] initWithFrame:lblFrame2];
	[lblTemp2 setBackgroundColor:[UIColor clearColor]];
	lblTemp2.tag = 2;
	lblTemp2.numberOfLines = 1;
	//	[lblTemp2 sizeToFit];
	//	[lblTemp2 setMinimumFontSize:8.0];
	if([settings.fontName isEqualToString:@""] || settings.fontName == nil){
		[lblTemp2 setFont:[UIFont systemFontOfSize:20]];
		[settings storeFontSettings:@"" size:20.0];
	}else{
		////NSLog(@"font name2 %@",settings.fontName);	
		NSArray *fontNames = [UIFont fontNamesForFamilyName:settings.fontName];
		[lblTemp2 setFont:[UIFont fontWithName:[fontNames objectAtIndex:0] size:settings.fontSize]];
		//	[lblTemp2 setFont:[UIFont systemFontOfSize:15]];
	}	[cell.contentView addSubview:lblTemp2];
	[lblTemp2 release];
	[settings release];
	return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	return 50.0f;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	self.istableviewclicked=YES;
	ShowVideoController *showMemoView = [[ShowVideoController alloc] initWithNibName:@"ShowVideoController" bundle:nil];
	showMemoView.filepath=[[memosArray objectAtIndex:indexPath.row] fileName];
	showMemoView.istableviewclicked=self.istableviewclicked;
	showMemoView.olddate=[[memosArray objectAtIndex:indexPath.row] dateStr];
	showMemoView.video_title=[[memosArray objectAtIndex:indexPath.row] video_title];
		showMemoView.oldtime=[[memosArray objectAtIndex:indexPath.row] timeStr];
	//DiaryObject *obj = (DiaryObject*)[memosArray objectAtIndex:indexPath.row];
	//showMemoView.diaryObj = obj;
	self.showVideoController = showMemoView;
	[self.view addSubview:self.showVideoController.view];
	[showMemoView release];
}
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(IBAction)leftPressed
{
	[memosArray removeAllObjects];
	VoiceMemo *obj=[[VoiceMemo alloc] init];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterNoStyle];
	
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit | NSMinuteCalendarUnit;
	NSDateComponents *comps = [gregorian components:unitFlags fromDate:self.videoMemoDate];
	
	NSDateComponents *compsTime = [gregorian components:unitFlags fromDate:[NSDate date]];
	[compsTime setDay:[comps day]-1];
	[compsTime setMonth:[comps month]];
	[compsTime setYear:[comps year]];
	self.videoMemoDate = [gregorian dateFromComponents:compsTime];
	NSLog(@"date %@",self.videoMemoDate);
	[gregorian release];
	dateLabel.text = [formatter stringFromDate:self.videoMemoDate];
	[formatter setDateFormat:@"YYYY-MM-dd"];
	//NSLog(dateLabel.text);
	
	memosArray=[obj selectvideowhere:[formatter stringFromDate:self.videoMemoDate]]; 
	
	
	[memoTable reloadData];
	Settings *settings = [[Settings alloc] init];
	
	[settings storeGlobalDate:[formatter stringFromDate:self.videoMemoDate]];
	[settings loadSettings];
	[settings release];
	[formatter release];
}

-(IBAction)rightPressed
{
	[memosArray removeAllObjects];
	VoiceMemo *obj=[[VoiceMemo alloc] init];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterNoStyle];
	
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit | NSMinuteCalendarUnit;
	NSDateComponents *comps = [gregorian components:unitFlags fromDate:self.videoMemoDate];
	
	NSDateComponents *compsTime = [gregorian components:unitFlags fromDate:[NSDate date]];
	[compsTime setDay:[comps day]+1];
	[compsTime setMonth:[comps month]];
	[compsTime setYear:[comps year]];
	self.videoMemoDate = [gregorian dateFromComponents:compsTime];
	NSLog(@"date %@",self.videoMemoDate);
	[gregorian release];
	dateLabel.text = [formatter stringFromDate:self.videoMemoDate];
	[formatter setDateFormat:@"YYYY-MM-dd"];
	
	memosArray=[obj selectvideowhere:[formatter stringFromDate:self.videoMemoDate]]; 
	
	
	
	[memoTable reloadData];
	Settings *settings = [[Settings alloc] init];
	
	[settings storeGlobalDate:[formatter stringFromDate:self.videoMemoDate]];
	[settings loadSettings];
	[settings release];
	[formatter release];
}
- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
