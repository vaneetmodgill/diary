///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   VoiceMemo.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "VoiceMemo.h"
#import "DBSqlite.h"
#import "Sqlite.h"

@implementation VoiceMemo
@synthesize dateStr;
@synthesize timeStr;
@synthesize title;
@synthesize fileName;
@synthesize video_title;
static sqlite3_stmt *insert_statement = nil;
static sqlite3_stmt *search_cases_statement=nil;
-(void)insertVideo:(NSString*)par_date time:(NSString*)par_time title:(NSString*)par_title filename:(NSString*)par_filename
{
	//Sqlite *sqlite;
	//if (![dbSqlite getDBConnection])
	//	return NO;
	//sqlite3 *database=sqlite._db;
	//sqlite = dbSqlite.sqlite;
	//sqlite3 *database=sqlite._db;
	sqlite3 *database;
	
	insert_statement=nil;
	self.dateStr =[[NSString alloc] initWithFormat:@"%@",par_date];
	self.timeStr =[[NSString alloc]initWithFormat:@"%@",par_time];
	self.video_title =[[NSString alloc]initWithFormat:@"%@",par_title];
	self.fileName=[[NSString alloc]initWithFormat:@"%@",par_filename];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"eDiary.sqlite"];
	
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
		//self.notes =[[NSString alloc] initWithFormat:@"%@",n1];
		if (insert_statement == nil) {
			// Note the '?' at the end of the query. This is a parameter which can be replaced by a bound variable.
			// This is a great way to optimize because frequently used queries can be compiled once, then with each
			// use new variable values can be bound to placeholders.
			//UIImage *img_local=[UIImage imageNamed:@"clock.png"];
			//	NSData *imgData = UIImagePNGRepresentation(par_picture);
			
			//		,todo_item , Notes_rem ,nqty1 , nqty2 ,pqty1 ,pqty2 , strt_date ,end_date ,rep , alpref , team_person , store_name
			const char *sql_query = "INSERT INTO video_table(date ,time ,title , file_name ) values (?,?,?,?)";
			//"INSERT INTO tb_SubCategory(subcategory_name,picture,notes,majorcategory_name,days,hours,minutes) values (?,?,?,?,?,?,?)";
			
			if (sqlite3_prepare_v2(database,sql_query, -1, &insert_statement, NULL) == SQLITE_OK)
			{
				
				sqlite3_bind_text(insert_statement, 1, [self.dateStr UTF8String],-1,SQLITE_TRANSIENT);
				
				sqlite3_bind_text(insert_statement, 2, [self.timeStr UTF8String],-1,SQLITE_TRANSIENT);
				
				sqlite3_bind_text(insert_statement, 3, [self.video_title UTF8String],-1,SQLITE_TRANSIENT);
				
				sqlite3_bind_text(insert_statement, 4, [self.fileName UTF8String], -1, SQLITE_TRANSIENT);
				
				
				int x=	sqlite3_step(insert_statement);
				NSLog(@"%d",x);
			}
			
		}
	}
	sqlite3_close(database);
	sqlite3_reset(insert_statement);
	sqlite3_finalize(insert_statement);
	
}

-(void)updatevideo:(NSString*)par_date time:(NSString*)par_time title:(NSString*)par_title filename:(NSString*)par_filename
{
	//Sqlite *sqlite;
	//if (![dbSqlite getDBConnection])
	//	return NO;
	//sqlite3 *database=sqlite._db;
	//sqlite = dbSqlite.sqlite;
	//sqlite3 *database=sqlite._db;
	sqlite3 *database;
	
	insert_statement=nil;
	self.dateStr =[[NSString alloc] initWithFormat:@"%@",par_date];
	self.timeStr =[[NSString alloc]initWithFormat:@"%@",par_time];
	self.video_title =[[NSString alloc]initWithFormat:@"%@",par_title];
	self.fileName=[[NSString alloc]initWithFormat:@"%@",par_filename];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"eDiary.sqlite"];
	
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
		//self.notes =[[NSString alloc] initWithFormat:@"%@",n1];
		if (insert_statement == nil) {
			// Note the '?' at the end of the query. This is a parameter which can be replaced by a bound variable.
			// This is a great way to optimize because frequently used queries can be compiled once, then with each
			// use new variable values can be bound to placeholders.
			//UIImage *img_local=[UIImage imageNamed:@"clock.png"];
			//	NSData *imgData = UIImagePNGRepresentation(par_picture);
			
			//		,todo_item , Notes_rem ,nqty1 , nqty2 ,pqty1 ,pqty2 , strt_date ,end_date ,rep , alpref , team_person , store_name
			const char *sql_query = "UPDATE video_table SET file_name = ? , file_name = ? WHERE date = ? and time = ?";
			//"INSERT INTO tb_SubCategory(subcategory_name,picture,notes,majorcategory_name,days,hours,minutes) values (?,?,?,?,?,?,?)";
			
			if (sqlite3_prepare_v2(database,sql_query, -1, &insert_statement, NULL) == SQLITE_OK)
			{
				
				sqlite3_bind_text(insert_statement, 1, [self.video_title UTF8String],-1,SQLITE_TRANSIENT);
				
				sqlite3_bind_text(insert_statement, 2, [self.fileName UTF8String],-1,SQLITE_TRANSIENT);
				
				sqlite3_bind_text(insert_statement, 3, [self.dateStr UTF8String],-1,SQLITE_TRANSIENT);
				
				sqlite3_bind_text(insert_statement, 4, [self.timeStr UTF8String], -1, SQLITE_TRANSIENT);
				
				
				int x=	sqlite3_step(insert_statement);
				NSLog(@"%d",x);
			}
			
		}
	}
	sqlite3_close(database);
	sqlite3_reset(insert_statement);
	sqlite3_finalize(insert_statement);
	
}
-(BOOL)deleteVideo:(NSString*)par_file
{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;
	BOOL result =[sqlite executeNonQuery:@"DELETE from video_table where file_name = ?;",par_file];
	[dbSqlite release];
	return result;
	
	
}

-(NSMutableArray*)selectvideowhere:(NSString*)par_date
{
	NSMutableArray *tempArray;
	tempArray =[[NSMutableArray alloc] init];
	/*DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	 Sqlite *sqlite;
	 if (![dbSqlite getDBConnection])
	 return NO;
	 sqlite = dbSqlite.sqlite;*/
	sqlite3 *database;
	
	char *sql_query = "Select * from video_table where date = ? ";
	search_cases_statement=nil;
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"eDiary.sqlite"];
	
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
		if(search_cases_statement ==nil)
		{
			
			if (sqlite3_prepare_v2(database, sql_query, -1, &search_cases_statement, NULL) == SQLITE_OK)
			{
				sqlite3_bind_text(search_cases_statement,1, [par_date UTF8String],-1,SQLITE_TRANSIENT);
				
				while (sqlite3_step(search_cases_statement) == SQLITE_ROW)
				{
					
					
					
					VoiceMemo *obj_savescore_local = [[VoiceMemo alloc] init];
					//[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,14)];
					obj_savescore_local.dateStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,0)];
					obj_savescore_local.timeStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,1)];

					obj_savescore_local.video_title=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,2)];
					obj_savescore_local.fileName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,3)];

				
					
					[tempArray addObject:obj_savescore_local];
					
				}
			}
		}
	}
	sqlite3_close(database);
	sqlite3_reset(search_cases_statement);
	sqlite3_finalize(search_cases_statement);
	return tempArray;
	
}
-(NSMutableArray*)selectAllVideos
{
	NSMutableArray *tempArray;
	tempArray =[[NSMutableArray alloc] init];
	/*DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	 Sqlite *sqlite;
	 if (![dbSqlite getDBConnection])
	 return NO;
	 sqlite = dbSqlite.sqlite;*/
	sqlite3 *database;
	
	char *sql_query = "Select * from video_table";
	search_cases_statement=nil;
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"eDiary.sqlite"];
	
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK) 
	{
		if(search_cases_statement ==nil)
		{
			
			if (sqlite3_prepare_v2(database, sql_query, -1, &search_cases_statement, NULL) == SQLITE_OK)
			{
				
				//sqlite3_bind_text(search_cases_statement,3, [par_notes UTF8String],-1,SQLITE_TRANSIENT);
				
				while (sqlite3_step(search_cases_statement) == SQLITE_ROW)
				{
					
					
					
					VoiceMemo *obj_savescore_local = [[VoiceMemo alloc] init];
					//[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,14)];
					obj_savescore_local.dateStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,0)];
					obj_savescore_local.timeStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,1)];
					obj_savescore_local.video_title=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,2)];

					obj_savescore_local.fileName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,3)];

					/*NSData *data = [[NSData alloc] initWithBytes:sqlite3_column_blob(search_cases_statement,2) length:sqlite3_column_bytes(search_cases_statement,2)];
					 
					 if(data == nil)
					 NSLog(@"No image found.");
					 else
					 {
					 
					 obj_savescore_local.background_image = [UIImage imageWithData:data];
					 [data release];
					 }*/
					
					[tempArray addObject:obj_savescore_local];
					//	obj_savescore_local.dateStr=[NSString stringWithUTF8String:(char *)sqlite3_column_text(search_cases_statement,14)];
					//	obj_savescore_local.timeStr = [NSString stringWithUTF8String:(char*)sqlite3_column_text(search_cases_statement, 15)];
					
				}
			}
		}
	}
	sqlite3_close(database);
	sqlite3_reset(search_cases_statement);
	sqlite3_finalize(search_cases_statement);
	return tempArray;
	
}
-(BOOL)insert{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;	
	BOOL result =[sqlite executeNonQuery:@"INSERT INTO voice_memo(date,time,title,file_name) VALUES (?, ?, ?, ?);",self.dateStr,self.timeStr,self.title,self.fileName];
	
	[dbSqlite release];	
	return result;
}
-(BOOL)updateMemo:(NSString *)fName{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;
	BOOL result = [sqlite executeNonQuery:@"UPDATE voice_memo set date =? , time =? , title = ? , file_name = ? where file_name = ?;",self.dateStr,self.timeStr,self.title,self.fileName,fName];
	[dbSqlite release];
	return result;
}
-(NSArray*)getData:(NSString *)memoDate{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select * from voice_memo where date = ?;",memoDate];	
	return results;
}
-(BOOL)deleteMemo:(NSString *)memoName{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return NO;
	sqlite = dbSqlite.sqlite;
	BOOL result =[sqlite executeNonQuery:@"DELETE from voice_memo where file_name = ?;",memoName];
	[dbSqlite release];
	return result;
}
-(NSArray*)getDatesInMonthVideo:(NSString *)fromDate	to:(NSString *)toDate{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select date from video_table where date between ? and ?;",fromDate,toDate];	
	return results;	
}
-(NSArray*)getDatesInMonth:(NSString *)fromDate	to:(NSString *)toDate{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select date from voice_memo where date between ? and ?;",fromDate,toDate];	
	return results;	
}
@end
