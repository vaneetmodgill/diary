///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   SettingsFontViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "SettingsFontViewController.h"
#import "SettingsViewController.h"
#import "Settings.h"
#import "eDiaryAppDelegate.h"
@implementation SettingsFontViewController
@synthesize settingsViewController;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        
        
        backgroundImageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        backgroundImageView.frame = CGRectMake(0, -20, 320, 568+40);
        
    }
    else
    {
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 480+20);
    }
    
    
	fontsArray = [[NSMutableArray alloc] init]; 
	fontsArray = [[UIFont familyNames] mutableCopy];
	for(int i = 0; i < [fontsArray count] ; i++) {
		for(int j = i+1; j < [fontsArray count]; j++) {
			
			NSString *str1 =  [fontsArray objectAtIndex:i];
			NSString *str2 =  [fontsArray objectAtIndex:j];
		//	//NSLog(@"str1 %@",str1);
		//	//NSLog(@"str2 %@",str2);
		//	//NSLog(@" compare %d",[str1 compare:str2]);
		//	Award *award2 = (Award *) [awardList objectAtIndex:(j-1)];
			if([str1 compare:str2]== 1) {
				[fontsArray exchangeObjectAtIndex:i withObjectAtIndex:j];
			}
			
		//	//NSLog(@"Sorting  : %@",award1.sponsor);
			
		}
	}
	//	//NSLog(@"sorted %@",fontsArray);
	
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
//	//NSLog(@"font size %f",settings.fontSize);
	[fontsPicker selectRow:[fontsArray indexOfObject:settings.fontName] inComponent:0 animated:NO];
	[sizeSlider setValue:settings.fontSize animated:NO];
	
	[fontSizeLbl setText:[NSString stringWithFormat:@"%d",[[NSNumber numberWithFloat:sizeSlider.value] intValue]]];
	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
-(IBAction)donePressed{
	Settings *settings = [[Settings alloc] init];
	////NSLog(@"back size %f",sizeSlider.value);
	[settings storeFontSettings:[fontsArray objectAtIndex:[fontsPicker selectedRowInComponent:0]] size:sizeSlider.value];
	[settings release];
	SettingsViewController *settingsView = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController"bundle:nil];
	self.settingsViewController = settingsView;
	[self.view addSubview:self.settingsViewController.view];
	[settingsView release];
}
-(IBAction)cancelPressed{
	SettingsViewController *settingsView = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController"bundle:nil];
	self.settingsViewController = settingsView;
	[self.view addSubview:self.settingsViewController.view];
	[settingsView release];
}
-(IBAction)sliderPressed{
	[fontSizeLbl setText:[NSString stringWithFormat:@"%d",[[NSNumber numberWithFloat:sizeSlider.value] intValue]]];
}
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
	return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
	
	return [fontsArray count]; 
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
	return (NSString*)[fontsArray objectAtIndex:row];
}

- (void)dealloc {
    [super dealloc];
}


@end
