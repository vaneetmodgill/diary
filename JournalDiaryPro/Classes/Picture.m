///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   Picture.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "Picture.h"
#import "DBSqlite.h"
#import "Sqlite.h"

@implementation Picture
-(void)insert:(NSString*)picName{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return;
	sqlite = dbSqlite.sqlite;	
	[sqlite executeNonQuery:@"INSERT INTO picture(name) VALUES (?);",picName];
	
	[dbSqlite release];
}
-(NSArray *)getData{
	DBSqlite *dbSqlite = [[[DBSqlite alloc] init] autorelease];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return [NSArray array];
	sqlite = dbSqlite.sqlite;	
	
	NSArray *results = [sqlite executeQuery:@"Select name from picture;"];	
	return results;	
}
-(void)deletePicture:(NSString*)picName{
	DBSqlite *dbSqlite = [[DBSqlite alloc] init];
	Sqlite *sqlite;
	
	if (![dbSqlite getDBConnection])
		return;
	sqlite = dbSqlite.sqlite;
	[sqlite executeNonQuery:@"DELETE from picture where name = ?;",picName];
	[dbSqlite release];
	
}
@end
