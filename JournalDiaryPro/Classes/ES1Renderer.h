//
//  ES1Renderer.h
//  CubeExample
//
//  Created by Brad Larson on 4/20/2010.
//

#import "ESRenderer.h"
#import "Texture2D.h"
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import <QuartzCore/QuartzCore.h>

@class PVRTexture;

@interface ES1Renderer : NSObject <ESRenderer>
{
@private
    EAGLContext *context;

	PVRTexture *pvrTexture;

    // The pixel dimensions of the CAEAGLLayer
    GLint backingWidth;
    GLint backingHeight;

    // The OpenGL ES names for the framebuffer and renderbuffer used to render to this view
    GLuint defaultFramebuffer, colorRenderbuffer;
	Texture2D *tex_h;
	CATransform3D currentCalculatedMatrix;
}
-(void)drawfakelabel:(int)x_par pary:(int)y_par tex_name:(GLuint)par_no angle:(float)par_angle;
- (void)renderByRotatingAroundX:(float)xRotation rotatingAroundY:(float)yRotation;
- (BOOL)resizeFromLayer:(CAEAGLLayer *)layer;
- (void)configureLighting;
- (void)convert3DTransform:(CATransform3D *)transform3D toMatrix:(GLfloat *)matrix;

@end
