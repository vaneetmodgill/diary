///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   CalendarViewController.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <UIKit/UIKit.h>
#import "TKCalendarView.h"
#import "VideoMemoViewController.h"
@class VideoMemoViewController;
@class TKCalendarView;
@class EventsViewController;
@class DiaryEntryViewController;
@class SettingsExportViewController;
@class VoiceMemoViewController;
@interface CalendarViewController : UIViewController <TKCalendarViewDelegate>{
	IBOutlet UIImageView *imageView;
	TKCalendarView *calendarView;
	NSCalendar *calendar;
	NSDate *selectedDate;
	NSDate *anotherDate;
	NSString *preView;
	VideoMemoViewController *videoMemoViewController;
	VoiceMemoViewController *voiceMemoViewController;
	EventsViewController *eventsViewController;
	SettingsExportViewController *fromViewCtlr;
	SettingsExportViewController *toViewCtlr;
	DiaryEntryViewController *diaryEntryViewController;
    
    IBOutlet UIImageView *backgrndImaView;
}
@property(retain,nonatomic)	VideoMemoViewController *videoMemoViewController;
@property (retain,nonatomic) TKCalendarView *calendarView;
@property (retain,nonatomic)VoiceMemoViewController *voiceMemoViewController;
@property (retain,nonatomic)EventsViewController *eventsViewController;
@property (retain,nonatomic)SettingsExportViewController *fromViewCtlr;
@property (retain,nonatomic)SettingsExportViewController *toViewCtlr;
@property (retain,nonatomic)DiaryEntryViewController *diaryEntryViewController;

@property(nonatomic,retain) NSDate *selectedDate;
@property(nonatomic,retain)NSDate *anotherDate;
@property(nonatomic,retain)NSString *preView;
-(IBAction)donePressed;
@end
