//
//  ConnectFacebookViewController.m
//  AtomicChalk
//
//  Created by administrator on 25/08/09.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ConnectFacebookViewController.h"
#import "FBDialog.h"
#import "FBConnect.h"
//#import "Databasemanager.h"
///////////////////////////////////////////////////////////////////////////////////////////////////
// your Facebook application's API key here:
#define kfaceBookApiKey					@"d19f7a2339f47ed58e2b4df6d9ef1dae"

// Enter either your API secret or a callback URL (as described in documentation):
#define kfaceBookApiSecret				@"a5e8a78f2a7f4b812a3862e080170183"

///////////////////////////////////////////////////////////////////////////////////////////////////
#define DOCSFOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/MyPicture"]


@implementation ConnectFacebookViewController
@synthesize _label,image_name,cont,imageupload;

static NSString* kfaceBookGetSessionProxy = nil; // @"<YOUR SESSION CALLBACK)>";


NSMutableArray *imageDetails;


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
		
		
		
		
    }
    return self;
}

-(UIButton *)buttonWithTitle:(NSString *)title target:(id)target selector:(SEL)selector frame:(CGRect)frame image:(UIImage *)image imagePressed:(UIImage *)imagePressed darkTextColor:(BOOL)darkTextColor
{	
	UIButton *button = [[UIButton  alloc] initWithFrame:frame];
	//button.frame=frame;
	
	button.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
	
	[button setTitle:title forState:UIControlStateNormal];	
	if (darkTextColor)
	{
		[button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	}
	else
	{
		[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	}
	
	UIImage *newImage = [image stretchableImageWithLeftCapWidth:12.0 topCapHeight:0.0];
	
	[button setBackgroundImage:newImage forState:UIControlStateNormal];
	
	
	UIImage *newPressedImage = [imagePressed stretchableImageWithLeftCapWidth:12.0 topCapHeight:0.0];
	[button setBackgroundImage:newPressedImage forState:UIControlStateHighlighted];
	
	[button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
	
	// in case the parent view draws with a custom color or gradient, use a transparent color
	button.backgroundColor = [UIColor clearColor];
	
	return button;
}
-(void)backfun
{
	[self.view removeFromSuperview];	
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
	//if(_session.isConnected)
	//[_session logout];
	
	if (kfaceBookGetSessionProxy) {
		_session = [[FBSession sessionForApplication:kfaceBookApiKey getSessionProxy:kfaceBookGetSessionProxy
											delegate:self] retain];
	} else {
		_session = [[FBSession sessionForApplication:kfaceBookApiKey secret:kfaceBookApiSecret delegate:self] retain];
	}
	
	self.view.backgroundColor=[UIColor grayColor];
	
	UIImageView *imagev=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg6.png"]]; 
	imagev.frame=CGRectMake(0, -20, 320, 480);
	[self.view addSubview:imagev];
	
	
	UIButton *bt=[self buttonWithTitle:@"" target:self selector:@selector(backfun) frame:CGRectMake(0, 0, 63, 38) image:nil imagePressed:nil darkTextColor:NO];
	[bt setImage:[UIImage imageNamed:@"bck.png"] forState:UIControlStateNormal];
	[self.view addSubview:bt];
	
	
	//blackColor];
	
	LogoutImageImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
	
	LogoutImageImg.image=[UIImage imageNamed:@"logout.png"];
	LogoutImageImg.hidden=YES;
	//[self.view addSubview:LogoutImageImg];
	
	ConnectoFacebookImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
	
	ConnectoFacebookImg.image=[UIImage imageNamed:@"login.png"]; //@"ConnectoFacebook1.png"];
	//[self.view addSubview:ConnectoFacebookImg];
	
	
	
	
	
	facebookLoginButton.style = FBLoginButtonStyleWide;
	
	_label=[[UILabel alloc]initWithFrame:CGRectMake(0, 50, 320, 30)];
	_label.lineBreakMode=UILineBreakModeWordWrap;
	_label.autoresizingMask = (UIViewAutoresizingFlexibleWidth || UIViewAutoresizingFlexibleHeight);
	_label.text=@"";
	_label.textColor=[UIColor whiteColor];
	_label.textAlignment = UITextAlignmentCenter; 
	_label.backgroundColor=[UIColor clearColor];
	_label.font=[UIFont boldSystemFontOfSize:15];
	_label.numberOfLines=1;
	[self.view addSubview:_label];
	
	
	timeStampButton=[UIButton buttonWithType:UIButtonTypeCustom];
	timeStampButton.frame=CGRectMake(30, 120, 260, 135);
	timeStampButton.backgroundColor=[UIColor clearColor];
	//UIImage *buttonBackground = [UIImage imageNamed:@"cell3.png"];
	//[timeStampButton setBackgroundImage:buttonBackground forState:UIControlStateNormal];
	[self.view addSubview:timeStampButton];
	[timeStampButton addTarget:self action:@selector(TimeClicked:) forControlEvents:UIControlEventTouchDown];
	
	
	logoutButton=[UIButton buttonWithType:UIButtonTypeCustom];
	logoutButton.frame=CGRectMake(5, 110, 150, 40);
	logoutButton.backgroundColor=[UIColor clearColor];
	logoutButton.hidden=YES;
	//UIImage *buttonBackground1 = [UIImage imageNamed:@"cell3.png"];
	//[logoutButton setBackgroundImage:buttonBackground1 forState:UIControlStateNormal];
	
	[self.view addSubview:logoutButton];
	[logoutButton addTarget:self action:@selector(logoutButtonClicked:) forControlEvents:UIControlEventTouchDown];
	
	
	uploadButton=[[UIButton buttonWithType:UIButtonTypeRoundedRect]retain];
	uploadButton.frame=CGRectMake(110, 310, 150, 40);
	//uploadButton.backgroundColor=[UIColor whiteColor];
	uploadButton.hidden=YES;
	[uploadButton setTitle:@"post photo" forState:UIControlStateNormal];
	[uploadButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	//UIImage *buttonBackground11 = [UIImage imageNamed:@"UploadtoFacebook.png"];
	//[uploadButton setImage:buttonBackground11 forState:UIControlStateNormal];
	
	[self.view addSubview:uploadButton];
	[uploadButton addTarget:self action:@selector(uploadButtonClicked:) forControlEvents:UIControlEventTouchDown];
	
	uploadButton1=[[UIButton buttonWithType:UIButtonTypeRoundedRect]retain];
	uploadButton1.frame=CGRectMake(110, 230, 150, 40);
	//uploadButton1.backgroundColor=[UIColor whiteColor];
	uploadButton1.hidden=YES;
	
	[uploadButton1 setTitle:@"post Feed" forState:UIControlStateNormal];
	[uploadButton1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

	
	[self.view addSubview:uploadButton1];
	[uploadButton1 addTarget:self action:@selector(uploadButtonClicked1:) forControlEvents:UIControlEventTouchDown];
	
	
	LoginButton=[[FBLoginButton alloc]init];
    [self.view addSubview:LoginButton];

	/*---
	 
	 connect_iphone_btn = [UIButton buttonWithType:UIButtonTypeCustom];
	 [connect_iphone_btn setImage:[UIImage imageNamed:@"Connect_iphone.png"] forState:UIControlStateNormal];
	 [connect_iphone_btn setImage:[UIImage imageNamed:@"Logout_iphone.png"] forState:UIControlStateHighlighted];
	 [connect_iphone_btn addTarget:self action:@selector(OnFBConnect:) forControlEvents:UIControlEventTouchUpInside];
	 connect_iphone_btn.frame = CGRectMake(10,50,80,30);
	 [self.view addSubview:connect_iphone_btn];	
	 //---
	 */
	if(_session.isConnected)
	{
		uploadButton1.hidden=NO;
				uploadButton.hidden=NO;
	}
	
	HomeClickedButton=[[UIButton buttonWithType:UIButtonTypeCustom]retain];
	[HomeClickedButton setFrame:CGRectMake(245, 400, 60, 40)];
	HomeClickedButton.backgroundColor=[UIColor clearColor];
	UIImage *buttonBackground = [UIImage imageNamed:@"homebutton.png"];
	[HomeClickedButton setImage:buttonBackground forState:UIControlStateNormal];
	
	[HomeClickedButton addTarget:self action:@selector(HomeClicked:) forControlEvents:UIControlEventTouchDown];
	[self.view addSubview:HomeClickedButton];
	
	
	//Databasemanager *IManager=[Databasemanager alloc];
	//imageDetails=[IManager GetBackgroundImage];
	//[IManager release];
	
	LoginButton.frame = CGRectMake(30, 120, 260, 145);
	
    [super viewDidLoad];
}
-(void)uploadButtonClicked1:(id)sender
{
	NSMutableDictionary *FeedStoryParams = [[[NSMutableDictionary alloc] init] autorelease];
	[FeedStoryParams setObject:@"1.0" forKey:@"v"];
	NSString *st=[NSString stringWithFormat:@"%@",self.cont];
	[FeedStoryParams setObject:st forKey:@"message"];
	
	//NSDictionary* params = [NSDictionary dictionaryWithObject:self.cont forKey:@"message"];

	[[FBRequest requestWithDelegate:self] call:@"facebook.stream.publish" params:FeedStoryParams];
	
	
	
	UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Feed Published" message:@"" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
	[alert show];
	[alert release];
}

-(void)HomeClicked:(id)sender
{
	[self dismissModalViewControllerAnimated:NO];
}


-(void)logoutButtonClicked:(id)sender
{
	//[_session logout];
	
}

-(void)uploadButtonClicked:(id)sender
{
	
	//int ImageCounter;
	if(_session.isConnected)
	{
		
			
			
			UIImage *Fimage=self.imageupload;
		
		if(Fimage != nil)
		{
			
			NSDictionary* params = [NSDictionary dictionaryWithObject:Fimage forKey:@"image"];
		
			[[FBRequest requestWithDelegate:self] call:@"photos.upload" params:params];
	
	
		
			
			UIAlertView *baseAlert = [[UIAlertView alloc] initWithTitle:@"Photo Uploaded Successfully." message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
			[baseAlert show];
			
		}
		else {
			UIAlertView *baseAlert = [[UIAlertView alloc] initWithTitle:@"No Photo Chosen" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
			[baseAlert show];
		}

		
		
		
	}
	
}

-(void)TimeClicked:(id)sender
{
	/*
	 dialog = [[FBLoginDialog alloc] initWithSession:_session];
	 dialog.title = @"Connect to Facebook";
	 [dialog show];
	 */
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// FBDialogDelegate

- (void)dialog:(FBDialog*)dialog didFailWithError:(NSError*)error {
	_label.text = [NSString stringWithFormat:@"Error(%d) %@", error.code,
				   error.localizedDescription];
}

- (void)dialogDidSucceed:(FBDialog*)dialog { 
	timeStampButton.hidden = YES;
	ConnectoFacebookImg.hidden=YES;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// FBSessionDelegate
-(void)statusWasSet:(BOOL)status
{
    //Instantiate the PermissionStatus class with the user id.
   /* if ( !permissionStatusForUser.userHasPermission )
    {
		FBPermissionDialog* dialog1 = [[[FBPermissionDialog alloc] init] autorelease];
		dialog1.delegate = self;
		dialog1.permission = @"publish_stream";
		[dialog1 show];
    }
    else
	{
		[self uploadButtonClicked1:nil];
		
	}*/
}
- (void)session:(FBSession*)session didLogin:(FBUID)uid {
	//_permissionButton.hidden = NO;
	
	//permissionStatusForUser = [[PermissionStatus alloc] initWithUserId:session.uid];
	//permissionStatusForUser.delegate = self;
	
	uploadButton.hidden = NO;
	uploadButton1.hidden = NO;
	logoutButton.hidden = NO;
	LogoutImageImg.hidden=NO;
	ConnectoFacebookImg.hidden=YES;
	timeStampButton.hidden = YES;
	
	NSString* fql = [NSString stringWithFormat:
					 @"select uid,name from user where uid == %lld", session.uid];
	
	NSDictionary* params = [NSDictionary dictionaryWithObject:fql forKey:@"query"];
	[[FBRequest requestWithDelegate:self] call:@"facebook.fql.query" params:params];
	
	
	
	LoginButton.frame = CGRectMake(20, 130, 167, 121);
	HomeClickedButton.hidden=YES;
	
	FBPermissionDialog* dialog1 = [[[FBPermissionDialog alloc] init] autorelease];
	dialog1.delegate = self;
	dialog1.permission = @"publish_stream";
	[dialog1 show];
	
}

#pragma mark RAD PermissionStatusDelegate

- (void)sessionDidLogout:(FBSession*)session {
	_label.text = @"";
	uploadButton.hidden = YES;
	uploadButton1.hidden = YES;

	logoutButton.hidden = YES;
	HomeClickedButton.hidden=NO;
	LoginButton.frame = CGRectMake(30, 120, 260, 145);
	
}

- (void)request:(FBRequest*)request didLoad:(id)result {
	
	if([request.method isEqualToString:@"facebook.fql.query"])
	{
		NSArray* users = result;
		if(users.count>0)
		{
			NSDictionary* user = [users objectAtIndex:0];
			NSString* name = [user objectForKey:@"name"];
			_label.text = [NSString stringWithFormat:@"Logged in as %@", name];
		}
		else {
			_label.text = [NSString stringWithFormat:@"You are logged in "];
		}
		
	}
}
- (void)request:(FBRequest*)request didFailWithError:(NSError*)error {
	_label.text = [NSString stringWithFormat:@"Error(%d) %@", error.code,
				   error.localizedDescription];
}




- (void)dealloc {
	[_session release];
    [super dealloc];
}


@end
