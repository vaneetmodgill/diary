///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   Events.h
/// Overview	:	Header class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import <Foundation/Foundation.h>


@interface Events : NSObject {
	NSString *dateStr;
	NSString *timeStr;
	NSString *locationStr;
	NSString *title;
	NSString *descriptionStr;
	NSDate *dateevent;
	NSString *datestringevent;
	int badgenumber;
}
@property 	int badgenumber;
@property(nonatomic,retain)	NSString *datestringevent;
@property(nonatomic,retain)NSDate *dateevent;
@property(nonatomic,retain)NSString *dateStr;
@property(nonatomic,retain)NSString *timeStr;
@property(nonatomic,retain)NSString *locationStr;
@property(nonatomic,retain)NSString *title;
@property(nonatomic,retain)NSString *descriptionStr;
-(BOOL)insert;
-(BOOL)updateEvent:(NSString *)oldDate time:(NSString *)oldTime title:(NSString *)sub;
-(BOOL)deleteEvent:(NSString *)oldDate time:(NSString *)oldTime title:(NSString *)sub;
-(NSArray*)getData:(NSString *)eventDate;
-(NSArray*)getExportData:(NSString *)from and:(NSString *)to;
-(NSArray*)getDatesInMonth:(NSString *)fromDate	to:(NSString *)toDate;
-(BOOL)checkDuplicate;
-(BOOL)insertdate;
-(NSMutableArray*)getDatadate:(NSString *)eventDate time:(NSString*)eventTime;
-(BOOL)updateEventdate:(NSString *)oldDate time:(NSString *)oldTime date:(NSString *)par_date;
-(NSMutableArray*)getDatadate;
-(BOOL)deleteEventDATE:(NSString *)oldDate time:(NSString *)oldTime ;
-(NSMutableArray*)getEventsInDays:(NSString *)fromDate	to:(NSString *)toDate;

@end
