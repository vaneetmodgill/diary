//
//  ShowVideoController.m
//  Journal Diary
//
//  Created by seasia on 15/09/10.
//  Copyright 2010 Sulaba Inc. All rights reserved.
//

#import "ShowVideoController.h"
#import "AddVideoMemo.h"
#import "VoiceMemo.h"
#import "VideoMemoViewController.h"
#import "HomeViewController.h"
@implementation ShowVideoController
@synthesize filepath,iseditmode,addVideoController,olddate,oldtime,videoMemoViewController,istableviewclicked,home;
@synthesize video_title,videolabel;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	UILabel *message=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
	message.hidden = NO;
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			message.text = @"Result: canceled";
			//[self dismissModalViewControllerAnimated:YES];
			
			break;
		case MFMailComposeResultSaved:
			message.text = @"Result: saved";
			//[self dismissModalViewControllerAnimated:YES];
			break;
		case MFMailComposeResultSent:
			message.text = @"Result: sent";
			UIAlertView *obj_alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Message sent" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			obj_alert.delegate=self;
			[obj_alert show];
			[obj_alert release];
			//[self dismissModalViewControllerAnimated:YES];
			
			
			break;
		case MFMailComposeResultFailed:
			message.text = @"Result: failed";
			UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:@"Error in sending" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			alert.delegate=self;
			[alert show];
			[alert release];
			//[self dismissModalViewControllerAnimated:YES];
			
			break;
		default:
			message.text = @"Result: not sent";
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
	
	//[self.navigationController popViewControllerAnimated:YES];
	
	
}


////////////////// ************ CHECKING INTERNET CONNECTIVITY  **************///////////////////////////
-(BOOL)connectionToInternet
{
	NSString *URLString=[NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://www.google.com"]];
	
	return((URLString!=NULL)?YES:NO);
	// Create zero addy
	
	
}

// Displays an email composition interface inside the application. Populates all the Mail fields. 
-(void)displayComposerSheet 
{
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	
	picker.mailComposeDelegate = self;
	
	[picker setSubject:@"Diary Entry"];
	//@"All Projects"];
	
	
	//strings containing 
	NSString *emailBody = @"";
	// Fill out the email body text
	
	
	
	emailBody=[NSString stringWithFormat:@" Video Recorded on %@ at %@ \n",self.olddate,self.oldtime];
	NSLog(@"%@",emailBody);
	
	
	//NSData *data1=UIImagePNGRepresentation(imageViewBackground.image);
	[picker setMessageBody:emailBody isHTML:NO];
	if([data_video length]!=0)
	{
		[picker addAttachmentData:data_video mimeType:@"video/MOV" fileName:@"Video.MOV"];
	}
	
	//	[self.view addSubview:picker.view];
	[self presentModalViewController:picker animated:YES];
    [picker release];
} 
-(IBAction)emailPressed
{
	NSURL *movieURL = [NSURL fileURLWithPath:self.filepath]; 
//	data_video=
	data_video=[NSData dataWithContentsOfURL:movieURL];
	if([data_video length]!=0)
	{
	[self displayComposerSheet];
	}
	else {
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"NO Video Found" message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
	//[movieURL release];

}
-(IBAction)backPressed
{
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	VideoMemoViewController *voiceMemoView = [[VideoMemoViewController alloc] initWithNibName:@"VideoMemoViewController" bundle:nil];
	self.videoMemoViewController = voiceMemoView;
	[self.view addSubview:self.videoMemoViewController.view];
	[voiceMemoView release];
	[UIView commitAnimations];
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
	self.iseditmode=NO;
	
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setBool:YES forKey:@"autorotateenabled"];
	videolabel.text=self.video_title;
    [super viewDidLoad];
}
-(IBAction)stopPressed
{
	[theMovie stop];	
}
-(IBAction)deletePressed
{
	VoiceMemo *obj_voice=[[VoiceMemo alloc] init];
	BOOL result=[obj_voice deleteVideo:self.filepath];
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	VideoMemoViewController *voiceMemoView = [[VideoMemoViewController alloc] initWithNibName:@"VideoMemoViewController" bundle:nil];
	self.videoMemoViewController = voiceMemoView;
	[self.view addSubview:self.videoMemoViewController.view];
	[UIView commitAnimations];
}
-(IBAction)updatePressed
{
	self.iseditmode=YES;
	AddVideoMemo *showMemoView = [[AddVideoMemo alloc] initWithNibName:@"AddVideoMemo" bundle:nil];
	//showMemoView.filepath=[[memosArray objectAtIndex:indexPath.row] fileName];
	showMemoView.iseditmode=self.iseditmode;
	showMemoView.old_date=self.olddate;
		showMemoView.old_time=self.oldtime;
	//DiaryObject *obj = (DiaryObject*)[memosArray objectAtIndex:indexPath.row];
	//showMemoView.diaryObj = obj;
	self.addVideoController = showMemoView;
	[self.view addSubview:self.addVideoController.view];
	[showMemoView release];
}
-(IBAction)playPressed
{
	if(self.istableviewclicked)
	{
	NSURL *movieURL = [NSURL fileURLWithPath:self.filepath]; 
	theMovie = [[MPMoviePlayerController alloc] initWithContentURL:movieURL];
	theMovie.fullscreen=YES;
	theMovie.view.frame=CGRectMake(13, 41, 303, 300);
	//[theMovie setFrame:CGRectMake(0, 0, 320, 480)];
	//[theMovie prepareToPlay];
	[theMovie play];
	//[self presentModalViewController:theMovie animated:NO];
	[self.view addSubview:theMovie.view];
	}
	else {
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"No Video Found" message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}

	
	
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return NO;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[theMovie release];
	[filepath release];
	[data_video release];
    [super dealloc];
}


@end
