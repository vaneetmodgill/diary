///////////////////////////////////////////////////////////////////////////////
///
/// Class Name	:   SaveMemoViewController.m
/// Overview	:	Implementation class
///
///////////////////////////////////////////////////////////////////////////////
//
// Author		 :	
// Change date	 :	
// Change reason :  initial version
//
// (c) COPYRIGHT 2009-2010 Sulaba . ALL RIGHTS RESERVED.
//
// THE SOURCE CODE CONTAINED IN THIS FILE IS THE PROPERTY OF AND
// CONSTITUTES TRADE SECRETS OF Sulaba . ("Sulaba").
// THE HOLDER OF THIS FILE MUST KEEP THIS FILE AND ALL ITS CONTENTS
// STRICTLY CONFIDENTIAL AND IS GRANTED NO RIGHTS TO USE THE SOURCE CODE
// EXCEPT TO THE EXTENT EXPRESSLY AUTHORIZED BY Sulaba. IF YOU ARE NOT
// AUTHORIZED TO POSSESS THIS SOURCE CODE, YOU MUST RETURN IT TO Sulaba
// IMMEDIATELY UPON RECEIPT OR, IF YOU HOLD IT IN A FORM THAT CANNOT BE
// RETURNED, YOU MUST DESTROY IT. IF YOU FAIL TO DO SO PROMPTLY, YOU MAY
// FACE LEGAL ACTION FOR THEFT AND MISAPPROPRIATION OF TRADE SECRETS.
//
///////////////////////////////////////////////////////////////////////////////

#import "SaveMemoViewController.h"
#import "VoiceMemo.h"
#import "eDiaryAppDelegate.h"
#import "VoiceMemoUtils.h"
#import "VoiceMemoViewController.h"
#import "ChooseDateViewController.h"
#import "AddMemoViewController.h"
#import "Settings.h"

@implementation SaveMemoViewController
@synthesize memoDate;
@synthesize fileName;
@synthesize oldFileName;
@synthesize tempFilePath;
@synthesize titleStr;
@synthesize editMode;
@synthesize voiceMemoViewController;
@synthesize dateViewController;
@synthesize addMemoViewController;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        
        self.edgesForExtendedLayout=UIRectEdgeNone;
        
        self.extendedLayoutIncludesOpaqueBars = YES;
        
    }
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    if (screenBounds.size.height == 568)
    {
        
        
        
        backgroundImageView.image = [UIImage imageNamed:@"bg6iPhone5.png"];
        backgroundImageView.frame = CGRectMake(0, -20, 320, 568+40);
        //   self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 568+20);
    }
    else
    {
        self.view.frame = CGRectMake(self.view.frame.origin.x, 0, 320, 480+20);
    }
    

    
    
	[titleField becomeFirstResponder];
	if(self.memoDate == nil){
	self.memoDate = [NSDate date];
	}
	if(self.titleStr == nil){
		self.titleStr = @"";
	}
	
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	dateLabel.text = [formatter stringFromDate:self.memoDate];
	titleField.text = self.titleStr;
	//NSLog(@"title %@",self.titleStr);
	Settings *settings = [[Settings alloc] init];
	[settings loadSettings];
	if(settings.picEnabled){
		imageView.image = [eDiaryAppDelegate getHomeImage];
		//		imageView.image = image;
	}
	[settings release];
	[formatter release];
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

-(IBAction)savePressed{
	if([titleField.text isEqualToString:@""]){
		[eDiaryAppDelegate showMessage:@"Please enter a title for the voice memo" title:@"Empty title"];
		return;
	}
	
	VoiceMemoUtils *utils = [[VoiceMemoUtils alloc] init];
	[utils saveRecording:self.tempFilePath nameOfFile:self.fileName];
	[utils release];
	
	VoiceMemo *voiceMemo = [[VoiceMemo alloc] init];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"YYYY-MM-dd"];
	voiceMemo.dateStr = [formatter stringFromDate:self.memoDate];
	[formatter setDateStyle:NSDateFormatterNoStyle];
	[formatter setTimeStyle:NSDateFormatterShortStyle];
	voiceMemo.timeStr = [formatter stringFromDate:self.memoDate];
	//NSLog(@"time %@",voiceMemo.timeStr);
	voiceMemo.title = titleField.text;
	voiceMemo.fileName = self.fileName;
	//NSLog(@"file name %@",self.fileName);
	if(self.editMode){
		//NSLog(@"oldfile %@",self.oldFileName);
		[voiceMemo updateMemo:self.oldFileName];
	}else{
		[voiceMemo insert];
		
		//[voiceMemo insertVideo:@"" time:@"" title:@"" filename:@""];
		
	}
	[voiceMemo release];
	[formatter release];
	[titleField resignFirstResponder];
//	[eDiaryAppDelegate showMessage:@"The voice memo is saved sucessfully" title:@"Sucess"];
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.view cache:YES];
	VoiceMemoViewController *voiceMemoView = [[VoiceMemoViewController alloc] initWithNibName:@"VoiceMemoViewController" bundle:nil];
	voiceMemoView.memoDate = self.memoDate;
	self.voiceMemoViewController = voiceMemoView;
	[self.view addSubview:self.voiceMemoViewController.view];
	[voiceMemoView release];
	[UIView commitAnimations];
}

-(IBAction)calendarPressed{
	[titleField resignFirstResponder];
	ChooseDateViewController *dateView = [[ChooseDateViewController alloc] initWithNibName:@"ChooseDateViewController" bundle:nil];
	dateView.preView = @"SaveMemo";
	dateView.fileName = self.fileName;
	dateView.oldFileName = self.oldFileName;
	dateView.tempFilePath = self.tempFilePath;
	dateView.initialDate = self.memoDate;
	////NSLog(@"intial dt %@",dateViewController.initialDate);
	dateView.isEditMode = self.editMode;
	dateView.content = titleField.text; 
	self.dateViewController = dateView;
	[self.view addSubview:self.dateViewController.view];
	[dateView release];
}
-(IBAction)backPressed{
	[titleField resignFirstResponder];
	[UIView beginAnimations:nil context:nil];
	//change to set the time
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlDown forView:self.view cache:YES];
	AddMemoViewController *addMemoView = [[AddMemoViewController alloc] initWithNibName:@"AddMemoViewController" bundle:nil];
	addMemoView.retainRecording = YES;
	addMemoView.fileName = self.fileName;
	addMemoView.tempFilePath = self.tempFilePath;
	addMemoView.editMode = self.editMode;
	self.addMemoViewController = addMemoView;
	[self.view addSubview:self.addMemoViewController.view];
	[addMemoView release];
	[UIView commitAnimations];
}
- (void)dealloc {
    [super dealloc];
}


@end
